.PHONY: build
all: build


.PHONY: build
build:
	go build ./algo/...
	go build ./learning/...
