package main

//https://leetcode.com/problems/concatenation-of-array/
//Given an integer array nums of length n, you want to create an array ans of length 2n where ans[i] == nums[i]
//and ans[i + n] == nums[i] for 0 <= i < n (0-indexed).
//
//Specifically, ans is the concatenation of two nums arrays.
//
//Return the array ans.
//
//
//
//Example 1:
//
//Input: nums = [1,2,1]
//Output: [1,2,1,1,2,1]
//Explanation: The array ans is formed as follows:
//- ans = [nums[0],nums[1],nums[2],nums[0],nums[1],nums[2]]
//- ans = [1,2,1,1,2,1]
//Example 2:
//
//Input: nums = [1,3,2,1]
//Output: [1,3,2,1,1,3,2,1]
//Explanation: The array ans is formed as follows:
//- ans = [nums[0],nums[1],nums[2],nums[3],nums[0],nums[1],nums[2],nums[3]]
//- ans = [1,3,2,1,1,3,2,1]
//
//
//Constraints:
//
//n == nums.length
//1 <= n <= 1000
//1 <= nums[i] <= 1000
//
func getConcatenation_1929(nums []int) []int {
	res := make([]int, len(nums)*2)
	for i := range nums {
		res[i] = nums[i]
		res[i+len(nums)] = nums[i]
	}

	return res
}
func getConcatenation_1929_v2(nums []int) []int {
	return append(nums, nums...)
}

/*
https://leetcode.com/problems/merge-sorted-array/
 */
func merge_88(nums1 []int, m int, nums2 []int, n int)  {
	if len(nums2) == 0 {
		return
	}

	//need the larger index
	idx := m + n- 1; //or nums1.length

	m--
	n--

	//m or n == 0 should never go inside while loo
	// 0 > -1
	for m >= 0 && n >= 0 {
		if nums1[m] >= nums2[n]{
			nums1[idx] = nums1[m]
			m--
		} else {
			nums1[idx] = nums2[n];
			n--
		}
		idx--
	}

	for n>= 0 {
		nums1[idx] = nums2[n];
		idx--
		n--
	}

}