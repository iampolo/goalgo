package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//  https://leetcode.com/problems/array-nesting/
//
// You are given an integer array nums of length n where nums is a permutation of the numbers in the range [0, n - 1].
//
//You should build a set s[k] = {nums[k], nums[nums[k]], nums[nums[nums[k]]], ... } subjected to the following rule:
//
//- The first element in s[k] starts with the selection of the element nums[k] of index = k.
//- The next element in s[k] should be nums[nums[k]], and then nums[nums[nums[k]]], and so on.
//- We stop adding right before a duplicate element occurs in s[k].
//
//Return the longest length of a set s[k].
//
//
//Example 1:
//
//Input: nums = [5,4,0,3,1,6,2]
//Output: 4
//Explanation:
//nums[0] = 5, nums[1] = 4, nums[2] = 0, nums[3] = 3, nums[4] = 1, nums[5] = 6, nums[6] = 2.
//One of the longest sets s[k]:
//s[0] = {nums[0], nums[5], nums[6], nums[2]} = {5, 6, 2, 0}
//
//Example 2:
//
//Input: nums = [0,1,2]
//Output: 1
//
//
//Constraints:
//
// 	1 <= nums.length <= 10^5
//	0 <= nums[i] < nums.length
//	All the values of nums are unique.
//

// #1
// My initial idea is mark visited number for each start and clear out the mark before loop next start point, got TLE obviously..
// The trick part here is that the numbers are always form a ring, and no matter which number of this ring you
// start with total count will always be same, so no need to step on it one more time......

// #2
//Actually, there are multiple loops, and no loops will intersect with another loop, and you need to enter in the loop
//(from anywhere), and find the length of the biggest loop.
//
//Consider Input: nums = [5,4,0,3,1,6,2]
//
//There is a loop formed by indices 0 ,2, 5, 6 : It doesn't matter you enter the loop from index 0, or 2 or 5, loop
//length will come out to be the same.
//Other loops in it are formed by [1, 4] and [3] respectively of sizes 2 and 1.
func arrayNesting(nums []int) int {
	res := 0
	//try each element
	for i := 0; i < len(nums); i++ {
		size := 0
		for j := i; nums[j] >= 0; {
			next := nums[j]
			nums[j] = -1 //mark visited- what is visited has visited all the connected node, no need to do again. */
			j = next
			size++
		}
		res = Max(res, size)
	}

	return res
}

func arrayNesting_v3(nums []int) int {
	seen := make([]bool, len(nums))

	res := 0
	for _, n := range nums {
		size := 0
		for !seen[n] {
			seen[n] = true

			n = nums[n]
			size++
		}
		res = Max(res, size)
	}
	return res
}

// better
func arrayNesting_v4(nums []int) int {
	vis := make([]bool, len(nums))
	res := 0
	//try each element
	for i := 0; i < len(nums); i++ {
		if vis[i] {
			continue
		}
		size := 0
		start := i
		for size == 0 || start != i {
			vis[i] = true

			i = nums[i] //go to the next one
			size++
		}
		res = Max(res, size)
	}
	return res
}

func arrayNesting_v2(nums []int) int {
	vis := make([]bool, len(nums))
	res := 0
	//try each element
	for i := 0; i < len(nums); i++ {
		if vis[i] {
			continue
		}
		size := 0
		start := math.MaxInt32
		for j := i; start != nums[j] && !vis[j]; {
			start = nums[j]
			vis[j] = true

			j = nums[j]
			size++
		}
		res = Max(res, size)
	}

	return res
}
