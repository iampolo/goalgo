package main

//https://leetcode.com/problems/find-all-duplicates-in-an-array/
func findDuplicates(nums []int) []int {
	//put nums back at right position
	//O(n)
	for i := 0; i < len(nums); i++ {
		for nums[i] != i+1 && nums[nums[i]-1] != nums[i] {
			nums[i], nums[nums[i]-1] =  nums[nums[i]-1], nums[i]
		}
	}

	var res []int
	for i := range nums {
		if nums[i] != i+1 && nums[i] == nums[nums[i]-1] {
			res = append(res, nums[i])
		}
	}
	return res
}

// https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/
// Given an array nums of n integers where nums[i] is in the range [1, n], return an array of all
// the integers in the range [1, n] that do not appear in nums.
//
//
//
//Example 1:
//
//Input: nums = [4,3,2,7,8,2,3,1]
//Output: [5,6]
//
//Example 2:
//
//Input: nums = [1,1]
//Output: [2]
//
//
//Constraints:
//
//n == nums.length
//1 <= n <= 10^5
//1 <= nums[i] <= n
//
//
//Follow up: Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count
//as extra space.
func findDisappearedNumbers_448(nums []int) []int {
	for i := 0; i < len(nums); i++ {
		for nums[i] != i+1 && nums[nums[i]-1] != nums[i] {
			nums[i], nums[nums[i]-1] =  nums[nums[i]-1], nums[i]
		}
	}

	var res []int
	for i := range nums {
		if nums[i] != i+1 {
			res = append(res, i+1)
		}
	}
	return res
}