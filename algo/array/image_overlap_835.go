package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"strconv"
)

/*
https://leetcode.com/problems/image-overlap/
*/

/*
 38ms
	O(n^4)
*/
func largestOverlap(img1 [][]int, img2 [][]int) int {

	cntOverLap := func(i, j int, one, two [][]int) int {
		//var cnt int
		lc, rc := 0, 0
		r2 := 0
		for x := i; x < len(one); x++ {
			c2 := 0
			for y := j; y < len(one); y++ {
				//cnt += one[x][y] * two[r2][c2]
				if one[x][y] == 1 && one[x][y] == two[r2][c2] {
					lc++
				}
				if one[x][c2] == 1 && one[x][c2] == two[r2][y] {
					rc++
				}
				c2++
			}
			r2++
		}
		return Max(lc, rc)
	}

	var res int
	for i := range img1 {
		for j := range img1[0] {
			res = Max(res, cntOverLap(i, j, img1, img2))
			res = Max(res, cntOverLap(i, j, img2, img1))
		}
	}

	return res
}

/*
	224ms

	ImageOverlap835
*/
func largestOverlap_dotcount(img1 [][]int, img2 [][]int) int {
	var one [][]int
	var two [][]int
	for i := 0; i < len(img1); i++ {
		for j := 0; j < len(img2); j++ {
			if img1[i][j] == 1 {
				one = append(one, []int{i, j})
			}
			if img2[i][j] == 1 {
				two = append(two, []int{i, j})
			}
		} //for
	} //for

	cntMap := make(map[string]int)
	var max int
	for _, p := range one {
		for _, q := range two {
			key := strconv.Itoa(p[0]-q[0]) + "+" + strconv.Itoa(p[1]-q[1])
			cntMap[key]++
			max = Max(max, cntMap[key])
		}
	}
	return max
}
