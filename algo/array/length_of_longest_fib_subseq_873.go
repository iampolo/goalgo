package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/length-of-longest-fibonacci-subsequence/
// A sequence x1, x2, ..., xn is Fibonacci-like if:
//
// - n >= 3
// - x_i + x_i+1 == x_i+2 for all i + 2 <= n
// Given a strictly increasing array arr of positive integers forming a sequence, return the length of the longest
// Fibonacci-like subsequence of arr. If one does not exist, return 0.
//
// A subsequence is derived from another sequence arr by deleting any number of elements (including none) from arr,
// without changing the order of the remaining elements. For example, [3, 5, 8] is a subsequence of [3, 4, 5, 6, 7, 8].
//
//
// Example 1:
//
// Input: arr = [1,2,3,4,5,6,7,8]
// Output: 5
// Explanation: The longest subsequence that is fibonacci-like: [1,2,3,5,8].
//
// Example 2:
//
// Input: arr = [1,3,7,11,12,14,18]
// Output: 3
// Explanation: The longest subsequence that is fibonacci-like: [1,11,12], [3,11,14] or [7,11,18].
//
// Constraints:
//
// 3 <= arr.length <= 1000
// 1 <= arr[i] < arr[i + 1] <= 10^9
//
func lenLongestFibSubseq_dp_v2(arr []int) int {
	set := make(map[int]int)
	for i, v := range arr {
		set[v] = i
	}
	dp := make([][]int, len(arr))
	for i := range dp {
		dp[i] = make([]int, len(arr))
		for j := range dp[i] {
			dp[i][j] = -1
		}
	} //

	ans := 0
	for i := 0; i < len(arr); i++ {
		for j := 0; j < i; j++ {
			k := -1
			if pos, exist := set[arr[i]-arr[j]]; exist {
				//two-sum approach
				k = pos
			}
			if k >= 0 && k < j {
				tmp := 3 //minimum 3 has formed
				if dp[k][j] > 2 {
					tmp = dp[k][j] + 1
				}
				dp[j][i] = tmp //TODO
				ans = Max(ans, tmp)
			}
		} //for
	}
	if ans > 2 {
		return ans
	}
	return 0
}

func lenLongestFibSubseq_dp(arr []int) int {

	//2-dimension array
	//1st index is the outer loop index i
	//2nd index is the inner loop index j
	memo := make([][]int, len(arr))
	for i := range memo {
		memo[i] = make([]int, len(arr))
		for j := range memo[i] {
			memo[i][j] = -1
		}
	} //for

	set := make(map[int]int)
	for i, v := range arr {
		set[v] = i
	}

	var search func(i, j int) int
	search = func(i, j int) int {
		if _, exist := set[arr[i]+arr[j]]; !exist {
			return 0
		}
		if memo[i][j] != -1 {
			return memo[i][j]
		}
		memo[i][j] = 1 + search(j, set[arr[i]+arr[j]])
		return memo[i][j]
	}

	longest := 0
	for i := 0; i < len(arr); i++ {
		for j := i + 1; j < len(arr); j++ {
			//i and j represent two initially
			longest = Max(longest, search(i, j))
		}
	}
	if longest > 0 {
		//it could be just 1 returned from search(), total length is 3
		return longest + 2
	}
	return 0
}

// O(n^2)
func lenLongestFibSubseq(arr []int) int {

	set := make(map[int]struct{})
	for _, v := range arr {
		set[v] = struct{}{}
	}

	//template of dp for loop
	longest := 0
	for i := 0; i < len(arr); i++ {
		for j := i + 1; j < len(arr); j++ {

			x, y, length := arr[i], arr[j], 2
			_, exist := set[x+y]
			for exist {
				y = x + y
				x = y - x
				length++
				_, exist = set[x+y]
			}
			longest = Max(longest, length)
		} //
	}
	if longest > 2 {
		return longest
	}
	return 0
}
