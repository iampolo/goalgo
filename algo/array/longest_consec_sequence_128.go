package main

/**
https://leetcode.com/problems/longest-consecutive-sequence/

*/

func longestConsecutive(nums []int) int {
	cntMap := make(map[int]int)

	longest := 0
	for _, n := range nums {
		if _, isExist := cntMap[n]; isExist {
			continue
		}
		cntMap[n] = 1

		end := n
		begin := n

		if _, ok := cntMap[n+1]; ok {
			end = end + cntMap[n+1] //move forward, the len increases
		}
		if _, ok := cntMap[n-1]; ok {
			begin = begin - cntMap[n-1] //move backward
		}

		length := end - begin + 1 //current length
		if length > longest {
			longest = length
		}

		cntMap[end] = length
		cntMap[begin] = length
	}

	return longest
}
