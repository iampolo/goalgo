package main

import . "gitlab.com/iampolo/goalgo/algo/util"

//https://leetcode.com/problems/longest-arithmetic-sequence/discuss/312932/Go-solution.-24ms-dfs-faster-than-dp
// DFS
func longestArithSeqLength(A []int) int {
	n := len(A)

	indexes := [10001][]int{}
	for i := 0; i < n; i++ {
		indexes[A[i]] = append(indexes[A[i]], i)
	}

	var dfs func(int, int) int
	dfs = func(j, diff int) int {
		next := A[j] + diff
		if next < 0 || 10000 < next {
			return 0
		}
		for _, k := range indexes[next] {
			if j < k {
				return 1 + dfs(k, diff)
			}
		}
		return 0
	}

	res := 0
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			diff := A[j] - A[i]
			res = max(res, dfs(j, diff)+2)
		}
	}

	return res
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

/***********************************************************************************/
func longestArithSeqLength_v2(nums []int) int {
	dp := make([]map[int]int, len(nums))
	for i := range dp {
		dp[i] = make(map[int]int)
	}

	var res int
	for i := 0; i < len(nums); i++ {
		for j := 0; j < i; j++ {
			d := nums[i] - nums[j]
			length := 2
			if cnt, exist := dp[j][d]; exist {//if there is a same previous diff. at position j
				length = cnt + 1
			}
			dp[i][d] = length

			res = max(res, dp[i][d])
		}
	}
	return res
}

//https://leetcode.com/problems/longest-arithmetic-subsequence-of-given-difference/

// Given an integer array arr and an integer difference, return the length of the longest subsequence in arr which
// is an arithmetic sequence such that the difference between adjacent elements in the subsequence equals difference.
//
// A subsequence is a sequence that can be derived from arr by deleting some or no elements without changing the
// order of the remaining elements.
//
//
//
// Example 1:
//
// Input: arr = [1,2,3,4], difference = 1
// Output: 4
// Explanation: The longest arithmetic subsequence is [1,2,3,4].
//
// Example 2:
//
// Input: arr = [1,3,5,7], difference = 1
// Output: 1
// Explanation: The longest arithmetic subsequence is any single element.
//
// Example 3:
//
// Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
// Output: 4
// Explanation: The longest arithmetic subsequence is [7,5,3,1].
//
//
// Constraints:
//
// 1 <= arr.length <= 10^5
// -10^4 <= arr[i], difference <= 10^4
//
func longestSubsequence(arr []int, d int) int {
	dp := make(map[int]int)
	var res int
	for _, v := range arr {
		if cnt, exist := dp[v-d]; exist {
			dp[v] = cnt + 1 //previous cnt plus the current 1
		} else {
			dp[v] = 1
		}
		res = Max(res, dp[v])
	}
	return res
}
