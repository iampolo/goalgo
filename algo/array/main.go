package main

import (
	"fmt"
)

func main() {
	test_2281()
}

func test_2281() {
	st := []int{1,3,1,2}
	fmt.Println(totalStrength(st))
}

func test_311() {

	A := [][]int{{1, 0, 0}, {-1, 0, 3}};
	B := [][]int{{7, 0, 0}, {0, 0, 0}, {0, 0, 1}}
	fmt.Println(multiply_v2(A, B))
}

func test_1769() {
	boxes := "001011"
	boxes = "00100"
	fmt.Println(minOperations_1769_v3(boxes))
	fmt.Println(minOperations_1769_v2(boxes))
}
func test_1027() {
	nums := []int{9, 4, 7, 2, 10}
	fmt.Println(longestArithSeqLength(nums))
	fmt.Println(longestArithSeqLength_v2(nums))
}

func test_835() {
	one := [][]int{{1, 1, 0}, {0, 1, 0}, {0, 1, 0}}
	two := [][]int{{0, 0, 0}, {0, 1, 1}, {0, 0, 1}}
	//fmt.Println(largestOverlap_dotcount(one, two))
	one = [][]int{{0, 0, 1}, {0, 0, 0}, {0, 0, 0}}
	two = [][]int{{0, 0, 0}, {0, 0, 0}, {1, 0, 0}}
	fmt.Println(largestOverlap_dotcount(one, two))
}

func test_873() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8}
	arr = []int{1, 3, 7, 11, 12, 14, 18}

	fmt.Println(lenLongestFibSubseq_dp_v2(arr))
	fmt.Println(lenLongestFibSubseq(arr))
}

func test_2025() {
	nums := []int{2, -1, 2}
	k := 3
	//fmt.Println(waysToPartition(nums, k))

	nums = []int{8, -5, 3, 2, -2, 2, 3}
	k = 3
	fmt.Println(waysToPartition(nums, k))
	//
	nums = []int{22, 4, -25, -20, -15, 15, -16, 7, 19, -10, 0, -13, -14}
	k = -33

	fmt.Println(waysToPartition(nums, k))
	//
	//nums = []int{0, 0, 0}
	//k = 1
	//fmt.Println(waysToPartition(nums, k))
}

func test_424() {
	s := "AABABBA"
	k := 1
	fmt.Println(characterReplacement_424(s, k))
}

func test_2018() {
	board := [][]byte{{'#', ' ', '#'}, {' ', 'b', '#'}, {'#', ' ', ' '}}
	word := "abc"

	fmt.Println(placeWordInCrossword(board, word))
}

func test_922() {
	nums := []int{4, 2, 5, 7}
	//nums = []int{2, 3}
	fmt.Println(sortArrayByParityII_v3(nums))
	fmt.Println(sortArrayByParityII(nums))
}

func test_54() {
	ma := [][]int{
		{1, 2, 3},
		{5, 6, 4},
		{15, 11, 8},
		//{9, 10, 7},
	}
	fmt.Println(spiralOrder_v4(ma))
}

func test_848() {
	s := "abc"
	shifts := []int{3, 5, 9}
	s = "ruu"
	shifts = []int{26, 9, 17}
	fmt.Println(shiftingLetters(s, shifts))
}

func test_1629() {

	rt := []int{19, 22, 28, 29, 66, 81, 93, 97}
	kp := "fnfaaxha"
	fmt.Println(string(slowestKey(rt, kp)))
}

func test_565() {
	nums := []int{5, 4, 0, 3, 1, 6, 2}
	fmt.Println(arrayNesting(nums))
}

func test_915() {
	nums := []int{5, 0, 3, 8, 6}
	//nums = []int{1, 1, 1, 0, 6, 12}
	fmt.Println(partitionDisjoint(nums))

}

func test() {

	mat := [][]int{{1, 2, 3, 4, 6, 7}}
	r, c := 2, 3
	fmt.Println(matrixReshape(mat, r, c))
	fmt.Println(matrixReshape_v2(mat, r, c))
}

func test_370() {
	updates := [][]int{{1, 3, 2}, {2, 4, 3}, {0, 2, -2}}
	len := 5

	_, _ = updates, len
}

func test_927() {
	nums := []int{1, 1, 0, 1, 1, 0, 1, 1}
	nums = []int{0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1}

	fmt.Println(threeEqualParts(nums))

}
