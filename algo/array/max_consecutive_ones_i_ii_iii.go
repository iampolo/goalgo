package main

// https://leetcode.com/problems/max-consecutive-ones/submissions/
func findMaxConsecutiveOnes_485(nums []int) int {
	maxFunc := func(i, j int) int {
		if i < j {
			return j
		}
		return i
	}
	cnt, max := 0, 0
	for _, v := range nums {
		if v == 1 {
			cnt++
			max = maxFunc(max, cnt)
		} else {
			cnt = 0
		}
	}
	return max
}

// https://leetcode.com/problems/max-consecutive-ones-ii/
func findMaxConsecutiveOnes_487(nums []int) int {
	flip, max, left := 0, 0, 0
	for i := range nums {
		if nums[i] == 0 {
			flip++
		}

		for flip > 1 {
			if nums[left] == 0 {
				flip--
			}
			left++
		}
		if i-left+1 > max {
			max = i - left + 1
		}
	}
	return max
}

// https://leetcode.com/problems/max-consecutive-ones-iii/
// Given a binary array nums and an integer k, return the maximum number of consecutive 1's in the array
// if you can flip at most k 0's.
//
//
//
//Example 1:
//
//Input: nums = [1,1,1,0,0,0,1,1,1,1,0], k = 2
//Output: 6
//Explanation: [1,1,1,0,0,1,1,1,1,1,1]
//Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.
//
//Example 2:
//
//Input: nums = [0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1], k = 3
//Output: 10
//Explanation: [0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,1,1,1,1]
//Bolded numbers were flipped from 0 to 1. The longest subarray is underlined.
//
//Constraints:
//
//1 <= nums.length <= 105
//nums[i] is either 0 or 1.
//0 <= k <= nums.length
func longestOnes_1004(nums []int, k int) int {
	flip, max, left := 0, 0, 0
	for i := range nums {
		if nums[i] == 0 {
			flip++
		}

		for flip > k {
			if nums[left] == 0 {
				flip--
			}
			left++
		}
		if i-left+1 > max {
			max = i - left + 1
		}
	}
	return max
}

func longestOnes_1004_v2(nums []int, k int) int {
	flip, max, left := 0, 0, 0

	//i moves only under certain situation
	for i := 0; i < len(nums); {

		if flip <= k {
			if nums[i] == 0 {
				flip++
			}
			i++
		}

		if flip > k {
			if nums[left] == 0 {
				flip--
			}
			left++
		}
		if i - left > max && flip <= k {
			max = i - left
		}
	}
	return max
}