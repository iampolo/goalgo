package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/maximum-number-of-ways-to-partition-an-array/

/*
You are given a 0-indexed integer array nums of length n. The number of ways to partition nums is the number of
pivot indices that satisfy both conditions:

- 1 <= pivot < n
- nums[0] + nums[1] + ... + nums[pivot - 1] == nums[pivot] + nums[pivot + 1] + ... + nums[n - 1]

You are also given an integer k. You can choose to change the value of one element of nums to k, or to leave the array unchanged.

Return the maximum possible number of ways to partition nums to satisfy both conditions after changing at most one element.



Example 1:

Input: nums = [2,-1,2], k = 3
Output: 1
Explanation: One optimal approach is to change nums[0] to k. The array becomes [3,-1,2].
There is one way to partition the array:
- For pivot = 2, we have the partition [3,-1 | 2]: 3 + -1 == 2.

Example 2:

Input: nums = [0,0,0], k = 1
Output: 2
Explanation: The optimal approach is to leave the array unchanged.
There are two ways to partition the array:
- For pivot = 1, we have the partition [0 | 0,0]: 0 == 0 + 0.
- For pivot = 2, we have the partition [0,0 | 0]: 0 + 0 == 0.

Example 3:

Input: nums = [22,4,-25,-20,-15,15,-16,7,19,-10,0,-13,-14], k = -33
Output: 4
Explanation: One optimal approach is to change nums[2] to k. The array becomes [22,4,-33,-20,-15,15,-16,7,19,-10,0,-13,-14].
There are four ways to partition the array.


Constraints:

* n == nums.length
* 2 <= n <= 10^5
* -10^5 <= k, nums[i] <= 10^5

PartitionEqualSubsetSum416
min_num_of_oper_move_all_balls_each_box_1769.go

https://leetcode.com/problems/maximum-number-of-ways-to-partition-an-array/discuss/1499365/C%2B%2B-Frequency-Map-O(N)
       2, -1, 2
	   2   1   3
       3   1   2
      _    1   -1

	   3   -1   2
       3   2   4    presum
       4   1   2    suffsum
      _    2   0        ans is the #of 0, ie: diff of left part and right part equal to 0
           ^ 3-1  		[3 | -1  2]
               ^ 2-2  	[3 -1 | 2]
               ^ this is already a way w/o using k

 */
func waysToPartition(nums []int, k int) int {

	n := len(nums)
	presum := make([]int, n)
	sufsum := make([]int, n)

	presum[0] = nums[0]
	sufsum[n-1] = nums[n-1]
	for i := 1; i < n; i++ {
		presum[i] = presum[i-1] + nums[i]
		sufsum[n-1-i] = sufsum[n-i] + nums[n-1-i]
	}
	fmt.Println(presum)
	fmt.Println(sufsum)
	//[22  26 1 -19 -34 -19 -35 -28 -9 -19 -19 -32 -46]
	//[22 -11 -36 -56 -71 -56 -72 -65 -46 -56 -56 -69 -83]

	//split the array and save the difference
	right := make(map[int64]int)
	for i := 0; i < n-1; i++ {
		diff := int64(presum[i] - sufsum[i+1])
		right[diff] = right[diff] + 1     //diff of the left and right portions if the pivot is at i
	}

	//#of 0 is the answer(can be split to half/half), it means no change is needed
	var res int
	if _, ok := right[int64(0)]; ok {
		//w/o change any element
		res += right[int64(0)]
	} //

	left := make(map[int64]int)
	//if change each element to k, count the # of pivot.
	//-d decreased a num by k
	//+d increased a num by k
	for i := 0; i < n; i++ {
		diff := int64(k - nums[i])
		var cur int
		//this diff is used to offset the diff in right, so that the diff is 0 now, an i can be a pivot
		if _, ok := right[-diff]; ok {
			cur += right[-diff]
		}
		//as i going to right, the presum-vs-sufsum diff. will be impacted and may become 0, which is another pivot
		if _, ok := left[diff]; ok {
			cur += left[diff]
		}

		//as we're trying every to see if it could be the pivot, as should save the best one.
		res = Max(res, cur)

		if i < n-1 {
			d := int64(presum[i] - sufsum[i+1])
			//add to left
			left[d] = left[d] + 1

			//remove from right if it is there
			if _, ok := right[d]; ok {
				right[d] = right[d] - 1
				if right[d] == 0 {
					delete(right, d)
				}
			} //if
		}
	} //for

	return res
}
