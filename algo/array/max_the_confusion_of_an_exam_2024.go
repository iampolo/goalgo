package main

import . "gitlab.com/iampolo/goalgo/algo/util"

// https://leetcode.com/problems/maximize-the-confusion-of-an-exam/
// A teacher is writing a test with n true/false questions, with 'T' denoting true and 'F' denoting false. He
// wants to confuse the students by maximizing the number of consecutive questions with the same answer (multiple
// trues or multiple falses in a row).
//
//You are given a string answerKey, where answerKey[i] is the original answer to the ith question. In addition,
//you are given an integer k, the maximum number of times you may perform the following operation:
//
//Change the answer key for any question to 'T' or 'F' (i.e., set answerKey[i] to 'T' or 'F').
//Return the maximum number of consecutive 'T's or 'F's in the answer key after performing the operation at most k times.
//
//
//Example 1:
//
//Input: answerKey = "TTFF", k = 2
//Output: 4
//Explanation: We can replace both the 'F's with 'T's to make answerKey = "TTTT".
//There are four consecutive 'T's.
//
//Example 2:
//
//Input: answerKey = "TFFT", k = 1
//Output: 3
//Explanation: We can replace the first 'T' with an 'F' to make answerKey = "FFFT".
//Alternatively, we can replace the second 'T' with an 'F' to make answerKey = "TFFF".
//In both cases, there are three consecutive 'F's.
//
//Example 3:
//
//Input: answerKey = "TTFTTFTT", k = 1
//Output: 5
//Explanation: We can replace the first 'F' to make answerKey = "TTTTTFTT"
//Alternatively, we can replace the second 'F' to make answerKey = "TTFTTTTT".
//In both cases, there are five consecutive 'T's.
//
//
//Constraints:
//
//- n == answerKey.length
//- 1 <= n <= 5 * 10^4
//- answerKey[i] is either 'T' or 'F'
//- 1 <= k <= n

func maxConsecutiveAnswers_v2(answerKey string, k int) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	//use two pointer to maintain/find the window of best size.
	return max(findLongest(answerKey, 'T', k), findLongest(answerKey, 'F', k))
}

func findLongest(s string, ch byte, k int) int {
	flip, max, left := 0, 0, 0

	for i := range s {
		if s[i] == ch {
			flip++
		}

		for flip > k {
			if s[left] == ch {
				flip--
			}
			left++
		}
		if i-left+1 > max {
			max = i - left + 1
		}
	}
	return max
}

/*
sliding window and combining the two counters

	T	T	F	T	T	F	T	T


 */
func maxConsecutiveAnswers_v3(answerKey string, k int) int {
	res, cntT, cntF := 0, 0, 0
	l := 0
	for i := range answerKey {
		if answerKey[i] == 'T' {
			cntT++
		} else {
			cntF++
		}

		//move only when the smallest counter is over k
		for Min(cntT, cntF) > k {
			if answerKey[l] == 'T' {
				cntT--
			} else {
				cntF--
			}
			l++
		}
		res = Max(res, i-l+1)
	}
	return res
}

//***********************************************************************************
func maxConsecutiveAnswers(answerKey string, k int) int {
	return characterReplacement_424(answerKey, k)
}

// https://leetcode.com/problems/longest-repeating-character-replacement/
// 424. Longest Repeating Character Replacement
func characterReplacement_424(s string, k int) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	var cnt [128]int //ascii
	left, maxCnt, maxLen := 0, 0, 0
	for i := range s { //scan from l to r and maintain the window
		cnt[s[i]-'A']++
		maxCnt = max(maxCnt, cnt[s[i]-'A'])

		//check the chars in the window are a combination of same
		//and those can be replaced to be the same
		if i-left+1 > maxCnt+k { //maxCnt(same) k(diff. but can be replaced)
			cnt[s[left]-'A']--
			left++
		}
		maxLen = max(maxLen, i-left+1)
	}
	return maxLen
}
