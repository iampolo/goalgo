package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

func maxArea(h int, w int, horizontalCuts []int, verticalCuts []int) int {

	sort.Ints(horizontalCuts)
	sort.Ints(verticalCuts)

	maxHeight := findLargest(horizontalCuts, h)
	maxWeight := findLargest(verticalCuts, w)

	return (maxHeight * maxWeight) % 1000000007
}

func findLargest(nums []int, bound int) int {
	max := Max(nums[0], bound-nums[len(nums)-1])

	for i := 1; i < len(nums); i++ {
		max = Max(max, nums[i]-nums[i-1])
	}
	return max
}

func Testmain() {
	h := []int {1,2,4}
	h = append([]int{0}, h...)
	h = append(h, h...)


	fmt.Print(h)
}