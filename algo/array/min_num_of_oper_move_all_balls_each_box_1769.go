package main

/*
https://leetcode.com/problems/minimum-number-of-operations-to-move-all-balls-to-each-box/
You have n boxes. You are given a binary string boxes of length n, where boxes[i] is '0' if the ith box is empty,
and '1' if it contains one ball.

In one operation, you can move one ball from a box to an adjacent box. Box i is adjacent to box j if abs(i - j) == 1.
Note that after doing so, there may be more than one ball in some boxes.

Return an array answer of size n, where answer[i] is the minimum number of operations needed to move all the balls
to the ith box.

Each answer[i] is calculated considering the initial state of the boxes.


Example 1:

Input: boxes = "110"
Output: [1,1,3]
Explanation: The answer for each box is as follows:
1) First box: you will have to move one ball from the second box to the first box in one operation.
2) Second box: you will have to move one ball from the first box to the second box in one operation.
3) Third box: you will have to move one ball from the first box to the third box in two operations, and move one
   ball from the second box to the third box in one operation.
Example 2:

Input: boxes = "001011"
Output: [11,8,5,4,3,4]


Constraints:

* n == boxes.length
* 1 <= n <= 2000
* boxes[i] is either '0' or '1'.

max_num_of_ways_partition_an_array_2025.go
*/

/*
https://leetcode.com/problems/minimum-number-of-operations-to-move-all-balls-to-each-box/discuss/1075669/C%2B%2B-O(N)-keep-track-of-of-balls-to-the-right-and-left
*/
func minOperations_1769(boxes string) []int {

	return nil
}

/*
        0 0 1 0 0
step	0 0 0 1 2    (l -> r)
step    2 1 0 0 0    (l <- r)

if there are other 1s, use cnt to record them and add up the total steps
dp[i] = dp[i-1] + (1 * #balls)

MinStepsToMakePilesEqualHeight
ProductofArrayExceptSelf238
*/
func minOperations_1769_v3(boxes string) []int {
	res := make([]int, len(boxes))

	//need to use ops!! lc238
	var cnt, ops int
	for i := 1; i < len(boxes); i++ {
		if boxes[i-1] == '1' {
			cnt++
		}
		ops += cnt
		res[i] = ops
	}
	cnt, ops = 0, 0
	for i := len(boxes) - 2; i >= 0; i-- {
		if boxes[i+1] == '1' {
			cnt++
		}
		ops += cnt
		res[i] += ops
	}
	return res
}

func minOperations_1769_v2(boxes string) []int {
	pre := make([]int, len(boxes))
	cnt := 0
	for i := 1; i < len(boxes); i++ {
		//use [i-1] to count he moving cost from previous to current position
		if boxes[i-1] == '1' {
			//additional parameter to count the steps
			cnt++
		}
		pre[i] = pre[i-1] + cnt
	}

	cnt = 0
	suf := make([]int, len(boxes))
	for i := len(boxes) - 2; i >= 0; i-- {
		if boxes[i+1] == '1' {
			cnt++
		}
		suf[i] = suf[i+1] + cnt
	}
	res := make([]int, len(boxes))

	for i := range pre {
		res[i] = pre[i] + suf[i]
	}
	return res
}
