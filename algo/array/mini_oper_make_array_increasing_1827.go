package main

import "fmt"

/**
https://leetcode.com/problems/minimum-operations-to-make-the-array-increasing/

You are given an integer array nums (0-indexed). In one operation, you can choose an element of the array
and increment it by 1.

For example, if nums = [1,2,3], you can choose to increment nums[1] to make nums = [1,3,3].
Return the minimum number of operations needed to make nums strictly increasing.

An array nums is strictly increasing if nums[i] < nums[i+1] for all 0 <= i < nums.length - 1. An array of
length 1 is trivially strictly increasing.


Example 1:

Input: nums = [1,1,1]
Output: 3
Explanation: You can do the following operations:
1) Increment nums[2], so nums becomes [1,1,2].
2) Increment nums[1], so nums becomes [1,2,2].
3) Increment nums[2], so nums becomes [1,2,3].

Example 2:

1 <= nums[i] <= 10^14

*/
func minOperations(nums []int) int {
	cnt := 0
	prev := 0
	for i := 0; i < len(nums); i++ {

		if (nums[i] <= prev) {
			cnt += prev  + 1 - nums[i]
			prev += 1  //Good: prev is the one up
		}  else {
			prev = nums[i]  //cur is bigger
		}
	}

	return cnt
}


func testmain() {
	nums := []int{1, 1, 1}
	fmt.Println(minOperations(nums))

	TestName()
}

func TestName() {
	cntMap := make(map[int]int)

	fmt.Println(cntMap)


}