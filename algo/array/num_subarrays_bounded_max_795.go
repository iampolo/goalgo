package main

import "fmt"

/**
https://leetcode.com/problems/number-of-subarrays-with-bounded-maximum/

We are given an array nums of positive integers, and two positive integers left and right (left <= right).

Return the number of (contiguous, non-empty) subarrays such that the value of the maximum array element in that
subarray is at least left and at most right.

Example:
Input:
nums = [2, 1, 4, 3]
left = 2
right = 3
Output: 3
Explanation: There are three subarrays that meet the requirements: [2], [2, 1], [3].


Note:

- left, right, and nums[i] will be an integer in the range [0, 109].
- The length of nums will be in the range of [1, 50000].

@see CountNumberofNiceSubarrays1248
@see SubarraysWithKDifferentIntegers992
//count the number of subarray
@see TwoSumSmaller
*/
// the nature of subarray:  contiguous
//  if nums[i] > R,

/**
number of subarray:  n(n+1)/2
*/
//https://leetcode.com/problems/number-of-subarrays-with-bounded-maximum/discuss/946420/Golang-0ms-solution-easy-to-understand
func numSubarrayBoundedMax(nums []int, left int, right int) int {
	var res int
	l, r := -1, -1

	/**
	   	- at the beginning, or when the element is not in left <= x <= right,
		  the count is -1-(-1) = 0
		- if found the 1st within bound, r(0) - l(-1) = 1
		- ...
	*/
	for i, n := range nums {
		//these check orders ensure the element are within bound
		if n > right { //bigger than right also means it is bigger than left
			l = i
		}
		if n >= left { //here bigger than left guarantees that it is within the bound
			r = i //set the ending element: meet the right criteria
		}
		res += r - l //count the # of subarray like TwoSumSmaller
	}
	return res
}

func main_795() {
	nums := []int{2, 1, 4, 3}
	l, r := 2, 3
	fmt.Println(numSubarrayBoundedMax(nums, l, r))

	//nums = []int{0, 3, 1, 4, 5, 2, 1, 5, 10, 6}
	//l, r = 3, 6

	fmt.Println(numSubarrayBoundedMax_v2(nums, l, r))
}

/**
For example, for input A = [0, 1, 2, -1], L = 2, R = 3:
for 0, no valid subarray ends at 0;
for 1, no valid subarray ends at 1;
for 2, three valid subarrays end at 2, which are [0, 1, 2], [1, 2], [2];
for -1, the number of valid subarrays is the same as 2, which are [0, 1, 2, -1], [1, 2, -1], [2, -1];

 */
func numSubarrayBoundedMax_v2(nums []int, left int, right int) int {
	var res int

	var start, cnt int
	for i, n := range nums {
		if left <= n && n <= right {
			res += i - start + 1
			cnt = i - start + 1 //hard to understand
		} else if n < left {
			// now, n is out of bound, but as long as there is/are valid subarray before, n can join them as well, so add them here
			res += cnt //hard to understand
		} else {
			//bigger than right, no subarray can be formed, reset.
			start = i + 1
			cnt = 0
		}

	}
	return res
}
