package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

/**
suppose the original left subarray is from 0 to partitionIdx, the max value of that is localMax.
If it is a valid partition, every value from partitionIdx + 1 to end should be >= localMax. But
if we find a value in the right part, a[i], is smaller than localMax, which means the partition
is not correct and we have to incorporate a[i] to form the left subarray. So the partitionIdx is
et to be i, and we have to recalculate the max value of the new left subarray.(recorded in max)


The test case must be in certain order: there are larger numbers to be put on the right.
   1 2 0 4 5  -> 3
  |     |

  1,2,3,0,4 -> 4

  2,3,4,0 1  > invalid
 */
func partitionDisjoint(nums []int) int {
	localMax := nums[0]
	max := nums[0]

	res := 0
	for i := 1; i < len(nums); i++ {
		if nums[i] < localMax {
			localMax = max
			res = i
		} else {
			max = Max(nums[i], max)
		}
	}

	//+1 as res is the array index
	return res + 1
}
