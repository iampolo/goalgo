package main

import "fmt"

/**
https://leetcode.com/problems/pascals-triangle/
*/
func generate(numRows int) [][]int {
	res := make([][]int, numRows)

	for i := 0; i < numRows; i++ {
		res[i] = make([]int, i+1)

		for j := 0; j <= i; j++ {
			if j == 0 || j == i {
				res[i][j] = 1
			} else {
				res[i][j] = res[i-1][j-1] + res[i-1][j]
			}
		}
	} //for

	return res
}

//BUG
func generate_v2(numRows int) [][]int {
	res := make([][]int, numRows)

	var cur []int
	//not_good := []int{}
	for i := 0; i < numRows; i++ {
		for j := len(cur) - 1; j > 0; j-- {
			cur[j] = cur[j] + cur[j-1]

		}
		cur = append(cur, 1)
		res[i] = make([]int, len(cur))
		copy(res[i], cur[:])
	}

	return res
}

func main_118() {
	//fmt.Println(generate(5))
	fmt.Println(generate_v2(5))
}

/**
https://leetcode.com/problems/pascals-triangle-ii/

*/
func getRow_v2(rowIndex int) []int {
	res := []int{} //slice

	for i := 0; i <= rowIndex; i++ {
		// shift to right, and put a 1 at the front
		res = append(res, 0)   //expand the size
		copy(res[1:], res[0:]) //move from res[0:] to res[1:]
		res[0] = 1             //add what is needed

		for j := 1; j < len(res)-1; j++ {
			res[j] = res[j] + res[j+1]
		}
	}

	return res
}

// forward, and backward will also work
func getRow(rowIndex int) []int {
	res := make([]int, rowIndex+1)
	res[0] = 1
	for i := 1; i <= rowIndex; i++ {

		pre := 0
		for j := 0; j <= i; j++ {
			t := res[j] + pre
			pre = res[j]
			res[j] = t
		}
	}

	return res
}
