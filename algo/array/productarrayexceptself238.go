package main

import "fmt"

//https://www.youtube.com/watch?v=XQlb25LGgHE&t=391s
func productExceptSelf(nums []int) []int {
	res := make([]int, len(nums))

	res[0] = 1
	for i := 1; i < len(res); i++ {
		res[i] = res[i - 1] * nums[i - 1]
	}
	right := 1;
	for i := len(res) - 1; i >= 0; i-- {
		res[i] *= right
		right *= nums[i]
	}
	return  res
}

func _main() {
	var x []int = []int{1, 2, 3, 4, 6}
	res := productExceptSelf(x)
	fmt.Println(res)
}