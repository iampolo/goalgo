package main

/**
https://leetcode.com/problems/reshape-the-matrix/

@see SearchSortedMatrixI_74
*/

//#1
func matrixReshape(mat [][]int, r int, c int) [][]int {
	if len(mat) == 0 || r*c != len(mat)*len(mat[0]) {
		return mat
	}

	var res [][]int
	res = make([][]int, r)
	for i := 0; i < r; i++ {
		res[i] = make([]int, c)
	}
	_, n := len(mat), len(mat[0])
	for i := 0; i < r*c; i++ { // common index to both arrays
		res[i/c][i%c] = mat[i/n][i%n] // conversion between two arrays
	}

	return res
}

//#2
func matrixReshape_v2(mat [][]int, r int, c int) [][]int {
	if len(mat) == 0 || r*c != len(mat)*len(mat[0]) {
		return mat
	}

	var res [][]int
	res = make([][]int, r)
	for i := 0; i < r; i++ {
		res[i] = make([]int, c)
	}

	m, n := 0, 0
	for i := 0; i < len(mat); i++ {
		for j := 0; j < len(mat[0]); j++ {
			res[m][n] = mat[i][j]
			n++
			if n == c { //ready for next row
				n = 0
				m++
			}
		}
	}
	return res
}
//#3
// save the data in mat into a container, and put back to result array
