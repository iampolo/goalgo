package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

func doSliceSep18() {
	months := [...]string{0:"notused",1: "Jan",3:"Mar", 12: "Dec"}
	fmt.Println(months)
	fmt.Println("cap:",cap(months))
	fmt.Println(months[0:])

	a := [...]int{0, 1, 2, 3, 4, 5}
	for i, j := 0, len(a)-1; i < j; i, j = i+1, j-1 {
		a[i], a[j] = a[j], a[i]
	}

	fmt.Println(a)
}

func RightShiftNum(s []int, p int) {
	p = p % len(s);
	if p == 0 {
		fmt.Println("not need to do:", len(s), p)
		return
	}

	reverse(s[:])
	reverse(s[:p])
	reverse(s[p:])

}
func reverse(s []int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i];
	}
}

func __main() {

	a := []int{0, 1, 2, 3, 4, 5}
	RightShiftNum(a, 1)

	fmt.Println("shift: ", a)

	str := util.ReverseString("dsfj")
	fmt.Println("res: ", str)

}

