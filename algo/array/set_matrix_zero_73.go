package main

// https://leetcode.com/problems/set-matrix-zeroes/
func setZeroes(matrix [][]int)  {

	//recording phase
	var colOne bool = false

	for r := 0; r < len(matrix); r++ {

		if matrix[r][0] == 0 {
			colOne = true
		}

		for c := 1; c < len(matrix[0]); c++ {
			if matrix[r][c] == 0 {
				matrix[r][0] = 0
				matrix[0][c] = 0
			}
		}
	} //


	for r := 1; r < len(matrix); r++ {
		for c := 1; c < len(matrix[0]); c++ {
			if matrix[r][0] == 0 || matrix[0][c] == 0 {
				matrix[r][c] = 0
			}
		}
	}

	if matrix[0][0] == 0 {
		for c := 1; c < len(matrix[0]); c++ {
			matrix[0][c] = 0
		}
	}

	if colOne {
		for r := 0; r < len(matrix); r++ {
			matrix[r][0] = 0
		}
	}
}
