package main

import (
	"strings"
)

// https://leetcode.com/problems/shifting-letters/
// You are given a string s of lowercase English letters and an integer array shifts of the same length.
//
//Call the shift() of a letter, the next letter in the alphabet, (wrapping around so that 'z' becomes 'a').
//
//- For example, shift('a') = 'b', shift('t') = 'u', and shift('z') = 'a'.
//
//Now for each shifts[i] = x, we want to shift the first i + 1 letters of s, x times.
//
//Return the final string after all such shifts to s are applied.
//
//
//
//Example 1:
//
//Input: s = "abc", shifts = [3,5,9]
//Output: "rpl"
//Explanation: We start with "abc".
//After shifting the first 1 letters of s by 3, we have "dbc".
//After shifting the first 2 letters of s by 5, we have "igc".
//After shifting the first 3 letters of s by 9, we have "rpl", the answer.
//
//Example 2:
//
//Input: s = "aaa", shifts = [1,2,3]
//Output: "gfd"
//
//
//Constraints:
//
// 1 <= s.length <= 10^5
// s consists of lowercase English letters.
// shifts.length == s.length
// 0 <= shifts[i] <= 10^9
func shiftingLetters(s string, shifts []int) string {
	sh := make([]int, len(shifts))
	sh[len(shifts)-1] = shifts[len(shifts)-1]
	for i := len(shifts) - 2; i >= 0; i-- {
		sh[i] += shifts[i] + sh[i+1]
	}

	var sb strings.Builder
	for i, ch := range s {
		sb.WriteString(string((int(ch-'a')+sh[i])%26 + 'a'))
	}

	return sb.String()
}

func shiftingLetters_v2(s string, shifts []int) string {
	res := make([]rune, len(shifts))

	total := 0
	for i := len(shifts) - 1; i >= 0; i-- {
		total += shifts[i]
		res[i] = rune((int(s[i]-'a')+total)%26) + 'a'
	}
	return string(res)
}

//
func replaceDigits_1844(s string) string {
	res := []byte(s)
	for i := 1; i < len(s); i += 2 {
		res[i] = res[i-1] + res[i] - '0'
	}
	return string(res)
}
