package main

// https://leetcode.com/problems/sort-array-by-parity-ii/

// Given an array of integers nums, half of the integers in nums are odd, and the other half are even.
//
// Sort the array so that whenever nums[i] is odd, i is odd, and whenever nums[i] is even, i is even.
//
// Return any answer array that satisfies this condition.
//
//
//
//Example 1:
//
//Input: nums = [4,2,5,7]
//Output: [4,5,2,7]
//Explanation: [4,7,2,5], [2,5,4,7], [2,7,4,5] would also have been accepted.
//
//Example 2:
//
//Input: nums = [2,3]
//Output: [2,3]
//
//
//Constraints:
//
//2 <= nums.length <= 2 * 104
//nums.length is even.
//Half of the integers in nums are even.
//0 <= nums[i] <= 1000
//
//
//Follow Up: Could you solve it in-place?
func sortArrayByParityII(nums []int) []int {
	//swap too many times
	for i := 0; i < len(nums); i++ {
		j := i + 1
		for i%2 != nums[i]%2 {
			nums[i], nums[j] = nums[j], nums[i]
			j++
		}
	}
	return nums
}

// 4 2 5 7
// i j
// swap if necessary
func sortArrayByParityII_v2(nums []int) []int {

	for i, j := 0, 1; i < len(nums) && j < len(nums); {
		//advancing i if nums[i] is already even
		for i < len(nums) && nums[i]%2 == 0 { // (nums[i] & 1) == 0 -> even
			i += 2
		}
		for j < len(nums) && nums[j]%2 == 1 {
			j += 2
		}
		//reached here means there is invalid nums, prepare to swap
		if i < len(nums) && j < len(nums) {
			nums[i], nums[j] = nums[j], nums[i]
		}
		//i, j not advanced until they are validated
	}
	return nums
}

func sortArrayByParityII_v3(nums []int) []int {
	//check of even position
	for i, j :=0, 1; i < len(nums); i+=2 {
		if nums[i] & 1 == 1 { //it is even, then do the further check and swap; otherwise, no need
			//look for a even num
			for j < len(nums) && nums[j] & 1 == 1 {
				j+=2 //+2 instead of +1 so that it won't touch those already sorted.
			}
			nums[i], nums[j] = nums[j],nums[i]
		}
	} //for
	return nums
}
