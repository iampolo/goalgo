package main

/*
https://leetcode.com/problems/sparse-matrix-multiplication/solution/
*/

func multiply_v2(A [][]int, B [][]int) [][]int {
	lstIdx := make([][]int, len(B))

	for i := 0; i < len(B); i++ {
		//var t []int   //nil
		for j := 0; j < len(B[0]); j++ {
			if B[i][j] != 0 {
				lstIdx[i] = append(lstIdx[i], j)
				//t = append(t, j)
			}
		}
		//lstIdx[i] = t
	}

	res := make([][]int, len(A))
	for i := range res {
		res[i] = make([]int, len(B[0]))
	}

	for i := 0; i < len(A); i++ {
		for j := 0; j < len(A[0]); j++ {
			if A[i][j] == 0 {
				continue
			}

			//idx was saved
			for _, k := range lstIdx[j] {
				res[i][k] += A[i][j] * B[j][k]
			}
		}
	}
	return res
}
