package main

// https://leetcode.com/problems/spiral-matrix/
// Given an m x n matrix, return all elements of the matrix in spiral order.
//
//
//
//Example 1:
//
//
//Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
//Output: [1,2,3,6,9,8,7,4,5]

//Example 2:
//
//Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
//Output: [1,2,3,4,8,12,11,10,9,5,6,7]
//
//
//Constraints:
//
//m == matrix.length
//n == matrix[i].length
//1 <= m, n <= 10
//-100 <= matrix[i][j] <= 100

// https://leetcode.com/problems/spiral-matrix/discuss/1408471/Golang-0ms-20-lines-Very-Short-and-Clean
func spiralOrder_v4(ma [][]int) []int {
	DIRS := [][]int{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}
	r, c, dir := 0, 0, 0
	total := len(ma) * len(ma[0])

	var res []int
	for len(res) < total {
		res = append(res, ma[r][c])
		ma[r][c] = 101 //the bound from Q
		//check if next move is valid
		nr, nc := r+DIRS[dir][0], c+DIRS[dir][1]
		if nr < 0 || nr >= len(ma) || nc < 0 || nc >= len(ma[0]) || ma[nr][nc] == 101 {
			dir = (dir + 1) % 4
		}
		//do the next move
		r += DIRS[dir][0]
		c += DIRS[dir][1]
	}
	return res
}

func spiralOrder_v2(ma [][]int) []int {
	//change of direction: u,d,l,u
	//change of boundaries: width and height

	DIRS := [][]int{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}

	//bound[1] pre-minus 1, as this row is process at the beginning
	bound := []int{len(ma[0]), len(ma) - 1}

	var res []int
	//col:-1 cos the first row is processed at the beginning: -1+1=>0
	r, c, dir := 0, -1, 0
	for bound[dir%2] > 0 {
		for i := 0; i < bound[dir%2]; i++ { //bound of direction_dir=> the step can go
			r, c = r+DIRS[dir][0], c+DIRS[dir][1]
			res = append(res, ma[r][c])
		}

		bound[dir%2]--
		dir = (dir + 1) % 4
	}
	return res
}

func spiralOrder_v3(ma [][]int) []int {
	var res []int
	u, b, l, r := 0, len(ma)-1, 0, len(ma[0])-1
	total := len(ma) * len(ma[0])

	for len(res) < total {
		for i := l; i <= r && len(res) < total; i++ {
			res = append(res, ma[u][i])
		}

		//u+1 as the cell has been processed
		for i := u + 1; i < b && len(res) < total; i++ {
			res = append(res, ma[i][r])
		}

		for i := r; i >= l && len(res) < total; i-- {
			res = append(res, ma[b][i])
		}

		//b-1 as that cell has been processed
		for i := b - 1; i > u && len(res) < total; i-- {
			res = append(res, ma[i][l])
		}
		u, b, l, r = u+1, b-1, l+1, r-1

	}
	return res
}

func spiralOrder(ma [][]int) []int {
	var res []int
	u, b, l, r := 0, len(ma)-1, 0, len(ma[0])-1
	for u < b && l < r {

		for i := l; i < r; i++ {
			res = append(res, ma[u][i])
		}

		for i := u; i < b; i++ {
			res = append(res, ma[i][r])
		}

		for i := r; i > l; i-- {
			res = append(res, ma[b][i])
		}

		for i := b; i > u; i-- {
			res = append(res, ma[i][l])
		}
		u, b, l, r = u+1, b-1, l+1, r-1
	}

	if l > r || u > b {
		return res
	}

	if l == r {
		for i := u; i <= b; i++ {
			res = append(res, ma[i][l])
		}
	} else {
		for i := l; i <= r; i++ {
			res = append(res, ma[u][i])
		}
	}
	return res
}
