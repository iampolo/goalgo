package main

import (
	"reflect"
	"testing"
)

func Test_spiralOrder_v2(t *testing.T) {
	type args struct {
		ma [][]int
	}
	var tests = []struct {
		name string
		args args
		want []int
	}{
		{
			name: "#1",
			args: args{ma: [][]int{
				{1, 2, 3},
				{5, 6, 4},
				{15, 11, 8},
				{9, 10, 7},
			}},
			want: []int{1, 2, 3, 4, 8, 7, 10, 9, 15, 5, 6, 11},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := spiralOrder_v3(tt.args.ma); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("spiralOrder_v3() = %v, want %v", got, tt.want)
			}
		})
	}
}
