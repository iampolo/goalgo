package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

func sortedSquares(A []int) []int {

	left := 0
	right := len(A) - 1

	res := make([]int, len(A))
	for i := len(A) - 1; i >= 0; i-- {
		if Abs(A[left]) <=  Abs(A[right]) {
			res[i] = A[right] * A[right]
			right--
		} else {
			res[i] = A[left] * A[left]
			left++
		}
	}

	return res
}
func testMain() {
	input := []int{1, 2, 3, 4, 5}
	input = []int{-3, -2, -1, 0, 2, 10}
	res := sortedSquares(input)
	fmt.Println(res)
}