package main

// https://leetcode.com/problems/sudoku-solver/submissions/
func solveSudoku(board [][]byte) {
	solvable(board, 0, 0)
}

func solvable(board [][]byte, r, c int) bool {
	if r == 9 {
		c++ //a col is fixed for all of the rows
		if c == 9 {
			return true
		}
		r = 0
	}
	if board[r][c] != '.' {
		return solvable(board, r+1, c)
	}

	for n := byte('1'); n <= '9'; n++ {
		if isValid(board, r, c, n) {
			board[r][c] = n
			if solvable(board, r+1, c) {
				return true
			}
		}
	}

	board[r][c] = '.'
	return false
}

func isValid(board [][]byte, r, c int, num byte) bool {
	for i := 0; i < 9; i++ {
		if board[i][c] == num {
			return false
		}
	}
	for i := 0; i < 9; i++ {
		if board[r][i] == num {
			return false
		}
	}
	rowos, colos := 3*(r/3), 3*(c/3)
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if num == board[rowos+i][colos+j] {
				return false
			}

		}
	}
	return true
}
