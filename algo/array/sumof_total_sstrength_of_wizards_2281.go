package main

/*
https://leetcode.com/problems/sum-of-total-strength-of-wizards/submissions/
https://leetcode.cn/problems/sum-of-total-strength-of-wizards/solution/dan-diao-zhan-qian-zhui-he-de-qian-zhui-d9nki/
*/
func totalStrength(strength []int) (res int) {
	n := len(strength)
	left := make([]int, n)
	right := make([]int, n)
	for i := range right {
		right[i] = n
	}
	st := []int{-1}
	for i, v := range strength {
		for len(st) > 1 && strength[st[len(st)-1]] >= v {
			right[st[len(st)-1]] = i
			st = st[:len(st)-1]
		}
		left[i] = st[len(st)-1]
		st = append(st, i)
	}

	s := 0
	preMul := make([]int, n+2)
	for i, v := range strength {
		s += v
		preMul[i+2] = (preMul[i+1] + s) % MOD
	}
	for i, v := range strength {
		l, r := left[i]+1, right[i]-1
		total := ((i-l+1)*(preMul[r+2]-preMul[i+1]) - (r-i+1)*(preMul[i+1]-preMul[l])) % MOD

		res = (res + v*total) % MOD
	}
	return (res + MOD) % MOD
}

const MOD int = 1e9 + 7
