package v2

import (
	"strings"
)

// https://leetcode.com/problems/check-if-word-can-be-placed-in-crossword/
//https://leetcode.com/problems/check-if-word-can-be-placed-in-crossword/discuss/1486692/Java-solution

//
func placeWordInCrossword(board [][]byte, word string) bool {

	rotate := make([][]byte, len(board[0]))
	for i := 0; i < len(rotate); i++ {
		rotate[i] = make([]byte, len(board))
	}

	//123   147
	//456-> 258
	//789   369
	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board[0]); j++ {
			rotate[j][i] = board[i][j]
		}
	}

	search := func(board [][]byte, words []string) bool {
		for _, row := range board {
			parts := strings.Split(string(row), "#")

			for _, w := range words {
				for _, p := range parts {
					if len(w) != len(p) {
						continue
					}
					i := 0
					for i < len(w) && (w[i] == p[i] || p[i] == ' ') {
						i++
					}
					if i == len(w) {
						return true
					}
				} //1
			} //
		}
		return false
	}

	words := []string{word, reverse(word)}

	return search(board, words) || search(rotate, words)
}

func reverse(str string) string {
	arr, size := []rune(str), len(str)
	for i := 0; i < len(arr)/2; i++ {
		arr[i], arr[size-i-1] = arr[size-i-1], arr[i]
	}
	return string(arr)
}
