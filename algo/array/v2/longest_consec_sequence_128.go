package v2

import (
	"sort"
)

/**
https://leetcode.com/problems/longest-consecutive-sequence/

*/

func longestConsecutive(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	sort.Ints(nums)
	longest := 1
	curlen := 1
	for i := 1; i < len(nums); i++ {
		if nums[i-1] == nums[i] {
			continue
		}

		if nums[i-1] == nums[i]-1 {
			curlen++
		} else {
			if curlen > longest {
				longest = curlen
			}
			curlen = 1
		}
	}

	if curlen > longest {
		return curlen
	}
	return longest
}
