package v2

/**
https://leetcode.com/problems/range-addition/

https://massivealgorithms.blogspot.com/2016/06/leetcode-370-range-addition.html

http://www.cnblogs.com/grandyang/p/5628786.html
Assume you have an array of length n initialized with all 0's and are given k update operations.
Each operation is represented as a triplet: [startIndex, endIndex, inc] which increments each element of
subarray A[startIndex ... endIndex] (startIndex and endIndex inclusive) with inc.
Return the modified array after all k operations were executed.

Example:
Given:

    length = 5,
    updates = [
        [1,  3,  2],
        [2,  4,  3],
        [0,  2, -2]
    ]

Output:

    [-2, 0, 3, 5, 3]
Explanation:
Initial state:
[ 0, 0, 0, 0, 0 ]

After applying operation [1, 3, 2]:
[ 0, 2, 2, 2, 0 ]

After applying operation [2, 4, 3]:
[ 0, 2, 5, 5, 3 ]

After applying operation [0, 2, -2]:
[-2, 0, 3, 5, 3 ]
Hint:
Thinking of using advanced data structures? You are thinking it too complicated.
For each update operation, do you really need to update all elements between i and j?
Update only the first and end element is sufficient.
The optimal time complexity is O(k + n) and uses O(1) extra space.

https://discuss.leetcode.com/topic/49691/java-o-k-n-time-complexity-solution
http://shirleyisnotageek.blogspot.com/2016/10/range-addition.html
The idea is to utilize the fact that the array initializes with zero. The hint suggests us that we only
needs to modify the first and last element of the range. In fact, we need to increment the first element
in the range and decreases the last element + 1 (if it's within the length) by inc. Then we sum up all
previous results. Why does this work? When we sum up the array, the increment is passed along to the
subsequent elements until the last element. When we decrement the end + 1 index, we offset the increment
so no increment is passed along to the next element.

*/

//https://massivealgorithms.blogspot.com/2016/06/leetcode-370-range-addition.html
func getModifiedArray(length int, updates [][]int) []int {


	return nil
}
