package v2

/**
https://leetcode.com/problems/three-equal-parts/
You are given an array arr which consists of only zeros and ones, divide the array into three non-empty parts such
that all of these parts represent the same binary value.

If it is possible, return any [i, j] with i + 1 < j, such that:

arr[0], arr[1], ..., arr[i] is the first part,
arr[i + 1], arr[i + 2], ..., arr[j - 1] is the second part, and
arr[j], arr[j + 1], ..., arr[arr.length - 1] is the third part.
All three parts have equal binary values.
If it is not possible, return [-1, -1].

Note that the entire part is used when considering what binary value it represents. For example, [1,1,0]
represents 6 in decimal, not 3. Also, leading zeros are allowed, so [0,1,1] and [1,1] represent the same value.



Example 1:

Input: arr = [1,0,1,0,1]
Output: [0,3]

Example 2:

Input: arr = [1,1,0,1,1]
Output: [-1,-1]

Example 3:

Input: arr = [1,1,0,0,1]
Output: [0,2]


Constraints:

3 <= arr.length <= 3 * 10^4
arr[i] is 0 or 1


@see RainbowSortII
@see MaxSumOfTwoNonOverlappingSubarrays1031

https://leetcode.com/problems/three-equal-parts/discuss/250203/Logical-Thinking
*/

func threeEqualParts(arr []int) []int {

	/**
	 	if there are certain amount of 1 in an array, how to divided into
		three parts where each part has equal number of these 1s.
		- each part must begin with an 1
		- the number of 1s in each part must be the same
		- how to handle 0 case?
	*/
	oneCnt := countOnes(arr)
	if oneCnt%3 != 0 {
		return []int{-1, -1}
	}

	if oneCnt == 0 {
		return []int{0, len(arr) - 1}
	}

	/**
	  divide by 3 to split to three subarrays
	  - find the position of 1st 1 in each group
	  totalCnt / 3 =>
	*/
	target := oneCnt / 3

	//find the beginning of 3rd part
	k := len(arr)
	for target > 0 {
		k--
		if arr[k] == 1 {
			target--
		}
	}

	//find the 1st 1 of the 1st part
	l := 0
	for arr[l] == 0 {
		l++
	}

	//check if 1st part is the same as 3rd part
	tmp := k
	for tmp < len(arr) && arr[l] == arr[tmp] {
		tmp++
		l++
	}
	if tmp != len(arr) {
		//1st and 3rd part don't match
		return []int{-1, -1}
	}
	posOne := l - 1 //save the first cut point

	//skip all 0 between 1st and 2nd part
	for arr[l] == 0 {
		l++
	}

	tmp = k
	//check if 2nd part is the same as 3rd part
	for tmp < len(arr) && arr[l] == arr[tmp] {
		tmp++
		l++
	}
	if tmp != len(arr) {
		//2nd and 3rd part don't match
		return []int{-1, -1}
	}

	return []int{posOne, l}
}

func countOnes(arr []int) int {
	cnt := 0
	for i := 0; i < len(arr); i++ {
		if arr[i] == 1 {
			cnt++
		}
	}
	return cnt
}
