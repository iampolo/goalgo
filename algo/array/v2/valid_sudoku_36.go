package v2

// https://leetcode.com/problems/valid-sudoku/discuss/934971/Simple-and-explained-solution-in-Go
func isValidSudoku(board [][]byte) bool {
	for r := 0; r < 9; r++ {
		rowCheck, colCheck, sqrCheck := 0, 0, 0

		// set the starting position for a square
		// the col loop will add up the offset, but still inside a square
		ros, cos := 3*(r/3), 3*(r%3)
		for c := 0; c < 9; c++ {
			rowCh := board[r][c]
			colCh := board[c][r]
			sqrCh := board[ros+c/3][cos+c%3] //manipulate the number so that the outcome is always in 3
			if !checkAndSet(rowCh, &rowCheck) {
				return false
			}
			if !checkAndSet(colCh, &colCheck) {
				return false
			}
			if !checkAndSet(sqrCh, &sqrCheck) {
				return false
			}
		} //
	}
	return true
}

func checkAndSet(ch byte, digits *int) bool {
	if ch == '.' { //not a digit
		return true
	}

	setBit := 1 << (ch - '0')
	if *digits&setBit != 0 { //already exists
		return false
	}
	*digits |= setBit
	return true
}
