package main

import . "gitlab.com/iampolo/goalgo/algo/util"

// https://leetcode.com/problems/valid-sudoku/discuss/934971/Simple-and-explained-solution-in-Go
func isValidSudoku(board [][]byte) bool {

	rowCheck := NewSet()
	colCheck := NewSet()
	sqrCheck := NewSet()

	for r := 0; r < len(board); r++ {

		ros, cos := 3*(r/3), 3*(r%3)

		for c := 0; c < len(board[0]); c++ {
			rowNum := board[r][c]
			colNum := board[c][r]
			sqrNum := board[ros+c/3][cos+c%3]

			if rowNum != '.' && rowCheck.Contains(rowNum) {
				return false
			}
			rowCheck.Add(rowNum)
			if colNum != '.' && colCheck.Contains(colNum) {
				return false
			}
			colCheck.Add(colNum)
			if sqrNum != '.' && sqrCheck.Contains(sqrNum) {
				return false
			}
			sqrCheck.Add(sqrNum)
		}
		rowCheck.Clean()
		colCheck.Clean()
		sqrCheck.Clean()
	}
	return true
}
