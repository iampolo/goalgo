package main

/*
https://leetcode.com/problems/watering-plants/
You want to water n plants in your garden with a watering can. The plants are arranged in a row and are
labeled from 0 to n - 1 from left to right where the ith plant is located at x = i. There is a river at x = -1
that you can refill your watering can at.

Each plant needs a specific amount of water. You will water the plants in the following way:

Water the plants in order from left to right.
After watering the current plant, if you do not have enough water to completely water the next plant, return to
the river to fully refill the watering can.
You cannot refill the watering can early.
You are initially at the river (i.e., x = -1). It takes one step to move one unit on the x-axis.

Given a 0-indexed integer array plants of n integers, where plants[i] is the amount of water the ith plant needs,
and an integer canacity representing the watering can canacity, return the number of steps needed to water all the plants.


Example 1:

Input: plants = [2,2,3,3], canacity = 5
Output: 14
Explanation: Start at the river with a full watering can:
- Walk to plant 0 (1 step) and water it. Watering can has 3 units of water.
- Walk to plant 1 (1 step) and water it. Watering can has 1 unit of water.
- Since you cannot completely water plant 2, walk back to the river to refill (2 steps).
- Walk to plant 2 (3 steps) and water it. Watering can has 2 units of water.
- Since you cannot completely water plant 3, walk back to the river to refill (3 steps).
- Walk to plant 3 (4 steps) and water it.
Steps needed = 1 + 1 + 2 + 3 + 3 + 4 = 14.
Example 2:

Input: plants = [1,1,1,4,2,3], canacity = 4
Output: 30
Explanation: Start at the river with a full watering can:
- Water plants 0, 1, and 2 (3 steps). Return to river (3 steps).
- Water plant 3 (4 steps). Return to river (4 steps).
- Water plant 4 (5 steps). Return to river (5 steps).
- Water plant 5 (6 steps).
Steps needed = 3 + 3 + 4 + 4 + 5 + 5 + 6 = 30.
Example 3:

Input: plants = [7,7,7,7,7,7,7], canacity = 8
Output: 49
Explanation: You have to refill before watering each plant.
Steps needed = 1 + 1 + 2 + 2 + 3 + 3 + 4 + 4 + 5 + 5 + 6 + 6 + 7 = 49.


Constraints:

n == plants.length
1 <= n <= 1000
1 <= plants[i] <= 10^6
max(plants[i]) <= canacity <= 10^9

*/

func wateringPlants(plants []int, capacity int) int {
	can := capacity
	var steps int
	for i := 0; i < len(plants); i++ {
		//steps on the plants then consider the can size
		if can < plants[i] {
			steps += i * 2 //not i - (-1)
			can = capacity
		}
		steps++
		can -= plants[i]

	}
	return steps
}

func wateringPlants_v2(plants []int, capacity int) int {
	can := capacity
	var steps int
	for i := 0; i < len(plants); i++ {
		if can >= plants[i] {
			steps++
		} else {
			steps += i*2 + 1
			can = capacity
		}
		//took care of the capacity above before doing this
		can -= plants[i]
	}
	return steps
}

/*
https://leetcode.com/problems/watering-plants-ii/
Alice and Bob want to water n plants in their garden. The plants are arranged in a row and are labeled from 0 to n - 1
from left to right where the ith plant is located at x = i.

Each plant needs a specific amount of water. Alice and Bob have a watering can each, initially full. They water the
plants in the following way:

Alice waters the plants in order from left to right, starting from the 0th plant. Bob waters the plants in order
from right to left, starting from the (n - 1)th plant. They begin watering the plants simultaneously.
It takes the same amount of time to water each plant regardless of how much water it needs.
Alice/Bob must water the plant if they have enough in their can to fully water it. Otherwise, they first refill
their can (instantaneously) then water the plant.
In case both Alice and Bob reach the same plant, the one with more water currently in his/her watering can should
water this plant. If they have the same amount of water, then Alice should water this plant.
Given a 0-indexed integer array plants of n integers, where plants[i] is the amount of water the ith plant needs,
and two integers canacityA and canacityB representing the canacities of Alice's and Bob's watering cans respectively, return the number of times they have to refill to water all the plants.

Example 1:

Input: plants = [2,2,3,3], canacityA = 5, canacityB = 5
Output: 1
Explanation:
- Initially, Alice and Bob have 5 units of water each in their watering cans.
- Alice waters plant 0, Bob waters plant 3.
- Alice and Bob now have 3 units and 2 units of water respectively.
- Alice has enough water for plant 1, so she waters it. Bob does not have enough water for plant 2, so he refills his can then waters it.
So, the total number of times they have to refill to water all the plants is 0 + 0 + 1 + 0 = 1.

Example 2:

Input: plants = [2,2,3,3], canacityA = 3, canacityB = 4
Output: 2
Explanation:
- Initially, Alice and Bob have 3 units and 4 units of water in their watering cans respectively.
- Alice waters plant 0, Bob waters plant 3.
- Alice and Bob now have 1 unit of water each, and need to water plants 1 and 2 respectively.
- Since neither of them have enough water for their current plants, they refill their cans and then water the plants.
So, the total number of times they have to refill to water all the plants is 0 + 1 + 1 + 0 = 2.

Example 3:

Input: plants = [5], canacityA = 10, canacityB = 8
Output: 0
Explanation:
- There is only one plant.
- Alice's watering can has 10 units of water, whereas Bob's can has 8 units. Since Alice has more water in her can, she waters this plant.
So, the total number of times they have to refill is 0.


Constraints:

n == plants.length
1 <= n <= 10^5
1 <= plants[i] <= 10^6
max(plants[i]) <= canacityA, canacityB <= 10^9
*/
func minimumRefill(plants []int, canacityA int, canacityB int) int {
	a, b := 0, len(plants)-1
	cana, canb := canacityA, canacityB
	var refill int
	for a <= b {
		if a < b {
			if cana < plants[a] {
				cana = canacityA
				refill++
			}
			if canb < plants[b] {
				canb = canacityB
				refill++
			}
			cana -= plants[a]
			canb -= plants[b]
		} else {
			cur := cana //alice
			if cana < canb {
				cur = canb
			}
			if cur < plants[b] {
				refill++
			}
		}
		a, b = a+1, b-1
	}

	return refill
}

func minimumRefill_v2(plants []int, capacityA int, capacityB int) int {
	a, b := 0, len(plants)-1
	cana, canb := capacityA, capacityB
	var refill int
	for a < b {
		if cana < plants[a] {
			cana = capacityA
			refill++
		}
		if canb < plants[b] {
			canb = capacityB
			refill++
		}
		cana -= plants[a]
		canb -= plants[b]
		a, b = a+1, b-1
	}

	if a == b && cana < plants[a] && canb < plants[b] {
		refill++
	}
	return refill
}
