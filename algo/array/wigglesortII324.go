package main

import (
	"sort"
)

/*
Given an unsorted array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....

Example 1:

Input: nums = [1, 5, 1, 1, 6, 4]
Output: One possible answer is [1, 4, 1, 5, 1, 6].

Example 2:

Input: nums = [1, 3, 2, 2, 3, 1]
Output: One possible answer is [2, 3, 1, 3, 1, 2].

Note:
You may assume all input has valid answer.

Follow Up:
Can you do it in O(n) time and/or in-place with O(1) extra space?

*/
func wiggleSortII(nums []int) {
	len := len(nums)
	res := make([]int, len)
	copy(res, nums)
	sort.Ints(res)
	m := (len - 1) / 2 //mid-point for odd length, half half for even length

	idx := 0
	for i := 0; i <= m; i++ {
		nums[idx] = res[m-i]
		if idx+1 < len {
			nums[idx+1] = res[len-1-i]
		}
		idx += 2
	}
}

func test324() {
	nums := []int{1, 3, 2, 4, 6, 8, 7}
	wiggleSort(nums)
}

