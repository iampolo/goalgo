package main

import "fmt"

// https://leetcode.com/problems/bitwise-and-of-numbers-range/
//
func rangeBitwiseAnd_201_BF(left int, right int) int {
	if left == 0 { //0 & ?? => 0
		return left
	}
	var res = left
	for i := left; i <= right; i++ {
		res &= i
	}
	return res
}

func rangeBitwiseAnd_201_v2(left int, right int) int {
	//idea: as long as there is a 1 and a 0, the outcome in a position is always 0
	//    	ie: find the common prefix of the binary forms of two numbers
	//00011010 - 00011110
	//00001101 - 00001111
	//00000110 - 00000111
	//00000011 - 00000011  until the bits are the same
	fmt.Printf("%08b - %08b \n", left, right)
	cnt := 0
	for left != right {
		left >>= 1

		right >>= 1

		cnt++
		fmt.Printf("%08b - %08b \n", left, right)
	}
	return left << cnt
}

// https://leetcode.com/problems/bitwise-and-of-numbers-range/
// Brian Kernighan's Algo
// turns off the rightmost bit one of a number
//   n & (n-1)
// used to find common prefix of two bit strings
func rangeBitwiseAnd_201_brian(left int, right int) int {

	//find the common prefix of their binary forms
	//left is the smaller one, so it holds of the boundary of common prefix
	//we continously apply n&(n-1) to right untill we find the common prefix
	for left < right {
		right &= right - 1
	}
	return left & right
}

// similar to ??/10, eliminate the last digit of a number
// ??/2, eliminate the last digits of a number in binary base: the remainer is 0, 1
//find common prefix with recursion
func rangeBitwiseAnd_201_recur(left int, right int) int {
	if left < right {
		return rangeBitwiseAnd_201_recur(left/2, right/2) << 1
	}
	return left //could be just 0, but if it is ???1 remaining, we find the common prefix
}
