package main

/**
https://leetcode.com/problems/count-words-obtained-after-adding-a-letter/


The conversion operation is described in the following two steps:

* Append any lowercase letter that is not present in the string to its end.
	- For example, if the string is "abc", the letters 'd', 'e', or 'y' can be added to it, but not 'a'.
	  If 'd' is added, the resulting string will be "abcd".
* Rearrange the letters of the new string in any arbitrary order.
	- For example, "abcd" can be rearranged to "acbd", "bacd", "cbda", and so on. Note that it can also be
      rearranged to "abcd" itself.

Constraints:

1 <= startWords.length, targetWords.length <= 5 * 10^4
1 <= startWords[i].length, targetWords[j].length <= 26
Each string of startWords and targetWords consists of lowercase English letters only.
No letter occurs more than once in any string of startWords or targetWords.


hits:
	- length different must be one
	-
 */
func wordCount(startWords []string, targetWords []string) int {
	//preprocess startwords -- convert them to binary representation and store in a Map for later lookup
	lookup := make(map[int]bool)
	for _, s := range startWords {
		var val int
		for _, c := range s {
			val ^= 1 << (c - 'a')
		}
		lookup[val] = true
	}

	var res int
	for _, t := range targetWords {
		var val int
		for _, c := range t {
			val ^= 1 << (c - 'a')
		}

		//check existence
		for _, c := range t {
			//eliminate a char and check if the new val is in the lookup map
			if ok := lookup[val ^ 1<< (c-'a')]; ok {
				res++
				break
			}
		}
	}
	return res
}
