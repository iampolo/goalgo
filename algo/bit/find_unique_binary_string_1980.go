package main

import (
	"math"
	"strconv"
	"strings"
)

//https://leetcode.com/problems/find-unique-binary-string/
//
//Given an array of strings nums containing n unique binary strings each of length n, return a binary string
//of length n that does not appear in nums. If there are multiple answers, you may return any of them.
//
//
//
//Example 1:
//
//Input: nums = ["01","10"]
//Output: "11"
//Explanation: "11" does not appear in nums. "00" would also be correct.
//
//Example 2:
//
//Input: nums = ["00","01"]
//Output: "11"
//Explanation: "11" does not appear in nums. "10" would also be correct.
//
//Example 3:
//
//Input: nums = ["111","011","001"]
//Output: "101"
//Explanation: "101" does not appear in nums. "000", "010", "100", and "110" would also be correct.
//
//
//Constraints:
//
// - n == nums.length
// - 1 <= n <= 16
// - nums[i].length == n
// - nums[i] is either '0' or '1'.
// - All the strings of nums are unique.
//
func findDifferentBinaryString(nums []string) string {
	//since the number of elements in nums is the same as the length of each elements in the nums
	//we can use the trick like this:
	//https://en.wikipedia.org/wiki/Cantor%27s_diagonal_argument
	res := make([]byte, len(nums[0]))
	for i := 0; i < len(nums); i++ {
		res[i] = '0'
		if nums[i][i] == '0' {
			res[i] = '1'
		}
	}
	return string(res)
}

func findDifferentBinaryString_v2(nums []string) string {
	res := make([]byte, len(nums[0]))
	for i := 0; i < len(nums); i++ {
		res[i] = ((nums[i][i] - '0') ^ 1) + '0'
	}
	return string(res)
}

func findDifferentBinaryString_v3(nums []string) string {

	nMap := make(map[int64]int)
	//store the binary strings as numbers
	for _, n := range nums {
		val, _ := strconv.ParseInt(n, 2, 32)
		nMap[val] = 1
	}
	bound := int64(math.Pow(2, float64(len(nums))))
	//generate number from 0 up to the limit, and return the one not in the map
	for i := int64(0); i < bound; i++ {
		if _, exist := nMap[i]; !exist {
			pad := strings.Repeat("0", len(nums))
			tmp := pad + strconv.FormatInt(i, 2)
			return tmp[len(tmp)-len(nums):]

		}
	}
	return "error"
}
