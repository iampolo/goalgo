package main

import "fmt"

func main() {
	test_2174()
}

func test_2174() {
	g := [][]int{{1, 1, 1}, {1, 1, 1}, {0, 1, 0}}
	g = [][]int{
		{0, 1, 0},
		{1, 0, 1},
		{0, 1, 0}}
	fmt.Println(removeOnes(g))
}

func test_1494() {
	n := 5
	dep := [][]int{{2, 1}, {3, 1}, {4, 1}, {1, 5}}
	k := 2

	n = 12
	dep = [][]int{{1, 2}, {1, 3}, {7, 5}, {7, 6}, {4, 8}, {8, 9}, {9, 10}, {10, 11}, {11, 12}}
	k = 2

	fmt.Println(minNumberOfSemesters(n, dep, k))
}

//
func test_1980() {
	nums := []string{"01", "10"}
	//nums = []string{"111", "011", "001"}
	fmt.Println(findDifferentBinaryString_v3(nums))
}

func test_bitwise_tips() {
	m, n := 16, 8
	m, n = 26, 230
	fmt.Println(rangeBitwiseAnd_201_v2(m, n))
}

func test_782() {
	board := [][]int{{0, 1, 1, 0},
		{0, 1, 1, 0},
		{1, 0, 0, 1},
		{1, 0, 0, 1}}

	board = [][]int{
		{0, 1, 1, 0},
		{1, 0, 0, 1},
		{0, 1, 1, 0},
		{1, 0, 0, 1}}

	board = [][]int{
		{1, 1, 0},
		{0, 0, 1},
		{0, 0, 1}}

	board = [][]int{
		{1, 0, 0, 1, 1},
		{1, 0, 0, 1, 1},
		{0, 1, 1, 0, 0},
		{0, 1, 1, 0, 0},
		{1, 0, 0, 1, 1}}
	fmt.Println(movesToChessboard(board))
}

func test_1239() {
	str := []string{"un", "iq", "ue"}
	fmt.Println(maxLength(str))
	fmt.Println(maxLength_v2(str))
}

func print_xor() {
	for i := 0; i < 6; i++ {
		fmt.Printf("%d %08b %08b\n", i, i, ^i)
	}

}

func test_600() {
	n := 4
	fmt.Println(findIntegers(n))
}

func printBit(n int) {
	for i := 0; i <= n; i++ {

		fmt.Printf("%08b", i)
		fmt.Println()
	}

	j := 4
	fmt.Printf("%b %b\n", j, j+(1<<4))
}

/**
101111101011110000100000000
*/
func TestBit() {
	var a byte = 0x0F
	fmt.Printf("%08b\n", a)
	fmt.Printf("%08b\n", ^a)

	a = 0xAB
	fmt.Printf("%08b\n", a)
	a &^= 0x0F
	fmt.Printf("%08b\n", a)
}
