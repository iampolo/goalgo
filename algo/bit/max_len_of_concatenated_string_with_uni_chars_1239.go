package main

// https://leetcode.com/problems/maximum-length-of-a-concatenated-string-with-unique-characters/

// Method 1 concatenate and check dup with set
func maxLength(arr []string) int {
	isUnique := func(str string) bool {
		curChBit, dup := 0, 0
		for _, ch := range str {
			dup |= curChBit & (1 << (ch - 'a'))
			if dup > 0 {
				return false
			}
			curChBit |= 1 << (ch - 'a')
		}
		return true
	}

	var dfs func(arr []string, idx int, cur string, max *int)
	dfs = func(arr []string, idx int, cur string, max *int) {
		//is the current concatenated string unique already
		var uni bool
		if uni = isUnique(cur); uni {
			if len(cur) > *max {
				*max = len(cur) //record the max
			}
		}
		//base case
		if idx == len(arr) && !uni {
			return
		}

		//concatenate more string from idx
		for i := idx; i < len(arr); i++ {
			dfs(arr, i+1, cur+arr[i], max) //care only the length not the concatenate order
		}
	}

	var res int
	dfs(arr, 0, "", &res)
	return res
}

// Method 2 concatenate and check dup with bitmask
func maxLength_v2(arr []string) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	bitCnt := func(n int) int {
		var cnt int
		for n != 0 {
			cnt += n & 1
			n >>= 1
		}
		return cnt
	}

	var res int
	var allStr []int           //all string in mask format
	allStr = append(allStr, 0) //start with an empty string
	for _, s := range arr {
		curChBit, dup := 0, 0

		for _, ch := range s {
			dup |= curChBit & (1 << (ch - 'a'))
			curChBit |= 1 << (ch - 'a')
		}
		if dup > 0 {
			continue
		}

		//backward, loop through and add new
		for i := len(allStr) - 1; i >= 0; i-- {
			if allStr[i]&curChBit > 0 {
				continue
			}
			allStr = append(allStr, allStr[i]|curChBit)
			res = max(res, bitCnt(allStr[i]|curChBit)) //# of 1 is concatenated characters
		}
	}

	return res
}
