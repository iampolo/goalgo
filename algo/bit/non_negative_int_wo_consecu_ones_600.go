package main

//https://leetcode.com/problems/non-negative-integers-without-consecutive-ones/

/**
Given a positive integer n, return the number of the integers in the range [0, n] whose binary
representations do not contain consecutive ones.


Example 1:

Input: n = 5
Output: 5
Explanation:
Here are the non-negative integers <= 5 with their corresponding binary representations:
0 : 0
1 : 1
2 : 10
3 : 11
4 : 100
5 : 101
Among them, only integer 3 disobeys the rule (two consecutive ones) and the other 5 satisfy the rule.

Example 2:

Input: n = 1
Output: 2

Example 3:

Input: n = 2
Output: 3


Constraints:
1 <= n <= 10^9

*/
func findIntegers(n int) int {
	return countIt(0, 0, n, false)
}

/**
O(x) : x total number to be generated with 1...n
	-> O(32*n)
*/
func countIt(p int, sum int, n int, added bool) int {
	if sum > n { //already overflow
		return 0
	}
	if (1 << p) > n { //one more appending(prefix) will overflow, so stop here
		return 1
	}
	if added { //1 has been appended at previous level, so skip
		return countIt(p+1, sum, n, false)
	}
	//two cases: append 1 as the prefix, append 1 as the prefix
	return countIt(p+1, sum, n, false) + countIt(p+1, sum+(1<<p), n, true) //<- add bit as prefix
	/** add bit to the end (LSB)
	(n << 1) + 1
	(n << 1) + 0
	*/
}

//*********************************************************************

/**
O(32*n)
*/
func findIntegers_tle(n int) int {
	var cnt int
	for i := 0; i <= n; i++ {
		if check(i) {
			cnt++
		}
	}
	return cnt
}

/**
00000011
*/
func check(n int) bool {
	bit := 31
	for bit > 0 {
		if (n&(1<<bit)) != 0 && (n&(1<<(bit-1))) != 0 {
			return false
		}
		bit--
	}
	return true
}
