package main

import "math"

/*
https://leetcode.com/problems/remove-all-ones-with-row-and-column-flips-ii/
You are given a 0-indexed m x n binary matrix grid.

In one operation, you can choose any i and j that meet the following conditions:

0 <= i < m
0 <= j < n
grid[i][j] == 1
and change the values of all cells in row i and column j to zero.

Return the minimum number of operations needed to remove all 1's from grid.

RemoveAllOnesWithFlips2128
*/

func removeOnes(grid [][]int) int {
	var find func(rBit, cBit int, moves int) int
	//market processed row and col, as the entire col or row are switched to 0 already.
	//use DFS to mark the first row and col processed only
	find = func(rBit, cBit int, moves int) int {
		min := math.MaxInt32
		for r := range grid {
			for c := range grid[0] {
				if grid[r][c] == 0 || rBit&(1<<r) > 0 || cBit&(1<<c) > 0 {
					continue
				}
				res := find(rBit|(1<<r), cBit|(1<<c), moves+1)
				if res < min {
					min = res
				}
			} //for
		} //for
		if min == math.MaxInt32 {
			return moves
		}
		return min
	}

	return find(0, 0, 0)
}
