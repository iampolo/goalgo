package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"strconv"
)

// https://leetcode.com/problems/transform-to-chessboard/
//1)
// in a valid chessboard any rectangle inside the board with corner NW, NE, SW, SE (NW here means north-west)
// must satisfy (NW xor NE) == (SW xor SE).
//
//	For example given a board
//
//	a b c
//	d e f
//
//	Since there are three rectangles insides the board, the validness property requires
//	(a xor b) == (d xor e)
//	(a xor c) == (d xor f)
//	(b xor c) == (e xor f)
//
//The proposition is, given a board satisfying validness property, this property always holds no matter
//how you swap any row or column.
//
//2)
// in a valid chessboard, there are two and only two types of 1 or 0 combination:
// if for one row(col), it is 01010011, the other row must have either the same 1, 0 combination or a complete reverse of it.
// e.g.: 01010011 or 10101100
//
//3)
// the # of 1s or 0s in a row or col depends on the size of the row or col:
//   even:  n / 2
//	  odd:  (n+1)/2
//
//4) to count the number of swap needed
//
//


/*
  	O(n^2) as we need to go through each cell
 */
func movesToChessboard(board [][]int) int {
	/*
		https://leetcode.com/problems/transform-to-chessboard/discuss/440085/Python-detailed-explanation
		rules:
			- each row must either be equal to first row or equal to the reverse of the first row
				- there are always two type of rows no matter how swapped the grid is.
			- count of 1 in the 1st row and 1st col must be equal to the count of 0 or at most differ by 1
				-
	*/

	// https://leetcode.com/problems/transform-to-chessboard/discuss/114847/C%2B%2BJavaPython-Solution-with-Explanation
	//for a valid chess board, the four corners must be either 0000, 1111, 1100 (2-1s,2-0s),
	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board); j++ {
			//check for each rectangle in this board (NW^NE) == (SW^SE) must be hold for a valid board
			if board[0][0]^board[i][0]^board[i][j]^board[0][j] == 1 {
				return -1
			}
		}
	}
	//valid the 1st row and 1st col is enough
	//assume the 1st cell starts with 0: 01010101, start with 1: 1010101 also works!
	rowDiff, colDiff := 0, 0
	rowSum, colSum := 0, 0
	for i := 0; i < len(board); i++ {
		rowSum += board[0][i]   // left to right
		colSum += board[i][0]   // up to bottom
		if board[i][0] == i%2 { //or '!='
			rowDiff++
		}
		if board[0][i] == i%2 {
			colDiff++
		}
	}

	n := len(board)
	//handle odd or even sized grid
	//# of 1 and 0 in the row and the col must be valid
	if rowSum != n/2 && rowSum != (n+1)/2 {
		return -1
	}
	if colSum != n/2 && colSum != (n+1)/2 {
		return -1
	}

	//a odd or even sized grid swaps differently
	if n%2 == 1 {
		//odd sized
		if rowDiff%2 == 1 {
			//take even, as one swap involves two rows
			rowDiff = n - rowDiff
		}
		if colDiff%2 == 1 {
			colDiff = n - colDiff
		}
	} else {
		//even sized, take the min for the answer
		rowDiff = Min(rowDiff, n-rowDiff)
		colDiff = Min(colDiff, n-colDiff)
	}
	//as one swap involves two cols or two rows
	return (rowDiff + colDiff) / 2
}

func test() {

	fmt.Println(strconv.FormatInt(29, 8))
	fmt.Println(strconv.FormatInt(29, 16))
	xor := 1
	mask := 0
	for i := 0; i < 4; i++ {
		xor ^= mask

		fmt.Println(strconv.FormatInt(int64(xor), 2))
		mask = 1 - mask
	}
}
