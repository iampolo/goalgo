package main

// https://leetcode.com/problems/find-winner-on-a-tic-tac-toe-game/

func tictactoe_v2(moves [][]int) string {
	board := make([][]int, 3)
	for i := 0; i < 3; i++ {
		board[i] = make([]int, 3)
	}

	//start with player A:1
	//player B:-1
	player := 1
	for i := 0; i < len(moves); i++ {
		m := moves[i]
		board[m[0]][m[1]] = player

		if m[0]+m[1] == 2 && checkADiag(player, board) || m[0] == m[1] && checkDiag(player, board) ||
			checkCol(m[1], player, board) || checkRow(m[0], player, board) {
			if player == 1 {
				return "A"
			} else {
				return "B"
			}
		}
		player *= -1
	}

	if len(moves) == 9 {
		return "Draw"
	}
	return "Pending"
}

func checkADiag(player int, board [][]int) bool {
	for i := 0; i < 3; i++ {
		if board[i][3-1-i] != player {
			return false
		}
	}
	return true
}

func checkDiag(player int, board [][]int) bool {
	for i := 0; i < 3; i++ {
		if board[i][i] != player {
			return false
		}
	}
	return true
}

func checkCol(col, player int, board [][]int) bool {
	for i := 0; i < 3; i++ {
		if board[i][col] != player {
			return false
		}
	}
	return true
}

func checkRow(row, player int, board [][]int) bool {
	for i := 0; i < 3; i++ {
		if board[row][i] != player {
			return false
		}
	}
	return true //all are from player
}

//***************************************************************
func tictactoe(moves [][]int) string {
	rows := make(map[int]int)
	cols := make(map[int]int)

	abs := func(a int) int {
		if a < 0 {
			return -a
		}
		return a
	}

	diag, adiag := 0, 0
	for i := 0; i < len(moves); i++ {
		cur := moves[i]

		player := i % 2
		if player == 0 {
			player = -1 //A
		}

		//for a row, A player +1, B player -1
		rows[cur[0]] = rows[cur[0]] + player
		cols[cur[1]] = cols[cur[1]] + player

		if cur[0] == cur[1] {
			diag += player
		}
		if cur[0]+cur[1] == 2 {
			adiag += player
		}

		if abs(diag) == 3 || abs(adiag) == 3 ||
			abs(rows[cur[0]]) == 3 || abs(cols[cur[1]]) == 3 {
			if player == -1 {
				return "A"
			} else {
				return "B"
			}
		}
	}

	if len(moves) == 9 {
		return "Draw"
	}
	return "Pending"
}
