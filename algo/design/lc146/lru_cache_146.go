package lc146

/*
https://leetcode.com/problems/lru-cache/

https://leetcode.com/discuss/interview-question/487498/Microsoft-or-Onsite-or-High-Concurrency-LRU-Cache
https://www.openmymind.net/High-Concurrency-LRU-Caching/
https://www.openmymind.net/Writing-An-LRU-Cache/
https://github.com/karlseguin/ccache

*/

type LRUCache struct {
}

func Constructor(capacity int) LRUCache {

	return LRUCache{}
}

func (lru *LRUCache) Get(key int) int {

	return -1
}

func (lru *LRUCache) Put(key int, value int) {

}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
