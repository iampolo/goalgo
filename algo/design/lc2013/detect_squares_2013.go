package lc2013

//https://leetcode.com/problems/detect-squares/

//https://leetcode.com/problems/detect-squares/discuss/1472558/Golang-map-solution

type DetectSquares struct {
	points map[int]map[int]int
}

func Constructor() DetectSquares {
	return DetectSquares{
		points: make(map[int]map[int]int),
	}
}

func (this *DetectSquares) Add(p []int) {
	this.points[p[0]][p[1]]++
}

func (ds *DetectSquares) Count(p []int) int {

	var cnt int
	//find the points share the same x-axis
	for y := range ds.points[p[0]] {
		if y == p[1] {
			continue
		}
		edge := p[1] - y //get the possible side width of a square: vertical
		if edge < 0 {
			edge = -edge
		}
		if p[0]+edge < 1001 {
			//expand to right for square
			nx := p[0] + edge //the horizontal width
			cnt += ds.points[p[0]][y] * ds.points[nx][y] * ds.points[nx][p[1]]
		}

		if p[0] >= edge {
			//expand to the left for square
			nx := p[0] - edge
			cnt += ds.points[p[0]][y] * ds.points[nx][y] * ds.points[nx][p[1]]
		}

		//dont' need to take care up-left or up-right, as we loop every points
	}
	return cnt
}

/**
 * Your DetectSquares object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Add(point);
 * param_2 := obj.Count(point);
 */
