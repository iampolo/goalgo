package lc2034

//https://leetcode.com/problems/stock-price-fluctuation/
//https://leetcode.com/problems/stock-price-fluctuation/discuss/1515381/Golang-solution

type StockPrice struct {
	prices     map[int]int //timestamp to price
	hist       []int       //ordered historical prices
	latestTime int
	latestPx   int
}

func Constructor() StockPrice {
	return StockPrice{
		prices: make(map[int]int),
		hist:   []int{},
	}
}

func (sp *StockPrice) Update(timestamp int, price int) {
	if timestamp >= sp.latestTime {
		sp.latestTime = timestamp
		sp.latestPx = price
	}

	if opx, exist := sp.prices[timestamp]; exist {
		l, r := 0, len(sp.hist)-1
		for l <= r {
			m := l + (r-l)/2
			if sp.hist[m] == opx {
				//move the right portion to overwrite the old price
				copy(sp.hist[m:], sp.hist[m+1:])
				sp.hist = sp.hist[:len(sp.hist)-1] //remove the last element
				break
			} else if sp.hist[m] < opx {
				l = m + 1
			} else {
				r = m - 1
			}
		} //
	}

	sp.prices[timestamp] = price

	//put all the prices in hist with order
	l, r := 0, len(sp.hist)-1
	for l <= r { //like java BS
		m := l + (r-l)/2
		if sp.hist[m] <= price {
			l = m + 1
		} else {
			r = m - 1
		}
	}
	sp.hist = append(sp.hist, 0)     //0 is a dummy & will be deleted
	copy(sp.hist[l+1:], sp.hist[l:]) //make a space for new new price
	sp.hist[l] = price               //put the new price in this sorted position
}

func (sp *StockPrice) Current() int {
	return sp.latestPx
}

func (sp *StockPrice) Maximum() int {
	return sp.hist[len(sp.hist)-1]
}

func (sp *StockPrice) Minimum() int {
	return sp.hist[0]
}

/**
 * Your StockPrice object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Update(timestamp,price);
 * param_2 := obj.Current();
 * param_3 := obj.Maximum();
 * param_4 := obj.Minimum();
 */
