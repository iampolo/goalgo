package lc588

import (
	"sort"
	"strings"
)

/**
https://leetcode.com/problems/design-in-memory-file-system/discuss/1187809/Go-beats-100-using-maps-and-FileNode-abstraction-Explanation

*/

type inode uint16 //

type FileSystem struct {
	root     *FileNode
	nodeData map[inode][]byte
	//the next available inode
	inodeCnt inode
}

type FileNode struct {
	children map[string]*FileNode
	inode    inode
	name     string
	isFile   bool
}

func Constructor() FileSystem {
	return FileSystem{
		root: &FileNode{
			name:     "/",
			isFile:   false,
			inode:    0,
			children: make(map[string]*FileNode),
		},
		nodeData: make(map[inode][]byte),
		inodeCnt: 1,
	}
}

func (fs *FileSystem) incrementInode() {
	fs.inodeCnt++
}

func NewFileNode(name string, fs *FileSystem) *FileNode {
	defer fs.incrementInode() //set next inode

	return &FileNode{
		name:     name,
		inode:    fs.inodeCnt,
		children: make(map[string]*FileNode),
		isFile:   false, //a directory by default
	}
}

func (fs *FileSystem) Ls(path string) []string {
	cur := fs.root

	//traverse to the last directory of hte path
	for _, dir := range splitPath(path) {
		cur, _ = cur.children[dir] //assuming the path is correct
	}
	if cur.isFile {
		return []string{cur.name}
	}
	var files []string
	for child := range cur.children {
		files = append(files, child)
	}
	sort.Strings(files)
	return files
}

func (fs *FileSystem) Mkdir(path string) {
	buildOrUpdateDirectory(path, fs)
}

func buildOrUpdateDirectory(path string, fs *FileSystem) *FileNode {
	cur := fs.root
	for _, dir := range splitPath(path) {
		child, ok := cur.children[dir]
		if !ok {
			child = NewFileNode(dir, fs)
			cur.children[dir] = child
		}
		cur = child
	}
	return cur
}

func splitPath(path string) []string {
	if path == "/" {
		return []string{}
	}
	return strings.Split(path, "/")[1:]
}

func (fs *FileSystem) AddContentToFile(filePath string, content string) {
	node := buildOrUpdateDirectory(filePath, fs)
	node.isFile = true
	fs.nodeData[node.inode] = append(fs.nodeData[node.inode], []byte(content)...)
}

func (fs *FileSystem) ReadContentFromFile(filePath string) string {
	node := buildOrUpdateDirectory(filePath, fs)
	return string(fs.nodeData[node.inode])
}

/**
 * Your FileSystem object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Ls(path);
 * obj.Mkdir(path);
 * obj.AddContentToFile(filePath,content);
 * param_4 := obj.ReadContentFromFile(filePath);
 */
