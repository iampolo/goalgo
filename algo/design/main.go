package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/design/lc2034"
	v2 "gitlab.com/iampolo/goalgo/algo/design/v2"
	v4 "gitlab.com/iampolo/goalgo/algo/design/v4"
	"log"
	"net/rpc"
)

func main() {

	//test_1268()
	//test_2034()
	test_1146()
}

func test_pgk() {
	xx := NumArray{}
	fmt.Println(xx)
}

func test_1146() {
	sa := v2.Constructor_1146(1)
	sa.Set(0, 15)
	fmt.Println(sa.Snap())
	fmt.Println(sa.Snap())
	fmt.Println(sa.Snap())
	fmt.Println(sa.Get(0, 2))
}

func test_710() {
	n := 10
	bl := []int{3, 9, 7, 6}
	c7 := v4.Constructor_710(n, bl)
	fmt.Println(c7.Pick())

}

func test_2034() {
	sp := lc2034.Constructor()
	sp.Update(1, 10)
	sp.Update(1, 5)
	sp.Update(1, 7)

}

func test_1275() {
	moves := [][]int{{0, 0}, {2, 0}, {1, 1}, {2, 1}, {2, 2}}
	moves = [][]int{{1, 0}, {0, 1}, {0, 0}, {2, 0}, {1, 1}, {2, 1}, {1, 2}}
	fmt.Println(tictactoe_v2(moves))
}

func test_677() {

}
func test_1268() {

	pr := []string{"mobile", "mouse", "moneypot", "monitor", "mousepad"}
	search := "mouse"

	fmt.Println(suggestedProducts(pr, search))
}

type Args struct{}

func testOnlY() {
	var reply int64
	args := Args{}

	client, err := rpc.DialHTTP("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing", err)
	}

	err = client.Call("", args, &reply)
	if err != nil {
		log.Fatal("", err)
	}
}
