package main

// https://leetcode.com/problems/map-sum-pairs/

type MapSum struct {
	val  int
	next []*MapSum
}

/** Initialize your data structure here. */
func Constructor() MapSum {
	return MapSum{
		next: make([]*MapSum, 26),
		val:  0,
	}
}

func (mp *MapSum) Insert(key string, val int) {

	for _, c := range key {
		if mp.next[c-'a'] == nil {
			mp.next[c-'a'] = &MapSum{
				next: make([]*MapSum, 26),
				val:  0,
			}
		}
		mp = mp.next[c-'a']
	}
	mp.val = val
}

func (mp *MapSum) Sum(prefix string) int {
	//go to the prefix point
	for _, c := range prefix {
		if mp.next[c-'a'] == nil {
			return 0
		}
		mp = mp.next[c-'a']
	}

	//dfs from this common point and dfs all the cases
	var res int
	var dfs func(mp *MapSum)
	dfs = func(mp *MapSum) {
		res += mp.val
		for _, c := range mp.next { //search from each next node :26 of'em
			if c != nil {
				dfs(c)
			}
		}
	}
	dfs(mp)

	return res
}
