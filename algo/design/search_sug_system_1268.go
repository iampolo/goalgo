package main

import (
	"sort"
)

//TODO
//https://leetcode.com/problems/search-suggestions-system/discuss/1243538/Trie-%2B-DFS-Go-solution

// https://leetcode.com/problems/search-suggestions-system/submissions/
func suggestedProducts(products []string, searchWord string) [][]string {
	//sort.Sort(sort.Reverse(sort.StringSlice(products)))

	sort.Strings(products)
	root := NewTrie()

	for _, p := range products {
		root.insert(p)
	}

	return root.search(searchWord)
}

func (t *Trie) search(word string) [][]string {
	var res [][]string

	for _, c := range word {
		if t != nil {
			t = t.next[c-'a']
		}
		if t == nil {
			res = append(res, []string{})
		} else {
			res = append(res, t.sug)
		}
	}
	return res
}

func (t *Trie) insert(p string) {
	for _, c := range p {
		if t.next[c-'a'] == nil {
			t.next[c-'a'] = NewTrie()
		}
		t = t.next[c-'a']
		if len(t.sug) < 3 {
			t.sug = append(t.sug, p)
		}
	}
}

type Trie struct {
	next []*Trie
	sug  []string
}

func NewTrie() *Trie {
	return &Trie{
		next: make([]*Trie, 26),
		sug:  []string{}, // slice
	}
}
