package main

/*
https://leetcode.com/problems/snapshot-array/discuss/557904/Go-85-binary-search
*/

type SnapshotArray struct {
	data []map[int]int
}

func Constructor_1146(length int) SnapshotArray {
	var d []map[int]int
	d = append(d, make(map[int]int))
	return SnapshotArray{
		data: d,
	}
}

func (s *SnapshotArray) Set(index int, val int) {
	//update the latest value
	cur := len(s.data) - 1
	s.data[cur][index] = val
}

func (s *SnapshotArray) Snap() int {
	s.data = append(s.data, make(map[int]int))
	return len(s.data) - 2
}

func (s *SnapshotArray) Get(index int, snap_id int) int {
	//we dont copy and save each snapshot value, only one copy of snapshot is saved.
	for i := snap_id; i >= 0; i-- {
		if _, ok := s.data[i][index]; ok {
			return s.data[i][index]
		}
	}
	return 0
}

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * obj := Constructor(length);
 * obj.Set(index,val);
 * param_2 := obj.Snap();
 * param_3 := obj.Get(index,snap_id);
 */
