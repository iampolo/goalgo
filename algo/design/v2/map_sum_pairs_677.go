package v2

import "strings"

// https://leetcode.com/problems/map-sum-pairs/

type MapSum struct {
	pairs map[string]int
}

/** Initialize your data structure here. */
func Constructor() MapSum {
	return MapSum{
		pairs: map[string]int{},
	}
}

func (mp *MapSum) Insert(key string, val int) {
	mp.pairs[key] = val
}

func (mp *MapSum) Sum(prefix string) int {
	//go to the prefix point
	var res int

	for s , v := range mp.pairs {
		if strings.HasPrefix(s, prefix) {
			res += v
		}
	}
	return res
}
