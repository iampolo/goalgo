package v2

import . "gitlab.com/iampolo/goalgo/algo/util"

type Solution struct {
	whiteLst map[int]int
	nonBlack int
}

//https://leetcode.com/problems/random-pick-with-blacklist/solution/
//https://leetcode.com/problems/random-pick-with-blacklist/discuss/144441/Java-Binary-Search-Solution-O(BlogB)-for-constructor-and-O(logB)-for-pick()

//- since we need to use random lib as less as possible, we can't do check and re-gen random
//
//- create a white list from the blacklist
//0..9
//blacklist:= [3,5,8,9]
//we can map 3->6, 5->7, blacks map to whites
//

func Constructor_710(n int, bl []int) Solution {
	//how many element are in the blacklist
	bmap := make(map[int]bool)
	for _, v := range bl {
		bmap[v] = true
	}

	//find range/the number of items, 0...nonBlack, that are not in the black list
	// n=7, bl:=[2,3,4]
	// 7 - 3 = 4
	//from 4 to 7 -> [5,6] are nonBlack
	nonBlack := n - len(bl)
	var wl []int
	for i := nonBlack; i < n; i++ {
		if _, exist := bmap[i]; !exist { //non including already known black items
			wl = append(wl, i)
		}
	}

	gMap := make(map[int]int)
	for _, b := range bl {
		//for those black items that are within the white list range, map them
		//to the white items, ie.[5,6]
		if b < nonBlack {
			g := wl[0]
			wl = wl[1:]
			gMap[b] = g //pick a value from gMap
		}
	} //

	return Solution{
		whiteLst: gMap,
		nonBlack: nonBlack,
	}
}

func (this *Solution) Pick() int {
	k := Random(this.nonBlack)
	if val, exist := this.whiteLst[k]; exist {
		return val
	}
	return k
}
