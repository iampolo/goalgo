package v2

type SnapshotArray struct {
	//each array element can have indefinitely amount of snapshots
	data    [][]Snap
	len     int
	curSnap int
}

// Snap has the snap id, and the value stored for this snap
type Snap struct {
	id  int
	val int
}

func Constructor_1146(length int) SnapshotArray {
	arr := SnapshotArray{}
	arr.len = length
	arr.data = make([][]Snap, length)
	arr.curSnap = 0
	return arr
}

func (this *SnapshotArray) Set(index int, val int) {
	all := this.data[index]
	cur := Snap{this.curSnap, val}
	if len(all) == 0 || all[len(all)-1].id != this.curSnap {
		//no snapshot for this curSnap Id yet, adding a new snapshot
		this.data[index] = append(this.data[index], cur)
	} else { //update
		this.data[index][len(all)-1] = cur
	}
}

func (this *SnapshotArray) Snap() int {
	this.curSnap++
	return this.curSnap - 1
}

func (this *SnapshotArray) Get(index int, snap_id int) int {
	arr := this.data[index]
	if len(arr) == 0 || snap_id < arr[0].id {
		return 0
	}

	//search the proper snapshot
	left, right := 0, len(arr)-1
	for left <= right {
		mi := left + (right - left) / 2
		if arr[mi].id == snap_id {
			return arr[mi].val
		} else if arr[mi].id < snap_id {
			left = mi + 1
		} else {
			right = mi - 1
		}
	}
	//left bound is the moving to right
	return arr[right].val
}
