package v3

import . "gitlab.com/iampolo/goalgo/algo/util"

type Solution struct {
	whiteLst map[int]int
	nonBlack int
}

//https://leetcode.com/problems/random-pick-with-blacklist/solution/
//https://leetcode.com/problems/random-pick-with-blacklist/discuss/144441/Java-Binary-Search-Solution-O(BlogB)-for-constructor-and-O(logB)-for-pick()

//- since we need to use random lib as less as possible, we can't do check and re-gen random
//
//- create a white list from the blacklist
//0..9
//blacklist:= [3,5,8,9]
//we can map 3->6, 5->7, blacks map to whites
//

func Constructor_710(n int, bl []int) Solution {
	//how many element are in the blacklist
	wmap := make(map[int]int)
	for _, v := range bl {
		wmap[v] = -1 //some are to be mapped to white number
	}

	nonBlack := n - len(bl)
	for _, b := range bl {
		if b < nonBlack {
			//find a nonblack and map to a black
			for ; n-1 >= 0; {
				if _, exist := wmap[n-1]; !exist {
					wmap[b] = n - 1
					n--
					break
				}
				n--
			} //for
		}
	}

	return Solution{
		whiteLst: wmap,
		nonBlack: nonBlack,
	}
}

func (this *Solution) Pick() int {
	k := Random(this.nonBlack)
	if val, exist := this.whiteLst[k]; exist {
		return val
	}
	return k
}
