package v4

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

type Solution struct {
	whiteLst []Interval
	whiteLen int
}

type Interval struct {
	lo, hi int
	cnt    int
}

//https://leetcode.com/problems/random-pick-with-blacklist/solution/

//Sort the blacklist then build the intervals list. e.g. N = 10, blacklist = [3, 8, 7, 6],
//we will have intervals: [0, 2], [4, 5], [9, 9]. Because of the sorting, time complexity is O(BlogB),
//where B is the length of blacklist.
//
//

//https://leetcode.com/problems/random-pick-with-blacklist/discuss/144441/Java-Binary-Search-Solution-O(BlogB)-for-constructor-and-O(logB)-for-pick()

func Constructor_710(n int, bl []int) Solution {
	sort.Ints(bl)

	var whiteLst []Interval
	pre, cnt := 0, 0
	for _, b := range bl {
		if pre != b {
			whiteLst = append(whiteLst, Interval{pre, b - 1, cnt})
			cnt += b - pre
		}
		pre = b + 1
	}

	whiteLst = append(whiteLst, Interval{pre, n - 1, cnt})

	return Solution{
		whiteLst: whiteLst,
		whiteLen: n - len(bl),
	}
}

func (this *Solution) Pick() int {
	rIdx := Random(this.whiteLen)
	l, r := 0, len(this.whiteLst)-1
	for l <= r {
		m := l + (r-l)/2
		cur := this.whiteLst[m]
		if cur.cnt <= rIdx && rIdx < cur.cnt+cur.hi-cur.lo+1 {
			return cur.lo + rIdx - cur.cnt
		} else if cur.cnt > rIdx {
			r = m - 1
		} else {
			l = m + 1
		}
	}
	return -1
}
