package main

//You are given an integer array cards of length 4. You have four cards, each containing a number in the range [1, 9].
//You should arrange the numbers on these cards in a mathematical expression using the operators ['+', '-', '*', '/']
//and the parentheses '(' and ')' to get the value 24.
//
//You are restricted with the following rules:
//
//- The division operator '/' represents real division, not integer division.
//   For example, 4 / (1 - 2 / 3) = 4 / (1 / 3) = 12.
//- Every operation done is between two numbers. In particular, we cannot use '-' as a unary operator.
//   For example, if cards = [1, 1, 1, 1], the expression "-1 - 1 - 1 - 1" is not allowed.
//- You cannot concatenate numbers together
//   For example, if cards = [1, 2, 1, 2], the expression "12 + 12" is not valid.
//
//Return true if you can get such expression that evaluates to 24, and false otherwise.
//
//
//Example 1:
//
//Input: cards = [4,1,8,7]
//Output: true
//Explanation: (8-4) * (7-1) = 24
//
//Example 2:
//
//Input: cards = [1,2,1,2]
//Output: false
//
//
//Constraints:
//
//cards.length == 4
//1 <= cards[i] <= 9

func judgePoint24(cards []int) bool {

	abs := func(a float32) float32 {
		if a < 0 {
			return -a
		}
		return a
	}
	compute := func(a, b float32) []float32 {
		return []float32{a + b, a - b, b - a, a * b, a / b, b / a}
	}
	var dfs func([]float32) bool
	dfs = func(nums []float32) bool {
		if len(nums) == 1 {
			return abs(nums[0]-24) < 1e-5
		}

		//pick two of the numbers each time, and the result of these two nums are combined with other two numbers
		//to do the same Dfs, but one number, at the end, there is only one number left.
		for i := 0; i < len(nums); i++ {
			for j := i + 1; j < len(nums); j++ {
				//select two nums i, j
				nLst := make([]float32, len(nums)-1)
				for k, l := 0, 0; k < len(nums); k++ {
					//add the remaining numbers that are not in i, j, to form a new/shorter list of nums
					if k != i && k != j {
						nLst[l] = nums[k]
						l++
					}
				}

				for _, num := range compute(nums[i], nums[j]) {
					//the calc of i, j are added to nLst for next step
					nLst[len(nums)-2] = num
					if dfs(nLst) {
						return true
					}
				}
			} //
		}

		return false
	}

	nCards := []float32{float32(cards[0]), float32(cards[1]), float32(cards[2]), float32(cards[3])}
	return dfs(nCards)
}
