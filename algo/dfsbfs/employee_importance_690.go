package main

//https://leetcode.com/problems/employee-importance/

/**
 * Definition for Employee.
 * type Employee struct {
 *     Id int
 *     Importance int
 *     Subordinates []int
 * }
 */

type Employee struct {
	Id           int
	Importance   int
	Subordinates []int
}

func getImportance(employees []*Employee, id int) int {
	all := make(map[int]*Employee)
	for _, e := range employees {
		all[e.Id] = e
	}

	return calcIm(all, id)
}
func calcIm(em map[int]*Employee, id int) int {
	val := em[id].Importance
	for _, nei := range em[id].Subordinates {
		val += calcIm(em, nei)
	}

	return val
}

func getImportance_v2(employees []*Employee, id int) int {
	emps := make(map[int]*Employee)

	var dfs func(emp map[int]*Employee, id int) int
	dfs = func(emp map[int]*Employee, id int) int {
		t := emp[id].Importance
		for _, nei := range emp[id].Subordinates {
			t += dfs(emp, nei)
		}
		return t
	}

	for _, e := range employees {
		emps[e.Id] = e
	}

	return dfs(emps, id)
}


func calcIm_bug(em map[int]*Employee, id int) int {
	var val int

	for _, nei := range em[id].Subordinates {
		//BUG
		val += em[id].Importance + calcIm_bug(em, nei)
	}

	return val
}
