package main

import (
	"strconv"
	"strings"
)

// https://leetcode.com/problems/expression-add-operators/

func addOperators(num string, target int) []string {

	var dfsAdd func(num string, idx int, curSum, preSum, target int, expr []string, resExpr *[]string)
	dfsAdd = func(num string, idx int, curSum, preSum, target int, expr []string, resExpr *[]string) {
		if idx == len(num) {
			if curSum == target {
				*resExpr = append(*resExpr, strings.Join(expr, ""))
			}
			return
		}

		for i := idx; i < len(num); i++ {
			if i != idx && num[idx] == '0' {
				break
			}
			cur, _ := strconv.Atoi(num[idx : i+1])

			if idx == 0 {
				dfsAdd(num, i+1, cur, 0, target, append(expr, strconv.Itoa(cur)), resExpr)
			} else {
				dfsAdd(num, i+1, curSum+cur, curSum, target, append(expr, "+"+strconv.Itoa(cur)), resExpr)
				dfsAdd(num, i+1, curSum-cur, curSum, target, append(expr, "-"+strconv.Itoa(cur)), resExpr)
				dfsAdd(num, i+1, (curSum-preSum)*cur+preSum, preSum, target, append(expr, "*"+strconv.Itoa(cur)), resExpr)
			}
		} //for
	}

	var res []string
	var tmp []string
	dfsAdd(num, 0, 0, 0, target, tmp, &res)
	return res
}
