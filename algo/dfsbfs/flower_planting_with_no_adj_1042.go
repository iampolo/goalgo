package main

//
//https://leetcode.com/problems/flower-planting-with-no-adjacent/

//You have n gardens, labeled from 1 to n, and an array paths where paths[i] = [xi, yi] describes a bidirectional
//path between garden xi to garden yi. In each garden, you want to plant one of 4 types of flowers.
//
//All gardens have at most 3 paths coming into or leaving it.
//
//Your task is to choose a flower type for each garden such that, for any two gardens connected by a path, they have
//different types of flowers.
//
//Return any such a choice as an array answer, where answer[i] is the type of flower planted in the (i+1)th garden.
//The flower types are denoted 1, 2, 3, or 4. It is guaranteed an answer exists.
//
//
//
//Example 1:
//
//Input: n = 3, paths = [[1,2],[2,3],[3,1]]
//Output: [1,2,3]
//Explanation:
//Gardens 1 and 2 have different types.
//Gardens 2 and 3 have different types.
//Gardens 3 and 1 have different types.
//Hence, [1,2,3] is a valid answer. Other valid answers include [1,2,4], [1,4,2], and [3,2,1].
//
//Example 2:
//
//Input: n = 4, paths = [[1,2],[3,4]]
//Output: [1,2,1,2]
//
//Example 3:
//
//Input: n = 4, paths = [[1,2],[2,3],[3,4],[4,1],[1,3],[2,4]]
//Output: [1,2,3,4]
//
//
//Constraints:
//
// - 1 <= n <= 10^4
// - 0 <= paths.length <= 2 * 10^4
// - paths[i].length == 2
// - 1 <= xi, yi <= n
// - xi != yi
// - Every garden has at most 3 paths coming into or leaving it.

func gardenNoAdj(n int, paths [][]int) []int {
	//4 flowers to n gardens
	grp := make(map[int][]int)

	for _, e := range paths {
		grp[e[0]] = append(grp[e[0]], e[1])
		grp[e[1]] = append(grp[e[1]], e[0])
	}

	res := make([]int, n) //0-index
	for i := range res {
		res[i] = -1 //not planted any flower yet
	}
	for i := 1; i <= n; i++ {
		used := make([]bool, 5) //nei planted flower already and which flower
		for _, nei := range grp[i] {
			if res[nei-1] != -1 { //if the nei. already planted flower
				used[res[nei-1]] = true //this res[nei] flower is used
			}
		}

		//select a flower for garden i
		for j := 1; j <= 4; j++ {
			//max # of nei. is 3, and there are 4 flowers, one flower must be available
			if !used[j] { //select any one of the flowers which is not used would be fine
				res[i-1] = j
				break
			}
		}
	}

	return res
}
