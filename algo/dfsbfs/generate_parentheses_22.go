package main

// https://leetcode.com/problems/generate-parentheses/discuss/877615/Golang-Recursive-solution-with-recursion-tree-diagram

func generateParenthesis(n int) []string {
	var s[] string
	gen(n, n, &s, "")
	return s
}

func gen(l, r int, res *[]string, cur string) {
	if l > r {
		return
	}
	if l == 0 && r == 0 {
		*res = append(*res, cur)
	}

	if l > 0 {
		gen(l-1, r, res, cur+"(")
	}
	if 0 < r {
		gen(l, r-1, res, cur+")")
	}
}
