package main

// https://leetcode.com/problems/island-perimeter/

func islandPerimeter_dfs(grid [][]int) int {

	vis := make([][]bool, len(grid))
	for i := range vis {
		vis[i] = make([]bool, len(grid[0]))
	}

	var dfs func(int, int) int
	dfs = func(r, c int) int {
		if r < 0 || r >= len(grid) || c < 0 || c >= len(grid[0]) {
			return 1
		}
		if grid[r][c] == 0 {
			return 1
		}
		if vis[r][c] {
			return 0
		}
		vis[r][c] = true
		var cnt int
		for i := 0; i < 4; i++ {
			cnt += dfs(r+dir[i], c+dir[i+1])
		}
		return cnt
	}

	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			if grid[i][j] == 1 {
				return dfs(i, j)
			}
		} //
	}
	return 0
}

func islandPerimeter(grid [][]int) int {

	u, d, l, r := 0, 0, 0, 0

	var res int
	//for each square with 1, count the # of neighbor which is also 1,
	//which ever side attached to another land will has no perimeter counted
	//4 - #attachedOtherland = perimeter of that square
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			if grid[i][j] == 0 {
				continue
			}

			if i == 0 {
				u = 0
			} else {
				u = grid[i-1][j]
			}
			if j == 0 {
				l = 0
			} else {
				l = grid[i][j-1]
			}
			if i == len(grid)-1 {
				d = 0
			} else {
				d = grid[i+1][j]
			}
			if j == len(grid[0])-1 {
				r = 0
			} else {
				r = grid[i][j+1]
			}
			res += 4 - (u + d + l + r)
		}
	}

	return res
}
