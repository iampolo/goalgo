package main

import (
	"fmt"
	dfsbfsv2 "gitlab.com/iampolo/goalgo/algo/dfsbfs/v2"
	"strings"
)

func main() {
	test_241()
}

func test_691() {
	stickers := []string{"with", "example", "science"}
	target := "thehat"
	fmt.Println(minStickers_v2(stickers, target))
}

func test_1042() {
	paths := [][]int{{1, 2}, {2, 3}, {3, 1}}
	n := 4

	fmt.Println(gardenNoAdj(n, paths))
}

func test_313() {
	n := 12
	pr := []int{2, 7, 13, 19}
	fmt.Println(nthSuperUglyNumber(n, pr))
}

func test_264() {
	n := 10
	fmt.Println(nthUglyNumber_v2(n))
}

func test_433() {
	start, end := "AACCGGTT", "AACCGGTA"
	bank := []string{"AACCGGTA"}
	fmt.Println(minMutation(start, end, bank))
}

func test_773() {
	board := [][]int{{}}
	fmt.Println(slidingPuzzle(board))
}

func test_996() {
	nums := []int{1, 17, 8}
	nums = []int{2, 2, 2}
	fmt.Println(numSquarefulPerms(nums))
}

func test_47() {
	nu := []int{1, 1, 2}
	nu = []int{1, 2, 3}
	fmt.Println(permuteUnique(nu))
}

func test_679() {
	nums := []int{4, 1, 8, 7}
	nums = []int{1, 2, 1, 2}
	nums = []int{3, 3, 8, 8}
	fmt.Println(judgePoint24(nums))
}

func test_689() {
	nums := []int{4, 3, 2, 3, 5, 2, 1}
	k := 4
	fmt.Println(canPartitionKSubsets(nums, k))
	fmt.Println(dfsbfsv2.CanPartitionKSubsets(nums, k))
}

func test_282() {
	num := "123"
	target := 6
	fmt.Println(addOperators(num, target))
}

func test() {
	nums := []int{1, 2, 2}
	fmt.Println(subsetsWithDup(nums))
}

func test_241() {
	str := "2*3-4*5"
	str = "7+3*1*2"
	str = "2-1-1+1"
	fmt.Println(strings.Index("_+*", "."))
	fmt.Println(diffWaysToCompute(str))
}
