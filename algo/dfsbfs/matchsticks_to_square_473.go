package main

import "sort"

func makesquare(matchsticks []int) bool {
	total := 0
	for _, v := range matchsticks {
		total += v
	}

	if total%4 != 0 {
		return false
	}

	/**
		sort to try to larger first, and find those invalid cases quicker
	 */
	sort.Ints(matchsticks)
	//sort.Sort(sort.Reverse(sort.IntSlice(matchsticks)))
	edges := [4]int{}
	return findMatch(matchsticks, len(matchsticks)-1, edges, total/4)
}

func findMatch(nums []int, idx int, sums [4]int, side int) bool {

	if idx < 0 { //reach -1 means all done
		return sums[0] == sums[1] && sums[1] == sums[2] && sums[2] == sums[3]
	}

	cur := nums[idx]

	for i := 0; i < 4; i++ {
		if sums[i]+cur <= side {
			sums[i] += cur
			if findMatch(nums, idx-1, sums, side) {
				return true
			}

			sums[i] -= cur
		}
	}
	return false
}
