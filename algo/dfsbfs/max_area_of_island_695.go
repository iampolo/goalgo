package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/max-area-of-island/

//You are given an m x n binary matrix grid. An island is a group of 1's (representing land) connected 4-directionally
//(horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.
//
//The area of an island is the number of cells with a value 1 in the island.
//
//Return the maximum area of an island in grid. If there is no island, return 0.
//
//
//
//Example 1:
//
//
//Input: grid = [
//[0,0,1,0,0,0,0,1,0,0,0,0,0],
//[0,0,0,0,0,0,0,1,1,1,0,0,0],
//[0,1,1,0,1,0,0,0,0,0,0,0,0],
//[0,1,0,0,1,1,0,0,1,0,1,0,0],
//[0,1,0,0,1,1,0,0,1,1,1,0,0],
//[0,0,0,0,0,0,0,0,0,0,1,0,0],
//[0,0,0,0,0,0,0,1,1,1,0,0,0],
//[0,0,0,0,0,0,0,1,1,0,0,0,0]]
//Output: 6

//Explanation: The answer is not 11, because the island must be connected 4-directionally.
//
//Example 2:
//
//Input: grid = [[0,0,0,0,0,0,0,0]]
//Output: 0
//
//
//Constraints:
//
//m == grid.length
//n == grid[i].length
//1 <= m, n <= 50
//grid[i][j] is either 0 or 1.
//

func maxAreaOfIsland(grid [][]int) int {

	vis := make([][]bool, len(grid)) //row
	for i := range vis {
		vis[i] = make([]bool, len(grid[0])) //col
	}

	maxArea := 0
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			if grid[i][j] == 1 {
				res := dfs(i, j, grid, vis)
				maxArea = Max(maxArea, res)
			}
		} //

	} //
	return maxArea
}

//https://qvault.io/golang/golang-constant-maps-slices/
var dir = [...]int{0, -1, 0, 1, 0} //fixed sized array instead of slice

func dfs(x, y int, grid [][]int, vis [][]bool) int {
	if x < 0 || x >= len(grid) || y < 0 || y >= len(grid[0]) || grid[x][y] != 1 || vis[x][y] {
		return 0
	}

	vis[x][y] = true
	res := 1
	for i := 0; i < 4; i++ {
		nx := x + dir[i]
		ny := y + dir[i+1]
		res += dfs(nx, ny, grid, vis)
	}

	return res
}
