package main

import "math"

// https://leetcode.com/problems/number-of-squareful-arrays/
//
// An array is squareful if the sum of every pair of adjacent elements is a perfect square.
//
// Given an integer array nums, return the number of permutations of nums that are squareful.
//
// Two permutations perm1 and perm2 are different if there is some index i such that perm1[i] != perm2[i].
//
//
// Example 1:
//
// Input: nums = [1,17,8]
// Output: 2
// Explanation: [1,8,17] and [17,8,1] are the valid permutations.
//
// Example 2:
//
// Input: nums = [2,2,2]
// Output: 1
//
// Constraints:
//
// 1 <= nums.length <= 12
// 0 <= nums[i] <= 10^9
//
func numSquarefulPerms(nums []int) int {
	//don't need to sort the nums
	var res int
	countSquareful(nums, 0, &res)
	return res
}

func countSquareful(nums []int, idx int, res *int) {
	//each level, the idx is fixed, and it swap with i which is changing from 0,1,2, ... len(nums)
	if idx == len(nums) {
		*res++
		return
	}
	isSquare := func(n int) bool {
		r := int(math.Sqrt(float64(n)))
		return r*r == n
	}

	//control the uniqueness of this level is enough
	vis := make(map[int]bool)
	for i := idx; i < len(nums); i++ {
		if _, exist := vis[nums[i]]; !exist {
			vis[nums[i]] = true //de-dup
			nums[i], nums[idx] = nums[idx], nums[i]
			// idx == 0 allows the combination to grow  TODO
			if idx == 0 || idx > 0 && isSquare(nums[idx]+nums[idx-1]) {
				countSquareful(nums, idx+1, res)
			}
			nums[i], nums[idx] = nums[idx], nums[i]
		}
	} //for
}

// can also be done by sort the array first:
// https://leetcode.com/problems/number-of-squareful-arrays/discuss/238612/4msC%2B%2B-Simple-Backtracking-like-Permutations-II

func numSquarefulPerms_map_map(nums []int) int {
//	https://leetcode.com/problems/number-of-squareful-arrays/discuss/238562/C%2B%2BPython-Backtracking
	return 0
}
