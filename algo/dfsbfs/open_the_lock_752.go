package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
	"gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/open-the-lock/

func openLock(deadends []string, target string) int {
	set := util.NewSet()
	for _, s := range deadends {
		set.Add(s)
	}

	queue := Queue{}
	queue.Offer("0000")
	vis := util.NewSet()
	vis.Add("0000")

	steps := 0

	for len(queue) > 0 {
		size := queue.Len()

		for size > 0 {
			size--

			cur := queue.Poll().(string)

			if set.Contains(cur) {
				continue
			}
			if cur == target {
				return steps
			}

			for i := 0; i < 4; i++ {
				newCh := ""
				if cur[i] == '9' {
					newCh = "0"
				} else {
					newCh = string(cur[i] - '0' + 1 + '0')
				}
				s1 := cur[0:i] + newCh + cur[i+1:]
				if !set.Contains(s1) && !vis.Contains(s1) {
					vis.Add(s1)
					queue.Offer(s1)
				}

				newCh = ""
				if cur[i] == '0' {
					newCh = "9"
				} else {
					newCh = string(cur[i] - '0' - 1 + '0')
				}
				s2 := cur[0:i] + newCh + cur[i+1:]
				if !set.Contains(s2) && !vis.Contains(s2) {
					vis.Add(s2)
					queue.Offer(s2)
				}
			} //
		}
		steps++
	}

	return -1
}

func main_() {

	deadends := []string{"0201", "0101", "0102", "1212", "2002"}
	target := "2020"

	deadends = []string{"0000"}
	target = "8888"

	fmt.Println(openLock(deadends, target))
}
