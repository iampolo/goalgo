package main

// https://leetcode.com/problems/partition-to-k-equal-sum-subsets/
func canPartitionKSubsets(nums []int, k int) bool {
	var sum int
	for _, n := range nums {
		sum += n
	}

	if sum%k != 0 {
		return false
	}
	target := sum / k
	return checkSubset(nums, 0, 0, target, k, make([]bool, len(nums)))
}

func checkSubset(nums []int, idx int, curSum int, target int, k int, used []bool) bool {
	if k == 0 {
		//divided to k groups
		return true
	}
	if curSum == target {
		//found one group, reset variables and do more till k is reached
		//but previous used are saved
		return checkSubset(nums, 0, 0, target, k-1, used)
	}
	for i := idx; i < len(nums); i++ {
		if used[i] || curSum+nums[i] > target {
			continue
		}
		used[i] = true
		//next i !!
		//idx + 1 is used, there are a lots of repetitions, as i moves, but idx is always the same
		if checkSubset(nums, i+1, curSum+nums[i], target, k, used) {
			return true
		}
		used[i] = false
	}

	return false
}
