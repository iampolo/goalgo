package main

// https://leetcode.com/problems/permutations-ii/submissions/

func permuteUnique(nums []int) [][]int {
	var res [][]int
	permuate(nums, 0, &res)
	return res
}

func permuate(nums []int, idx int, res *[][]int) {
	//each level, the idx is fixed, and it swap with i which is changing from 0,1,2, ... len(nums)
	if idx == len(nums) {
		tmp := make([]int, len(nums))
		copy(tmp, nums)
		*res = append(*res, tmp) //append the entire row
		return
	}

	//control the uniqueness of this level is enough
	vis := make(map[int]bool)
	for i := idx; i < len(nums); i++ {
		if _, exist := vis[nums[i]]; !exist {
			vis[nums[i]] = true
			nums[i], nums[idx] = nums[idx], nums[i]
			permuate(nums, idx+1, res)
			nums[i], nums[idx] = nums[idx], nums[i]
		}
	} //for
}
