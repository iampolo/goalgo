package main

import "fmt"

//https://leetcode.com/problems/sliding-puzzle
//hints: each swap involves '0'
//
func slidingPuzzle(board [][]int) int {

	dir := make([][]int, 3)
	dir[0] = make([]int, 3)
	dir[1] = []int{0, 4}
	dir[2] = []int{1, 3, 5}

	fmt.Println(dir)
	return 0
}
