package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/stickers-to-spell-word/
/*
We are given n different types of stickers. Each sticker has a lowercase English word on it.

You would like to spell out the given string target by cutting individual letters from your collection of stickers
and rearranging them. You can use each sticker more than once if you want, and you have infinite quantities of each sticker.

Return the minimum number of stickers that you need to spell out target. If the task is impossible, return -1.

Note: In all test cases, all words were chosen randomly from the 1000 most common US English words, and target was
chosen as a concatenation of two random words.


Example 1:

Input: stickers = ["with","example","science"], target = "thehat"
Output: 3
Explanation:
We can use 2 "with" stickers, and 1 "example" sticker.
After cutting and rearrange the letters of those stickers, we can form the target "thehat".
Also, this is the minimum number of stickers necessary to form the target string.

Example 2:

Input: stickers = ["notice","possible"], target = "basicbasic"
Output: -1
Explanation:
We cannot form the target "basicbasic" from cutting letters from the given stickers.


Constraints:

n == stickers.length
1 <= n <= 50
1 <= stickers[i].length <= 10
1 <= target <= 15
stickers[i] and target consist of lowercase English letters.

*/

/*
https://leetcode.com/problems/stickers-to-spell-word/discuss/108318/C%2B%2BJavaPython-DP-%2B-Memoization-with-optimization-29-ms-(C%2B%2B)
 */
func minStickers(stickers []string, target string) int {

	freq := make([][]int, len(stickers))
	for i := range freq {
		freq[i] = make([]int, 26)
	}

	//we need to match target against all the stickers
	//preprocessing sticker before searching
	for i, s := range stickers {
		for _, c := range s {
			freq[i][c-'a']++
		}
	}

	//all tried string and their matching status
	dp := make(map[string]int)
	dp[""] = 0 //an empty target needs 0 stickers

	return search(freq, dp, target)
}

func search(freq [][]int, dp map[string]int, target string) int {
	if _, ok := dp[target]; ok {
		return dp[target]
	}

	ans := math.MaxInt32
	tarCnt := make([]int, 26)
	for _, c := range target {
		tarCnt[c-'a']++
	}

	for i := range freq { //O(len(sticker) ^ len(target))
		//tuning
		if freq[i][target[0]-'a'] == 0 {
			continue
		}

		//yet to match chars
		var next []byte
		for j := 0; j < 26; j++ {
			if tarCnt[j] == 0 {
				continue
			}

			//do the match, and record the un-matched
			for k := 0; k < Max(0, tarCnt[j]-freq[i][j]); k++ {
				next = append(next, byte(j+'a'))
			}
		}
		//target is smaller, find its match
		ret := search(freq, dp, string(next))
		if ret != -1 {
			ans = Min(ans, 1+ret)
		}

		//tuning
		if next == nil {
			break
		}
	} //for
	dp[target] = -1
	if ans != math.MaxInt32 {
		dp[target] = ans
	}
	return dp[target]
}

/*
https://leetcode.com/problems/stickers-to-spell-word/discuss/108333/Rewrite-of-contest-winner's-solution

Subset of the target string : 2^(len(target)) - 1
 ab => a, b, ab
*/
func minStickers_v2(stickers []string, target string) int {

	cnt := 1 << len(target)
	dp := make([]int, cnt)
	for i := range dp {
		dp[i] = math.MaxInt32
	}
	dp[0] = 0

	//cnt=3 000...111
	//we want to eventually match all the chars in target, ie: cnt == 111
	//we do it gradually from 000, --> 001 -- > each step will fill in the dp more
	//e.g  from 000, may become 011 after fill with stickers
	//   dp[011] => 1+dp[000]
	for i := 0; i < cnt; i++ {
		//fmt.Printf("%08b \n", i)
		if dp[i] == math.MaxInt32 {
			continue
		}

		for _, s := range stickers {
			sup := i
			for _, c := range s {
				for r, tc := range target {
					// a char from a sticker matches a char in target
					if c == tc && ((sup>>r)&1) == 0 {
						sup |= 1 << r //missing, fill in the matched char
						break         //the char in this sticker is used
					}
				}
			} //for
			dp[sup] = Min(dp[sup], 1+dp[i])
		} //for
	} //for

	if dp[cnt-1] != math.MaxInt32 {
		return dp[cnt-1]
	}
	return -1
}
