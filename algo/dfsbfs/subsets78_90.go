package main

import (
	"sort"
	"strconv"
	"strings"
)

//https://leetcode.com/problems/subsets-ii/discuss/1195004/Golang-bitmap-solution
func subsetsWithDup_v2(nums []int) [][]int {
	return nil
}

func toString(nums []int) string {
	s := make([]string, len(nums))
	for i, v := range nums {
		s[i] = strconv.Itoa(v)
	}

	return strings.Join(s, ",")
}
//*******************************************************************

//https://leetcode.com/problems/subsets-ii/
func subsetsWithDup(nums []int) [][]int {
	res := [][]int{}
	var cur []int

	sort.Ints(nums)
	subset(nums, 0, cur, &res)

	return res
}

func subset(nums []int, idx int, cur []int, res *[][]int) {
	newRes := make([]int, len(cur))
	copy(newRes, cur)
	*res = append(*res, newRes)

	for i := idx; i < len(nums); i++ {
		if i != idx && nums[i-1] == nums[i] {
			continue
		}

		cur = append(cur, nums[i])
		subset(nums, i+1, cur, res)

		cur = cur[:len(cur)-1] //backtracking
	}

}

//https://leetcode.com/problems/subsets/
