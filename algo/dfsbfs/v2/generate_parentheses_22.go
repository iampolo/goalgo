package dfsbfsv2

import "fmt"

// https://leetcode.com/problems/generate-parentheses/discuss/877615/Golang-Recursive-solution-with-recursion-tree-diagram

/**
	https://yourbasic.org/golang/convert-string-to-byte-slice/
 */
func generateParenthesis(n int) []string {
	res := make([]string, 0)
	gen(0, 0, n, &res, []byte{})
	return res
}

func gen(l, r, n int, res *[]string, cur []byte) {
	if n*2 == len(cur) {
		*res = append(*res, string(cur))
		return
	}

	if l < n {
		gen(l+1, r, n, res, append(cur, '('))
	}
	if l > r {
		gen(l, r+1, n, res, append(cur, ')'))
	}
}

func main_xx() {
	fmt.Println(generateParenthesis(3))
}
