package dfsbfsv2

// https://leetcode.com/problems/partition-to-k-equal-sum-subsets/
//
// Java version
// sort the nums and try from the biggest num will be much faster
func CanPartitionKSubsets(nums []int, k int) bool {
	var sum int
	for _, n := range nums {
		sum += n
	}

	if sum%k != 0 {
		return false
	}
	target := sum / k
	return checkSubset(nums, 0, target, make([]int, k))
}

func checkSubset(nums []int, idx int, target int, grp []int) bool {
	if idx == len(nums) {
		//check group values-- no necessary.
		for _, val := range grp {
			if val != target {
				return false
			}
		}
		return true //success
	}
	//get the current num and try to put into a suitable group
	cur := nums[idx]
	for i := 0; i < len(grp); i++ {
		if grp[i]+cur <= target {
			grp[i] += cur
			if checkSubset(nums, idx+1, target, grp) {
				return true
			}
			grp[i] -= cur
		}
	}
	return false
}
