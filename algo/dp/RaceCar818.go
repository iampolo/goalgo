package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
	"strconv"
	"strings"
)

/*
https://leetcode.com/problems/race-car/

Your car starts at position 0 and speed +1 on an infinite number line. Your car can go into negative positions.
Your car drives automatically according to a sequence of instructions 'A' (accelerate) and 'R' (reverse):

When you get an instruction 'A', your car does the following:
position += speed
speed *= 2
When you get an instruction 'R', your car does the following:
If your speed is positive then speed = -1
otherwise speed = 1
Your position stays the same.
For example, after commands "AAR", your car goes to positions 0 --> 1 --> 3 --> 3,
and your speed goes to 1 --> 2 --> 4 --> -1.

Given a target position target, return the length of the shortest sequence of instructions to get there.



Example 1:

Input: target = 3
Output: 2
Explanation:
The shortest instruction sequence is "AA".
Your position goes from 0 --> 1 --> 3.
Example 2:

Input: target = 6
Output: 5
Explanation:
The shortest instruction sequence is "AAARA".
Your position goes from 0 --> 1 --> 3 --> 7 --> 7 --> 6.


Constraints:

* 1 <= target <= 10^4
*/

type pair struct {
	pos   int
	speed int
}

func racecar_queue(target int) int {
	var queue []pair
	vis := make(map[string]Void)
	queue = append(queue, pair{0, 1})
	k := strings.Join([]string{strconv.Itoa(0), strconv.Itoa(1)}, "-")
	vis[k] = Empty

	var steps int
	bound := target << 1
	/*
		 	O(t * log(t)) : log(t) since we double speed for each A
			[-t, t] all position needs to be visited in worst case
	*/
	for len(queue) > 0 {
		for i := len(queue); i > 0; i-- {
			cur := queue[0]
			queue = queue[1:]

			if cur.pos == target {
				return steps
			}

			//move with A
			np, ns := cur.pos+cur.speed, cur.speed<<1
			k := strings.Join([]string{strconv.Itoa(np), strconv.Itoa(ns)}, "-")
			_, ok := vis[k]
			if 0 < np && np < bound && !ok {
				queue = append(queue, pair{np, ns})
			}
			//move with R
			np, ns = cur.pos, 1
			if cur.speed > 0 {
				ns = -1
			}
			k = strings.Join([]string{strconv.Itoa(np), strconv.Itoa(ns)}, "-")
			_, ok = vis[k]
			if 0 < np && np < bound && !ok {
				queue = append(queue, pair{np, ns})
			}
		}
		steps++
	}
	return steps
}

/*
https://leetcode.com/problems/race-car/discuss/124326/Summary-of-the-BFS-and-DP-solutions-with-intuitive-explanation
*/
func racecar_top_down(target int) int {
	dp := make([]int, target+1)
	for i := 1; i < len(dp); i++ { //dp[0] = 0
		dp[i] = -1
	}

	var dfs func(t int) int
	dfs = func(t int) int {

		if dp[t] >= 0 {
			return dp[t]
		}

		dp[t] = math.MaxInt32
		m, j := 1, 1

		for ; j < t; j = (1 << m) - 1 {
			for p, q := 0, 0; p < j; p = (1 << q) - 1 {
				dp[t] = Min(dp[t], m+1+q+1+dfs(t-(j-p)))
				q++
			}
			m++
		}
		//reach on t
		cur := m //taking m 'A'
		if j > t {
			//walked beyond t
			cur += 1 + dfs(j-t) //take too many 'A' and needs a 'R'
		}
		dp[t] = Min(dp[t], cur)
		return dp[t]
	}

	return dfs(target)
}

/*
	O(t * (log(t))^2)
	O(t)
*/
func racecar_bottom_up(target int) int {

	dp := make([]int, target+1)

	//build up dp array for target from smallest distance
	for i := 1; i <= target; i++ {
		dp[i] = math.MaxInt32 //to be tuned below

		//moving with some 'A' (m) first and reach to position j
		//j could be before i, equals to i, or after i
		m, j := 1, 1

		//for loop: initial value will be processed in the first loop
		for ; j < i; j = (1 << m) - 1 {
			//if j is before i, we need 2 'R's to get back on track
			//q=# of 'A', p=position
			for p, q := 0, 0; p < j; p = (1 << q) - 1 {
				//build dp[i]
				dp[i] = Min(dp[i], m+1+q+1+dp[i-(j-p)])
				q++
			}
			m++ //2nd 'A'
		}
		//# of steps used
		cur := m
		if j > i {
			cur += 1 + dp[j-i]
		}
		dp[i] = Min(dp[i], cur)
	}
	return dp[target]
}
