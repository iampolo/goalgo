package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
	"sort"
)

// https://leetcode.com/problems/allocate-mailboxes/
//  Constraints:
//
//  - n == houses.length
//  - 1 <= n <= 100
//  - 1 <= houses[i] <= 10^4
//  - 1 <= k <= n
//  - Array houses contain unique integers.
//
//BestMeetingPoint296
//
//  two [1, 4]
//  [1,2] mailboxes to be a valid question
//
/*
http://leetcode.jp/leetcode-1478-allocate-mailboxes-%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/
 */

func minDistance(houses []int, k int) int {
	sort.Ints(houses)
	dp := make([][]int, len(houses))
	for i := range dp {
		dp[i] = make([]int, k)
		for j := 0; j < k; j++ {
			dp[i][j] = -1
		}
	}
	return solve(houses, 0, 0, k, dp)
}

func computeCost(houses []int) [][]int {
	costs := make([][]int, len(houses))
	for i := range costs {
		costs[i] = make([]int, len(houses))
	}

	//costs[i][j]
	//the costs if place a mailbox between i ... j
	for i := 0; i < len(houses); i++ {
		for j := 0; j < len(houses); j++ {
			median := houses[(i+j)/2]
			sum := 0
			for l := i; l <= j; l++ {
				sum += Abs(median - houses[l])
			}
			costs[i][j] = sum
		}
	}
	return costs
}

/**
1	4	7	10	20
i
	i+1

*/
// O(k * n!) ?
func solve(houses []int, hi, ki, k int, dp [][]int) int {
	if hi == len(houses) {
		if ki == k {
			return 0 //placed all the mailboxes successfully
		}
		return math.MaxInt32 //not this path
	}
	if ki == k {
		return math.MaxInt32 //running out of mailboxes, there should be better way: not this path
	}
	if dp[hi][ki] != -1 {
		return dp[hi][ki] //tried before
	}
	minCost := math.MaxInt32
	//try to place mailboxes between houses: try all the cases, and then find the minimum
	for i := hi; i < len(houses); i++ {
		median := houses[(i+hi)/2] //from hi ... i, place a box at the median position, and calc. the total cost.

		//cost needed to place a mailbox at median between houses hi ... i
		var cost int
		for j := hi; j <= i; j++ {
			cost += Abs(median - houses[j])
		}
		//recursive try all the remaining houses, if this median is taken
		minCost = Min(minCost, cost+solve(houses, i+1, ki+1, k, dp))

	}
	dp[hi][ki] = minCost
	return minCost
}

// TODO
// https://www.youtube.com/watch?v=iS6mGjHvH0o&t=858s&ab_channel=HuifengGuan
//
func minDistance_todo(houses []int, k int) int {
	//minimum total distance
	//trial and error? no
	//dp - find groups

	sort.Ints(houses)

	presum := make([]int, len(houses)+1)
	for i := 0; i < len(houses); i++ {
		presum[i+1] = presum[i] + houses[i]
	}

	for i := 1; i <= len(houses); i++ {
		for j := i; j <= len(houses); j++ {

			sum := 0
			for k = i; k <= j; k++ {
				sum += houses[k] - houses[(i+j)/2]
			}
			fmt.Print(sum, " ")
		} ///
	}

	return 0
}
