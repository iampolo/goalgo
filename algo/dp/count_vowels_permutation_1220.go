package main

/**
https://leetcode.com/problems/count-vowels-permutation/


Given an integer n, your task is to count how many strings of length n can be formed
under the following rules:

Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
Each vowel 'a' may only be followed by an 'e'.
Each vowel 'e' may only be followed by an 'a' or an 'i'.
Each vowel 'i' may not be followed by another 'i'.
Each vowel 'o' may only be followed by an 'i' or a 'u'.
Each vowel 'u' may only be followed by an 'a'.
Since the answer may be too large, return it modulo 10^9 + 7.


Example 1:

Input: n = 1
Output: 5
Explanation: All possible strings are: "a", "e", "i" , "o" and "u".

Example 2:

Input: n = 2
Output: 10
Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io", "iu", "oi", "ou" and "ua".

Example 3:

Input: n = 5
Output: 68


Constraints:

1 <= n <= 2 * 10^4


@see knight_dialer_935.go
*/
func countVowelPermutation(n int) int {
	//DP : build from the minimum count: 1 -> a vowel itself.
	aCnt, eCnt, iCnt, oCnt, uCnt := make([]int, n), make([]int, n), make([]int, n), make([]int, n), make([]int, n)
	// base case where the solution is built from.
	aCnt[0], eCnt[0], iCnt[0], oCnt[0], uCnt[0] = 1, 1, 1, 1, 1

	// From a vowel, develop the next count.
	for i := 1; i < n; i++ {
		aCnt[i] += (eCnt[i-1] + iCnt[i-1] + uCnt[i-1]) % MOD
		eCnt[i] += (aCnt[i-1] + iCnt[i-1]) % MOD
		iCnt[i] += (eCnt[i-1] + oCnt[i-1]) % MOD
		oCnt[i] += (iCnt[i-1]) % MOD
		uCnt[i] += (iCnt[i-1] + oCnt[i-1]) % MOD
	}

	n--
	res := (aCnt[n] + eCnt[n] + iCnt[n] + oCnt[n] + uCnt[n]) % MOD
	return res
}


func countVowelPermutation_recur(n int) int {
	return -1

}
func countVowelPermutation_v2(n int) int {
	aCnt, eCnt, iCnt, oCnt, uCnt := 1, 1, 1, 1, 1

	for i := 1; i < n; i++ {
		aCur := (eCnt + eCnt + uCnt) % MOD
		eCur := (aCnt + iCnt) % MOD
		iCur := (eCnt + oCnt) % MOD
		oCur := (iCnt) % MOD
		uCur := (iCnt + oCnt) % MOD
		//set the new to the base variables
		aCnt, eCnt, iCnt, oCnt, uCnt = aCur, eCur, iCur, oCur, uCur
	}
	return (aCnt + eCnt + iCnt + oCnt + uCnt) % MOD
}
