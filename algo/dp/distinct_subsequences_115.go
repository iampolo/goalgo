package main

// https://leetcode.com/problems/distinct-subsequences/

// Given two strings s and t, return the number of distinct subsequences of s which equals t.
//
//A string's subsequence is a new string formed from the original string by deleting some (can be none) of
//the characters without disturbing the remaining characters' relative positions.
//(i.e., "ACE" is a subsequence of "ABCDE" while "AEC" is not).
//
//It is guaranteed the answer fits on a 32-bit signed integer.
//

//
//Example 1:
//
//Input: s = "rabbbit", t = "rabbit"
//Output: 3
//Explanation:
//As shown below, there are 3 ways you can generate "rabbit" from S.
//rabbbit
//rabbbit
//rabbbit

//Example 2:
//
//Input: s = "babgbag", t = "bag"
//Output: 5
//Explanation:
//As shown below, there are 5 ways you can generate "bag" from S.
//babgbag
//babgbag
//babgbag
//babgbag
//babgbag
//

//Constraints:
//
// 1 <= s.length, t.length <= 1000
// s and t consist of English letters.
// longest_uncommon_subsequence_ii_522.go
//
// LongestCommonSubsequence1143
type Keys struct {
	i int
	j int
}

func numDistinct(s string, t string) int {
	memo := make(map[Keys]int)
	return recursion(s, t, 0, 0, memo)
}

// O(len(s) * len(t)
func recursion(s, t string, i, j int, memo map[Keys]int) int {
	//len(s)-i < len(t)-j, there is likely no match, prune the recursion.
	if i == len(s) || j == len(t) || len(s)-i < len(t)-j {
		//since reached the end of t and t can't be empty, if j has moved,there was a match, then we could have 1; otherwise, 0
		if j == len(t) {
			return 1
		}
		return 0
	}

	key := Keys{i: i, j: j}
	if _, exist := memo[key]; exist {
		return memo[key]
	}

	//in this level, start from current i, j, see how many matches we can find
	//i moves, there is either a match or not
	res := recursion(s, t, i+1, j, memo)
	if s[i] == t[j] {
		//j moves only when there is a match
		res += recursion(s, t, i+1, j+1, memo)
	}

	memo[key] = res
	return res
}

//****************************************************************************************
func numDistinct_v2(s string, t string) int {
	//@see java version
	return -1
}

//****************************************************************************************
// https://leetcode.com/problems/distinct-subsequences/discuss/37327/Easy-to-understand-DP-in-Java
// in tabular form.
// row is the string S, col is the string T
//                    r  a  b  b  i  t  ""
//          r      r [0, 0, 0, 0, 0, 0, 1]
//          aa     a [0, 0, 0, 0, 0, 0, 1]
//          bbb    b [0, 0, 0, 0, 0, 0, 1]
//          bbbb   b [0, 0, 0, 0, 0, 0, 1]
//          bbbbb  b [0, 0, 0, 0, 0, 0, 1]
//          iiiiii i [0, 0, 0, 0, 0, 0, 1]
//          tttttttt [0, 0, 0, 0, 0, 1, 1]
//                 ""[0, 0, 0, 0, 0, 0, 1]
//
// TODO
func numDistinct_v1_1(s string, t string) int {
	dp := make([]int, len(t)+1)
	dp[len(dp)-1] = 1

	for i := len(s) - 1; i >= 0; i-- {
		prev := 1 //empty string is a match
		for j := len(t) - 1; j >= 0; j-- {
			saved := dp[j] //dp[] is the row in previous version
			if s[i] == t[j] {
				dp[j] += prev
			}
			prev = saved
		} //
	}

	return dp[0]
}

func numDistinct_v1(s string, t string) int {

	dp := make([][]int, len(s)+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(t)+1)
		dp[i][len(t)] = 1
	}

	for i := len(s) - 1; i >= 0; i-- {
		for j := len(t) - 1; j >= 0; j-- {
			dp[i][j] = dp[i+1][j]
			if s[i] == t[j] {
				dp[i][j] += dp[i+1][j+1]
			}
		} //
	}

	return dp[0][0]
}
