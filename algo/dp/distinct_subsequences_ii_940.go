package main

// https://leetcode.com/problems/distinct-subsequences-ii/
//
// Given a string s, return the number of distinct non-empty subsequences of s. Since the answer may be very large,
// return it modulo 109 + 7.
//
// A subsequence of a string is a new string that is formed from the original string by deleting some (can be none)
// of the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a
// subsequence of "abcde" while "aec" is not.
//
//
// Example 1:
//
// Input: s = "abc"
// Output: 7
// Explanation: The 7 distinct subsequences are "a", "b", "c", "ab", "ac", "bc", and "abc".
//
// Example 2:
//
// Input: s = "aba"
// Output: 6
// Explanation: The 6 distinct subsequences are "a", "b", "ab", "aa", "ba", and "aba".
//
// Example 3:
//
// Input: s = "aaa"
// Output: 3
// Explanation: The 3 distinct subsequences are "a", "aa" and "aaa".
//
//
// Constraints:
//
// 1 <= s.length <= 2000
// s consists of lowercase English letters.
//
// IMPORTANT: write down the creation of all the subsequences:
// dp[0] = 2, as it counts ("", "a")
// dp[1] = 4, as it counts ("", "a", "b", "ab");
// dp[2] = 7 as it counts ("", "a", "b", "aa", "ab", "ba", "aba");
// dp[3] = 12, as it counts ("", "a", "b", "aa", "ab", "ba", "bb", "aab", "aba", "abb", "bab", "abab").
//
// S = "abcx", we have dp[k] = dp[k-1] * 2
//   a
//
// dp[i] = dp[i-1] * 2 can be proved by listing those subsequences
//
//
//  https://www.geeksforgeeks.org/count-distinct-subsequences/
//
//https://www.youtube.com/watch?v=boT3gkVPlH4&ab_channel=HuifengGuan
// num_of_unique_good_subseq_1987.go
func distinctSubseqII_v4(s string) int {
	var lastCnt = [26]int{}

	length := len(s)

	s = "@"+s
	dp := make([]int, len(s))
	dp[0] = 1

	for i := 1; i <= length; i++ {
		j := lastCnt[s[i]-'a'] //could be 0 or last occurrence
		cur := dp[i-1] * 2 % MOD
		if j >= 1 { //is there dup. ?
			cur -= dp[j-1] //minus those dup.
		}
		dp[i] = (cur + MOD) % MOD //cur could be -ve; save cur
		lastCnt[s[i]-'a'] = i     //save the cur char index for de-dup later
	}
	return dp[len(s)-1] - 1
}

func distinctSubseqII_v3(s string) int {
	pre, cur := 1, 1

	var lastCnt = [26]int{}
	for i := 0; i < len(s); i++ {
		idx := s[i] - 'a'
		cur = (pre * 2) % MOD
		cur -= lastCnt[idx] //if there is any
		if cur < 0 {        //could be -ve
			cur += MOD
		} else {
			cur %= MOD
		}
		lastCnt[idx] = pre //save for dup char handling
		pre = cur
	}
	cur-- //empty string
	return cur
}

func distinctSubseqII_v2(s string) int {

	dp := make([]int, len(s))
	for i := range dp {
		dp[i] = 1 //each can be a unique subseq
	}

	//		a b a b
	//        i
	//      j
	//
	var res int
	for i := 0; i < len(s); i++ {
		for j := 0; j < i; j++ {
			if s[i] != s[j] {
				dp[i] += dp[j]
				dp[i] %= MOD
			} //if the chars are the same, we don't use it:dont add the dp[j]
			//simply use the dp[i] to the result
		}
		res += dp[i]
		res %= MOD

	} //for
	return res
}

func distinctSubseqII(s string) int {
	nMOD := int64(MOD)

	occ := make(map[byte]int64)
	lastCnt := int64(1)
	for i := 0; i < len(s); i++ {

		tmp := lastCnt % nMOD
		//dp[i] = dp[i-1]*2
		//value := (lastCnt * 2) % nMOD
		lastCnt = (lastCnt * 2) % nMOD
		if pre, exist := occ[s[i]]; exist {
			//if a char repeats, the subseq from the previous one needs to be subtracted
			lastCnt = (lastCnt - pre%nMOD + nMOD) % nMOD
		}
		occ[s[i]] = tmp
	}
	return int(lastCnt - 1) //minus the empty string
}
