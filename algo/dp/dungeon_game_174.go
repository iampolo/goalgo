package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/dungeon-game/
//    {-2, -3, 3},
//    {-5, -10, 1},
//    {10, 30, -5}
//    only needs 1 pt to survive, last cell has -5
//    	-5 + x = 1 => x = 6  (needs 6 pt survive)
//		cell: 30, nextcol:6
//			6-30 -> -24 (24 extra) too many point, but we need just 1
//				max(1, 6-30)
//

// https://leetcode.jp/leetcode-174-dungeon-game-%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/
func calculateMinimumHP_bottom_up(dungeon [][]int) int {
	dp := make([][]int, len(dungeon))
	for i := range dp {
		dp[i] = make([]int, len(dungeon[0]))
	}

	rows, cols := len(dungeon), len(dungeon[0])
	dp[rows-1][cols-1] = 1
	if dungeon[rows-1][cols-1] < 0 {
		dp[rows-1][cols-1] = 1 - dungeon[rows-1][cols-1]
	}

	for i := rows - 1; i >= 0; i-- {
		for j := cols - 1; j >= 0; j-- {
			if i == rows-1 && j == cols-1 {
				continue
			}

			down := math.MaxInt32
			if i < rows-1 && dp[i+1][j] < down {
				down = dp[i+1][j]
			}
			right := math.MaxInt32
			if j < cols-1 && dp[i][j+1] < right {
				right = dp[i][j+1]
			}

			dp[i][j] = Max(1, Min(down, right)-dungeon[i][j])
		}
	}
	return dp[0][0]
}

func calculateMinimumHP(dungeon [][]int) int {
	//dp matrix stores the health requirement on each step
	dp := make([][]int, len(dungeon))
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(dungeon[0]))
		for j := 0; j < len(dungeon[0]); j++ {
			dp[i][j] = math.MaxInt32
		}
	}

	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	nextPoint := func(cur int, r, c int) int {
		if r >= len(dungeon) || c >= len(dungeon[0]) {
			return math.MaxInt32 //undefined
		}
		return max(1, dp[r][c]-cur)
	}

	//start from the destination(bottom-right)
	for i := len(dungeon) - 1; i >= 0; i-- {
		for j := len(dungeon[0]) - 1; j >= 0; j-- {

			//check the right and down and collect their health requirement
			right := nextPoint(dungeon[i][j], i, j+1)
			down := nextPoint(dungeon[i][j], i+1, j)
			//then select the minimum
			next := min(right, down)

			if next != math.MaxInt32 { //is it undefined
				dp[i][j] = next
			} else {
				if dungeon[i][j] >= 0 { //doesn't have health requirement, use the minimum (1)
					dp[i][j] = 1
				} else { //-ve -- reducing health, to survive, we need 1, so we need 1-(reduce) amount of health
					dp[i][j] = 1 - dungeon[i][j]
				}
			}
		}
	} //
	fmt.Println(dp)

	return dp[0][0]
}

func calculateMinimumHP_top_down(du[][]int) int {
	dp := make([][]int, len(du))
	for i := range dp {
		dp[i] = make([]int, len(du[0]))

	}
	var dfs func(r, c int) int
	dfs = func(r, c int) int {
		if r >= len(du) || c >= len(du[0]) {
			return math.MaxInt32
		}
		if dp[r][c] > 0 {
			return dp[r][c]
		}

		down := dfs(r+1,c)
		right := dfs(r,c+1)

		next := Min(down, right)
		if next == math.MaxInt32 {
			next = 1
		}

		cur := 1
		if next > du[r][c] {
			cur = next - du[r][c]
		}
		dp[r][c] = cur
		return cur
	}
	return dfs(0, 0)
}