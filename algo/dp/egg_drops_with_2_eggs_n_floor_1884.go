package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/egg-drop-with-2-eggs-and-n-floors/
//You are given two identical eggs and you have access to a building with n floors labeled from 1 to n.
//
//You know that there exists a floor f where 0 <= f <= n such that any egg dropped at a floor higher than f will break,
//and any egg dropped at or below floor f will not break.
//
//In each move, you may take an unbroken egg and drop it from any floor x (where 1 <= x <= n). If the egg breaks, you
//can no longer use it. However, if the egg does not break, you may reuse it in future moves.
//
//Return the minimum number of moves that you need to determine with certainty what the value of f is.
//
//
//
//Example 1:
//
//Input: n = 2
//Output: 2
//Explanation: We can drop the first egg from floor 1 and the second egg from floor 2.
//If the first egg breaks, we know that f = 0.
//If the second egg breaks but the first egg didn't, we know that f = 1.
//Otherwise, if both eggs survive, we know that f = 2.
//
//Example 2:
//
//Input: n = 100
//Output: 14
//Explanation: One optimal strategy is:
//- Drop the 1st egg at floor 9. If it breaks, we know f is between 0 and 8. Drop the 2nd egg starting from floor 1
//  and going up one at a time to find f within 7 more drops. Total drops is 1 + 7 = 8.
//- If the 1st egg does not break, drop the 1st egg again at floor 22. If it breaks, we know f is between 9 and 21.
//  Drop the 2nd egg starting from floor 10 and going up one at a time to find f within 12 more drops. Total drops is 2 + 12 = 14.
//- If the 1st egg does not break again, follow a similar process dropping the 1st egg from floors 34, 45, 55, 64, 72,
//  79, 85, 90, 94, 97, 99, and 100.
//Regardless of the outcome, it takes at most 14 drops to determine f.
//
//Constraints:
//
//1 <= n <= 1000
func twoEggDrop(n int) int {
	//try at floor-i, if the egg broke, need to i-1 trials, total 1+(i-1)
	//try at floor-i, the egg didn't break, recursively try (n-i) floor, total: 1+dfs(n-i)
	k := 2 //number of eggs - lc887 (tle)
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, k+1) //3 for two eggs
	}
	return recursive(n, k, dp)
}

// https://leetcode.com/problems/egg-drop-with-2-eggs-and-n-floors/discuss/1299197/Java-Generic-DP-Solution-with-N-floors-K-eggs.
//o(n^2 * k)
func recursive(n int, eggs int, dp [][]int) int {
	if n <= 2 || eggs == 1 {
		return n
	}
	if dp[n][eggs] > 0 {
		return dp[n][eggs]
	}
	minTrial := math.MaxInt32
	for f := 1; f < n; f++ {
		//if at f, the egg broke, try f-1 trial, with one less egg
		broke := recursive(f-1, eggs-1, dp)

		//if at f, the egg didn't break, try upper floor
		unbroken := recursive(n-f, eggs, dp)
		trials := Max(broke, unbroken) + 1 //+1 the current trial
		//minimize the maximum trials
		minTrial = Min(minTrial, trials)
	}

	dp[n][eggs] = minTrial
	return minTrial
}
