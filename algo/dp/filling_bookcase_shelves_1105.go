package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

/*
https://leetcode.com/problems/filling-bookcase-shelves/

*/

func minHeightShelves(books [][]int, shelfWidth int) int {
	dp := make([]int, len(books))
	for i := range dp {
		dp[i] = -1
	}

	var dfs func(i int) int
	dfs = func(idx int) int {
		if idx == len(books) {
			return 0
		}
		if dp[idx] != -1 {
			return dp[idx]
		}

		var best = math.MaxInt32
		curWid, curHei := 0, 0
		for i := idx; i < len(books); i++ {
			if books[i][0]+curWid > shelfWidth {
				//this shelf leve is full
				break
			}
			//putting one more book in this level
			curWid += books[i][0]
			curHei = Max(books[i][1], curHei)

			//next level may grow one or more shelf, if not, MaxInt32 is returned
			nextHei := dfs(i + 1)
			if nextHei != math.MaxInt32 {
				best = Min(best, curHei+nextHei)
			}
		}

		dp[idx] = best
		return best
	}

	return dfs(0)
}
