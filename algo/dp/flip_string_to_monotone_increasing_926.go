package main

func minFlipsMonoIncr(s string) int {
	oneCnt, flipCnt := 0, 0
	for _, c := range s {
		if c == '1' {
			oneCnt++
		} else {
			flipCnt++
		}

		if oneCnt < flipCnt {
			flipCnt = oneCnt
		}

	}
	return flipCnt
}
