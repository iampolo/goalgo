package main

/*
https://leetcode.com/problems/handshakes-that-dont-cross/
You are given an even number of people numPeople that stand around a circle and each person shakes hands with
someone else so that there are numPeople / 2 handshakes total.

Return the number of ways these handshakes could occur such that none of the handshakes cross.

Since the answer could be very large, return it modulo 10^9 + 7.


Example 1:

Input: numPeople = 4
Output: 2
Explanation: There are two ways to do it, the first way is [(1,2),(3,4)] and the second one is [(2,3),(4,1)].

Example 2:

Input: numPeople = 6
Output: 5


Constraints:

- 2 <= numPeople <= 1000
- numPeople is even.

https://leetcode.jp/leetcode-1259-handshakes-that-dont-cross-%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/
hits:
   	n = 8
	0	1	2	3	4	5	6	7
    i-1	i	i+1 ... end
	^^^		^^^^^^^^^^^^^^^^^^^^^
			i-1	i	i+1
        ^^^^^       ^^^^^^^^^^^^^

To prevent cross-hand handshakes, one person and other person should only have even number of ppl in between.
*/
func numberOfWays(numPeople int) int {
	dp := make([]int, numPeople+1)
	for i := 0; i <= numPeople; i++ {
		dp[i] = -1
	}

	var dfs func(n int) int
	dfs = func(n int) int {
		if dp[n] != -1 {
			return dp[n]
		}
		if n <= 2 {
			return 1
		}
		var res int
		//split people to two groups
		for i := 1; i < n; i += 2 {
			//split to smaller number of people
			res += (dfs(i-1) * dfs(n-i-1)) % MOD
		}
		dp[n] = res % MOD
		//dp[n] = res
		return dp[n]
	}
	return dfs(numPeople)
}
