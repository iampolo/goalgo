package main

/*
	https://leetcode.com/problems/jump-game-vi/discuss/1260691/Golang-DP-%2B-monotonous-queue-solution
*/
func maxResult(nums []int, k int) int {
	// base case
	dp := make([]int, len(nums))
	dp[0] = nums[0]

	//deque stores index
	deque := make([]int, 0)
	deque = append(deque, 0)

	for i := 1; i < len(nums); i++ {
		// eliminate those out of window k
		for len(deque) > 0 && deque[0] < i-k {
			deque = deque[1:]
		}

		dp[i] = nums[i] + dp[deque[0]]

		for len(deque) > 0 && dp[deque[len(deque)-1]] <= dp[i] {
			deque = deque[:len(deque)-1] //remove the last one
		}

		deque = append(deque, i)
	}

	return dp[len(dp)-1]
}

// https://leetcode.com/problems/jump-game/
// You are given an integer array nums. You are initially positioned at the array's first index, and
// each element in the array represents your maximum jump length at that position.
//
//Return true if you can reach the last index, or false otherwise.
//
//
//
//Example 1:
//
//Input: nums = [2,3,1,1,4]
//Output: true
//Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
//
//Example 2:
//
//Input: nums = [3,2,1,0,4]
//Output: false
//Explanation: You will always arrive at index 3 no matter what. Its maximum jump length is 0, which makes it impossible to reach the last index.
//
//
//Constraints:
//
//1 <= nums.length <= 10^4
//0 <= nums[i] <= 10^5
func canJump_55_v2(nums []int) bool {

	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	cur, next := 0, 0
	for i := 0; i < len(nums); i++ {
		if i > cur { //current i is over cur: last position reached
			if cur == next{
				//but cur is already the position as far as we can go
				return false
			}
			//let's jump as far as possible
			cur = next
		}
		//where can we can if we jump from i
		next = max(next, nums[i]+i)
	}
	return true
}

func canJump_55(nums []int) bool {
	last := len(nums) - 1

	for i := len(nums) - 1; i >= 0; i-- {
		if i+nums[i] >= last {
			last = i
		}
	}
	return last == 0
}
