package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

/*

https://leetcode.com/problems/k-inverse-pairs-array/

For an integer array nums, an inverse pair is a pair of integers [i, j] where 0 <= i < j < nums.length
and nums[i] > nums[j].

Given two integers n and k, return the number of different arrays consist of numbers from 1 to n such
that there are exactly k inverse pairs. Since the answer can be huge, return it modulo 109 + 7.


Example 1:

Input: n = 3, k = 0
Output: 1
Explanation: Only the array [1,2,3] which consists of numbers from 1 to 3 has exactly 0 inverse pairs.

Example 2:

Input: n = 3, k = 1
Output: 2
Explanation: The array [1,3,2] and [2,1,3] have exactly 1 inverse pair.


Constraints:

1 <= n <= 1000
0 <= k <= 1000


@see num_subarrays_bounded_max_795.go
*/

const MOD int = 1e9 + 7

var mem [][]int

//TLE
func kInversePairs(n int, k int) int {
	mem = make([][]int, n+1)
	for i := 0; i <= n; i++ {
		mem[i] = make([]int, k+1)
		for j := 0; j <= k; j++ {
			mem[i][j] = math.MaxInt32
		}
	}

	return getPair(n, k)
}

func getPair(n, k int) int {
	if n == 0 {
		return 0
	}
	if k == 0 {
		//when the array is sorted: no inverse pair
		return 1
	}

	if mem[n][k] != math.MaxInt32 {
		return mem[n][k]
	}

	res := 0
	for i := 0; i <= Min(k, n-1); i++ {
		res = (res + getPair(n-1, k-i)) % MOD
	}

	return res
}

func main_629() {
	fmt.Println(kInversePairs(3, 1))
}
