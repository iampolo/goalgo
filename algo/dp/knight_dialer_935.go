package main

/**
https://leetcode.com/problems/knight-dialer/


@see count_vowels_permutation_1220.go
*/

var DIRS = [][]int{{-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}}

func knightDialer(n int) int {
	dp := make([][][]int, n+1)
	for i := range dp {
		dp[i] = make([][]int, 4) //4 rows
		for j := range dp[i] {
			dp[i][j] = make([]int, 3)
		}
	}

	var dfs func(r, c, n int) int
	dfs = func(r, c, n int) int {
		if r < 0 || r >= 4 || c < 0 || c >= 3 || r == 3 && c != 1 {
			return 0
		}
		if n == 1 {
			return 1
		}
		if dp[n][r][c] > 0 {
			return dp[n][r][c]
		}
		for _, d := range DIRS {
			dp[n][r][c] = (dp[n][r][c] + dfs(r+d[0], c+d[1], n-1)) % MOD
		}

		return dp[n][r][c]
	}

	var res int
	for i := 0; i < 4; i++ {
		for j := 0; j < 3; j++ {
			res = (res + dfs(i, j, n)) % MOD
		}
	}
	return res
}
