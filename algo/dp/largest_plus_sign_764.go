package main

// https://leetcode.com/problems/largest-plus-sign/discuss/113314/JavaC%2B%2BPython-O(N2)-solution-using-only-one-grid-matrix

func orderOfLargestPlusSign(n int, mines [][]int) int {
	dp := make([][]int, n)
	for i := 0; i < n; i++ {
		dp[i] = make([]int, n)
		for j := 0; j < n; j++ {
			dp[i][j] = n
		}
	}

	for _, m := range mines {
		dp[m[0]][m[1]] = 0
	}

	getCnt := func(cnt int, r, c int) int {
		if dp[r][c] == 0 {
			return 0
		} else {
			return cnt + 1
		}
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	max := func(a, b int) int {
		if a > b {
			return a
		}
		return b
	}

	for i := 0; i < n; i++ {
		//j is the left or top boundary, k is the bottom or right boundary
		for j, k, l, r, u, d := 0, n-1, 0, 0, 0, 0; j < n; j, k = j+1, k-1 {
			l = getCnt(l, i, j)
			dp[i][j] = min(dp[i][j], l)

			r = getCnt(r, i, k)
			dp[i][k] = min(dp[i][k], r)

			d = getCnt(d, j, i)
			dp[j][i] = min(dp[j][i], d)

			u = getCnt(u, k, i)
			dp[k][i] = min(dp[k][i], u)
		}
	}
	res := 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			res = max(res, dp[i][j])
		}
	}
	return res
}
