package main

/*
https://leetcode.com/problems/largest-sum-of-averages/
You are given an integer array nums and an integer k. You can partition the array into at most k
non-empty adjacent subarrays. The score of a partition is the sum of the averages of each subarray.

Note that the partition must use every integer in nums, and that the score is not necessarily an integer.

Return the maximum score you can achieve of all the possible partitions. Answers within 10^-6 of the actual
answer will be accepted.


Example 1:

Input: nums = [9,1,2,3,9], k = 3
Output: 20.00000
Explanation:
The best choice is to partition nums into [9], [1, 2, 3], [9]. The answer is 9 + (1 + 2 + 3) / 3 + 9 = 20.
We could have also partitioned nums into [9, 1], [2], [3, 9], for example.
That partition would lead to a score of 5 + 2 + 6 = 13, which is worse.

Example 2:

Input: nums = [1,2,3,4,5,6,7], k = 4
Output: 20.50000


Constraints:

* 1 <= nums.length <= 100
* 1 <= nums[i] <= 104
* 1 <= k <= nums.length

https://www.cnblogs.com/grandyang/p/9504413.html

java: largestSumOfAverages_813
*/

func largestSumOfAverages_dfs(nums []int, k int) float64 {
	dp := make([][]float64, len(nums)+1)
	for i := range dp {
		dp[i] = make([]float64, k+1)
	}

	var cur float64
	//i is the #ofelements picked for the part
	//with one cut
	for i := 0; i < len(nums); i++ {
		cur += float64(nums[i])
		dp[i+1][1] = cur / float64(i+1)
	}
	//for _, d := range dp {
	//	fmt.Println(d)
	//}

	var dfs func(k, j int) float64
	dfs = func(k, j int) float64 {
		if dp[j][k] > 0 {
			return dp[j][k]
		}

		var cur float64
		//try adding each of the elements starting from j
		for i := j - 1; i > 0; i-- {
			cur += float64(nums[i])
			dp[j][k] = max(dp[j][k], dfs(k-1, i)+cur/float64(j-i))
		}
		return dp[j][k]
	}

	return dfs(k, len(nums))
}

func max(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}

func largestSumOfAverages(nums []int, k int) float64 {
	dp := make([][]float64, len(nums))
	for i := range dp {
		dp[i] = make([]float64, k)
	}

	//average if there is zero cut
	psum := make([]int, len(nums)+1)
	//avg := make([]float64, len(nums))
	for i, n := range nums {
		psum[i+1] = psum[i] + n
		//dp[i][0] = float64(psum[i+1]) / float64(i+1)
	}

	n := len(nums)
	for i := 0; i < n; i++ {
		dp[i][0] = float64(psum[n]-psum[i]) / float64(n-i)
	}

	for c := 1; c < k; c++ { //from cut 1... do only k - 1 cuts
		for i := 0; i < len(nums)-1; i++ {
			for j := i + 1; j < len(nums); j++ {
				/*
					avg(i,j) + dp[j][c-1]
					              ^ j is the ending element of the subarray
				*/
				dp[i][c] = max(float64(psum[j]-psum[i])/float64(j-i)+dp[j][c-1], dp[i][c])
			}
		} //for
	} //for
	return dp[0][k-1]
}
