package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/*
https://leetcode.com/problems/last-stone-weight-ii/

You are given an array of integers stones where stones[i] is the weight of the ith stone.

We are playing a game with the stones. On each turn, we choose any two stones and smash them together. Suppose
the stones have weights x and y with x <= y. The result of this smash is:

* If x == y, both stones are destroyed, and
* If x != y, the stone of weight x is destroyed, and the stone of weight y has new weight y - x.

At the end of the game, there is at most one stone left.

Return the smallest possible weight of the left stone. If there are no stones left, return 0.


Example 1:

Input: stones = [2,7,4,1,8,1]
Output: 1
Explanation:
We can combine 2 and 4 to get 2, so the array converts to [2,7,1,8,1] then,
we can combine 7 and 8 to get 1, so the array converts to [2,1,1,1] then,
we can combine 2 and 1 to get 1, so the array converts to [1,1,1] then,
we can combine 1 and 1 to get 0, so the array converts to [1], then that's the optimal value.

Example 2:

Input: stones = [31,26,33,21,40]
Output: 5


Constraints:

* 1 <= stones.length <= 30
* 1 <= stones[i] <= 100

PartitionEqualSubsetSum416
CoinChangeI_322
*/

// 0/1 knapsack
// https://leetcode.jp/tag/%e8%83%8c%e5%8c%85%e9%97%ae%e9%a2%98/
func lastStoneWeightII(stones []int) int {
	var total int
	for _, s := range stones {
		total += s
	}

	n := len(stones)
	//the maximum stones can carry for taking half of the total stone quantity !!
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, total/2+1)
	}
	for i := 1; i <= n; i++ { //stones available to place to the sack
		for j := 0; j <= total/2; j++ { //sack capacity
			//this sack has enough space for stone[i-1]
			if j >= stones[i-1] {
				dp[i][j] = Max(dp[i-1][j], dp[i-1][j-stones[i-1]]+stones[i-1])
			} else {
				//no enough space, take what we have currently
				dp[i][j] = dp[i-1][j]
			}
		} //for
	} //for

	return total - 2*dp[n][total/2]
}

func lastStoneWeightII_v2(stones []int) int {
	var total int
	for _, s := range stones {
		total += s
	}

	n := len(stones)
	dp := make([]int, total/2+1)

	for i := 1; i <= n; i++ { //stones available to place to the sack
		for j := total / 2; j >= stones[i-1]; j-- { //sack capacity
			dp[j] = Max(dp[j], dp[j-stones[i-1]]+stones[i-1])
		}
	}
	return total - 2*dp[total/2]
}

// PartitionEqualSubsetSum416
// TODO
func lastStoneWeightII_v3(stones []int) int {
	var total int
	for _, s := range stones {
		total += s
	}

	n := len(stones)
	dp := make([][]bool, n+1)
	for i := range dp {
		dp[i] = make([]bool, total+1)
		dp[i][0] = true
	}

	for i := 1; i <= n; i++ { //stones available to place in the sack
		for j := 1; j <= total; j++ { //sack capacity
			dp[i][j] = dp[i-1][j]
			if j >= stones[i-1] {
				dp[i][j] = dp[i][j] || dp[i-1][j-stones[i-1]]
			}
		}
	} //for

	for i := total / 2; i >= 0; i-- {
		if dp[n][i] {
			return total - i*2
		}
	}

	return 0
}
