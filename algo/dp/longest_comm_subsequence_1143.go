package main

import "fmt"

// https://leetcode.com/problems/longest-common-subsequence/solution/

func longestCommonSubsequence(text1 string, text2 string) int {

	dp := make([][]int, len(text1)+1)
	for i := range dp {
		dp[i] = make([]int, len(text2)+1)
	}

	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	//scan from the end of the strings
	for c := len(text2) - 1; c >= 0; c-- {
		for r := len(text1) - 1; r >= 0; r-- {
			if text1[r] == text2[c] {
				dp[r][c] = dp[r+1][c+1] + 1
			} else {
				dp[r][c] = max(dp[r+1][c], dp[r][c+1])
			}
		}
	} //for
	return dp[0][0]
}

// approach 3
// [0 0 0 0 0 0 0 0]
// [0 0 0 1 1 1 1 1]
// [0 0 1 1 2 2 2 2]
// [0 1 1 1 2 2 2 2]
// [0 1 1 1 2 3 3 3]
// [0 1 1 1 2 3 3 4]
func longestCommonSubsequence_dp2(text1 string, text2 string) int {

	dp := make([][]int, len(text1)+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(text2)+1)
	}

	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	//scan from the left of the strings
	for i := 1; i < len(dp); i++ {
		for j := 1; j < len(dp[0]); j++ {
			if text1[i-1] == text2[j-1] {
				dp[i][j] = 1 + dp[i-1][j-1] //1 more and plus previous matched.
			} else {
				dp[i][j] = max(dp[i-1][j], dp[i][j-1])
			}
		}
	}

	return dp[len(text1)][len(text2)]
}

//space better
func longestCommonSubsequence_dp2_better(text1 string, text2 string) int {
	//two rows are enough: use modulo to switch over
	dp := make([][]int, 2)
	for i := range dp {
		//the first row and col are all 0s for the base cases: no match at all
		dp[i] = make([]int, len(text2)+1)
	}

	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	for i := 1; i <= len(text1); i++ {
		for j := 1; j <= len(text2); j++ {
			if text1[i-1] == text2[j-1] {
				dp[i%2][j] = dp[(i-1)%2][j-1] + 1
			} else {
				dp[i%2][j] = max(dp[(i-1)%2][j], dp[i%2][j-1])
			}
		}
	}
	return dp[len(text1)%2][len(text2)]
}

// 160ms TODO
/*
  c  b  a  b  d  f  e ""
a[4 -1 -1  -1 -1 -1 -1 0]
b[3 -1 -1  3  -1 -1 -1 0]
c[3 -1  2  2  2  -1 -1 0]
d[2  2  2  2  2  -1 -1 0]
e[1  1  1  1  1  1  -1 0]
 [0  0  0  0  0  0  0  0]]
*/
func longestCommonSubsequence_recur(text1 string, text2 string) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	indexOf := func(s byte, t string, pos int) int {
		for i := pos; i < len(t); i++ {
			if s == t[i] {
				return i
			}
		}
		return -1
	}

	memo := make([][]int, len(text1)+1)
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, len(text2)+1)
		if i == len(memo)-1 {
			break //row is not filled
		}
		for j := 0; j < len(text2); j++ { //last column is not filled
			memo[i][j] = -1
		}
	}
	var dfs func(p1, p2 int) int
	dfs = func(p1, p2 int) int {
		if memo[p1][p2] != -1 {
			return memo[p1][p2]
		}

		opt1 := dfs(p1+1, p2)
		pos := indexOf(text1[p1], text2, p2)

		var opt2 int
		if pos >= 0 {
			opt2 = 1 + dfs(p1+1, pos+1)
		}

		memo[p1][p2] = max(opt1, opt2)
		return memo[p1][p2]
	}

	res := dfs(0, 0)
	fmt.Println(memo)
	return res
}

// min_insertion_make_str_palindrome_1312.go
func longestCommonSubsequence_recur_better(text1 string, text2 string) int {
	dp := make([][]int, len(text1))
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(text2))
		for j := 0; j < len(text2); j++ {
			dp[i][j] = -1
		}
	} //

	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	var dfs func(i, j int) int
	dfs = func(i, j int) int {
		if i == len(text1) || j == len(text2) {
			return 0
		}
		if dp[i][j] != -1 {
			return dp[i][j] //already processed before
		}
		var res int
		if text1[i] == text2[j] { //both chars at the front are the same
			//there is a match, go to next chars
			res = 1 + dfs(i+1, j+1)
		} else {
			//two cases if no match
			res = max(dfs(i+1, j), dfs(i, j+1))
		}

		dp[i][j] = res
		return res
	}

	return dfs(0, 0)
}
