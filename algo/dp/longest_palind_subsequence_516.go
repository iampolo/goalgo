package main

// https://leetcode.com/problems/longest-palindromic-subsequence/description/
// Given a string s, find the longest palindromic subsequence's length in s.
//
// A subsequence is a sequence that can be derived from another sequence by deleting some or no elements
// without changing the order of the remaining elements.
//
//
//
//Example 1:
//
//Input: s = "bbbab"
//Output: 4
//Explanation: One possible longest palindromic subsequence is "bbbb".
//
//Example 2:
//
//Input: s = "cbbd"
//Output: 2
//Explanation: One possible longest palindromic subsequence is "bb".
//
//
//Constraints:
//
//- 1 <= s.length <= 1000
//- s consists only of lowercase English letters.
// valid_palindrome_iii_1216.go
func longestPalindromeSubseq(s string) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
	}

	for i := len(s) - 1; i >= 0; i-- {
		dp[i][i] = 1 //a char itself is a palindrome

		//scan fro char_i, right till the end of the string to find palindrome pattern
		for j := i + 1; j < len(s); j++ {
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1] + 2
			} else {
				dp[i][j] = max(dp[i+1][j], dp[i][j-1])
			}
		}
	}
	return dp[0][len(s)-1]
}
