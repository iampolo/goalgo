package main

//  https://leetcode.com/problems/longest-palindromic-substring/submissions/

func longestPalindrome(s string) string {
	longest := ""
	for i := 0; i < len(s); i++ {
		tmp := findPalindStr(s, i, i)
		if len(tmp) > len(longest) {
			longest = tmp
		}
		tmp  = findPalindStr(s, i, i +1)
		if len(tmp) > len(longest) {
			longest = tmp
		}
	}
	return longest
}

func findPalindStr(s string, l, r int) string {
	for l >= 0 && r < len(s) && s[l] == s[r] {
		l--
		r++
	}
	return s[l+1 : r]
}
