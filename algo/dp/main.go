package main

import (
	"fmt"
)

func main() {
	//test_1143()
	test_1312()
	//test_1937()
}

func test_1259() {
	n := 4
	fmt.Println(numberOfWays(n))
	n = 140
	fmt.Println(numberOfWays(n))
}

func test_search() {
	st := []int{1, 2, 3, 3}
	et := []int{3, 4, 5, 6}
	p := []int{50, 10, 40, 70}

	jobs := make([][]int, len(st))
	for i := range st {
		jobs[i] = []int{st[i], et[i], p[i]}
	}
}

func test_818() {
	target := 10
	fmt.Println(racecar_top_down(target))
	target = 6
	fmt.Println(racecar_top_down(target))
}

func test_1269() {
	steps, arrLen := 27, 7

	fmt.Println(numWays_1269(steps, arrLen))
}

func test_1105() {
	books := [][]int{{1, 1}, {2, 3}, {2, 3}, {1, 1}, {1, 1}, {1, 1}, {1, 2}}
	wid := 4
	fmt.Println(minHeightShelves(books, wid))
}

func test_1547() {
	cuts := []int{1, 3, 4, 5}
	n := 7
	fmt.Println(minCost_1547(n, cuts))
}

func test_1458() {
	nums1 := []int{2, 1, -2, 5}
	nums2 := []int{3, 0, -6}
	fmt.Println(maxDotProduct_dfs(nums1, nums2))
}

func test_813() {
	nums := []int{9, 1, 2, 3, 9}
	k := 3
	fmt.Println(largestSumOfAverages_dfs(nums, k))
	fmt.Println(largestSumOfAverages(nums, k))
}

func test_1312() {
	s := "mbadm"
	s = "leeetcode"
	s = "abc"
	fmt.Println(minInsertions(s))
}

func test_903() {
	s := "DID"
	fmt.Println(numPermsDISequence(s))
	s = "D"
	fmt.Println(numPermsDISequence(s))
}

func test_1349() {
	seats := [][]byte{
		{'#', '.', '.', '.', '#'},
		{'.', '#', '.', '#', '.'},
		{'.', '.', '#', '.', '.'},
		{'.', '#', '.', '#', '.'},
		{'#', '.', '.', '.', '#'}}
	fmt.Println(maxStudents(seats))
}

func test_956() {
	rods := []int{1, 2, 3, 6}
	rods = []int{2, 4, 8, 16}
	rods = []int{1, 2, 3, 4, 5, 6}
	rods = []int{102, 103, 104, 108, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100}
	fmt.Println(tallestBillboard(rods))
	fmt.Println(tallestBillboard_dfs_memo(rods))
	fmt.Println(tallestBillboard_dfs_TLE(rods))
}

func test_416() {
	nums := []int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 100}
	nums = []int{1, 5, 11, 5}
	nums = []int{1, 5, 10, 6}
	fmt.Println(canPartitionBottomUp_bottomup_v2(nums))
	fmt.Println(canPartitionBottomUp(nums))
}

func test_879() {
	n := 5
	minProfit := 3
	group := []int{2, 2}
	profit := []int{2, 3}

	fmt.Println(profitableSchemes(n, minProfit, group, profit))
	fmt.Println(profitableSchemes_2D(n, minProfit, group, profit))
}

func test_1320() {
	word := "HAPPY"
	fmt.Println(minimumDistance_bottom_up(word))
}

func test_920() {
	n, goal, k := 2, 3, 1
	fmt.Print(numMusicPlaylists(n, goal, k))
}

func test_1230() {
	prob := []float64{0.5, 0.5, 0.5}
	target := 1
	fmt.Println(probabilityOfHeads(prob, target))
	fmt.Println(probabilityOfHeads_bu(prob, target))
}

func test_1092() {
	str1, str2 := "abac", "cab"
	fmt.Println(shortestCommonSupersequence(str1, str2))
}

func test_1531() {
	s := "aaabcccd"
	k := 2
	//s, k = "aabbaa", 2
	fmt.Println(getLengthOfOptimalCompression(s, k))
}

func test_1884() {
	n := 100
	fmt.Println(twoEggDrop(n))

	n = 100
	k := 2
	fmt.Println("887:", superEggDrop_bs(k, n))
}
func test_975() {
	arr := []int{5, 1, 3, 4, 2}
	arr = []int{10, 13, 12, 14, 15}
	fmt.Println(oddEvenJumps(arr))
}

func test_788() {
	n := 857
	n = 493
	fmt.Println(rotatedDigits(n))
	fmt.Println(rotatedDigits_dp(n))
}

func test_1478() {
	houses := []int{1, 4, 8, 10, 20}
	k := 3
	//houses = []int{1, 4, 8}
	k = 3
	fmt.Println(minDistance(houses, k))
}

func test_1553() {
	n := 10
	fmt.Println(minDays_bfs(n))
}

func test_1255() {
	words := []string{"dog", "cat", "dad", "good"}
	letters := []byte{'a', 'a', 'c', 'd', 'd', 'd', 'g', 'o', 'o'}
	score := []int{1, 0, 9, 5, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	words = []string{"baa", "bba", "ccb", "ac"}
	letters = []byte{'a', 'b', 'b', 'b', 'b', 'c'}
	score = []int{2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	fmt.Println(maxScoreWords(words, letters, score))
}

func test_1024() {
	clips := [][]int{{0, 2}, {4, 6}, {8, 10}, {1, 9}, {1, 5}, {5, 9}}
	time := 10

	//clips = [][]int{{0,1},{1,2}}
	//time = 5
	//
	//clips = [][]int{{0,0}}
	//time = 0
	fmt.Println(videoStitching_v3(clips, time))
	fmt.Println(videoStitching(clips, time))
}

func test_1411() {
	n := 2
	fmt.Println(numOfWays(n))
}

func test_1987() {
	s := "1001"
	fmt.Println(numberOfUniqueGoodSubsequences(s))
}

func test_940() {
	s := "abcc"
	//s = "abcbca"
	//s = "aba"

	fmt.Println(distinctSubseqII_v4(s))
	fmt.Println(distinctSubseqII_v3(s))
	fmt.Println(distinctSubseqII_v2(s))
	fmt.Println(distinctSubseqII(s))
}
func test_1937() {
	points := [][]int{{1, 2, 3}, {1, 5, 1}, {3, 1, 1}}
	fmt.Println(maxPoints_v1(points))
	points = [][]int{{1, 5}, {2, 3}, {4, 2}}
	fmt.Println(maxPoints(points))
}

func test_931() {
	ma := [][]int{{2, 1, 3}, {6, 5, 4}, {7, 8, 9}}
	fmt.Println(minFallingPathSum_v2(ma))
}

func test_279() {
	n := 7
	fmt.Println(numSquares_dp(n))
}

func test_174() {
	dungeon := [][]int{{-2, -3, 3}, {-5, -10, 1}, {10, 30, -5}}
	dungeon = [][]int{{0}}
	dungeon = [][]int{{100}}
	fmt.Println(calculateMinimumHP_bottom_up(dungeon))
	fmt.Println(calculateMinimumHP(dungeon))

}

func test_115() {
	s := "rabbbit"
	t := "rabbit"
	t = "rab"

	//s, t = "babgbag", "bag"

	fmt.Println(numDistinct_v1(s, t))
	fmt.Println(numDistinct_v1_1(s, t))
}

func test_1143() {
	s := "abcde"
	t := "cbabdfe"
	fmt.Println(longestCommonSubsequence(s, t))
	fmt.Println(longestCommonSubsequence_dp2(s, t))
	fmt.Println(longestCommonSubsequence_dp2_better(s, t))
	fmt.Println(longestCommonSubsequence_recur(s, t))
	fmt.Println(longestCommonSubsequence_recur_better(s, t))
}

func test_2019() {
	exp := "7+3*1*2"
	ans := []int{20, 13, 42}

	exp = "3+5*2"
	ans = []int{13, 0, 10, 13, 13, 16, 16}

	fmt.Println(scoreOfStudents(exp, ans))
}

func test_1691() {
	cub := [][]int{{50, 45, 20}, {95, 37, 53}, {45, 23, 12}}
	cub = [][]int{{74, 7, 80}, {7, 52, 61}, {62, 41, 37}, {91, 58, 26}, {88, 98, 5}, {72, 93, 23}, {56, 58, 94}, {88, 8, 64}, {32, 55, 5}}
	fmt.Println(maxHeight(cub))
}

func test_764() {
	n := 5
	mines := [][]int{{4, 2}}

	fmt.Println(orderOfLargestPlusSign(n, mines))
}

func test_1235() {
	st := []int{1, 2, 3, 3}
	et := []int{3, 4, 5, 6}
	p := []int{50, 10, 40, 70}

	fmt.Println(jobScheduling(st, et, p))
	st = []int{1, 2, 3, 4, 6}
	et = []int{3, 5, 10, 6, 9}
	p = []int{20, 20, 100, 70, 60}
	fmt.Println(jobScheduling(st, et, p))

}

func test_91() {
	s := "226"
	fmt.Println(numDecodings_v2(s))
	fmt.Println(numDecodings_v4(s))
}

func test546() {
	nums := []int{1, 3, 2, 2, 2, 3, 4, 3, 1}
	fmt.Println(removeBoxes(nums))
}

func test_276() {
	n, k := 7, 2
	fmt.Println(numWays(n, k))
}

func test_132() {
	s := "aab"

	fmt.Println(minCut(s))
}

func test_542() {
	//compare withe the value from the 1st direction
	matrix := [][]int{{1, 1, 0, 1, 1},
		{1, 1, 0, 1, 1},
		{1, 1, 1, 1, 0},
		{1, 1, 1, 1, 1},
		{1, 0, 0, 1, 1}}

	fmt.Println(updateMatrix(matrix))
}
func test_935() {
	n := 3
	fmt.Println(knightDialer(n))
}

func test_1220() {
	n := 3

	fmt.Println(countVowelPermutation(n))
	fmt.Println(countVowelPermutation_v2(n))
}

func test_1444() {
	pi := []string{"A..", "AAA", "..."}
	k := 3
	fmt.Println(ways(pi, k))
}

func test() {
	ma := [][]int{{1, 0, 1}, {0, -2, 3}}
	k := 2

	fmt.Println(maxSumSubmatrix(ma, k))

}
