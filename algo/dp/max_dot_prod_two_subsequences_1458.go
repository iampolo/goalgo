package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

/*
https://leetcode.com/problems/max-dot-product-of-two-subsequences/
http://leetcode.jp/leetcode-1458-max-dot-product-of-two-subsequences-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
*/

func maxDotProduct_dfs(nums1 []int, nums2 []int) int {
	dp := make([][]int, len(nums1))
	for i := range dp {
		dp[i] = make([]int, len(nums2))
		for j := 0; j < len(dp[0]); j++ {
			dp[i][j] = math.MinInt32
		}
	}

	var dfs func(i, j int) int
	dfs = func(i, j int) int {
		if dp[i][j] != math.MinInt32 {
			return dp[i][j]
		}
		//take nums from both
		max := nums1[i] * nums2[j]
		if i+1 < len(nums1) && j+1 < len(nums2) {
			res := dfs(i+1, j+1)
			//taking nums from both array
			max = Max(max, max+res)
			//not taking a num from either array
			max = Max(max, res)
		}
		if i+1 < len(nums1) {
			//TODO
			max = Max(max, dfs(i+1, j))
		}
		if j+1 < len(nums2) {
			//TODO
			max = Max(max, dfs(i, j+1))
		}

		dp[i][j] = max
		return max
	}
	return dfs(0, 0)
}

//
func maxDotProduct(nums1 []int, nums2 []int) int {
	dp := make([][]int, len(nums1)+1)
	for i := 0; i < len(dp); i++ {
		dp[i] = make([]int, len(nums2)+1)
		for j := 0; j < len(dp[0]); j++ {
			//the input/product could be -ve
			dp[i][j] = math.MinInt32
		}
	}

	for i := 1; i <= len(nums1); i++ {
		for j := 1; j <= len(nums2); j++ {
			dp[i][j] = Max(nums1[i-1]*nums2[j-1], //take nums from two arrays
				dp[i-1][j-1]+nums1[i-1]*nums2[j-1]) //take nums from two arrays & previous result

			dp[i][j] = Max(dp[i][j], dp[i-1][j]) //take from nums2 only
			dp[i][j] = Max(dp[i][j], dp[i][j-1]) //take from nums1 only
		}
	}
	return dp[len(nums1)][len(nums2)]
}
