package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
	"sort"
)

// https://leetcode.com/problems/maximum-height-by-stacking-cu/discuss/1449320/Simple-Java-Solution-O(N2)-time-DP-Solution
//
// If we have a solution that contains at least one cuboid with longest edge not as height, we can rotate it without
// affecting adjacent cuboid and make the total height larger:
//
//- if the one with longest edge not as height is on the top of the cuboid stack, we can simply rotate it so the it
//	contributes more height
//- if the one with longest edge is in the middle, let's say it is A and the 3 edges are [A1, A3, A2] (A3 > A2 && A3 > A1,
//	A2 is not longest but it is the height), the one on top of A is B[B1, B2, B3] (B3 >= B2 >= B1):
//	- we have A1 >= B1 && A3 >= B2 && A2 >= B3
//	- then: A3 > A2 >= B3, A2 >= B3 >= B2, A1 >= B1
//  - so we can rotate A from [A1, A3, A2] to [A1, A2, A3] without affecting B but make the total height larger
// 	  (increase by A3 - A2)
//

// 1996. The Number of Weak Characters in the Game
// 354. Russian Doll Envelopes
//
//
// If we have a solution that contains at least one cuboid with longest edge not as height, we can rotate it without
// affecting adjacent cuboid and make the total height larger:
//
// if the one with longest edge not as height is on the top of the cuboid stack, we can simply rotate it so that it
// contributes more height.
// if the one with longest edge is in the middle, let's say it is A and the 3 edges are [A1, A3, A2] (A3 > A2 && A3 > A1,
// A2 is not longest but it is the height), the one on top of A is B[B1, B2, B3] (B3 >= B2 >= B1):
// - we have A1 >= B1 && A3 >= B2 && A2 >= B3
// - then: A3 > A2 >= B3, A2 >= B3 >= B2, A1 >= B1
// - so we can rotate A from [A1, A3, A2] to [A1, A2, A3] without affecting B but make the total height larger (increase by A3 - A2)
func maxHeight(cu [][]int) int {
	//sort by column - BUG -- TODO
	sort.Slice(cu, func(i, j int) bool {
		if cu[i][0] != cu[j][0] {
			return cu[i][0] > cu[j][0]
		} else if cu[i][1] != cu[j][1] {
			return cu[i][1] > cu[j][1]
		}
		return cu[i][2] > cu[j][2]
	})
	//sort the elements in a row
	for i := range cu {
		sort.Ints(cu[i])
	}

	for i := range cu {
		fmt.Println(cu[i])
	}

	dp := make([]int, len(cu))

	var res = math.MinInt32
	for i := 0; i < len(cu); i++ {
		dp[i] = cu[i][2]
		for j := 0; j < i; j++ {
			if cu[j][0] >= cu[i][0] && cu[j][1] >= cu[i][1] && cu[j][2] >= cu[i][2] {
				dp[i] = Max(dp[i], cu[i][2]+dp[j])
			}
		}
		res = Max(res, dp[i])
	}
	return res
}
