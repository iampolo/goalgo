package main

// https://leetcode.com/problems/maximum-number-of-points-with-cost/
/*
Constraints:

m == points.length
n == points[r].length
1 <= m, n <= 10^5
1 <= m * n <= 10^5
0 <= points[r][c] <= 10^5

@see MinimumFallingPathSum931
 */

// https://leetcode.com/problems/maximum-number-of-points-with-cost/discuss/1344893/Similar-to-931.-Minimum-Falling-Path-Sum
func maxPoints_v1(points [][]int) int64 {
	max := func(a, b int64) int64 {
		if a < b {
			return b
		}
		return a
	}

	prev := make([]int64, len(points[0]))
	curr := make([]int64, len(points[0]))

	for _, row := range points {
		runningMax := int64(0)
		//find the max from the left to right
		//mainly to find the max value of the previous run
		for j := 0; j < len(row); j++ {
			runningMax = max(runningMax-1, prev[j])
			curr[j] = runningMax
		}

		//scan from right to left and find the max
		for j := len(points[0]) - 1; j >= 0; j-- {
			//-1 to reflect the column movement.
			runningMax = max(runningMax-1, prev[j])
			//curr[j] could be bigger, compare to runningMax, pick the larger one
			curr[j] = max(curr[j], runningMax) + int64(row[j])
		}

		prev = curr
	}

	res := prev[0]
	for i := 1; i < len(prev); i++ {
		res = max(res, prev[i])
	}
	return res
}

// O(rows*cols)
func maxPoints(points [][]int) int64 {
	max := func(a, b int64) int64 {
		if a < b {
			return b
		}
		return a
	}

	width := len(points[0])
	dp := make([]int64, width)

	for i := 0; i < len(points); i++ {
		for j := 0; j < width; j++ {
			dp[j] += int64(points[i][j])
		}

		for j := 1; j < width; j++ {
			//keep the current dp[j] or use the one at previous col
			//-1 to reflect the subtraction
			dp[j] = max(dp[j], dp[j-1]-1)
		}

		//check backward like other regular DPs,
		//find the max possible points
		for j := width - 2; j >= 0; j-- {
			dp[j] = max(dp[j], dp[j+1]-1)
		}
	}

	res := dp[0]
	for i := 1; i < width; i++ {
		res = max(res, dp[i])
	}
	return res
}
