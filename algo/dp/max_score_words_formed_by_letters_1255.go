package main

import . "gitlab.com/iampolo/goalgo/algo/util"

//  https://leetcode.com/problems/maximum-score-words-formed-by-letters/
//Given a list of words, list of  single letters (might be repeating) and score of every character.
//
//Return the maximum score of any valid set of words formed by using the given letters (words[i] cannot be used two
//or more times).
//
//It is not necessary to use all characters in letters and each letter can only be used once. Score of letters
//'a', 'b', 'c', ... ,'z' is given by score[0], score[1], ... , score[25] respectively.
//
//
//
//Example 1:
//
//Input: words = ["dog","cat","dad","good"], letters = ["a","a","c","d","d","d","g","o","o"],
//score = [1,0,9,5,0,0,3,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0]
//Output: 23
//Explanation:
//Score  a=1, c=9, d=5, g=3, o=2
//Given letters, we can form the words "dad" (5+1+5) and "good" (3+2+2+5) with a score of 23.
//Words "dad" and "dog" only get a score of 21.
//
//Example 2:
//
//Input: words = ["xxxz","ax","bx","cx"], letters = ["z","a","b","c","x","x","x"],
//score = [4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,10]
//Output: 27
//Explanation:
//Score  a=4, b=4, c=4, x=5, z=10
//Given letters, we can form the words "ax" (4+5), "bx" (4+5) and "cx" (4+5) with a score of 27.
//Word "xxxz" only get a score of 25.
//
//Example 3:
//
//Input: words = ["leetcode"], letters = ["l","e","t","c","o","d"],
//score = [0,0,1,1,1,0,0,0,0,0,0,1,0,0,1,0,0,0,0,1,0,0,0,0,0,0]
//Output: 0
//Explanation:
//Letter "e" can only be used once.
//
//
//Constraints:
//
// - 1 <= words.length <= 14
// - 1 <= words[i].length <= 15
// - 1 <= letters.length <= 100
// - letters[i].length == 1
// - score.length == 26
// - 0 <= score[i] <= 10
// - words[i], letters[i] contains only lower case English letters.
//
func maxScoreWords(words []string, letters []byte, score []int) int {
	//calc. the score for each word first
	//then find the combination of these words that yield the highest score

	lcnt := make([]int, 26)
	for i := range letters {
		lcnt[letters[i]-'a']++
	}
	return calcScore_v2(words, 0, lcnt, score)
}

func calcScore_v2(words []string, wIdx int, lcnt []int, score []int) int {

	maxScore := 0
	for i := wIdx; i < len(words); i++ {
		curScore := 0
		found := true
		for j := 0; j < len(words[i]); j++ {
			ch := words[i][j] - 'a'
			if lcnt[ch] == 0 {
				found = false
			}
			curScore += score[ch]
			lcnt[ch]--
		}

		if found {
			//curScore plus the words if possible: find out at the next level
			curScore += calcScore_v2(words, i+1, lcnt, score)
			maxScore = Max(maxScore, curScore)
		}

		for j := 0; j < len(words[i]); j++ {
			lcnt[words[i][j]-'a']++
		}
	}
	return maxScore
}

//laioffer dfs classic lc78
//since the data size is small, dfs w/o memoization is enough
func calcScore(words []string, wIdx int, lcnt []int, score []int) int {
	if wIdx == len(words) {
		return 0
	}
	getScore := func(w string, cnt []int) int {
		curScore := 0
		i := 0
		for ; i < len(words[wIdx]); i++ {
			ch := words[wIdx][i] - 'a'
			if cnt[ch] == 0 {
				curScore = 0
				break
			}
			curScore += score[ch]
			cnt[ch]--
		}
		return curScore
	}

	maxScore := 0
	tmpCnt := make([]int, 26)
	copy(tmpCnt, lcnt)
	curScore := getScore(words[wIdx], tmpCnt)
	if curScore > 0 {
		maxScore = calcScore(words, wIdx+1, tmpCnt, score) + curScore //take the word
	}
	maxScore = Max(maxScore, calcScore(words, wIdx+1, lcnt, score)) //not take the word
	return maxScore
}
