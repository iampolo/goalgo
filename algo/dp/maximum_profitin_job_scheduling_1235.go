package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

// https://leetcode.com/problems/maximum-profit-in-job-scheduling/
func jobScheduling(startTime []int, endTime []int, profit []int) int {

	jobs := make([][]int, len(startTime))
	for i := range startTime {
		jobs[i] = []int{startTime[i], endTime[i], profit[i]}
	}

	sort.Slice(jobs, func(i, j int) bool {
		return jobs[i][0] < jobs[j][0]
	})

	dp := make([]int, len(jobs))
	for i := range dp {
		dp[i] = -1
	}

	var dfs func(curIdx int, lastEndTime int) int
	dfs = func(idx int, lastEndTime int) int {

		fmt.Println(idx)

		if idx >= len(jobs) {
			return 0
		}
		//check memoe
		if dp[idx] != -1 {
			return dp[idx]
		}

		//take this job, then find the next jobs and pass to next level
		profit := jobs[idx][2]
		res := profit + dfs(search(jobs, idx, jobs[idx][1]), jobs[idx][1])

		//not taking job, start from next job at the next level
		res = Max(res, dfs(idx+1, jobs[idx][1]))

		dp[idx] = res
		return res
	}

	return dfs(0, 0)
}

/*
	Binary Search
	MaxFontFitSentenceInScreen1618
*/
// return the index of next available job with start time >= endTime
func findNextJob(jobs [][]int, curIdx int, endTime int) int {
	/*
		this failed to handle case 1, 2, 3,3
			idx = 3, as it can't get into the loop:lo == hi
	*/
	//FIX
	//if jobs[len(jobs)-1][0] < endTime {
	//	return len(jobs) //no more jobs
	//}

	lo, hi := curIdx+1, len(jobs)-1
	for lo < hi {
		mi := lo + (hi-lo)/2
		if jobs[mi][0] < endTime {
			lo = mi + 1 // try a better one
		} else {
			hi = mi //overlapped endTime
		}
	}
	return hi
}

func search(jobs [][]int, idx int, endTime int) int {
	lo, hi := idx+1, len(jobs)-1
	best := len(jobs) //base case, nothing can't be find
	for lo <= hi {
		mi := lo + (hi-lo)/2
		if jobs[mi][0] >= endTime {
			best = mi
			hi = mi - 1
		} else {
			lo = mi + 1
		}
	}
	return best
}