package main

import . "gitlab.com/iampolo/goalgo/algo/util"

func minCostClimbingStairs(cost []int) int {
	dp := make([]int, len(cost))

	dp[0] = cost[0]
	dp[1] = cost[1]
	for i := 2; i < len(cost); i++ {
		dp[i] = cost[i] + Min(dp[i-1], dp[i-2])
	}

	return Min(dp[len(cost)-1], dp[len(cost)-2])
}
