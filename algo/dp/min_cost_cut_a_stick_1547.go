package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
	"sort"
)

/*
https://leetcode.com/problems/minimum-cost-to-cut-a-stick/

Given a wooden stick of length n units. The stick is labelled from 0 to n. For example, a stick of length 6 is
labelled as follows:


Given an integer array cuts where cuts[i] denotes a position you should perform a cut at.

You should perform the cuts in order, you can change the order of the cuts as you wish.

The cost of one cut is the length of the stick to be cut, the total cost is the sum of costs of all cuts. When
you cut a stick, it will be split into two smaller sticks (i.e. the sum of their lengths is the length of the
stick before the cut). Please refer to the first example for a better explanation.

Return the minimum total cost of the cuts.


Example 1:


Input: n = 7, cuts = [1,3,4,5]
Output: 16
Explanation: Using cuts order = [1, 3, 4, 5] as in the input leads to the following scenario:

The first cut is done to a rod of length 7 so the cost is 7. The second cut is done to a rod of length 6
(i.e. the second part of the first cut), the third is done to a rod of length 4 and the last cut is to a rod of length 3.
The total cost is 7 + 6 + 4 + 3 = 20.
Rearranging the cuts to be [3, 5, 1, 4] for example will lead to a scenario with total cost = 16
(as shown in the example photo 7 + 4 + 3 + 2 = 16).

Example 2:

Input: n = 9, cuts = [5,6,1,4,2]
Output: 22
Explanation: If you try the given cuts ordering the cost will be 25.
There are much ordering with total cost <= 25, for example, the order [4, 6, 5, 2, 1] has total cost = 22
which is the minimum possible.


Constraints:

2 <= n <= 10^6
1 <= cuts.length <= min(n - 1, 100)
1 <= cuts[i] <= n - 1
All the integers in cuts array are distinct.


WaysToSplitArrayIntoThreeSubarrays1712
*/

/*
https://leetcode.jp/leetcode-1547-minimum-cost-to-cut-a-stick-%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/

本题由于切割的顺序会影响到最终的成本，因此我们需要寻找到一个最优解。而先切割哪一个最优我们无法一眼看出，只能遍历所有方式然后比较
得出最优方案。这就是动态规划找最优解的思想。对于DP类题目，我比较倾向使用递归加记忆数组的方式来解题，本题也不例外。

解题时，我们定义一个递归函数fun(left, right)，返回值代表分割区间[left, right]需要的最少成本。递归中，我们尝试用cuts数组中的
每一个切割点cuts[i]进行分割（注意cuts[i]要在当前left和right之间），本次的切割成本为当前棍子的长度。分割后，棍子变成两个部分，
我们将这两部分分别作为两个子问题交给自递归函数去求解。两个子问题的返回值再加上当前的分割成本即是首先切割cuts[i]的最小成本。
我们使用该成本更新本层递归函数中的成本最小值。循环完所有切割点后，成本最小值即是本层递归的返回结果。这里用代码简单总结下这部分逻辑：

int help(left, right){ // 递归函数
    int min; // 最小成本
    for(int i=0;i<cuts.length;i++){ // 循环每一个切割点
        if(cuts[i]>=left && cuts[i]<=right){ // 保证当前切割点在当前区间内
            // 分割后的两个部分分别交个子问题求最小成本
            // cost为先切割cuts[i]后的最优成本
            int cost=help(left, cuts[i]) + help(cuts[i], right);
            // 用当前cost更新最优cost（循环的目的是为了比较先切割哪个点的总成本最少）
            min=Math.min(min, cost);
        }
    }
    min+=(right-left); // 最小成本加上当前切割cuts[i]的成本（区间长度）
    return min;
}
有了核心的递归逻辑之后，我们再加上一个记忆数组变会大功告成。但是我们发现，递归函数中的两个变量left和right，他们分别代表区间的左右边界，
而区间的取值范围在10^6次方，也就是说，如果我们建立一个二维数组，每一个维度的空间都要占用10^6个空间，两个维度总共要占用10^12个空间，
这样做即使不会TLE，也会出现内存不足的问题。然而解决这个问题并不难，其实记忆数组保存的是递归函数的结果，而递归函数的参数是一个区间范围，
该范围的取值实际上并没有那么多种可能性，因为所有被分割的区间实际上都是通过cuts数组中的分割点得到的，而cuts数组的个数最多不会超过100，
也就是说left与right的取值最多分别只有100种而已。

因此我们可以将递归函数改造为fun(leftIndex, rightIndex)。两个参数分别代表cuts数组的下标，也就是说当前棍子的区间为
[cuts[leftIndex], cuts[rightIndex]]。但是这样做会牵扯出两个问题：

因为cuts数组并不是严格递增的，我们如何保证cuts[leftIndex]一定小于cuts[rightIndex]，也就是说我们如何保证区间的合法性。
cuts数组中并不包含棍子首尾两个点，这样我们无法取得递归的初始边界。

解决方案：

排序一下
将首尾两个点加入到cuts中
其他细节请参照代码注释
 */
func minCost_1547(n int, cuts []int) int {

	//use cuts instead of n !!!
	dp := make([][]int, len(cuts)+2)
	for i := range dp {
		dp[i] = make([]int, len(cuts)+2)
	}

	//the extra 2 is for beginning and end of the wood [0, n]
	nCuts := make([]int, len(cuts)+2)
	for i := 1; i <= len(cuts); i++ {
		nCuts[i] = cuts[i-1]
	}
	nCuts[len(nCuts)-1] = n
	//sort to ensure that the cuts are in order,
	//li and ri are always in order: li<=ri
	sort.Ints(nCuts)

	var dfs func(li, ri int) int
	dfs = func(li, ri int) int {
		if dp[li][ri] > 0 {
			return dp[li][ri]
		}
		//prune
		if nCuts[ri]-nCuts[li] <= 1 {
			return 0
		}

		var cost int = math.MaxInt32
		//try each of the cuts in within[left,right] and find the minimal cost
		for i := li + 1; i < ri; i++ {
			//do a cut at i
			cur := dfs(li, i) + dfs(i, ri)
			cost = Min(cost, cur)
		}

		if cost == math.MaxInt32 {
			return 0
		}
		dp[li][ri] = cost + (nCuts[ri] - nCuts[li])
		return dp[li][ri]
	}

	return dfs(0, len(nCuts)-1)
}
