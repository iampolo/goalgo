package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

/*
https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/
You have a keyboard layout as shown above in the X-Y plane, where each English uppercase letter is located
at some coordinate.

For example, the letter 'A' is located at coordinate (0, 0), the letter 'B' is located at coordinate (0, 1), the
letter 'P' is located at coordinate (2, 3) and the letter 'Z' is located at coordinate (4, 1).
Given the string word, return the minimum total distance to type such string using only two fingers.

The distance between coordinates (x1, y1) and (x2, y2) is |x1 - x2| + |y1 - y2|.

Note that the initial positions of your two fingers are considered free so do not count towards your total distance,
also your two fingers do not have to start at the first letter or the first two letters.


Example 1:

Input: word = "CAKE"
Output: 3
Explanation: Using two fingers, one optimal way to type "CAKE" is:
Finger 1 on letter 'C' -> cost = 0
Finger 1 on letter 'A' -> cost = Distance from letter 'C' to letter 'A' = 2
Finger 2 on letter 'K' -> cost = 0
Finger 2 on letter 'E' -> cost = Distance from letter 'K' to letter 'E' = 1
Total distance = 3

Example 2:

Input: word = "HAPPY"
Output: 6
Explanation: Using two fingers, one optimal way to type "HAPPY" is:
Finger 1 on letter 'H' -> cost = 0
Finger 1 on letter 'A' -> cost = Distance from letter 'H' to letter 'A' = 2
Finger 2 on letter 'P' -> cost = 0
Finger 2 on letter 'P' -> cost = Distance from letter 'P' to letter 'P' = 0
Finger 1 on letter 'Y' -> cost = Distance from letter 'A' to letter 'Y' = 4
Total distance = 6


Constraints:

 - 2 <= word.length <= 300
 - word consists of uppercase English letters.
*/

//https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/477654/Java-DP-Solution-with-detailed-written-and-video-explanation
//https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/1136289/golang-from-fang2018's-Java.-easy-understand.

//MinDistanceTypeWordTwoFingers1320  #3-D

//https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/640140/From-3D-DP-to-2D-and-1D-explanation-is-easy-to-understand
//from 3D -> 2D
func minimumDistance_TLE(word string) int {

	dp := make([][]int, len(word))
	for i := range dp {
		dp[i] = make([]int, 27)
	}

	var dfs func(word string, idx int, right int) int
	dfs = func(word string, idx int, right int) int {
		if idx == len(word) {
			return 0
		}
		if dp[idx][right] != 0 {
			return dp[idx][right]
		}

		res := math.MaxInt32
		cur, prev := int(word[idx]-'A'), int(word[idx-1]-'A')
		res = Min(res, dist(prev, cur)+dfs(word, idx+1, right))
		res = Min(res, dist(right, cur)+dfs(word, idx+1, prev))

		dp[idx][right] = res
		return res
	}

	//type each char one by one and sequentially

	//26 - the finger is still hovering
	//start with 1, as one finger is at the right position, cost is 0
	return dfs(word, 1, 26)
}

func dist(f1, f2 int) int {
	//26:fingers are in hovering state, no cost.
	if f1 == 26 || f2 == 26 {
		return 0
	}
	return Abs(f1/6-f2/6) + Abs(f1%6-f2%6)
}

//https://leetcode.com/problems/minimum-distance-to-type-a-word-using-two-fingers/discuss/477762/Java-DP-easy-to-understand-with-explanation
func minimumDistance_bottom_up(word string) int {
	dp := make([][][]int, len(word) + 1)

	_ = dp
	// TODO

	return 1
}

