package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/*

https://leetcode.com/problems/minimum-insertion-steps-to-make-a-string-palindrome/
https://leetcode.jp/leetcode-1312-minimum-insertion-steps-to-make-a-string-palindrome-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/


Given a string s. In one step you can insert any character at any index of the string.

Return the minimum number of steps to make s palindrome.

A Palindrome String is one that reads the same backward as well as forward.



Example 1:

Input: s = "zzazz"
Output: 0
Explanation: The string "zzazz" is already palindrome we don't need any insertions.
Example 2:

Input: s = "mbadm"
Output: 2
Explanation: String can be "mbdadbm" or "mdbabdm".
Example 3:

Input: s = "leetcode"
Output: 5
Explanation: Inserting 5 characters the string becomes "leetcodocteel".


Constraints:

* 1 <= s.length <= 500
* s consists of lowercase English letters.

longest_comm_subsequence_1143.go
LongestPalindromicSubsequence516
java:minInsertions_1312()

*/
func minInsertions(s string) int {
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1
		}
	}

	/*
		O(n^2) TODO
			-> O(2^n)
	 */
	var dfs func(l, r int) int
	dfs = func(l, r int) int {
		if l >= r {
			return 0
		}
		if dp[l][r] != -1 {
			return dp[l][r]
		}

		var cnt int
		if s[l] == s[r] {
			//equal, no insertion is needed
			cnt = dfs(l+1, r-1)
		} else {
			//no equal, insert a char to left, or to the right
			cnt = 1 + Min(dfs(l, r-1), dfs(l+1, r))
		}

		//finished this round, save the result
		dp[l][r] = cnt
		return cnt
	}

	return dfs(0, len(s)-1)
}
