package main

import "math"

// https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/

//There are n oranges in the kitchen and you decided to eat some of these oranges every day as follows:
//
//Eat one orange.
//If the number of remaining oranges (n) is divisible by 2 then you can eat  n/2 oranges.
//If the number of remaining oranges (n) is divisible by 3 then you can eat  2*(n/3) oranges.
//You can only choose one of the actions per day.
//
//Return the minimum number of days to eat n oranges.
//
//
//
//Example 1:
//
//Input: n = 10
//Output: 4
//Explanation: You have 10 oranges.
//Day 1: Eat 1 orange,  10 - 1 = 9.
//Day 2: Eat 6 oranges, 9 - 2*(9/3) = 9 - 6 = 3. (Since 9 is divisible by 3)
//Day 3: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1.
//Day 4: Eat the last orange  1 - 1  = 0.
//You need at least 4 days to eat the 10 oranges.
//
//Example 2:
//
//Input: n = 6
//Output: 3
//Explanation: You have 6 oranges.
//Day 1: Eat 3 oranges, 6 - 6/2 = 6 - 3 = 3. (Since 6 is divisible by 2).
//Day 2: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1. (Since 3 is divisible by 3)
//Day 3: Eat the last orange  1 - 1  = 0.
//You need at least 3 days to eat the 6 oranges.
//
//Example 3:
//
//Input: n = 1
//Output: 1
//
//Example 4:
//
//Input: n = 56
//Output: 6
//
//
//Constraints:
//
// - 1 <= n <= 2*10^91
//
//
func minDays_bfs(n int) int {

	var queue []int
	set := make(map[int]bool)

	var dayCnt int
	queue = append(queue, n)
	for len(queue) > 0 {
		//it has to be layer by layer
		size := len(queue)
		//#of orange can be consume in 3 different ways
		//BFS to expands...
		//all the cases are stored in the queue.
		//the one consume all the oranges the first is the result: the minimum days
		for size > 0 {
			size--
			cur := queue[0]
			queue = queue[1:]
			if _, exist := set[cur]; exist {
				continue
			}
			set[cur] = true
			if cur == 0 {
				return dayCnt
			}

			//expand
			if cur%3 == 0 {
				queue = append(queue, cur/3)
			}
			if cur%2 == 0 {
				queue = append(queue, cur/2)
			}
			if cur > 0 {
				queue = append(queue, cur-1)
			}
		} //for
		dayCnt++
	}
	return dayCnt
}

//*************************************************************************************************

// https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/discuss/794075/C%2B%2B-Java-DP-Understandable-Code-with-comments-and-Key-idea.
func minDays(n int) int {
	// n - 2n/3
	// -> n/3
	// 1 + min((n%2)+T(n/2), (n%3)+T(n/3))
	//

	return 0
}

func consume(n int, memo map[int]int) int {

	curDay := math.MaxInt32
	if _, exist := memo[n]; exist {
		return memo[n] //previously processed
	}

	return curDay
}
