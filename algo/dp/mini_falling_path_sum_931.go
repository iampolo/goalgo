package main

import "math"

// https://leetcode.com/problems/minimum-falling-path-sum/submissions/
func minFallingPathSum_v2(matrix [][]int) int {
	dp := make([][]int, len(matrix))
	for i := range matrix {
		dp[i] = make([]int, len(matrix[0]))
	}

	length := len(matrix)
	dp[0] = matrix[0]             //copy the first row
	for i := 1; i < length; i++ { //start from the 2nd row
		for j := 0; j < length; j++ {
			dp[i][j] = matrix[i][j] + dp[i-1][j]
			if j > 0 { //if it is ok to check previous col at previous row
				dp[i][j] = min(dp[i][j], matrix[i][j]+dp[i-1][j-1])
			}
			if j < length-1 {
				dp[i][j] = min(dp[i][j], matrix[i][j]+dp[i-1][j+1])
			}
		}
	}
	res := math.MaxInt32
	for _, v := range dp[length-1] {
		res = min(res, v)
	}
	return res
}

//*******************************************************************************
func minFallingPathSum(matrix [][]int) int {

	dp := make([][]int, len(matrix))
	for i := range matrix {
		dp[i] = make([]int, len(matrix[0]))
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}

	res := math.MaxInt32
	for i := 0; i < len(matrix[0]); i++ {
		cur := path(matrix, 0, i, dp)
		res = min(res, cur)
	}
	return res
}

func path(m [][]int, r, c int, dp [][]int) int {
	if r < 0 || r >= len(m) || c < 0 || c >= len(m[0]) {
		return math.MaxInt32
	}

	if r == len(m)-1 {
		return m[r][c]
	}
	if dp[r][c] != -1 {
		return dp[r][c]
	}
	left := path(m, r+1, c-1, dp)
	if left != math.MaxInt32 {
		left += m[r][c] //left + current
	}

	mid := path(m, r+1, c, dp)
	if mid != math.MaxInt32 {
		mid += m[r][c] //left + current
	}
	right := path(m, r+1, c+1, dp)
	if right != math.MaxInt32 {
		right += m[r][c] //left + current
	}
	dp[r][c] = min(left, min(mid, right))
	return dp[r][c]
}
