package main

import (
	"container/heap"
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/minimum-number-of-refueling-stops/solution/
// TODO

func minRefuelStops(target int, startFuel int, stations [][]int) int {

	maxHeap := new(MaxHeap)

	tank := startFuel
	lastLoc := 0
	stops := 0
	for i, a := range stations {

		tank -= lastLoc
		if i < len(stations) {
			tank -= a[0]  //consume

			maxHeap.Push(a[1])
			lastLoc = a[0]
		}

		for maxHeap.Len() > 0 && tank < 0 {
			tank += maxHeap.Pop().(int) //refuel
			stops++
		}
		if tank < 0 {
			return -1
		}
	}
	//for _, a := range stations {
	//	fmt.Println(a)
	//
	//	tank -= a[0] - lastLoc //consume
	//	for maxHeap.Len() > 0 && tank < 0 {
	//		tank += maxHeap.Pop().(int) //refuel
	//		stops++
	//	}
	//
	//	if tank < 0 {
	//		return -1
	//	}
	//
	//	maxHeap.Push(a[1])
	//	lastLoc = a[0]
	//}

	return stops
}

func main_871() {
	sta := [][]int{{10, 60}, {20, 30}, {30, 30}, {60, 40}}
	t := 100
	start := 10

	fmt.Println(minRefuelStops(t, start, sta))
}

func TestHeap() {
	h := new(MaxHeap)

	heap.Push(h, 1)
	heap.Push(h, 20)
	heap.Push(h, 5)
	fmt.Println(h.Pop())
}
