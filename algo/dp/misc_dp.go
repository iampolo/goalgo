package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

/*
https://leetcode.com/problems/maximum-product-subarray/solution/
*/
func maxProduct_152(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	res, curMax, curMin := nums[0], nums[0], nums[0]
	for i := 1; i < len(nums); i++ {
		nextMin, nextMax := curMin*nums[i], curMax*nums[i]

		curMin = Min(nums[i], Min(nextMax, nextMin))
		curMax = Max(nums[i], Max(nextMax, nextMin))

		res = Max(res, curMax)
	}
	return res
}

// https://leetcode.com/problems/unique-binary-search-trees/submissions/
// 1 <= n <= 19
//
// Catalan Number: https://www.youtube.com/watch?v=CMaZ69P1bAc&ab_channel=TECHDOSE
//  C(0) = C(1) = 1
//  C(2) = C0C1 + C1C0 => 1*1 + 1*1 = 2
//  C(3) = C0C2 + C1C1 + C2C0 = 2+1+2=5
//  C(4) = C0C3 + C1C2 + C2C1 + C3C0 = 5 + 2 + 2 + 5 = 15
//  ...
//  C(n) = C0Cn-1 + C1Cn-2 + ... + CiCn-i-1 + ...
//
// Bottom-up
func numTrees_96(n int) int {
	dp := make([]int, n+1)

	dp[0] = 1
	dp[1] = 1
	for i := 2; i <= n; i++ {
		//think i is the right tree
		//j is the left tree?!!
		for j := 1; j <= i; j++ {
			dp[i] += dp[j-1] * dp[i-j]
		}
	}
	return dp[n]
}

/**
https://www.youtube.com/watch?v=-rlQCg_TJac&ab_channel=XinghaoHuang

split n node to left and right tree
left could be: i:= 0, 1, 2, 3...
right is: n-1-i  #-1 is the root

Top-down
*/
func numTrees_96_v2(n int) int {
	if n <= 1 {
		return 1
	}
	cnt := 0
	for i := 0; i < n; i++ {
		//left is i:=0,1,2, right := n-1-i, ..
		//the combination is the product
		cnt += numTrees_96_v2(i) * numTrees_95_v2(n-1-i)
	}

	return cnt
}

// And one of the more convenient forms for calculating Catalan Number is defined as follows:
func numTrees_95_v2(n int) int {
	c := 1
	for i := 0; i < n; i++ {
		c = c * 2 * (2*i + 1) / (i + 2)
	}
	return c
}

//https://leetcode.com/problems/unique-binary-search-trees-ii/submissions/
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
// O(n!)
// LC241
func generateTrees_95(n int) []*TreeNode {
	if n == 0 {
		return nil
	}
	return build(1, n)
}

func build(start, end int) []*TreeNode {
	if start > end {
		return []*TreeNode{nil}
	}

	list := []*TreeNode{}
	for i := start; i <= end; i++ {
		lTree := build(start, i-1)
		rTree := build(i+1, end)

		for _, l := range lTree {
			for _, r := range rTree {
				node := &TreeNode{
					Val:   i,
					Left:  l,
					Right: r,
				}
				list = append(list, node)
			} //for
		} //for
	}
	return list
}

/*
https://leetcode.com/problems/minimum-time-to-build-blocks/
MinTimeToBuildBlocks1199
*/
func minBuildTime_1199(blocks []int, split int) int {
	dp := make([][]int, len(blocks)+1)
	for i := range dp {
		dp[i] = make([]int, len(blocks)+1)
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}
	sort.Ints(blocks)
	var dfs func(idx int, worker int) int
	//only 1 worker at the beginning
	dfs = func(idx int, worker int) int {
		if worker >= idx+1 {
			return blocks[idx] //enough worker, take the biggest block
		}
		//this recurrence has been processed already
		if dp[worker][idx] != -1 {
			return dp[worker][idx]
		}
		//not enough worker, split; continue until there is enough workers
		doSplit := split + dfs(idx, worker*2)
		if worker == 1 {
			return doSplit
		}
		//1 worker, or #ofworker is less than blocks, but there are at least 2 workers!!
		noSplit := Max(blocks[idx], dfs(idx-1, worker-1))

		dp[worker][idx] = Min(doSplit, noSplit)
		return dp[worker][idx]
	}
	return dfs(len(blocks)-1, 1)
}
