package main

// https://leetcode.com/problems/number-of-unique-good-subsequences/#:~:text=You%20are%20given,and%20%271%27s.
// You are given a binary string binary. A subsequence of binary is considered good if it is not empty and has no
// leading zeros (with the exception of "0").
//
// Find the number of unique good subsequences of binary.
//
// For example, if binary = "001", then all the good subsequences are ["0", "0", "1"], so the unique good subsequences
// are "0" and "1". Note that subsequences "00", "01", and "001" are not good because they have leading zeros.
// Return the number of unique good subsequences of binary. Since the answer may be very large, return it modulo 109 + 7.
//
// A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing
// the order of the remaining elements.
//
//
//
// Example 1:
//
// Input: binary = "001"
// Output: 2
// Explanation: The good subsequences of binary are ["0", "0", "1"].
// The unique good subsequences are "0" and "1".
// Example 2:
//
// Input: binary = "11"
// Output: 2
// Explanation: The good subsequences of binary are ["1", "1", "11"].
// The unique good subsequences are "1" and "11".
// Example 3:
//
// Input: binary = "101"
// Output: 5
// Explanation: The good subsequences of binary are ["1", "0", "1", "10", "11", "101"].
// The unique good subsequences are "0", "1", "10", "11", and "101".
//
//
// Constraints:
//
// 1 <= binary.length <= 10^5
// binary consists of only '0's and '1's.
//
// distinct_subsequences_ii_940.go

//   		101
// 1 ->  1
// 0 ->  1, 0, 10
// 1 ->  11, 01(x), 101, 1(x)
// total 5: 1, 0, 10, 11, 101
//
//   		1011
// 1 ->  1
// 0 ->  1, 0, 10
// 1 ->  11, 01(x), 101, 1(x)
// 1 ->  111, 1011,
// total 7: 1, 0, 10, 11, 101, 111, 1011
//
// https://www.youtube.com/watch?v=nSPyO93ZEek&t=1265s&ab_channel=HuifengGuan
// https://github.com/wisdompeak/LeetCode/blob/master/Dynamic_Programming/1987.Number-of-Unique-Good-Subsequences/1987.Number-of-Unique-Good-Subsequences_v1.cpp
func numberOfUniqueGoodSubsequences(s string) int {

	length := len(s)
	s = "@" + s
	dp := make([]int, len(s))

	idx0 := 1
	for idx0 < len(s) && s[idx0] == '0' {
		idx0++
	}
	if idx0 == len(s) {
		return 1 //all '0'
	}
	dp[idx0] = 1 //the initial '1'

	//previous 0's idx, and 1's idx, a variable each is enough!!
	last0, last1 := 0, 0 //initially, no 0, and no 1 yet
	//loop from the 2nd '1'
	for i := idx0 + 1; i <= length; i++ {
		j := last1
		if s[i] == '0' {
			j = last0 //leading '0' handling
		}

		cur := dp[i-1] * 2 //cur subseq can be created with previous one
		if j >= 1 {
			cur -= dp[j-1]
		}
		dp[i] = (cur + MOD) % MOD
		if s[i] == '0' {
			last0 = i
		} else {
			last1 = i
		}
	}

	if idx0 > 1 || last0 > 0 {
		return dp[length] + 1 //'0' is a subseq
	}
	return dp[length]
}
