package main

/*
https://leetcode.com/problems/number-of-ways-to-stay-in-the-same-place-after-some-steps/
You have a pointer at index 0 in an array of size arrLen. At each step, you can move 1 position to
the left, 1 position to the right in the array, or stay in the same place (The pointer should not be
placed outside the array at any time).

Given two integers steps and arrLen, return the number of ways such that your pointer still at index 0
after exactly steps steps. Since the answer may be too large, return it modulo 10^9 + 7.

Example 1:

Input: steps = 3, arrLen = 2
Output: 4
Explanation: There are 4 different ways to stay at index 0 after 3 steps.
Right, Left, Stay
Stay, Right, Left
Right, Stay, Left
Stay, Stay, Stay

Example 2:

Input: steps = 2, arrLen = 4
Output: 2
Explanation: There are 2 different ways to stay at index 0 after 2 steps
Right, Left
Stay, Stay

Example 3:

Input: steps = 4, arrLen = 2
Output: 8


Constraints:

- 1 <= steps <= 500
- 1 <= arrLen <= 10^6

*/

func numWays_1269(steps int, arrLen int) int {
	/* we want to check if pointer == 0, when idx == 0
		if steps > arrLen ?
	*/
	//dp[i][j] i-th step and at j position how many ways to reach idx==0
	dp := make([][]int, steps+1)
	for i := range dp {
		//TODO, prevent stepping away too far? not difference to '/2' or not, but use less space after '/2'
		dp[i] = make([]int, steps/2+1)
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}
	var dfs func(idx int, steps int) int
	dfs = func(idx int, steps int) int {
		if idx == 0 && steps == 0 {
			return 1
		}
		if idx < 0 || idx >= arrLen || steps == 0 || idx > steps {
			// idx > steps: jumped too far, can never go back to idx==0
			return 0
		}

		if dp[steps][idx] != -1 { //had done it before
			return dp[steps][idx]
		}

		var res int
		//dfs
		res = (res + dfs(idx-1, steps-1)) % MOD //step to left
		res = (res + dfs(idx+1, steps-1)) % MOD //step to right
		res = (res + dfs(idx, steps-1)) % MOD   //stay

		dp[steps][idx] = res
		return res
	}

	return dfs(0, steps)
}
