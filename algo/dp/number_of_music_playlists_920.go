package main

/*
https://leetcode.com/problems/number-of-music-playlists/
Your music player contains n different songs. You want to listen to goal songs (not necessarily different) during your
trip. To avoid boredom, you will create a playlist so that:

 - Every song is played at least once.
 - A song can only be played again only if k other songs have been played.

Given n, goal, and k, return the number of possible playlists that you can create. Since the answer can be very large,
return it modulo 10^9 + 7.

--------
Your music player contains `N` different songs and she wants to listen to `L` (not necessarily different) songs during
your trip.  You create a playlist so that:
	- Every song is played at least once
	- A song can only be played again only if K other songs have been played
Return the number of possible playlists.  As the answer can be very large, return it modulo 10^9 + 7.
---------

Example 1:

Input: n = 3, goal = 3, k = 1
Output: 6
Explanation: There are 6 possible playlists: [1, 2, 3], [1, 3, 2], [2, 1, 3], [2, 3, 1], [3, 1, 2], and [3, 2, 1].

Example 2:

Input: n = 2, goal = 3, k = 0
Output: 6
Explanation: There are 6 possible playlists: [1, 1, 2], [1, 2, 1], [2, 1, 1], [2, 2, 1], [2, 1, 2], and [1, 2, 2].

Example 3:

Input: n = 2, goal = 3, k = 1
Output: 2
Explanation: There are 2 possible playlists: [1, 2, 1] and [2, 1, 2].


Constraints:

 - 0 <= k < n <= goal <= 100


 - n songs available, you want to listen to at least n songs, n <= goal   -> every song should be played at least once
 - say, there are 10 songs, and we want to create playlist of 20 songs? how many playlists can be created?
 [https://www.youtube.com/watch?v=uvjn6UDXVrY&ab_channel=RoshanSrivastava]
 - to form a playlist: it is the choice of selecting a song which is either selected(old) or not-selected(new) before.
 - if select an old song:
	- len_of_the_playlist: len + 1, num_of_unique_songs: num;
		- select a song from the existing num, and take k into account:  num - k, num could be < k at first, so,
		  max(0, num - k)
		-
 - if select an new song:
	- len_of_the_playlist: len + 1, num_of_unique_songs: num + 1
		- select a song from (n - num) available songs
 - dp[i][j] -> we have i songs selected, of which j num of songs are unique

 [other ]
 https://leetcode.com/problems/number-of-music-playlists/discuss/529374/who's-she
 - the key part of this problem is to figure out what is the sub problem. in this case the sub problem is to use j songs out
   of N songs to fill i slots (i < L). the sub problem is not using a total of j songs to fill i slots. realizing this,
   then we can start the induction process:
 - for problem d[i][j] it means filling i slots, using j out of N songs:
   - the last slot could either be an existing song, in this case we need to look at d[i-1][j] and we have j-k choices
     for the last slot
   - or the last slot could be a new song from the remaining unused songs, then we need to look at d[i-1][j-1] and we
     have N - j + 1 choices for the last slot.
 - then we get d[i][j] = d[i-1][j-1] * (N - j + 1) + d[i-1][j] * (j - k)

https://www.cnblogs.com/grandyang/p/11741490.html
https://leetcode.com/problems/number-of-music-playlists/discuss/180338/DP-solution-that-is-Easy-to-understand


	- n > g is wrong
	- a song for the playlist could be 1) an existing song in the playlist or
                                       2) a new song was never selected from n.
	-

*/

func numMusicPlaylists(n int, goal int, k int) int {
	dp := make([][]int, goal+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}
	dp[0][0] = 1

	for i := 1; i <= goal; i++ {
		for j := 1; j <= n; j++ {
			//case 1, select a unique song, j-1 to get the previous count, current is j
			dp[i][j] = (dp[i-1][j-1] * (n - (j - 1))) % MOD
			if j > k { //j is the unique songs selected so far
				//plus case 2, select a existing song, but take k into account
				dp[i][j] = (dp[i][j] + (dp[i-1][j]*(j-k))%MOD) % MOD
			}
		}
	}
	return dp[goal][n]
}
