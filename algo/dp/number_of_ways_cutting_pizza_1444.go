package main

/**
https://leetcode.com/problems/number-of-ways-of-cutting-a-pizza/
NumWaysCuttingPizza1444 (java version)
*/

// https://leetcode.com/problems/number-of-ways-of-cutting-a-pizza/discuss/884460/Java-DFS-%2BMemorization-Easy-to-Understand-with-explanation
func ways(pizza []string, k int) int {
	return cut(pizza, k, 0, len(pizza)-1, 0, len(pizza[0])-1, make(map[string]int)) % MOD
}

func cut(pi []string, k int, sr, er int, sc, ec int, memo map[string]int) int {
	if sr > er || sc > ec || k <= 0 {
		return 0
	}

	key := string(sr+'0') + string(er+'0') + string(sc+'0') + string(ec+'0') + "-" + string(k+'0')
	if _, exist := memo[key]; exist {
		return memo[key]
	}

	var res int
	if k == 1 {//check if the last piece is valid
		res = 0
		if isValid(pi, sr, er, sc, ec) {
			res = 1
		}
	} else {
		//cut horizontally
		for r := sr; r <= er; r++ {
			if isValid(pi, sr, r, sc, ec) { //check rows between sr <= r
				res += cut(pi, k-1, r+1, er, sc, ec, memo) % MOD //try next row
			}
		}
		//cur vertically
		for c := sc; c <= ec; c++ {
			if isValid(pi, sr, er, sc, c) { //check cols between sc <= c
				res += cut(pi, k-1, sr, er, c+1, ec, memo) % MOD //try next col
			}
		}
	} //

	memo[key] = res
	return res
}

func isValid(pi []string, sr, er int, sc, ec int) bool {
	for r := sr; r <= er; r++ {
		for c := sc; c <= ec; c++ {
			if pi[r][c] == 'A' {
				return true
			}
		} //for
	}
	return false
}
