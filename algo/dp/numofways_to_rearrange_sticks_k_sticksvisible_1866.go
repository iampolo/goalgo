package main

// https://leetcode.com/problems/number-of-ways-to-rearrange-sticks-with-k-sticks-visible/
//
//
// There are n uniquely-sized sticks whose lengths are integers from 1 to n. You want to arrange the sticks such that
// exactly k sticks are visible from the left. A stick is visible from the left if there are no longer sticks to the left of it.
//
// - For example, if the sticks are arranged [1,3,2,5,4], then the sticks with lengths 1, 3, and 5 are visible from the left.
//
// Given n and k, return the number of such arrangements. Since the answer may be large, return it modulo 10^9 + 7.
//
//
// Example 1:
//
// Input: n = 3, k = 2
// Output: 3
// Explanation: [1,3,2], [2,3,1], and [2,1,3] are the only arrangements such that exactly 2 sticks are visible.
// The visible sticks are underlined.
//
// Example 2:
//
// Input: n = 5, k = 5
// Output: 1
// Explanation: [1,2,3,4,5] is the only arrangement such that all 5 sticks are visible.
// The visible sticks are underlined.
//
// Example 3:
//
// Input: n = 20, k = 11
// Output: 647427950
// Explanation: There are 647427950 (mod 10^9 + 7) ways to rearrange the sticks such that exactly 11 sticks are visible.
//
//
// Constraints:
//
// 1 <= n <= 1000
// 1 <= k <= n
//
//
// ******************************************************************************************************
// Consider base cases:
//
//a. if n == k: there is only one way to rearrange sticks
//b. if k == 0: there are no more way to rearrange sticks
//
//Then
//
//There are 2 ways to add a stick:
//
//- insert the shortest stick in front of all of the sticks it would be visible. So there are k -1 sticks left to insert
//  in n-1 places, thus self.rearrangeSticks(n - 1, k - 1) as there is only one place to insert
//- insert the shorted stick at any position except the beginning, it would not be visible. So there are k more sticks
//  left to insert in n-1 places, there are in total n-1 way to do it => self.rearrangeSticks(n - 1, k) * (n - 1)
//
//  https://cpexplanations.blogspot.com/2021/05/1866-number-of-ways-to-rearrange-sticks.html
//  https://leetcode.com/problems/number-of-ways-to-rearrange-sticks-with-k-sticks-visible/discuss/1211070/C%2B%2B-Detailed-Explanation-with-Thought-Process-or-DP
//

func rearrangeSticks_bottom_up(n int, k int) int {
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, k+1)
	}
	dp[0][0] = 1

	for i := 1; i <= n; i++ {
		for j := 1; j <= k; j++ { //to be left-visible
			dp[i][j] = (dp[i-1][j-1] + ((i-1)*dp[i-1][j])%MOD) % MOD
		}
	}
	return dp[n][k]
}

func rearrangeSticks(n int, k int) int {
	//dp[#ofstick][tobeleftvisible]
	//
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, k+1)
		for j := 0; j < len(dp[0]); j++ {
			dp[i][j] = -1
		}
	}

	return arrange(n, k, dp)
}

func arrange(n, k int, dp [][]int) int {
	if k <= 0 || k > n {
		return 0
	}

	if n <= 2 { //1 or 2 sticks, there is only one way.
		return 1
	}

	if dp[n][k] != -1 {
		return dp[n][k]
	}
	var res int

	res = (res + arrange(n-1, k-1, dp)) % MOD     //case 1, place the longest on the rightmost
	res = (res + (n-1)*arrange(n-1, k, dp)) % MOD //case 2: make a shorter stick invisible: put at any where but the 1st place
	//  so, k is not changed
	dp[n][k] = res
	return res
}
