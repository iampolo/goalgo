package main

import (
	"fmt"
	"math"
)

/**
https://leetcode.com/problems/out-of-boundary-paths/


*/

// O(4^n)
// need to record the r, c and maxMove to prune
func findPaths_bf(m int, n int, maxMove int, r int, c int) int {
	if r < 0 || c < 0 || r >= m || c >= n {
		return 1
	}

	if maxMove == 0 {
		return 0
	}

	return findPaths_bf(m, n, maxMove-1, r+1, c) +
		findPaths_bf(m, n, maxMove-1, r, c+1) +
		findPaths_bf(m, n, maxMove-1, r, c-1) +
		findPaths_bf(m, n, maxMove-1, r-1, c)
}

func main_576() {
	m, n, max, r, c := 2, 2, 2, 0, 0
	fmt.Println(findPaths(m, n, max, r, c))
}

var dir = [...]int{0, -1, 0, 1, 0} //fixed sized array instead of slice

/********************************************************************************************************
[i][j][k] is used to store the number of possible moves leading to a path out of the boundary if
the current position is given by the indices (i,j) and number of moves left is k.
*/
func findPaths(m int, n int, maxMove int, startRow int, startColumn int) int {
	//[r][c][maxMove]   - 3D
	dp := make([][][]int, m)
	for i := 0; i < m; i++ {
		dp[i] = make([][]int, n)
		for j := 0; j < n; j++ {
			dp[i][j] = make([]int, maxMove+1)
			for k := 0; k < maxMove+1; k++ {
				dp[i][j][k] = math.MinInt32
			}
		}
	}

	return findPaths_dfs(m, n, maxMove, startRow, startColumn, dp)
}

func findPaths_dfs(m, n int, t, r, c int, dp [][][]int) int {

	if r >= m || c >= n || r < 0 || c < 0 {
		return 1
	}
	if t == 0 {
		return 0
	}

	if dp[r][c][t] >= 0 {
		return dp[r][c][t]
	}

	dp[r][c][t] = ((findPaths_dfs(m, n, t-1, r+1, c, dp)+
		findPaths_dfs(m, n, t-1, r, c+1, dp))%MOD +
		(findPaths_dfs(m, n, t-1, r, c-1, dp)+
			findPaths_dfs(m, n, t-1, r-1, c, dp))%MOD) % MOD

	return dp[r][c][t]
}
