package main

// https://leetcode.com/problems/paint-fence/

func numWays(n int, k int) int {
	memo = make(map[int]int)
	return calcWays(n, k)
}

var memo map[int]int

func calcWays(n, k int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return k
	}
	if n == 2 {
		return k * k
	}
	if v, ok := memo[n]; ok {
		return v
	}
	// k-1 available is level, one color is used at n-1 fence
	memo[n] = (k - 1) * (calcWays(n-1, k) + calcWays(n-2, k))
	return memo[n]
}

// TODO
func numWays_v2(n, k int) int {
	if n == 0 {
		return 0
	}
	if n == 1 {
		return k
	}

	oneFence := k     //the 1st fence
	twoFence := k * k //the 2nd fence

	//start with the 3rd fence
	for i := 3; i <= n; i++ {
		cur := (k - 1) * (oneFence + twoFence)
		twoFence = oneFence
		oneFence = cur
	}
	return oneFence
}
