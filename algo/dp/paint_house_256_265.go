package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

func minCost(costs [][]int) int {
	if len(costs) == 0 {
		return 0
	}

	for i := 1; i < len(costs); i++ {
		costs[i][0] += Min(costs[i-1][1], costs[i-1][2])
		costs[i][1] += Min(costs[i-1][0], costs[i-1][2])
		costs[i][2] += Min(costs[i-1][0], costs[i-1][1])
	}

	length := len(costs)
	return Min(costs[length-1][0], Min(costs[length-1][1], costs[length-1][2]))
}

// https://leetcode.com/problems/paint-house-ii/discuss/69492/AC-Java-solution-without-extra-space
// https://leetcode.com/problems/paint-house-ii/discuss/69509/easiest-o1-space-java-solution
func minCostII(costs [][]int) int {
	if len(costs) == 0 {
		return 0
	}

	//saved the 1st and 2nd best color
	minc1, minc2 := 0, 0 //previous row best two
	idx1 := -1           //best cost selected at the previous : not necessary the best in that row

	//paint house with every row of the available colors
	for i := 0; i < len(costs); i++ {
		m1, m2 := math.MaxInt32, math.MaxInt32
		i1 := -1
		//find a color that is not used by previous house,
		for c := 0; c < len(costs[0]); c++ {
			curCost := costs[i][c]
			/* consider each cost in the row, combine with previous rows' selected cost, and find the best one
				0 	4 	5
			 	2	3 	4
			*/
			if c == idx1 { //column conflict
				curCost += minc2 //use the 2nd best from the previous
			} else { //use the best from the previous row
				curCost += minc1
			}

			//save the 1st and 2nd best color of the current row
			if curCost < m1 { //found better cost
				m2 = m1
				m1 = curCost
				i1 = c //each loop must get one
			} else if curCost < m2 { //found the one better than the 2nd cost
				m2 = curCost
			}
		} //

		//save for the next house
		minc1, minc2, idx1 = m1, m2, i1
	}
	return minc1
}
