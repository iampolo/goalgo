package main

import . "gitlab.com/iampolo/goalgo/algo/util"

func minCut(s string) int {
	//default to max length-1 : the max cut
	return findMinCut(s, 0, len(s)-1)
}

// O(2^n * n)
func findMinCut(s string, l, r int) int {
	if l == r || isPalindrome(s, l, r) {
		//the end or s is already a palindrome
		return 0 //no more cut
	}

	mCut := len(s)-1
	//i fixes the starting point at this level
	//the first recursion is the max cut!!, then move the fix point to right and do the recursion.
	for i := l; i <= r; i++ {
		if isPalindrome(s, l, i) {
			//continue to cut
			mCut = Min(mCut, 1+findMinCut(s, i+1, r))
		}
	}
	return mCut
}

func isPalindrome(s string, l, r int) bool {
	for l < r {
		if s[l] != s[r] {
			return false
		}
		l++
		r--
	}
	return true
}
