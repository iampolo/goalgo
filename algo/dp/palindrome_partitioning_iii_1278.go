package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

/*
https://leetcode.com/problems/palindrome-partitioning-iii/

You are given a string s containing lowercase letters and an integer k. You need to :

First, change some characters of s to other lowercase English letters.
Then divide s into k non-empty disjoint substrings such that each substring is a palindrome.
Return the minimal number of characters that you need to change to divide the string.



Example 1:

Input: s = "abc", k = 2
Output: 1
Explanation: You can split the string into "ab" and "c", and change 1 character in "ab" to make it palindrome.

Example 2:

Input: s = "aabbc", k = 3
Output: 0
Explanation: You can split the string into "aa", "bb" and "c", all of them are palindrome.

Example 3:

Input: s = "leetcode", k = 8
Output: 0


Constraints:

* 1 <= k <= s.length <= 100.
* s only contains lowercase English letters.

PalindromePartitioningIII1278
*/

/*
https://leetcode.jp/leetcode-1278-palindrome-partitioning-iii-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/

*/
func palindromePartition(s string, k int) int {
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, k+1)
		for j := 0; j < len(dp[j]); j++ {
			dp[i][j] = -1
		}
	}

	var dfs func(s string, idx int, k int) int
	dfs = func(s string, idx int, k int) int {
		if len(s)-idx <= k {
			//not enough chars
			return 0
		}

		if dp[idx][k] != -1 {
			return dp[idx][k] //processed before
		}

		start := len(s) - 1 //k==1, only one partition is needed
		if k > 1 {
			start = idx
		}

		res := math.MaxInt32
		for i := start; i <= len(s)-k; i++ {
			//based on idx, we try all the i from idx
			l, r := idx, i //set up the boundary for a substring
			var cnt int
			for l < r {
				if s[l] != s[r] {
					cnt++
				}
				l, r = l+1, r-1
			}

			if cnt > res {//pruning
				continue
			}

			res = Min(res, cnt+dfs(s, i+1, k-1))
		}

		dp[idx][k] = res
		return res
	}

	return dfs(s, 0, k)
}
