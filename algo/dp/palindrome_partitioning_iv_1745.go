package main

/*
https://zhuanlan.zhihu.com/p/351460250
https://leetcode.com/problems/palindrome-partitioning-iv/

Given a string s, return true if it is possible to split the string s into three non-empty palindromic substrings.
Otherwise, return false.

A string is said to be palindrome if it the same string when reversed.


Example 1:

Input: s = "abcbdd"
Output: true
Explanation: "abcbdd" = "a" + "bcb" + "dd", and all three substrings are palindromes.

Example 2:

Input: s = "bcbddxy"
Output: false
Explanation: s cannot be split into 3 palindromes.


Constraints:

* 3 <= s.length <= 2000
* s consists only of lowercase English letters.

*/

/*
	true: [0, i], [i+1, j], [j+1, n-1]
*/
func checkPartitioning_bf(s string) bool {
	if len(s) < 3 {
		return false
	}

	for i := 0; i < len(s)-2; i++ {
		for j := i + 1; j < len(s)-1; j++ {
			if isPalindrome(s, 0, i) &&
				isPalindrome(s, i+1, j) &&
				isPalindrome(s, j+1, len(s)-1) {
				return true
			}
		}
	}
	return false
}

func checkPartitioning(s string) bool {
	dp := make([][]bool, len(s))
	for i := range dp {
		dp[i] = make([]bool, len(s))
	}
	// true if it is substring dp[i][j]: [i..j] is a palindrome
	for j := 0; j < len(s); j++ {
		for i := j; i >= 0; i-- {
			l := j - i + 1
			if l == 1 {
				dp[i][j] = true
			} else if l == 2 {
				dp[i][j] = s[i] == s[j]
			} else {
				dp[i][j] = s[i] == s[j] && dp[i+1][j-1]
			}
		}
	}

	//split the string
	for i := 0; i < len(s)-1; i++ {
		for j := i + 2; j < len(s); j++ {
			if dp[0][i] && dp[i+1][j-1] && dp[j][len(s)-1] {
				return true
			}
		}
	}

	return false
}
