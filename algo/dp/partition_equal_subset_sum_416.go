package main

import (
	"strconv"
)

//https://leetcode.com/problems/partition-equal-subset-sum/

var exist struct{}

func canPartition(nums []int) bool {
	var total int
	for _, n := range nums {
		total += n
	}

	if total%2 != 0 {
		return false
	}
	target := total / 2
	vis := make(map[string]struct{})

	var dfs func(nums []int, idx int, cur int) bool
	dfs = func(nums []int, idx int, cur int) bool {
		if cur == target {
			return true
		}
		key := strconv.Itoa(idx) + strconv.Itoa(cur)
		if idx == len(nums) || cur > target {
			vis[key] = exist //mark as not possible
			return false
		}

		if _, ok := vis[key]; ok {
			return false //already process and didn't work
		}

		found := dfs(nums, idx+1, cur+nums[idx]) ||
			dfs(nums, idx+1, cur)
		if !found {
			vis[key] = exist //mark as not possible
		}
		return found
	}

	res := dfs(nums, 0, 0)
	return res
}

/*
https://leetcode.jp/leetcode-416-partition-equal-subset-sum%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
*/
func canPartitionBottomUp_bottomup_v2(nums []int) bool {
	var sum int
	for _, v := range nums {
		sum += v
	}
	if sum%2 != 0 {
		return false
	}

	target := sum / 2
	dp := make([]bool, target+1)
	dp[0] = true

	algo := func() bool {
		for i := 1; i <= len(nums); i++ {
			//build up dp array for target(sum)
			for j := target; j >= nums[i-1]; j-- {
				dp[j] = dp[j] || dp[j-nums[i-1]] //how to build dp[j], using numbers from larger to smaller
			}
		}
		return dp[target]
	}

	_ = func() bool {
		for i := 0; i < len(nums); i++ {
			for j := target - nums[i]; j >= 0; j-- {
				if !dp[j] {
					continue
				}
				dp[j+nums[i]] = true
				if dp[target] {
					return true
				}
			}
		}
		return dp[target]
	}
	return algo()
}

//

func canPartitionBottomUp(nums []int) bool {
	var total int
	for _, v := range nums {
		total += v
	}
	if total%2 != 0 {
		return false
	}
	target := total / 2
	dp := make([][]bool, len(nums)+1)
	for i := range dp {
		dp[i] = make([]bool, target+1)
		dp[i][0] = true //0 number is used, sum to 0 is always true
	}

	for i := 1; i <= len(nums); i++ {
		for j := 1; j <= target; j++ {
			//j is the sum of the first i-1 numbers(not including i),
			//so we should check at which nums can be used
			dp[i][j] = dp[i-1][j] //not taking nums[i]
			if j >= nums[i-1] {   //check if it is ok to take this nums[i]
				//curr i_j depends on the previous cases: two cases
				dp[i][j] = dp[i][j] || dp[i-1][j-nums[i-1]]
			}
		} //for
	}
	return dp[len(nums)][target]
}
