package main

import "math"

// https://leetcode.com/problems/perfect-squares/
func numSquares_dp(n int) int {
	//dp[i] is how many factors their squares sum up to number i
	dp := make([]int, n+1)
	dp[0] = 0 //base case for dp, dp[0]+1 is a default for any case.

	for i := 1; i <= n; i++ {
		//i represent the target number
		dp[i] = i //default to the number is 1 needed

		//find better combination
		//try from 1 .. up to i, any square is better than use 1?
		for j := 1; j*j <= i; j++ {
			dp[i] = min(dp[i-j*j]+1, dp[i])
		}
	}
	return dp[n]
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func numSquares_recur_tle(n int) int {
	//split n to smaller and smaller with recursion
	//count the # of factor from the lower level
	var dfs func(n int, bound int) int
	dfs = func(n int, bound int) int {
		if n <= 0 {
			return 0
		}
		mCnt := dfs(n-1*1, bound)
		//split by 1, the try to increase the factor
		for i := 2; i <= bound; i++ {
			if n >= i*i {
				//at lower level, it is 2, then min = 0, could be bette then split by 1
				mCnt = min(mCnt, dfs(n-i*i, bound))
			}
		}

		return mCnt + 1
	}

	opt := int(math.Sqrt(float64(n)))
	return dfs(n, opt)
}
