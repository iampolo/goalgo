package main

import . "gitlab.com/iampolo/goalgo/algo/util"

//https://leetcode.com/problems/profitable-schemes/
//number_of_music_playlists_920.go

/*
There is a group of n members, and a list of various crimes they could commit. The ith crime generates a profit[i]
and requires group[i] members to participate in it. If a member participates in one crime, that member can't
participate in another crime.

Let's call a profitable scheme any subset of these crimes that generates at least minProfit profit, and the total
number of members participating in that subset of crimes is at most n.

Return the number of schemes that can be chosen. Since the answer may be very large, return it modulo 10^9 + 7.


Example 1:

Input: n = 5, minProfit = 3, group = [2,2], profit = [2,3]
Output: 2
Explanation: To make a profit of at least 3, the group could either commit crimes 0 and 1, or just crime 1.
In total, there are 2 schemes.

Example 2:

Input: n = 10, minProfit = 5, group = [2,3,5], profit = [6,7,8]
Output: 7
Explanation: To make a profit of at least 5, the group could commit any crimes, as long as they commit one.
There are 7 possible schemes: (0), (1), (2), (0,1), (0,2), (1,2), and (0,1,2).


Constraints:

 - 1 <= n <= 100
 - 0 <= minProfit <= 100
 - 1 <= group.length <= 100
 - 1 <= group[i] <= 100
 - profit.length == group.length
 - 0 <= profit[i] <= 100
*/

// https://leetcode.com/problems/profitable-schemes/discuss/157099/Java-original-3d-to-2d-DP-solution
// 0/1 knapsack

// https://www.cnblogs.com/grandyang/p/11108205.html

func profitableSchemes_2D(n int, minProfit int, group []int, profit []int) int {

	dp := make([][]int, n+1) //# of crimes
	for i := range dp {
		dp[i] = make([]int, minProfit+1)
	}
	dp[0][0] = 1
	for i := 1; i <= len(group); i++ {
		g, p := group[i-1], profit[i-1] //member needed and profit can be obtained.
		for j := n; j >= g; j-- {
			for k := minProfit; k >= 0; k-- {
				cur := Max(0, k-p)
				dp[j][k] = (dp[j][k] + dp[j-g][cur]) % MOD
			}
		} //for
	}
	var sum int
	for i := 0; i <= n; i++ {
		sum = (sum + dp[i][minProfit]) % MOD
	}
	return sum
}

func profitableSchemes(n int, minProfit int, group []int, profit []int) int {
	//the first k crimes with i members, and at least j profit
	dp := make([][][]int, len(group)+1) //# of crimes
	for i := range dp {
		dp[i] = make([][]int, n+1) //#of members
		for j := range dp[i] {
			dp[i][j] = make([]int, minProfit+1) //at least j profit
		}
	}
	dp[0][0][0] = 1 //base case to start with: no crime at all is a scheme

	for i := 1; i <= len(group); i++ {
		g, p := group[i-1], profit[i-1] //member needed and profit can be obtained.
		for j := 0; j <= n; j++ {
			for k := 0; k <= minProfit; k++ { //from 0 up to minProfile, we're building DP from smaller cases.

				dp[i][j][k] = dp[i-1][j][k]
				if j >= g { //we have enough member to commit...
					//# of scheme can be generated would be
					cur := Max(0, k-p)
					dp[i][j][k] = (dp[i][j][k] + dp[i-1][j-g][cur]) % MOD
				}
			}
		} //for
	}
	var sum int
	for i := 0; i <= n; i++ {
		sum = (sum + dp[len(group)][i][minProfit]) % MOD
	}
	return sum
}

// https://leetcode.com/problems/profitable-schemes/discuss/160435/DFS-%2B-Memory

func profitableSchemes_dfs(n int, minProfit int, group []int, profit []int) int {

	return -1
}

func dfs879(idx int, n, p int, group []int, profit []int) int {
	if idx == len(group) {
		return 0
	}

	var res int

	return res
}
