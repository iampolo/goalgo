package main

import . "gitlab.com/iampolo/goalgo/algo/util"

// https://leetcode.com/problems/remove-boxes/

func removeBoxes(boxes []int) int {
	n := len(boxes)
	memo := make([][][]int, n)
	for j := range memo {
		memo[j] = make([][]int, n)
		for i := range memo[j] {
			memo[j][i] = make([]int, n)
		}
	}

	return remove(boxes, 0, n-1, 0, memo)
}

func remove(boxes []int, i, j, k int, memo [][][]int) int {
	if i > j {
		return 0
	}

	if memo[i][j][k] > 0 {
		return memo[i][j][k]
	}

	i0, k0 := i, k
	for i+1 <= j && boxes[i] == boxes[i+1] {
		i++
		k++ //same color
	}

	//consume/remove this box immediately
	//take the k (aka:#ofsamecolor) brought from the previous level
	res := (k+1)*(k+1) + remove(boxes, i+1, j, 0, memo) //no more k

	//those boxes has the same color as i
	//there could be 1, 2 or ... many
	for m := i + 1; m <= j; m++ {
		if boxes[i] == boxes[m] { //will skip those diff colors until find one with same color

			//[i+1,m-1]:those differ colors are process in the next lvl
			res = Max(res, remove(boxes, i+1, m-1, 0, memo)+
				remove(boxes, m, j, k+1, memo)) //the remaining subarray are process in the next lvl with k+1
		}
	}
	memo[i0][j][k0] = res
	return res
}
