package main

import (
	"fmt"
	"strconv"
	"strings"
)

// https://leetcode.com/problems/rotated-digits/
//
//An integer x is a good if after rotating each digit individually by 180 degrees, we get a valid number that is
//different from x. Each digit must be rotated - we cannot choose to leave it alone.
//
//A number is valid if each digit remains a digit after rotation. For example:
//
// - 0, 1, and 8 rotate to themselves,
// - 2 and 5 rotate to each other (in this case they are rotated in a different direction, in other words,
//   2 or 5 gets mirrored),
// - 6 and 9 rotate to each other, and
// - the rest of the numbers do not rotate to any other number and become invalid.
//
//Given an integer n, return the number of good integers in the range [1, n].
//
//
//Example 1:
//
//Input: n = 10
//Output: 4
//Explanation: There are four good numbers in the range [1, 10] : 2, 5, 6, 9.
//Note that 1 and 10 are not good numbers, since they remain unchanged after rotating.
//
//Example 2:
//
//Input: n = 1
//Output: 0
//
//Example 3:
//
//Input: n = 2
//Output: 1
//
//
//Constraints:
//
// - 1 <= n <= 10^4
//
func rotatedDigits_dp(n int) int {
	//dp[i] == 0: invalid number
	//dp[i] == 1: rotatable but same number
	//dp[i] == 2: rotatable and diff number
	dp := make([]int, n+1)
	var cnt int
	for i := 0; i <= n; i++ {
		if i < 10 {
			if i == 0 || i == 1 || i == 8 {
				dp[i] = 1 //used to build the dp
			} else if i == 2 || i == 5 || i == 6 || i == 9 {
				dp[i] = 2
				cnt++
			}
		} else {
			left, right := dp[i/10], dp[i%10]
			if left == 1 && right == 1 {
				dp[i] = 1 //not a good number
			} else if left >= 1 && right >= 1 {
				dp[i] = 2
				cnt++
			}
		}
	}
	return cnt
}

func rotatedDigits(n int) int {
	//remains, skips := "2569", "347"
	//0, 1, 8 is good only when it is used with other different digits
	//and those digits have to be one of 2569
	//each digit must be rotated

	canRotate := func(n int) bool {
		// 11, 888 remain as false
		str := strconv.Itoa(n)
		//this check goes first
		if strings.Index(str, "3") >= 0 ||
			strings.Index(str, "4") >= 0 ||
			strings.Index(str, "7") >= 0 {
			return false
		}
		if strings.Index(str, "2") >= 0 ||
			strings.Index(str, "5") >= 0 ||
			strings.Index(str, "6") >= 0 ||
			strings.Index(str, "9") >= 0 {
			return true
		}
		return false
	}
	canRotate2 := func(n int) bool {
		// 11, 888 remain as false
		valid := false
		for n > 0 {
			switch n % 10 {
			case 2, 5, 6, 9:
				valid = true
			case 3, 4, 7:
				return false
			}
			n /= 10
		}
		return valid
	}
	fmt.Println(canRotate(35), canRotate2(35))
	var cnt int
	for i := 1; i <= n; i++ {
		if canRotate(i) {
			cnt++
		}
	}

	return cnt
}
