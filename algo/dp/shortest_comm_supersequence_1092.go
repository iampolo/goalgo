package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

/*
https://leetcode.com/problems/shortest-common-supersequence/

Given two strings str1 and str2, return the shortest string that has both str1 and str2 as subsequences.
If there are multiple valid strings, return any of them.

A string s is a subsequence of string t if deleting some number of characters from t (possibly 0) results
in the string s.



Example 1:

Input: str1 = "abac", str2 = "cab"
Output: "cabac"
Explanation:
str1 = "abac" is a subsequence of "cabac" because we can delete the first "c".
str2 = "cab" is a subsequence of "cabac" because we can delete the last "ac".
The answer provided is the shortest such string that satisfies these properties.

Example 2:

Input: str1 = "aaaaaaaa", str2 = "aaaaaaaa"
Output: "aaaaaaaa"


Constraints:

- 1 <= str1.length, str2.length <= 1000
- str1 and str2 consist of lowercase English letters.

longest_comm_subsequence_1143.go

*/

func shortestCommonSupersequence(str1 string, str2 string) string {
	dp := make([][]int, len(str1)+1)
	for i := range dp {
		dp[i] = make([]int, len(str2)+1)
	}

	//find LCS, dp[i][j]==the length of LCS up to s1[0...i] and s2[0...j]
	for i := 1; i < len(dp); i++ {
		for j := 1; j < len(dp[0]); j++ {
			if str1[i-1] == str2[j-1] {
				dp[i][j] = 1 + dp[i-1][j-1]
			} else {
				dp[i][j] = Min(dp[i-1][j], dp[i][j-1])
			}
		}
	} //

	for i := range dp {
		fmt.Println(dp[i])
	}

	i, j := len(str1), len(str2)
	length := i + j - dp[i][j] - 1
	res := make([]byte, length)
	idx := length - 1

	for i > 0 && j > 0 {
		if str1[i-1] == str2[j-1] {
			res[idx] = str1[i-1]
			i, idx, j = i-1, idx-1, j-1
		} else if dp[i-1][j] < dp[i][j-1] {
			res[idx] = str1[i-1]
			idx, i = idx-1, i-1
		} else {
			res[idx] = str2[j-1]
			idx, j = idx-1, j-1
		}
	}
	for j > 0 {
		res[idx] = str2[j]
		j, idx = j-1, idx-1
	}
	for i > 0 {
		res[idx] = str1[i]
		i, idx = i-1, idx-1
	}

	return string(res)
}
