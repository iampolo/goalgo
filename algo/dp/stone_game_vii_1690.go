package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/stone-game-vii/solution/
//MiniMax

func stoneGameVII(stones []int) int {
	memo := make([][]int, len(stones))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, len(stones))
		for j := 0; j < len(memo[i]); j++ {
			memo[i][j] = math.MaxInt32
		}
		fmt.Println(memo[i])
	}

	preSum := make([]int, len(stones)+1)
	for i := 0; i < len(stones); i++ {
		preSum[i+1] = preSum[i] + stones[i]
	}

	return dfs(0, len(stones)-1, preSum, memo, true)
}

// O(n^2)
func dfs(start, end int, preSum []int, memo [][]int, alice bool) int {
	if start == end {
		return 0
	}

	if memo[start][end] != math.MaxInt32 {
		return memo[start][end]
	}

	// for current person : alice or alice, he has two options:
	fromFirst := preSum[end+1] - preSum[start+1]
	fromLast := preSum[end] - preSum[start]

	diff := 0
	if alice {
		opt1 := dfs(start+1, end, preSum, memo, !alice) + fromFirst
		opt2 := dfs(start, end-1, preSum, memo, !alice) + fromLast
		diff = Max(opt1, //alice took the first
			opt2) //alice took the last
	} else {
		opt1 := dfs(start+1, end, preSum, memo, !alice) - fromFirst
		opt2 := dfs(start, end-1, preSum, memo, !alice) - fromLast
		diff = Min(opt1, opt2)
	}
	memo[start][end] = diff
	return diff
}

func _main() {
	stones := []int{5, 3, 1, 4, 2}
	stones = []int{5, 3, 1}
	fmt.Println(stoneGameVII(stones))
}
