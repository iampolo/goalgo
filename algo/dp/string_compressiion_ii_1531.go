package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/string-compression-ii
//Run-length encoding is a string compression method that works by replacing consecutive identical characters
//(repeated 2 or more times) with the concatenation of the character and the number marking the count of the
//characters (length of the run). For example, to compress the string "aabccc" we replace "aa" by "a2" and
//replace "ccc" by "c3". Thus the compressed string becomes "a2bc3".
//
//Notice that in this problem, we are not adding '1' after single characters.
//
//Given a string s and an integer k. You need to delete at most k characters from s such that the run-length
//encoded version of s has minimum length.
//
//Find the minimum length of the run-length encoded version of s after deleting at most k characters.
//
//
//
//Example 1:
//
//Input: s = "aaabcccd", k = 2
//Output: 4
//Explanation: Compressing s without deleting anything will give us "a3bc3d" of length 6. Deleting any of
//the characters 'a' or 'c' would at most decrease the length of the compressed string to 5, for instance
//delete 2 'a' then we will have s = "abcccd" which compressed is abc3d. Therefore, the optimal way is to
//delete 'b' and 'd', then the compressed version of s will be "a3c3" of length 4.
//
//Example 2:
//
//Input: s = "aabbaa", k = 2
//Output: 2
//Explanation: If we delete both 'b' characters, the resulting compressed string would be "a4" of length 2.
//
//Example 3:
//
//Input: s = "aaaaaaaaaaa", k = 0
//Output: 3
//Explanation: Since k is zero, we cannot delete anything. The compressed string is "a11" of length 3.
//
//
//Constraints:
//
//1 <= s.length <= 100
//0 <= k <= s.length
//s contains only lowercase English letters.
// https://leetcode.com/problems/string-compression-ii/discuss/756022/C%2B%2B-Top-Down-DP-with-explanation-64ms-short-and-clear
// https://leetcode.com/problems/string-compression-ii/discuss/755991/Java-Recursive-solution-50ms/
//
func getLengthOfOptimalCompression(s string, k int) int {
	//change length:
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, k+1)
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}
	return replace([]byte(s), 0, k, dp)
}

//if a string is 'aaa' -> 'a3'
//chars to remove: 3 - cntLen(3) -> 3-1 -> 2
func replace(s []byte, idx int, k int, dp [][]int) int {
	if k < 0 {
		return 0
	}
	if idx == len(s) || len(s)-idx <= k {
		return 0 //reaches end, or remove all remaining chars, 0 left
	}

	if dp[idx][k] != -1 {
		return dp[idx][k]
	}

	var freq [26]int
	var maxCnt int
	res := math.MaxInt32 //res: the string length while building
	for i := idx; i < len(s); i++ {
		pos := s[i] - 'a'
		freq[pos]++

		maxCnt = Max(maxCnt, freq[pos])
		if k >= i-idx+1-maxCnt {
			next := replace(s, i+1, k-(i-idx+1-maxCnt), dp)
			res = Min(res, 1+cntLen(maxCnt)+next)
		} //if
	}
	dp[idx][k] = res
	return res
}

//1-> 2 (a -> aa)
//9->10
//99->100
//we consider to remove a char or not removing it at all
//if 'aaaaaa..'-> a23, and k==23, k>=23, so 'a' can all be removed,
func cntLen(cnt int) int {
	if cnt == 1 {
		return 0
	} else if cnt < 10 {
		return 1
	} else if cnt < 100 {
		return 2
	}
	return 3
}

// https://leetcode.com/problems/string-compression-ii/discuss/755991/Java-Recursive-solution-50ms
