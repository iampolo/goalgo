package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/super-egg-drop/
//
//You are given k identical eggs and you have access to a building with n floors labeled from 1 to n.
//
//You know that there exists a floor f where 0 <= f <= n such that any egg dropped at a floor higher than f will break,
//and any egg dropped at or below floor f will not break.
//
//Each move, you may take an unbroken egg and drop it from any floor x (where 1 <= x <= n). If the egg breaks, you
//can no longer use it. However, if the egg does not break, you may reuse it in future moves.
//
//Return the minimum number of moves that you need to determine with certainty what the value of f is.
//  ->>>>>>> try all the moves and certain that minimum can be determined.
//
//Example 1:
//
//- Input: k = 1, n = 2
//- Output: 2
//- Explanation:
//- Drop the egg from floor 1. If it breaks, we know that f = 0.
//- Otherwise, drop the egg from floor 2. If it breaks, we know that f = 1.
//- If it does not break, then we know f = 2.
//- Hence, we need at minimum 2 moves to determine with certainty what the value of f is.
//
//Example 2:
//
//Input: k = 2, n = 6
//Output: 3
//
//Example 3:
//
//Input: k = 3, n = 14
//Output: 4
//
//
//Constraints:
//
//1 <= k <= 100
//1 <= n <= 10^4
//
//egg_drops_with_2_eggs_n_floor_1884.go
func superEggDrop_bs(k int, n int) int {
	dp := make([][]int, k+1)
	for i := range dp {
		dp[i] = make([]int, n+1) //
	}
	return dropEgg(k, n, dp)
}

func dropEgg(k, n int, memo [][]int) int {
	if n <= 2 {
		return n
	}
	if k == 1 { //one egg only, try n floor
		return n
	}
	if memo[k][n] > 0 {
		return memo[k][n]
	}
	lf, hf := 0, n
	//find how many move needs to take to find trial all the cases(reach to highest floor)

	minTrial := math.MaxInt32
	for lf < hf {
		mf := lf + (hf-lf)/2
		//try this mf
		broken := dropEgg(k-1, mf-1, memo)
		notBroken := dropEgg(k, n-mf, memo)

		minTrial = Min(minTrial, Max(broken, notBroken)+1)
		if broken == notBroken {
			break
		} else if broken < notBroken {
			lf = mf + 1
		} else {
			hf = mf
		}
	}
	memo[k][n] = minTrial
	return minTrial
}

//*****************************************************************************************
func superEggDrop(k int, n int) int {
	//TODO
	//1st dimension: # of moves spent
	dp := make([][]int, n+1)
	for i := range dp {
		//2nd dimension: the eggs broken/used
		dp[i] = make([]int, k+1)
	}
	//dp[m][k] = which floor we can reach to given m moves and k eggs
	var m int
	for dp[m][k] < n {
		m++
		for j := 1; j <= k; j++ {
			dp[m][j] = dp[m-1][j-1] + dp[m-1][j] + 1
			//if egg broken and if the egg is not broken + 1 (current trial)
		}
	}
	return m
}

// https://leetcode.jp/leetcode-887-super-egg-drop-%e9%b8%a1%e8%9b%8b%e6%8e%89%e8%90%bd%e9%97%ae%e9%a2%98%e5%88%86%e6%9e%90/