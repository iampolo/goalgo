package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
	"strconv"
)

//https://leetcode.com/problems/tallest-billboard/
/*
You are installing a billboard and want it to have the largest height. The billboard will have two steel supports,
one on each side. Each steel support must be an equal height.

You are given a collection of rods that can be welded together. For example, if you have rods of lengths 1, 2, and 3,
you can weld them together to make a support of length 6.

Return the largest possible height of your billboard installation. If you cannot support the billboard, return 0.

Example 1:

Input: rods = [1,2,3,6]
Output: 6
Explanation: We have two disjoint subsets {1,2,3} and {6}, which have the same sum = 6.

Example 2:

Input: rods = [1,2,3,4,5,6]
Output: 10
Explanation: We have two disjoint subsets {2,3,5} and {4,6}, which have the same sum = 10.
   ----->  dont' need to use all of the rods

Example 3:

Input: rods = [1,2]
Output: 0
Explanation: The billboard cannot be supported, so we return 0.


Constraints:

 - 1 <= rods.length <= 20
 - 1 <= rods[i] <= 1000
 - sum(rods[i]) <= 5000

*/

// PartitionEqualSubsetSum416
// https://www.cnblogs.com/grandyang/p/13517199.html

func tallestBillboard(rods []int) int {
	n := len(rods)
	bound := 5000 //the biggest possible difference - 2nd dimension

	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, bound+1)
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = -1000 //!!
		}
	}
	dp[0][0] = 0
	for i := 1; i <= n; i++ {
		for j := 0; j <= bound; j++ { //difference
			dp[i][j] = dp[i-1][j] //default

			if j >= rods[i-1] {
				dp[i][j] = Max(dp[i][j], dp[i-1][j-rods[i-1]]+rods[i-1])
			}
			if j < rods[i-1] { //case 3
				dp[i][j] = Max(dp[i][j], dp[i-1][rods[i-1]-j]+j)
			}
			if j+rods[i-1] <= bound { //case 2
				dp[i][j] = Max(dp[i][j], dp[i-1][j+rods[i-1]])
			}
		} //for
	}

	//fmt.Println(dp)

	return dp[n][0]
}

//https://leetcode.com/problems/tallest-billboard/discuss/833057/Java-4ms-clever-recursive-solution-O(rods.length)-space-no-memoization!

func tallestBillboard_dfs_TLE(rods []int) int {

	var max int
	var dfs func(rods []int, idx int, remain int, left, right int)
	dfs = func(rods []int, idx int, remain int, left, right int) {
		if left == right && left > max {
			max = left
		}

		//pruning:
		//- max*2 is the best length that is already achieved
		//- remaining is not enough to add to any support to make any difference
		if Abs(left-right) > remain || left+right+remain <= max*2 || idx < 0 {
			//if left+right+remain <= max*2 {
			//	fmt.Println("give up..", max, left, right, remain)
			//}
			return
		}
		//classic DFS
		remain -= rods[idx]
		dfs(rods, idx-1, remain, left+rods[idx], right)

		if left != right { //pruning as well
			dfs(rods, idx-1, remain, left, right+rods[idx])
		}

		dfs(rods, idx-1, remain, left, right)
	}

	var remain int
	for _, v := range rods {
		remain += v
	}
	sort.Ints(rods)
	dfs(rods, len(rods)-1, remain, 0, 0)

	return max
}

// https://leetcode.com/problems/tallest-billboard/discuss/204160/C%2B%2B-16-ms-DFS-%2B-memo

// Method 1
// PartitionEqualSubsetSum416
func tallestBillboard_dfs_memo(rods []int) int {

	dp := make(map[string]int)
	var dfs func(rods []int, i int, left, right int) int
	dfs = func(rods []int, i int, left, right int) int {
		if i == len(rods) {
			if left == right {
				return left
			}
			return -1
		}
		dpKey := strconv.Itoa(i) + strconv.Itoa(Abs(left-right))
		if _, ok := dp[dpKey]; ok {
			if dp[dpKey] == -1 {
				return -1
			}
			return Max(left, right) + dp[dpKey] //TODO
		}

		lres := dfs(rods, i+1, left+rods[i], right)
		rres := dfs(rods, i+1, left, right+rods[i])
		res := dfs(rods, i+1, left, right)
		max := Max(res, Max(lres, rres))

		if max == -1 { //meaning left and right didn't match
			dp[dpKey] = -1
		} else {
			dp[dpKey] = max - Max(left, right)  //TODO
		}
		return max
	}

	return dfs(rods, 0, 0, 0)
}

//https://leetcode.com/problems/tallest-billboard/discuss/203261/Java-knapsack-O(N*sum)
