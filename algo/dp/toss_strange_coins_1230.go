package main

//https://leetcode.com/problems/toss-strange-coins/
//
//You have some coins.  The i-th coin has a probability prob[i] of facing heads when tossed.
//
//Return the probability that the number of coins facing heads equals target if you toss every coin exactly once.
//
//
//
//Example 1:
//
//Input: prob = [0.4], target = 1
//Output: 0.40000
//
//Example 2:
//
//Input: prob = [0.5,0.5,0.5,0.5,0.5], target = 0
//Output: 0.03125
//
//
//Constraints:
//
// - 1 <= prob.length <= 1000
// - 0 <= prob[i] <= 1
// - 0 <= target <= prob.length
// - Answers will be accepted as correct if they are within 10^-5 of the correct answer.
//
func probabilityOfHeads(prob []float64, target int) float64 {
	//there could be more coins available to toss to meet the target, we need to find all the combinations of them. //TODO why
	//once the target is met, those other coins have not been tossed, we need to ensure the don't toss head (1-prob[i])

	var dfs func(idx int, target int, memo [][]float64) float64
	dfs = func(idx int, target int, memo [][]float64) float64 {
		if idx == len(prob) && target == 0 {
			//no more head is needed, and no more coins to toss
			return 1.0
		}
		if idx == len(prob) {
			return 0.0
		}
		if memo[idx][target] != -1 {
			return memo[idx][target]
		}

		var res float64
		if target > 0 { //toss this coin for head
			res += prob[idx] * dfs(idx+1, target-1, memo)
		}
		//target reached, make sure the coins are tossing tails
		//assume this coins is tail
		res += (1 - prob[idx]) * dfs(idx+1, target, memo)

		memo[idx][target] = res
		return res
	}

	memo := make([][]float64, len(prob)+1)
	for i := range memo {
		memo[i] = make([]float64, target+1)
		for j := 0; j < len(memo[i]); j++ {
			memo[i][j] = -1
		}
	}
	return dfs(0, target, memo)
}

//https://leetcode.com/problems/toss-strange-coins/discuss/408518/C%2B%2B-DP-solution-with-intuitive-explanation
//https://leetcode.com/problems/toss-strange-coins/discuss/408509/C%2B%2B-clean-dp-with-explanation
func probabilityOfHeads_bu(prob []float64, target int) float64 {
	dp := make([][]float64, len(prob)+1)
	for i := range dp {
		dp[i] = make([]float64, target+1)
	}
	//no coins to toss and no target
	dp[0][0] = 1.0

	for i := 1; i <= len(prob); i++ {
		for j := 0; j <= target; j++ {
			if j == 0 { //no target at all, we want tail
				dp[i][j] = dp[i-1][j] * (1.0 - prob[i-1])
			} else {
				//to get j head, could be from [i-1] toss, which got [j-1] head * prob[i]_current
				//or from [i-1] toss, which get[j] heads already, (tail):(1-prob[i])_current
				dp[i][j] = dp[i-1][j-1]*prob[i-1] + dp[i-1][j]*(1.0-prob[i-1])
			}
		}
	}
	return dp[len(prob)][target]
}
