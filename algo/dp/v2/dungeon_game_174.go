package main

import (
	"fmt"
	"math"
)

// https://leetcode.com/problems/dungeon-game/
//    {-2, -3, 3},
//    {-5, -10, 1},
//    {10, 30, -5}
//    only needs 1 pt to survive, last cell has -5
//    	-5 + x = 1 => x = 6  (needs 6 pt survive)
//		cell: 30, nextcol:6
//			6-30 -> -24 (24 extra) too many point, but we need just 1
//				max(1, 6-30)
//

// better space
func calculateMinimumHP(dungeon [][]int) int {

	//a compressed array to represent the cols of the dungeon
	//works as a circular array
	dp := make([]int, len(dungeon[0]))

	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	rs, cs := len(dungeon), len(dungeon[0])
	nextPoint := func(cur int, r, c int) int {
		if r < 0 || c < 0 {
			return math.MaxInt32 //undefined
		}
		//compress 2d indices to 1d
		idx := (r*cs + c) % len(dp)
		return max(1, dp[idx]-cur)
	}

	//start from the destination(bottom-right) !
	//begin with 0 idx for easier to handle the dp array
	for i := 0; i < rs; i++ {
		for j := 0; j < cs; j++ {

			//check the right and down and collect their health requirement
			right := nextPoint(dungeon[rs-1-i][cs-1-j], i, j-1)
			down := nextPoint(dungeon[rs-1-i][cs-1-j], i-1, j)
			//then select the minimum
			next := min(right, down)

			idx := (i*cs + j) % len(dp)
			if next != math.MaxInt32 { //is it undefined
				dp[idx] = next
			} else {
				if dungeon[rs-1-i][cs-1-j] >= 0 { //doesn't have health requirement, use the minimum (1)
					dp[idx] = 1
				} else { //-ve -- reducing health, to survive, we need 1, so we need 1-(reduce) amount of health
					//? + (-5) = 1 => ?=1-(-5) => 6
					dp[idx] = 1 - dungeon[rs-1-i][cs-1-j]
				}
			}
		}
	} //
	fmt.Println(dp)

	return dp[len(dp)-1]
}
