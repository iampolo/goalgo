package main

import . "gitlab.com/iampolo/goalgo/algo/util"

// https://leetcode.com/problems/largest-plus-sign/discuss/1454475/Go-Solution

func orderOfLargestPlusSign(n int, mines [][]int) int {
	zeroes := make(map[int]bool)

	for _, m := range mines {
		zeroes[m[0]*n+m[1]] = true
	}

	dp := make([][]int, n)
	for i := 0; i < n; i++ {
		dp[i] = make([]int, n)
	}

	//func
	getCnt := func(cnt *int, r, c int) int {
		if zeroes[r*n+c] {
			*cnt = 0
		} else {
			*cnt++
		}
		return *cnt
	}

	//loops
	for r := 0; r < n; r++ {
		cnt := 0
		for c := 0; c < n; c++ { //left to right
			dp[r][c] = getCnt(&cnt, r, c)
		}
		cnt = 0
		for c := n - 1; c >= 0; c-- { //right to left
			dp[r][c] = Min(dp[r][c], getCnt(&cnt, r, c))
		}
	}

	max := 0
	for c := 0; c < n; c++ {
		cnt := 0
		for r := 0; r < n; r++ { //top to bottom
			dp[r][c] = Min(dp[r][c], getCnt(&cnt, r, c))
		}
		cnt = 0
		for r := n - 1; r >= 0; r-- { //bottom to top
			dp[r][c] = Min(dp[r][c], getCnt(&cnt, r, c))

			if dp[r][c] > max {
				max = dp[r][c]
			}
		}
	}

	return max
}
