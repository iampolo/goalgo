package main

//  https://leetcode.com/problems/longest-palindromic-substring/submissions/

func longestPalindrome(s string) string {
	var li, ri int
	for i := 0; i < len(s); i++ {
		l, r := findPalindStr(s, i, i)
		if r-l > ri-li {
			li, ri = l, r
		}
		l, r = findPalindStr(s, i, i+1)
		if r-l > ri-li {
			li, ri = l, r
		}
	}
	return s[li : ri+1]
}

func findPalindStr(s string, l, r int) (int, int) {
	for l >= 0 && r < len(s) && s[l] == s[r] {
		l--
		r++
	}
	return l + 1, r - 1
}
