package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/min-cost-climbing-stairs/solution/
func minCostClimbingStairs(cost []int) int {
	dp := make([]int, len(cost)+1)

	for i := 2; i < len(dp); i++ {
		// define the previous steps
		one := dp[i-1] + cost[i-1]
		two := dp[i-2] + cost[i-2]
		dp[i] = Min(one, two)
	}

	return dp[len(dp)-1]
}

func main_746() {
	nums := []int{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}
	fmt.Println(minCostClimbingStairs(nums))
}

////////////////
func minCostClimbingStairs_dfs(cost []int) int {
	mem := make(map[int]int)

	return dfs(cost, len(cost), mem)
}

func dfs(cost []int, i int, mem map[int]int) int {
	if i <= 1 {
		return 0
	}
	if _, ok := mem[i]; ok {
		return mem[i]
	}

	//from the end of the array
	one := cost[i-1] + dfs(cost, i-1, mem)
	two := cost[i-2] + dfs(cost, i-2, mem)
	mem[i] = Min(one, two)

	return mem[i]
}

////////////////////////////////
