package main

import . "gitlab.com/iampolo/goalgo/algo/util"

var dp [][]int    //improvement 1
var isPal [][]int //improvement 2

func minCut(s string) int {
	dp = make([][]int, len(s))

	for i := 0; i < len(s); i++ {
		dp[i] = make([]int, len(s))
		for j := 0; j < len(s); j++ {
			dp[i][j] = -1
		}
	} //

	isPal = make([][]int, len(s))
	for i := 0; i < len(s); i++ {
		isPal[i] = make([]int, len(s))
		for j := 0; j < len(s); j++ {
			isPal[i][j] = -1
		}
	} //

	return findMinCut(s, 0, len(s)-1)
}

//
func findMinCut(s string, l, r int) int {
	if l == r || isPalindrome(s, l, r) == 1 {
		//the end or s is already a palindrome
		return 0 //no more cut
	}
	if dp[l][r] != -1 {
		return dp[l][r]
	}

	mCut := len(s) - 1
	//i fixes the starting point at this level
	//the first recursion is the max cut!!, then move the fix point to right and do the recursion.
	for i := l; i <= r; i++ {
		if isPalindrome(s, l, i) == 1 {
			//continue to cut
			mCut = Min(mCut, 1+findMinCut(s, i+1, r))
		}
	}
	dp[l][r] = mCut //save
	return mCut
}

// 1 = true; 0 = false
func isPalindrome(s string, l, r int) int {
	if l >= r {
		return 1
	}
	if isPal[l][r] != -1 {
		return isPal[l][r]
	}
	if s[l] == s[r] && isPalindrome(s, l+1, r-1) == 1 {
		isPal[l][r] = 1
	} else {
		isPal[l][r] = 0
	}
	return isPal[l][r]
}
