package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/stone-game-vii/solution/
//MiniMax

func stoneGameVII(stones []int) int {
	memo := make([][]int, len(stones))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, len(stones))
		for j := 0; j < len(memo[i]); j++ {
			memo[i][j] = math.MaxInt32
		}
	}

	preSum := make([]int, len(stones)+1)
	for i := 0; i < len(stones); i++ {
		preSum[i+1] = preSum[i] + stones[i]
	}

	return dfs_game(0, len(stones)-1, preSum, memo, stones)
}

// O(n^2)
func dfs_game(s, e int, preSum []int, memo [][]int, stones []int) int {
	if s == e {
		return 0
	}

	if memo[s][e] != math.MaxInt32 {
		return memo[s][e]
	}
	//
	if s+1 == e { //two elements left
		return Max(stones[s], stones[e])
	}

	// for current person : alice or alice, he has two options:
	takeFront := preSum[e+1] - preSum[s+1]
	takeLast := preSum[e] - preSum[s]

	// take the front or take the last (classic DFS), call recursion to take the max from the next level
	op1 := dfs_game(s+1, e, preSum, memo, stones)
	op2 := dfs_game(s, e-1, preSum, memo, stones)
	memo[s][e] = Max(takeFront-op1, takeLast-op2)

	return memo[s][e]
}

func main_1690() {
	stones := []int{5, 3, 1, 4, 2}
	stones = []int{5, 3, 1}
	fmt.Println(stoneGameVII(stones))
}
