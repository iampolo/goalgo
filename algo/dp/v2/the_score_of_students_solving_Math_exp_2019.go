package main

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

/*
 Constraints:

  - 3 <= s.length <= 31
  - s represents a valid expression that contains only digits 0-9, '+', and '*' only.
  - All the integer operands in the expression are in the inclusive range [0, 9].
  - 1 <= The count of all operators ('+' and '*') in the math expression <= 15
  - Test data are generated such that the correct answer of the expression is in the range of [0, 1000].
  - n == answers.length
  - 1 <= n <= 10^4
  - 0 <= answers[i] <= 1000
*/
// https://leetcode.com/problems/the-score-of-students-solving-math-expression/
//find all the possible calculations
//find the one correct calculation
//
//diff_ways_to_add_parentheses_241.go + lc224 + lc227
//BurstBalloons312
func scoreOfStudents(s string, answers []int) int {
	correctAns := func(s string) int {
		var res int
		//j helps for '*'
		for i, j := 0, 0; i <= len(s); i++ {
			if i == len(s) || s[i] == '+' {
				mul := 1
				for ; j < i; j += 2 { //process all the j ie: '*'
					mul *= int(s[j] - '0')
				}
				res += mul
			}
		}
		return res
	}

	dp := make(map[int][]map[int]int)
	fmt.Println(dp)


	//collect all the possible answers
	allCombo := allCombination(s)
	corrAns := correctAns(s)

	var res int
	for _, ans := range answers {
		if ans == corrAns {
			res += 5
		} else if _, ok := allCombo[ans]; ok {
			res += 2
		}
	}
	return res
}

func allCombination(exp string) map[int]int {

	//key is the expression value, v in map can be ignored
	//what value we can evaluate in this level.
	lst := make(map[int]int)
	for i, ch := range exp {
		if strings.Index("*+", string(ch)) == -1 {
			continue
		}
		left := allCombination(exp[0:i])
		right := allCombination(exp[i+1:])

		for ln, _ := range left {
			for rn, _ := range right {
				val := 0
				if ch == '+' {
					val = ln + rn
				} else {
					val = ln * rn
				}
				//pay attention to the requirement!
				//since we are collecting all the cases, some may over the required limit which we can skip.
				if val <= 1000 {
					lst[val] = 1
				}
			}
		} //for
	}
	//check if the above recursion is executed and generated new values
	if len(lst) == 0 { //no operator generate result
		x, _ := strconv.Atoi(exp) //it is a number only
		lst[x] = 1
	}
	return lst
}

func calcVal(s string) int {
	var st []int

	for i := range s {
		if unicode.IsDigit(rune(s[i])) {
			if i > 0 && s[i] == '*' {
				//'*' needs special handle as it needs to be process first
				st = append(st, st[len(st)-1]*int(s[i]-'0'))
			} else {
				//number or '+' sign, simple add to stack
				st = append(st, int(s[i]-'0'))
			}
		}
	}
	var res int
	for _, n := range st {
		res += n
	}

	return res
}
