package main

/**
https://leetcode.com/problems/where-will-the-ball-fall/submissions/

*/

/**
A2: Ball will tend to fall right (slope = grid[r][x] = 1) or left (slope = grid[r][x] = -1),
it need the corresponding neighbor's same tendency (or slope) to fall through, otherwise get stuck.
e.g.,
grid[r][x] = 1, ball will tend to fall right and we need to check right neighbor's slope
grid[r][x + 1]; If grid[r][x + 1] = 1, then the ball will fall right to the next row at (r + 1, x + 1),
otherwise get stuck at (r, x).
*/
func findBall(grid [][]int) []int {
	if len(grid) == 0 || len(grid[0]) == 0 {
		return []int{}
	}

	var res = []int{}

	for i := 0; i < len(grid[0]); i++ { //cols
		rs  := dropBall(grid, 0, i)
		res = append(res, rs)
	}

	return res
}

func dropBall(grid [][]int, r, c int) int {

	if r == len(grid) {
		return c
	}

	if isValid(r, c, grid) {
		if grid[r][c] == 1 {
			if isValid(r, c+1, grid) && grid[r][c+1] == 1 {
				return dropBall(grid, r+1, c+1)
			}
		} else {
			if isValid(r, c-1, grid) && grid[r][c-1] == -1 {
				return dropBall(grid, r+1, c-1)
			}
		}
	}

	return -1
}

func isValid(r, c int, grid [][]int) bool {
	return r >= 0 && c >= 0 && r < len(grid) && c < len(grid[0])
}

// use the slope
// @see lc149
