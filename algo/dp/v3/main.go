package main

import "fmt"

func main() {
	grid := [][]int{{1, 1, 1, -1, -1},
		{1, 1, 1, -1, -1},
		{-1, -1, -1, 1, 1},
		{1, 1, 1, 1, -1},
		{-1, -1, -1, -1, -1}}

	fmt.Println(findBall(grid))
}
