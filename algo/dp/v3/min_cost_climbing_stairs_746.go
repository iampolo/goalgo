package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/min-cost-climbing-stairs/solution/
func minCostClimbingStairs(cost []int) int {
	dp := make([]int, len(cost))

	return Min(minCost_v2(cost, len(cost)-1, dp), minCost_v2(cost, len(cost)-2, dp))
}

func minCost_v2(cost []int, n int, dp []int) int {
	if n < 0 {
		return 0
	}
	if n == 0 || n == 1 {
		return cost[n] //reached to the base case
	}
	if dp[n] != 0 {
		return dp[n]
	}

	dp[n] = cost[n] + Min(minCost_v1(cost, n-1), minCost_v1(cost, n-2))

	return dp[n]
}

//
func minCost_v1(cost []int, n int) int {
	if n < 0 {
		return 0
	}
	if n == 0 || n == 1 {
		return cost[n] //reached to the base case
	}

	return cost[n] + Min(minCost_v1(cost, n-1), minCost_v1(cost, n-2))
}

func main_746() {
	nums := []int{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}
	fmt.Println(minCostClimbingStairs(nums))
}
