package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/stone-game-vii/solution/
//MiniMax

func stoneGameVII(stones []int) int {
	length := len(stones)

	dp := make([][]int, length)
	for i := 0; i < length; i++ {
		dp[i] = make([]int, len(dp))
	}

	preSum := make([]int, length+1)
	for i := 0; i < length; i++ {
		preSum[i+1] = preSum[i] + stones[i]
	}

	/*  l=2			l=3
	    s=0 e=1		s=0	e=2
	    s=1	e=2
		s=2	e=3

		tabulation
	      e e e e
		s
		s
		s
		s
	*/
	for l := 2; l <= length; l++ { //length of array to handle, l=2,3,4,...,len(stones) :  minimum length is 2
		for s := 0; s+l-1 < length; s++ { //-1 to get the 0-idx
			e := s + l - 1
			takeFront := preSum[e+1] - preSum[s+1]
			takeLast := preSum[e] - preSum[s]

			dp[s][e] = util.Max(takeFront-dp[s+1][e], takeLast-dp[s][e-1])

		}
		fmt.Println(dp[0])

	}

	return dp[0][length-1]
}

func main_1690() {
	stones := []int{5, 3, 1, 4, 2}
	stones = []int{5, 3, 1, 4}
	fmt.Println(stoneGameVII_v5(stones))
}

func stoneGameVII_v5(stones []int) int {
	length := len(stones)

	preSum := make([]int, length+1)
	for i := 0; i < length; i++ {
		preSum[i+1] = preSum[i] + stones[i]
	}

	dp := make([][]int, length)
	for i := 0; i < length; i++ {
		dp[i] = make([]int, len(dp))
	}

	/* length=4
				e=3, 4, 5
	       	s=2
	 		s=3
			s=4
	 */
	for s := length - 2; s >= 0; s-- { 			//fix a starting point
		for e := s + 1; e < length; e++ { 		//expand from starting point
			takeFront := preSum[e+1] - preSum[s+1]
			takeLast := preSum[e] - preSum[s]

			fmt.Println(takeFront, takeLast)
		}
		fmt.Println("-")
	}
	return dp[0][length-1]
}
