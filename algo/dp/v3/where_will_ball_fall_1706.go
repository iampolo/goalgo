package main

/**
https://leetcode.com/problems/where-will-the-ball-fall/submissions/

*/

/**
A2: Ball will tend to fall right (slope = grid[r][x] = 1) or left (slope = grid[r][x] = -1),
it need the corresponding neighbor's same tendency (or slope) to fall through, otherwise get stuck.
e.g.,
grid[r][x] = 1, ball will tend to fall right and we need to check right neighbor's slope
grid[r][x + 1]; If grid[r][x + 1] = 1, then the ball will fall right to the next row at (r + 1, x + 1),
otherwise get stuck at (r, x).
*/
func findBall(grid [][]int) []int {
	if len(grid) == 0 || len(grid[0]) == 0 {
		return []int{}
	}
	var res = []int{}
outer:
	//the values in the grid could be either 1 or -1: slope 1 or slope -1
	for c := 0; c < len(grid[0]); c++ { //cols
		diff := c

		//the use of math instead of slope
		for r := 0; r < len(grid); r++ {
			ori := grid[r][diff] //either 1 or -1
			diff += ori          //-2 or 2, cell before or after
			if diff < 0 || diff >= len(grid[0]) || grid[r][diff] != ori {
				//out of boundaries or the cell values are different: get stuck
				res = append(res, -1)
				continue outer
			}
		}
		res = append(res, diff)
	}

	return res
}

// use the slope:the use of math instead of slope
// @see lc149
