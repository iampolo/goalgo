package main

import . "gitlab.com/iampolo/goalgo/algo/util"

// https://leetcode.com/problems/valid-palindrome-iii/

// Given a string s and an integer k, return true if s is a k-palindrome.
//
// A string is k-palindrome if it can be transformed into a palindrome by removing at most k characters
// from it.
//
//
//
//Example 1:
//
//Input: s = "abcdeca", k = 2
//Output: true
//Explanation: Remove 'b' and 'e' characters.
//
//Example 2:
//
//Input: s = "abbababa", k = 1
//Output: true
//
//
//Constraints:
//
//- 1 <= s.length <= 1000
//- s consists of only lowercase English letters.
//- 1 <= k <= s.length
//
// longest_palind_subsequence_516
//
func isValidPalindrome_recur(s string, k int) bool {
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}

	var dfs func(l, r int) int
	dfs = func(l, r int) int {
		// one letter left: odd # of chars
		if l == r {
			return 0 //nothing can be removed
		}

		// two letters left, just do a quick check
		// even # of chars
		// this part is OPTIONAL, if we change the 1st base case to `l>r`
		if l == r-1 {
			if s[l] != s[r] {
				return 1
			}
			return 0
		}

		// already done before, just return the result
		if dp[l][r] != -1 {
			return dp[l][r]
		}

		// there is a match
		if s[l] == s[r] {
			dp[l][r] = dfs(l+1, r-1)
			return dp[l][r]
		}

		// there is no match : two cases
		// trim from left
		left := dfs(l+1, r)
		// trim from right
		right := dfs(l, r-1)

		// need 1 modification and pick the min of the two cases
		dp[l][r] = 1 + Min(left, right)
		return dp[l][r]
	}

	return dfs(0, len(s)-1) <= k
}

//
func isValidPalindrome_bottomup_2D_v2(s string, k int) bool {
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
	}
	for k := 2; k <= len(s); k-- { //set up the window size
		for i := 0; i+k-1 < len(s); i++ { //i => left pointer
			j := i + k - 1 //j => right pointer
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1] //build from previous result
			} else {
				//there are two cases
				dp[i][j] = Min(dp[i+1][j], dp[i][j-1]) + 1
			}
		} //for
	}
	return dp[0][len(s)-1] <= k
}

func isValidPalindrome_bottomup_2D(s string, k int) bool {
	dp := make([][]int, len(s))
	for i := range dp {
		dp[i] = make([]int, len(s))
	}

	for i := len(s) - 2; i >= 0; i-- {
		for j := i + 1; j < len(s); j++ {
			if s[i] == s[j] {
				dp[i][j] = dp[i+1][j-1] //build from previous result
			} else {
				//there are two cases
				dp[i][j] = Min(dp[i+1][j], dp[i][j-1]) + 1
			}
		} //for
	}
	//depends on how the table was built
	return dp[0][len(s)-1] <= k
}

func isValidPalindrome_bottomup_1D(s string, k int) bool {
	dp := make([]int, len(s))

	//only the previous row and previous col are needed
	for i := len(s) - 2; i >= 0; i-- {
		prev := 0 //previous row
		for j := i + 1; j < len(s); j++ {
			tmp := dp[j] //before it may be update
			if s[i] == s[j] {
				dp[j] = prev
			} else {
				dp[j] = 1 + Min(dp[j], dp[j-1])
			}
			prev = tmp
		}
	}

	return dp[len(s)-1] <= k
}

// *******************************************************************************************
func isValidPalindrome(s string, k int) bool {
	diff := len(s) - longestPalindromeSubseq(s)

	return diff < k
}
