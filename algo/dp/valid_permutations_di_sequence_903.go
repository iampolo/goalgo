package main

//https://leetcode.com/problems/valid-permutations-for-di-sequence/

/*
You are given a string s of length n where s[i] is either:

'D' means decreasing, or
'I' means increasing.
A permutation perm of n + 1 integers of all the integers in the range [0, n] is called a valid permutation if
for all valid i:

If s[i] == 'D', then perm[i] > perm[i + 1], and
If s[i] == 'I', then perm[i] < perm[i + 1].
Return the number of valid permutations perm. Since the answer may be large, return it modulo 109 + 7.


Example 1:

Input: s = "DID"
Output: 5
Explanation: The 5 valid permutations of (0, 1, 2, 3) are:
(1, 0, 3, 2)
(2, 0, 3, 1)
(2, 1, 3, 0)
(3, 0, 2, 1)
(3, 1, 2, 0)

Example 2:

Input: s = "D"
Output: 1


Constraints:

* n == s.length
* 1 <= n <= 200
* s[i] is either 'I' or 'D'.

*/
/*

IMPORTANT TO UNDERSTAND:

DID
1032

DIDD
1032 -> [2,1,0]
 - 10432  [2] elevate 2 or above to prevent duplicates

DIDD
1032 -> [1, 0]
 - 20431
 - 21430

DIDI
1032 -> [2,3,4]
 - 10432 - [2] ..>=2 + 1
 - 10423 - [3] elevate 3 or above to prevent dup.


DID
213      [2, 1, 0]
 - 1032    [2]  decrement those <= 2

NextPermutation31
*/



// https://leetcode.com/problems/valid-permutations-for-di-sequence/discuss/196939/Easy-to-understand-solution-with-detailed-explanation
func numPermsDISequence(s string) int {

	n := len(s)
	//dp[i][j] the numer of permutation using [0...i] and the i-th (ending) number is j
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}

	//base, designed
	dp[0][0] = 1

	//matrix scanning technique
	/*
	 'D'	dp[i][j] += dp[i-1][k]  where j<= k <= i-1
	 'I'	dp[i][j] += dp[i-1][k]  where 0<= k <= j-1
	 */
	for i := 1; i <= n; i++ {
		for j := 0; j <= i; j++ {
			if s[i-1] == 'D' {
				for k := j; k <= i-1; k++ {
					//add up previous bigger j: right side the previous row
					dp[i][j] = (dp[i][j] + dp[i-1][k]) % MOD
				}
			} else { //assuming 'I'
				for k := 0; k <= j-1; k++ {
					//add up previous smaller j: left side of previoius row
					dp[i][j] = (dp[i][j] + dp[i-1][k]) % MOD
				}
			}
		} //for
	}

	var res int
	//last row,
	for i := 0; i <= n; i++ {
		res = (res + dp[n][i]) % MOD
	}

	return res
}
