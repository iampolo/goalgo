package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

// https://leetcode.com/problems/video-stitching/
//You are given a series of video clips from a sporting event that lasted time seconds. These video clips can be
//overlapping with each other and have varying lengths.
//
//Each video clip is described by an array clips where clips[i] = [starti, endi] indicates that the ith clip started
//at starti and ended at endi.
//
//We can cut these clips into segments freely.
//
//For example, a clip [0, 7] can be cut into segments [0, 1] + [1, 3] + [3, 7].
//Return the minimum number of clips needed so that we can cut the clips into segments that cover the entire sporting
//event [0, time]. If the task is impossible, return -1.
//
//
//
//Example 1:
//
//Input: clips = [[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]], time = 10
//Output: 3
//Explanation:
//We take the clips [0,2], [8,10], [1,9]; a total of 3 clips.
//Then, we can reconstruct the sporting event as follows:
//We cut [1,9] into segments [1,2] + [2,8] + [8,9].
//Now we have segments [0,2] + [2,8] + [8,10] which cover the sporting event [0, 10].
//
//Example 2:
//
//Input: clips = [[0,1],[1,2]], time = 5
//Output: -1
//Explanation: We can't cover [0,5] with only [0,1] and [1,2].
//
//Example 3:
//
//Input: clips = [[0,1],[6,8],[0,2],[5,6],[0,4],[0,3],[6,7],[1,3],[4,7],[1,4],[2,5],[2,6],[3,4],[4,5],[5,7],[6,9]], time = 9
//Output: 3
//Explanation: We can take clips [0,4], [4,7], and [6,9].
//
//Example 4:
//
//Input: clips = [[0,4],[2,8]], time = 5
//Output: 2
//Explanation: Notice you can have extra video after the event ends.
//
//
//Constraints:
//
//- 1 <= clips.length <= 100
//- 0 <= start_i <= end_i <= 100
//- 1 <= time <= 100
//
//1326. Minimum Number of Taps to Open to Water a Garden
//
//
func videoStitching_v3(clips [][]int, time int) int {
	// we track our current stitching position(st). for each iteration, we check all overlapping clips,
	// and pick the one that advances our stitching position the furthest
	sort.Slice(clips, func(i, j int) bool {
		//sort by start time, if not, then end time
		if clips[i][0] == clips[j][0] {
			return clips[i][1] < clips[j][1]
		}
		return clips[i][0] < clips[j][0]
	})

	var res int
	for i, st, et := 0, 0, 0; st < time; {
		//each time scann all the clips, and check the overlapping ones
		for ; i < len(clips) && clips[i][0] <= st; i++ {
			et = Max(clips[i][1], et)
		}

		if st == et { //no good clips available
			return -1
		}
		st = et //advance further
		res++
	} //

	return res
}

// [0,1], [0,2], [0,8]
// how to pick  [0,8] as it is the desired one
func videoStitching(clips [][]int, time int) int {
	sort.Slice(clips, func(i, j int) bool {
		//sort by start time, if not, then end time
		if clips[i][0] == clips[j][0] {
			return clips[i][1] < clips[j][1]
		}
		return clips[i][0] < clips[j][0]
	})

	times := make([]int, 101) //large enough to store all possible time values
	for i := range clips {    //for fast retrieval
		times[clips[i][0]] = clips[i][1]
	}

	if times[0] == 0 { //start, and end times are both 0
		return -1
	}
	//if end time is already >= time, we have 1 clip as the result
	cnt := 1 //there is always one clip

	start, end := 0, times[0]
	for end < time {
		max, min := 0, -1
		//find the max that is smaller than end
		for t := end; t > start; t-- { //retrieve time from end,end-1,end-2, and find the best end time
			if times[t] > max {
				max = times[t]
				min = t
			}
		}
		if min == -1 || max < end {
			return -1 //no advance at all, no clip available
		}
		cnt++
		start, end = min, max //advancing the interval
	}
	return cnt
}

// ****************************************************
func videoStitching_v1(clips [][]int, time int) int {
	//
	dp := make([]int, time+1)
	for i := range dp {
		dp[i] = time + 1
	}
	//dp[i]: the minimum # of clip need to reach time i
	//if time is 0, no clip is needed at all
	//when t > 0, we need at least 1 clip.
	//
	dp[0] = 0
	for t := 0; t <= time; t++ {
		for j := range clips {
			if clips[j][0] <= t && t <= clips[j][1] {
				dp[t] = Min(dp[t], dp[clips[j][0]]+1)
			}
		}
		//no clip is there to help to reach dp[t] at all, it shows impossible already
		if dp[t] == time+1 { //equals to default
			return -1
		}
	} //
	return dp[time]
}
