package main

import "fmt"

/**
https://leetcode.com/problems/where-will-the-ball-fall/submissions/

*/
func main_1706() {
	grid := [][]int{{1, 1, 1, -1, -1},
		{1, 1, 1, -1, -1},
		{-1, -1, -1, 1, 1},
		{1, 1, 1, 1, -1},
		{-1, -1, -1, -1, -1}}

	fmt.Println(findBall(grid))
}


/**
A2: Ball will tend to fall right (slope = grid[r][x] = 1) or left (slope = grid[r][x] = -1),
it need the corresponding neighbor's same tendency (or slope) to fall through, otherwise get stuck.
e.g.,
grid[r][x] = 1, ball will tend to fall right and we need to check right neighbor's slope
grid[r][x + 1]; If grid[r][x + 1] = 1, then the ball will fall right to the next row at (r + 1, x + 1),
otherwise get stuck at (r, x).
*/
func findBall(grid [][]int) []int {
	if len(grid) == 0 || len(grid[0]) == 0 {
		return []int{}
	}

	//Good, follow the flow

	var res = []int{}
	for i := 0; i < len(grid[0]); i++ { //cols
		r, c := 0, i
		for r < len(grid) {
			if grid[r][c] == 1 && c+1 < len(grid[0]) && grid[r][c+1] == 1 {
				//go to the next row successfully
				r++
				c++
			} else if grid[r][c] == -1 && c-1 >= 0 && grid[r][c-1] == -1 {
				r++
				c--
			} else {
				//either hit the wall or V
				break
			}
		}
		if r == len(grid) { //if it reaches the last row
			res = append(res, c)
		} else {
			res = append(res, -1)
		}
	}

	return res
}

// use the slope
// @see lc149
