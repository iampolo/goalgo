package main

import . "gitlab.com/iampolo/goalgo/algo/util"

//https://leetcode.com/problems/01-matrix/solution/
//DP
func updateMatrix(mat [][]int) [][]int {

	limit := len(mat) * len(mat[0])
	dist := make([][]int, len(mat))
	for i := 0; i < len(dist); i++ {
		dist[i] = make([]int, len(mat[0]))
	}
	//build from left to right, top to bottom
	for i := 0; i < len(mat); i++ {
		for j := 0; j < len(mat[0]); j++ { //col
			if mat[i][j] != 0 {
				r, c := limit, limit
				if i > 0 {
					r = dist[i-1][j] //take from previous row
				}
				if j > 0 {
					c = dist[i][j-1] //take from previous col
				}
				dist[i][j] = Min(r, c) + 1   //+1 -> current used step
			}
		}
	}

	// second direction
	for i := len(mat) - 1; i >= 0; i-- {
		for j := len(mat[0]) - 1; j >= 0; j-- {
			r, c := limit, limit
			if i < len(mat)-1 {
				r = dist[i+1][j]
			}
			if j < len(mat[0])-1 {
				c = dist[i][j+1]
			}
			//compare withe the value from the 1st direction
			dist[i][j] = Min(Min(r, c)+1, dist[i][j])
		}
	}

	return dist
}
