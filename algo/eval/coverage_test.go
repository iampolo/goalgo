package eval

import (
	"fmt"
	"testing"
)

func TestCoverage(t *testing.T) {
	var tests = []struct {
		input string
		env   Env
		want  string
	}{
		{"x %2", nil, "unexpected '%'"},
	}
	for _, test := range tests {
		expr, err := Parse(test.input)
		if err == nil {
			err = expr.Check(map[Var]bool{})
		}
		if err != nil {
			if err.Error() != test.want {
				t.Errorf("%s: got %q, want %q", test.input, err, test.want)
			}
			continue
		}

		got := fmt.Sprintf("%.6g", expr.Eval(test.env))
		if got != test.want {
			t.Errorf("%s: %v => %s, want %s", test.input, test.env, got, test.want)
		}
		fmt.Println("debug.", got)
	}
}
