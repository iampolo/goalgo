package eval

import (
	"fmt"
	"math"
	"testing"
)

func TestEval(t *testing.T) {
	tests := []struct {
		expr string
		env  Env
		want string
	}{
		{"sqrt(A/ pi)", Env{"A": 87616, "pi": math.Pi}, "167"},
		{"5/9 * (F - 32)", Env{"F": 212}, "100"},
		{"-1 -x", Env{"x": 1}, "-2"},
		{"-1 -x", Env{"x": 1}, "-2"},
	}
	var preExpr string
	for _, test := range tests {
		if test.expr != preExpr { //if statement
			fmt.Printf("\n%s\n", test.expr)
			preExpr = test.expr
		}
		expr, err := Parse(test.expr)
		if err != nil {
			t.Error(err) //can't parse the expr
			continue
		}
		got := fmt.Sprintf("%.6g", expr.Eval(test.env))
		fmt.Printf("\t%v => %s\n", test.env, got)
		if got != test.want {
			t.Errorf("%s.Eval() in %v = %q, want %q\n",
				test.expr, test.env, got, test.want)
		}
	}
}

func TestError(t *testing.T) {
	for _, test := range [] struct{ expr, wantErr string }{
		{"x % 2", "unexpected '%'"},
		{"log(10)", `unknown function "log"`},
		{`"hello"`, "unexpected '\"'"},

	} {
		expr, err := Parse(test.expr)
		if err == nil {
			vars := make(map[Var]bool)
			err = expr.Check(vars)
			if err == nil {
				t.Errorf("unexpeted success: %s", test.wantErr)
				continue
			}
		}
		fmt.Printf("%-20s%v\n", test.expr, err)
		if err.Error() != test.wantErr {
			t.Errorf("got error %s, want %s", err, test.wantErr)
		}
	}
}
