package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/as-far-from-land-as-possible/

//Given an n x n grid containing only values 0 and 1, where 0 represents water and 1 represents land,
//find a water cell such that its distance to the nearest land cell is maximized, and return the distance.
//If no land or water exists in the grid, return -1.
//
//The distance used in this problem is the Manhattan distance: the distance between two cells (x0, y0)
//and (x1, y1) is |x0 - x1| + |y0 - y1|
//--------------->  the (minimum) number of cells you travel from point A to point B is the Manhattan distance.
//
//ZeroOne01Matrix542
// https://leetcode.jp/leetcode-1162-as-far-from-land-as-possible-%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/
func maxDistance_1162(grid [][]int) int {
	//find a land and a water cell that has the farthest distance

	//if from 0-water instead of 1-land, too many duplicated visits, every water, needs to visit those lands, and then
	//compare and find the farthest at the end.
	//we should start from 1-land
	vis := make([][]bool, len(grid))
	for i := range vis {
		vis[i] = make([]bool, len(grid[i]))
	}

	var queue [][]int
	for i := range grid {
		for j := range grid[i] {
			if grid[i][j] == 1 { //start with lands
				vis[i][j] = true
				queue = append(queue, []int{i, j})
			}
		} //for
	} //for

	res := -1
	for len(queue) > 0 {
		//for s := len(queue); s > 0; s-- { //Redundant, as the distance is stored in the queue
		cur := queue[0]
		queue = queue[1:]

		res = Max(res, grid[cur[0]][cur[1]]-1) //-1 to consider the 0-water, visited cell with be 1-land

		for i := 0; i < 4; i++ {
			nx, ny := cur[0]+dirs[i], cur[1]+dirs[i+1]
			if nx >= 0 && nx < len(grid) && ny >= 0 && ny < len(grid[0]) && !vis[nx][ny] {
				vis[nx][ny] = true
				grid[nx][ny] = grid[cur[0]][cur[1]] + 1 //store the travel distance 1->2->3->...
				queue = append(queue, []int{nx, ny})
			}
		}
		//}
	}

	if res == 0 {
		return -1
	}
	return res
}

func maxDistance_1162_v2(grid [][]int) int {
	var queue [][]int
	for i := range grid {
		for j := range grid[i] {
			if grid[i][j] == 1 { //start with lands
				queue = append(queue, []int{i, j})
			}
		} //for
	} //for
	if len(queue) == len(grid)*len(grid[0]) {
		return -1 //all are the lands
	}
	steps := 0
	for len(queue) > 0 {
		//layer by layer
		for s := len(queue); s > 0; s-- {
			cur := queue[0]
			queue = queue[1:]

			for i := 0; i < 4; i++ {
				nx, ny := cur[0]+dirs[i], cur[1]+dirs[i+1]
				if nx >= 0 && nx < len(grid) && ny >= 0 && ny < len(grid[0]) && grid[nx][ny] == 0 {
					grid[nx][ny] = 1 //dont' need to use visit grid
					queue = append(queue, []int{nx, ny})
				}
			}
		}
		steps++
	}

	return steps - 1
}

//GOOD
//ZeroOne01Matrix542
func maxDistance_1162_dp(grid [][]int) int {
	rows, cols := len(grid), len(grid[0])
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if grid[i][j] == 1 {
				continue
			}
			grid[i][j] = math.MaxInt32
			if i > 0 {
				//previous row + 1
				grid[i][j] = Min(grid[i-1][j]+1, grid[i][j])
			}
			if j > 0 {
				//previous col + 1
				grid[i][j] = Min(grid[i][j-1]+1, grid[i][j])
			}
		}
	}

	var res int
	for i := rows - 1; i >= 0; i-- {
		for j := cols - 1; j >= 0; j-- {
			if grid[i][j] == 1 {
				continue
			}
			//keep the minimum distance
			if i < rows-1 {
				grid[i][j] = Min(grid[i][j], grid[i+1][j]+1)
			}
			if j < cols-1 {
				grid[i][j] = Min(grid[i][j], grid[i][j+1]+1)
			}
			//get the maximum distance among those minimum
			res = Max(res, grid[i][j])
		}
	}
	if res == math.MaxInt32 { //there must be at latest one 1-land, but may be no water
		return -1
	}
	return res - 1
}

//******************************************************
//613ms
func maxDistance_1162_dfs(grid [][]int) int {
	rows, cols := len(grid), len(grid[0])
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if grid[i][j] == 1 {
				grid[i][j] = 0 //so that 1-land will start with 1 in dfs function, then 2, 3, ...
				recur(grid, i, j, 1)
			}
		}
	}
	res := -1
	for i := 0; i < rows; i++ {
		for j := 0; j < cols; j++ {
			if grid[i][j] > 1 {
				res = Max(res, grid[i][j]-1)
			}
		}
	}
	return res
}

func recur(grid [][]int, i, j int, dist int) {
	if i < 0 || i >= len(grid) || j < 0 || j >= len(grid[i]) || grid[i][j] != 0 && grid[i][j] <= dist { //better dist.
		return
	}
	grid[i][j] = dist
	recur(grid, i-1, j, dist+1)
	recur(grid, i+1, j, dist+1)
	recur(grid, i, j-1, dist+1)
	recur(grid, i, j+1, dist+1)
}

//******************************************************

///https://leetcode.com/problems/shortest-distance-from-all-buildings/
func shortestDistance_317(grid [][]int) int {

	return 0
}
