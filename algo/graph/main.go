package main

import (
	"fmt"
	v2 "gitlab.com/iampolo/goalgo/algo/graph/v2"
)

func main() {
	test_947()
}

func test_947() {
	stones := [][]int{{0,0}}
	fmt.Println(removeStones(stones))
}


func test_1101() {
	logs := [][]int{{0, 2, 0}, {1, 0, 1}, {3, 0, 3}, {4, 1, 2}, {7, 3, 1}}
	n := 4
	fmt.Println(earliestAcq(logs, n))
}

func test_1162() {
	grid := [][]int{{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}
	//grid = [][]int{{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}
	//fmt.Println(maxDistance_1162(grid))
	fmt.Println(maxDistance_1162_dfs(grid))
}

func test_990() {
	equ := []string{"c==c", "f!=a", "f==b", "b==c"}
	fmt.Println(equationsPossible_uf(equ))
	fmt.Println(equationsPossible_coloring(equ))
}

func test_305() {
	lands := [][]int{{0, 0}, {0, 1}, {1, 2}, {2, 1}}
	m, n := 3, 3
	fmt.Println(numIslands2(m, n, lands))
}

func test_803() {
	grid := [][]int{{1, 0, 0, 0}, {1, 1, 1, 0}}
	hits := [][]int{{1, 0}}
	fmt.Println(v2.HitBricks(grid, hits))
}

func test_1331() {
	nums := []int{37, 12, 28, 9, 100, 56, 80, 5, 12}
	fmt.Println(arrayRankTransform(nums))
}

func do684() {
	edges := [][]int{{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}}
	fmt.Println(findRedundantConnection(edges))
}

func do1059() {
	n := 4
	edges := [][]int{{0, 1}, {0, 2}, {1, 3}, {2, 3}}
	src := 0
	dst := 2

	fmt.Println(leadsToDestination(n, edges, src, dst))
}
