package main

import . "gitlab.com/iampolo/goalgo/algo/util"

func largestIsland(grid [][]int) int {
	allIslands := make([]int, len(grid)*len(grid[0])+2)

	idx := 2 //0 and 1 are used
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			if grid[i][j] == 1 {
				allIslands[idx] = mark(i, j, grid, idx)
				idx++
			}
		}
	}

	max := 0
	if allIslands[2] > 0 {
		max = allIslands[2]
	}
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[0]); j++ {
			if grid[i][j] == 0 {
				set := map[int]struct{}{}
				for k := 0; k < 4; k++ {
					ni, nj := i+dirs[k], j+dirs[k+1]
					if ni < 0 || nj < 0 || ni >= len(grid) || nj >= len(grid[0]) || grid[ni][nj] < 2 {
						continue
					}
					//save the neighbor islands
					set[grid[ni][nj]] = struct{}{}
				}

				//add up to find the total size and the max one
				cur := 1
				for k := range set {
					cur += allIslands[k]
				}
				max = Max(max, cur)
			} //if
		} //for
	}
	return max
}

var dirs = [...]int{0, -1, 0, 1, 0} //fixed sized array instead of slice

//
func mark(r, c int, grid [][]int, idx int) int {
	size := 1
	grid[r][c] = idx
	for i := 0; i < 4; i++ {
		nr, nc := r+dirs[i], c+dirs[i+1]
		if nr >= 0 && nc >= 0 && nr < len(grid) && nc < len(grid[0]) && grid[nr][nc] == 1 {
			size += mark(nr, nc, grid, idx)
		}
	}
	return size
}
