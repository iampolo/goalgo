package main

//https://leetcode.com/problems/map-of-highest-peak/
//
//You are given an integer matrix isWater of size m x n that represents a map of land and water cells.
//
//If isWater[i][j] == 0, cell (i, j) is a land cell.
//If isWater[i][j] == 1, cell (i, j) is a water cell.
//You must assign each cell a height in a way that follows these rules:
//
//- The height of each cell must be non-negative.
//- If the cell is a water cell, its height must be 0.
//- Any two adjacent cells must have an absolute height difference of at most 1. A cell is adjacent to another cell if
//  the former is directly north, east, south, or west of the latter (i.e., their sides are touching).
//
//Find an assignment of heights such that the maximum height in the matrix is maximized.
//
//Return an integer matrix height of size m x n where height[i][j] is cell (i, j)'s height. If there are multiple
//solutions, return any of them.
//
func highestPeak(isWater [][]int) [][]int {
	var queue [][]int

	//we can also create a height:=[][]int, if we don't the input isWater matrix
	for i := range isWater {
		for j := range isWater[0] {
			if isWater[i][j] == 1 {
				isWater[i][j] = 0 //water changes to 0
				//next round starts from this cell, 3rd is the height to be incremented
				queue = append(queue, []int{i, j})
			} else {
				isWater[i][j] = -1 //non water
			}
		} //for
	}

	for len(queue) > 0 {
		//poll from queue
		cur := queue[0]
		queue = queue[1:]

		for i := 0; i < 4; i++ {
			nx, ny := cur[0]+dirs[i], cur[1]+dirs[i+1]
			if nx < 0 || nx >= len(isWater) || ny < 0 || ny >= len(isWater[0]) || isWater[nx][ny] != -1 {
				continue
			}
			//plus 1 from the original cell, and save to the next cell, the next cell also marked as visited.
			isWater[nx][ny] = isWater[cur[0]][cur[1]] + 1
			queue = append(queue, []int{nx, ny})
		}
	}
	return isWater
}
