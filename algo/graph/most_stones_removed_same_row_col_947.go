package main

import . "gitlab.com/iampolo/goalgo/algo/unionfind"

//https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/

//On a 2D plane, we place n stones at some integer coordinate points. Each coordinate point may have at most one stone.
//
//A stone can be removed if it shares either the same row or the same column as another stone that has not been removed.
//
//Given an array stones of length n where stones[i] = [xi, yi] represents the location of the ith stone, return the
//largest possible number of stones that can be removed.
//
//
//
//Example 1:
//
//Input: stones = [[0,0],[0,1],[1,0],[1,2],[2,1],[2,2]]
//Output: 5
//Explanation: One way to remove 5 stones is as follows:
//1. Remove stone [2,2] because it shares the same row as [2,1].
//2. Remove stone [2,1] because it shares the same column as [0,1].
//3. Remove stone [1,2] because it shares the same row as [1,0].
//4. Remove stone [1,0] because it shares the same column as [0,0].
//5. Remove stone [0,1] because it shares the same row as [0,0].
//Stone [0,0] cannot be removed since it does not share a row/column with another stone still on the plane.
//
//Example 2:
//
//Input: stones = [[0,0],[0,2],[1,1],[2,0],[2,2]]
//Output: 3
//Explanation: One way to make 3 moves is as follows:
//1. Remove stone [2,2] because it shares the same row as [2,0].
//2. Remove stone [2,0] because it shares the same column as [0,0].
//3. Remove stone [0,2] because it shares the same row as [0,0].
//Stones [0,0] and [1,1] cannot be removed since they do not share a row/column with another stone still on the plane.
//
//Example 3:
//
//Input: stones = [[0,0]]
//Output: 0
//Explanation: [0,0] is the only stone on the plane, so you cannot remove it.
//
//
//Constraints:
//
//1 <= stones.length <= 1000
//0 <= xi, yi <= 10^4
//No two stones are at the same coordinate point.
func removeStones(stones [][]int) int {
	//https://www.youtube.com/watch?v=beOCN7G4h-M&ab_channel=ShiranAfergan
	//image the 2D plane as a grid, [0,0] at the top-right corner
	//- for those stones/vertex are connected, we can always remove all stones except one
	//- moves one stones doesn't affect other stones
	//- each stones belongs to exactly one group
	//total stones - total groups =  answer

	//the trick is to find the correct ID for UF
	uf := NewUnionFind(len(stones))
	for i := range stones {
		uf.Ids[i] = i
	}

	//if i==j, UF will take care and won't connect the same stone
	for i := range stones {
		g1 := stones[i]
		for j := range stones {
			g2 := stones[j]
			if g1[0] == g2[0] || g1[1] == g2[1] {
				uf.Union(i, j)
			}
		}
	}
	//total stones - groups in the UF
	return len(stones) - uf.Cnt
}
