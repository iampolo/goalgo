package main

//https://leetcode.com/problems/n-queens-ii/

func totalNQueens(n int) int {

	col := make([]bool, n)
	diag := make([]bool, n*2-1)
	rdiag := make([]bool, n*2-1) //reversed diagonal

	var count int
	//
	var dfs func(r int)
	dfs = func(r int) {
		if r == n {
			count++
			return
		}

		for c := 0; c < n; c++ {
			if !col[c] && !diag[r+c] && !rdiag[r-c+n-1] {
				col[c], diag[r+c], rdiag[r-c+n-1] = true, true, true
				dfs(r + 1)
				col[c], diag[r+c], rdiag[r-c+n-1] = false, false, false
			}
		}
	}

	dfs(0)
	return count
}
