package main

import (
	. "gitlab.com/iampolo/goalgo/algo/unionfind"
)

/*
https://leetcode.com/problems/number-of-islands-ii/discuss/2037832/Union-By-Rank-with-Path-Compression-in-Golang
 */
//https://leetcode.com/problems/number-of-islands-ii/
func numIslands2(m int, n int, positions [][]int) []int {

	//each cell is an individual cell
	uf := NewUnionFind(m * n)
	for i := range uf.Ids {
		uf.Ids[i] = -1
	}

	var res []int
	var count int
	for _, pos := range positions {
		curIdx := pos[0]*n + pos[1]
		if uf.Ids[curIdx] != -1 {
			//this lands is a dup, already handled
			res = append(res, count)
			continue
		} //

		count++
		uf.Ids[curIdx] = curIdx //like regular UF, id is itself at the beginning
		//look for lands in those four directions
		for i := 0; i < 4; i++ {
			nx, ny := pos[0]+dirs[i], pos[1]+dirs[i+1]

			if nx >= 0 && ny >= 0 && nx < m && ny < n {
				idx := nx*n + ny
				if uf.Ids[idx] == -1 {
					continue
				}
				if !uf.IsConnected(curIdx, idx) {
					uf.Union(curIdx, idx)
					count--
				}
			}
		} //for
		res = append(res, count)
	}

	return res
}
