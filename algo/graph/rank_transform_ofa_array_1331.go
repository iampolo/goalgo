package main

import (
	"math"
	"sort"
)

//Given an array of integers arr, replace each element with its rank.
//
//The rank represents how large the element is. The rank has the following rules:
//
//Rank is an integer starting from 1.
//The larger the element, the larger the rank. If two elements are equal, their rank must be the same.
//Rank should be as small as possible.
//
//
//Example 1:
//
//Input: arr = [40,10,20,30]
//Output: [4,1,2,3]
//Explanation: 40 is the largest element. 10 is the smallest. 20 is the second smallest. 30 is the third smallest.
//
//Example 2:
//
//Input: arr = [100,100,100]
//Output: [1,1,1]
//Explanation: Same elements share the same rank.
//
//Example 3:
//
//Input: arr = [37,12,28,9,100,56,80,5,12]
//Output: [5,3,4,2,8,6,7,1,3]
//
//
//Constraints:
// 	0 <= arr.length <= 10^5
//	-10^9 <= arr[i] <= 10^9
//
func arrayRankTransform(arr []int) []int {
	//given Go doesn't have build-int treeMap.
	mapping := make(map[int][]int)
	for i, v := range arr {
		mapping[v] = append(mapping[v], i)
	}

	res := make([]int, len(arr))
	//copy(res, arr)
	sort.Ints(arr)

	rank := 1
	prev := math.MinInt32
	for _, v := range arr {
		if v == prev {
			continue
		}
		for _, idx := range mapping[v] {
			res[idx] = rank
		}
		prev = v
		rank++
	}

	return res
}


// *****************************
type Ints []int

func (arr Ints) Len() int {
	return len(arr)
}

func (arr Ints) Swap(i, j int) {
	arr[i], arr[j] = arr[j], arr[i]
}

func (arr Ints) Less(i, j int) bool {
	return arr[i] < arr[j]
}