package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/**
https://leetcode.com/problems/redundant-connection/discuss/444800/DFS-simple-solution-in-golang
bricks_falling_when_hit_803.go
*/
func findRedundantConnection(edges [][]int) []int {
	var res []int

	vis := NewSet()
	grp := make(map[int][]int)

	var hasPath func(u, v int) bool
	hasPath = func(u, v int) bool {
		if u == v {
			return true
		}

		vis.Add(u)
		for _, nei := range grp[u] {
			if vis.Contains(nei) {
				continue
			}
			if hasPath(nei, v) {
				return true
			}
		}

		return false
	}

	//the order the edges are added to the graph matters.
	for _, e := range edges {
		vis.Clean()
		if hasPath(e[0], e[1]) {
			return e
		}

		grp[e[0]] = append(grp[e[0]], e[1])
		grp[e[1]] = append(grp[e[1]], e[0])
	}

	return res
}
