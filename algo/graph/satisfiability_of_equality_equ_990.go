package main

// https://leetcode.com/problems/satisfiability-of-equality-equations/solution/
// You are given an array of strings equations that represent relationships between variables where each
// string equations[i] is of length 4 and takes one of two different forms: "xi==yi" or "xi!=yi".Here, xi and yi
// are lowercase letters (not necessarily different) that represent one-letter variable names.
//
// Return true if it is possible to assign integers to variable names so as to satisfy all the given
// equations, or false otherwise.
//
//
//
// Example 1:
//
// Input: equations = ["a==b","b!=a"]
// Output: false
// Explanation: If we assign say, a = 1 and b = 1, then the first equation is satisfied, but not the second.
// There is no way to assign the variables to satisfy both equations.
//
// Example 2:
//
// Input: equations = ["b==a","a==b"]
// Output: true
// Explanation: We could assign a = 1 and b = 1 to satisfy both equations.
//
// Example 3:
//
// Input: equations = ["a==b","b==c","a==c"]
// Output: true
//
// Example 4:
//
// Input: equations = ["a==b","b!=c","c==a"]
// Output: false
//
// Example 5:
//
// Input: equations = ["c==c","b==d","x!=z"]
// Output: true
//
//
// Constraints:
//
// 1 <= equations.length <= 500
// equations[i].length == 4
// equations[i][0] is a lowercase letter.
// equations[i][1] is either '=' or '!'.
// equations[i][2] is '='.
// equations[i][3] is a lowercase letter.
//

func equationsPossible_uf(equations []string) bool {

	ids := make([]int, 26)
	for i := 0; i < 26; i++ {
		ids[i] = i
	}
	for _, e := range equations {
		if e[1] == '=' {
			u := find(ids, int(e[0]-'a'))
			v := find(ids, int(e[3]-'a'))
			ids[u] = v //union simplified
		}
	}

	for _, e := range equations {
		if e[1] == '!' {
			u := find(ids, int(e[0]-'a'))
			v := find(ids, int(e[3]-'a'))
			if u == v {
				return false
			}
		}
	}
	return true
}

func find(ids []int, i int) int {
	if ids[i] == i {
		return i
	}
	ids[i] = find(ids, ids[ids[i]]) //path compression
	return ids[i]
}

/**************************************************************************/
func equationsPossible_coloring(equations []string) bool {
	//similar to Bipartite785
	//create a graph DS for those variables linked by '=='
	//and then mark them as the same color
	//for those linked by '!=', if the are the same variable, return false
	//if they are different variables, but have different colors, return false
	grp := make(map[int][]int)
	for _, e := range equations {
		if e[1] == '!' {
			continue
		}
		x, y := int(e[0]-'a'), int(e[3]-'a')
		grp[x] = append(grp[x], y)
		grp[y] = append(grp[y], x)
	}

	//traverse the grp and market vertices as different colors/group
	colors := make([]int, 26) //26 letters
	gup := 0
	for i := 0; i < 26; i++ {
		if colors[i] == 0 { //if 0, color is not marketed yet

			gup++ //start with 1 is important
			var queue []int
			queue = append(queue, i) //starting node

			for len(queue) > 0 {
				cur := queue[0]
				queue = queue[1:]
				for _, nei := range grp[cur] {
					if colors[nei] == 0 {
						colors[nei] = gup
						queue = append(queue, nei) //enqueue for next expansion
					}
				} //for
			}
		} //for
	}

	for _, e := range equations {
		if e[1] == '!' {
			x, y := int(e[0]-'a'), int(e[3]-'a')
			if x == y || colors[x] != 0 && colors[x] == colors[y] {
				return false
			}
		}
	}
	return true
}

/**************************************************************************/
func equationsPossible(equations []string) bool {
	//parse and store variables into a map
	//all the equations are valid if we can process all of them w/o conflict
	//bug on:
	// "a!=a"
	// ["c==c","f!=a","f==b","b==c"]
	vars := make(map[byte]int)

	cnt := 1
	for _, e := range equations {
		x, y := e[0], e[3]

		_, xfound := vars[x]
		_, yfound := vars[y]
		if xfound && yfound {
			if e[2] == '=' && vars[x] != vars[y] {
				return false
			}
			if e[2] == '!' && vars[x] == vars[y] {
				return false
			}
		}

		valX, valY := 0, 0
		if e[2] == '=' { //equal equations
			if val, exist := vars[x]; exist {
				valX = val
				valY = valX
			} else {
				valY = cnt
				if v, found := vars[y]; found { //find variable Y
					valY = v
				}
				valX = valY
				cnt++
			} //
		} else {
			//not equal, the variables should be assigned different cnt values
			valX = cnt
			if val, found := vars[x]; found {
				valX = val
			}
			cnt++
			valY = cnt
			if val, found := vars[y]; found {
				valY = val
			}
			cnt++
		}
		vars[x] = valX
		vars[y] = valY
	}
	return true
}
