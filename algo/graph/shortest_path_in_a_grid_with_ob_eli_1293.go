package main

import "math"

// https://leetcode.com/problems/shortest-path-in-a-grid-with-obstacles-elimination/discuss/1485928/BFS-with-comments

func shortestPath(grid [][]int, k int) int {
	rows, cols := len(grid), len(grid[0])

	if k >= rows+cols-2 {
		return rows + cols - 2
	}
	var queue [][]int
	queue = append(queue, []int{0, 0, 0})

	vis := make([][]int, len(grid))
	for i := 0; i < len(vis); i++ {
		vis[i] = make([]int, len(grid[0]))
		for j := range vis[i] {
			vis[i][j] = math.MaxInt32
		}
	}
	steps := 0
	for len(queue) > 0 {
		size := len(queue)
		for size > 0 {
			cur := queue[0]
			queue = queue[1:]
			if cur[0] == len(grid)-1 && cur[1] == len(grid[0])-1 {
				return steps
			}

			for i := 0; i < 4; i++ {
				nx, ny := cur[0]+dirs[i], cur[1]+dirs[i+1]
				if nx < 0 || nx >= rows || ny < 0 || ny >= cols {
					continue
				}
				nextEli := grid[nx][ny] + cur[2]
				if nextEli <= k && nextEli < vis[nx][ny] {
					vis[nx][ny] = nextEli
					queue = append(queue, []int{nx, ny, nextEli})
				}
			}
			size--
		}
		steps++
	}
	return -1
}
