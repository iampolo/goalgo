package v2

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/unionfind"
)

/**
https://leetcode.com/problems/redundant-connection/discuss/444800/DFS-simple-solution-in-golang
*/
func findRedundantConnection(edges [][]int) []int {
	var res []int

	var uf = unionfind.NewUnionFind(len(edges) + 1)

	for i := 0; i < len(edges); i++ {
		uf.Ids[i] = i
	}

	for _, ed := range edges {
		p, q := uf.Find(ed[0]), uf.Find(ed[1])
		if p == q {
			return ed
		}
		uf.Union(ed[0], ed[1])

	}
	return res
}

func main() {
	Do684_v2()
}

func Do684_v2() {
	edges := [][]int{{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}}
	fmt.Println(findRedundantConnection(edges))

}
