package main

/*
https://leetcode.com/problems/walls-and-gates/solution/


TLE
 */
func wallsAndGates(rooms [][]int) {

	var dfs func(r, c int, steps int)
	dfs = func(r, c int, steps int) {
		if r < 0 || r >= len(rooms) || c < 0 || c >= len(rooms[0]) || rooms[r][c] == -1 {
			return
		}

		//in the beginning, so that the cell with gate(0) can be processed
		if steps <= rooms[r][c] {
			rooms[r][c] = steps
			dfs(r+1, c, steps+1)
			dfs(r-1, c, steps+1)
			dfs(r, c+1, steps+1)
			dfs(r, c-1, steps+1)
		}
	}

	for r := 0; r < len(rooms); r++ {
		for c := 0; c < len(rooms[0]); c++ {
			if rooms[r][c] == 0 {
				dfs(r, c, 0)
			}
		}
	}
}
