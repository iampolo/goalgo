package main

import "gitlab.com/iampolo/goalgo/algo/util"

func candy_v2(ratings []int) int {
	if len(ratings) == 0 {
		return 0
	}

	up, down, peak := 0, 0, 0
	res := 1       //!
	for i := 1; i < len(ratings); i++ {
		if ratings[i-1] < ratings[i] {
			up++
			peak = up
			down = 0
			res += up + 1 //
		} else if ratings[i-1] == ratings[i] { //plateau
			up, down, peak = 0, 0, 0
			res++
		} else {
			//down
			down, up = down+1, 0
			res += down
			if down > peak {
				// Give the peak point one more candy because there are more down points than up points
				res++
			}
		}
	}

	return res
}

func candy(ratings []int) int {
	cnt := make([]int, len(ratings))
	for i := 0; i < len(cnt); i++ {
		cnt[i] = 1
	}

	for i := 1; i < len(ratings); i++ {
		if ratings[i-1] < ratings[i] {
			cnt[i] = cnt[i-1] + 1 //get candies more than the neighbors
		}
	}

	for i := len(ratings) - 2; i >= 0; i-- {
		if ratings[i] > ratings[i+1] {
			cnt[i] = util.Max(cnt[i], cnt[i+1]+1)
		}
	}

	//this can be part of the second loop
	res := 0
	for _, v := range cnt {
		res += v
	}

	return res
}
