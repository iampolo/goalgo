package main

import (
	"fmt"
)

func main() {
	test_857()
}

func test_857() {
	quality, wage := []int{3, 1, 10, 10, 1}, []int{4, 8, 2, 2, 7}
	k := 3
	fmt.Println(mincostToHireWorkers(quality, wage, k))
}

func test_1328() {
	s := "abccba"
	fmt.Println(breakPalindrome(s))
}

func test1338() {
	nums := []int{0, 1, 20, 9, 8, 7}
	fmt.Println(candy_v2(nums))

	run1338()
}

func run1338() {
	arr := []int{3, 3, 3, 3, 5, 5, 5, 2, 2, 7}
	fmt.Println(minSetSize(arr))
	fmt.Println(minSetSize_v2(arr))
}

func test838() {
	dom := ".L.R...LR..L.."
	dom = ".L.R...LR..L.."
	fmt.Println(pushDominoes(dom))
}
