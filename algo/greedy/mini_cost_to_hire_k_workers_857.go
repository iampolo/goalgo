package main

import (
	"math"
	"sort"
)

// https://leetcode.com/problems/minimum-cost-to-hire-k-workers/

// There are n workers. You are given two integer arrays quality and wage where quality[i] is the quality of the ith
// worker and wage[i] is the minimum wage expectation for the ith worker.
//
// We want to hire exactly k workers to form a paid group. To hire a group of k workers, we must pay them according
// to the following rules:
//
// - Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.
// - Every worker in the paid group must be paid at least their minimum wage expectation.
//
// Given the integer k, return the least amount of money needed to form a paid group satisfying the above conditions.
// Answers within 10^-5 of the actual answer will be accepted.
//
//
// Example 1:
//
// Input: quality = [10,20,5], wage = [70,50,30], k = 2
// Output: 105.00000
// Explanation: We pay 70 to 0th worker and 35 to 2nd worker.
//
// Example 2:
//
// Input: quality = [3,1,10,10,1], wage = [4,8,2,2,7], k = 3
// Output: 30.66667
// Explanation: We pay 4 to 0th worker, 13.33333 to 2nd and 3rd workers separately.
//
//
// Constraints:
//
// n == quality.length == wage.length
// 1 <= k <= n <= 10^4
// 1 <= quality[i], wage[i] <= 10^4
//
// ***********************************************************
//     10, 20, 5
//     70, 50, 30
//	 k=2
//  there is at least one worker to be paid with his minimum wage.
//  if we pick worker-0 as the base, we paid 70, so 1Q=1/7W
//  hire worker-2 : 5 * 1/7d = 35
//      total 70+35 = 105    (the optimal)
//

//https://www.youtube.com/watch?v=o8emK4ehhq0&ab_channel=ShiranAfergan
//
func mincostToHireWorkers(quality []int, wage []int, k int) float64 {
	//loop for each work, set him as the base, and them hire other k- 1 work
	//each worker can be the base, and find the optimal team.

	// O(n^2*log(n))
	//n => length
	var optimal float64 = math.MaxFloat64
	length := len(quality)
	for i := 0; i < length; i++ {
		//get a base rate from each of the workers
		rate := float64(wage[i]) / float64(quality[i])

		//see how other team members can be hired.
		//actually, here the base already included
		var teamPrice []float64
		var idx int
		for j := 0; j < length; j++ {
			tRate := rate * float64(quality[j])
			if tRate < float64(wage[j]) {
				continue //not hireable
			}
			teamPrice = append(teamPrice, tRate)
			idx++
		}


		if len(teamPrice) < k {
			continue  //team can't be formed due to wage issue
		}
		sort.Float64s(teamPrice)
		//hire k members
		curRate := 0.0
		for l := 0; l < k && l < len(teamPrice); l++ {
			curRate += teamPrice[l]
		}

		optimal = math.Min(optimal, curRate)
	}

	return optimal
}

func mincostToHireWorkers_heap(quality []int, wage []int, k int) float64 {
	//java version
	return 0.0
}
