package main

/**
https://leetcode.com/problems/push-dominoes/
https://leetcode.jp/leetcode-838-push-dominoes-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
*/

func pushDominoes(dominoes string) string {

	length := len(dominoes)
	idx, sym := make([]int, length+2), make([]byte, length+2)

	idx[0] = -1
	sym[0] = 'L'
	pt := 1
	for i := 0; i < len(dominoes); i++ {
		if dominoes[i] != '.' {
			idx[pt] = i
			sym[pt] = dominoes[i]
			pt++
		}
	}

	idx[pt] = length
	sym[pt] = 'R'
	pt++

	res := []byte(dominoes)
	for l := 0; l < pt-1; l++ {
		i, j := idx[l], idx[l+1]
		ci, cj := sym[l], sym[l+1]

		if ci == cj {
			for k := i + 1; k < j; k++ {
				res[k] = ci
			}
		} else if ci > cj { //RL
			for k := i + 1; k < j; k++ {
				if k-i == j-k {
					res[k] = '.'
				} else if k-i > j-k {
					res[k] = 'L'
				} else {
					res[k] = 'R'
				}
			}
		}
	}
	return string(res)
}
