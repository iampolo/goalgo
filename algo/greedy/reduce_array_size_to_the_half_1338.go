package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

/**
https://leetcode.com/problems/reduce-array-size-to-the-half/

Given an array arr.  You can choose a set of integers and remove all the occurrences of these integers
in the array.

Return the minimum size of the set so that at least half of the integers of the array are removed.



Example 1:

Input: arr = [3,3,3,3,5,5,5,2,2,7]
Output: 2
Explanation: Choosing {3,7} will make the new array [5,5,5,2,2] which has size 5 (i.e equal to half of the size of the old array).
Possible sets of size 2 are {3,5},{3,2},{5,2}.
Choosing set {2,7} is not possible as it will make the new array [3,3,3,3,5,5,5] which has size greater than half of the size of the old array.
Example 2:

Input: arr = [7,7,7,7,7,7]
Output: 1
Explanation: The only possible set you can choose is {7}. This will make the new array empty.
Example 3:

Input: arr = [1,9]
Output: 1
Example 4:

Input: arr = [1000,1000,3,7]
Output: 1
Example 5:

Input: arr = [1,2,3,4,5,6,7,8,9,10]
Output: 5


Constraints:

1 <= arr.length <= 10^5
arr.length is even.
1 <= arr[i] <= 10^5

@see MaxiFreqStack895
*/
func minSetSize_v2(arr []int) int {
	cnt := make([]int, 1e5+1)
	for _, v := range arr {
		cnt[v]++
	}

	//sort in descending order
	sort.Slice(cnt, func(i, j int) bool {
		return cnt[i] > cnt[j]
	})

	total, res := len(arr), 0
	for i := 0; i < len(cnt) && cnt[i] != 0; i++ {
		res++           //unique count
		total -= cnt[i] //total freq./count deduction
		if total <= len(arr)/2 {
			return res
		}
	}
	return len(arr)
}

func minSetSize(arr []int) int {
	cntMap := make(map[int]int)

	for _, n := range arr {
		//if _, exists := cntMap[n]; !exists {
		//	cntMap[n] = 0 //dont need to
		//}
		cntMap[n]++
	}

	maxCnt := 0
	bucket := make([]int, 1e5+1)
	for _, c := range cntMap {
		bucket[c]++ // number of unique values in this index
		maxCnt = Max(maxCnt, c)
	}

	//res:is the unique count
	//removed:the is size of the unique elements removed
	res, half := 0, len(arr)/2
	removed := 0
	for half > removed {
		res++ //an unique removal from the array
		for bucket[maxCnt] == 0 {
			maxCnt-- //scan to next available count
		}
		removed += maxCnt //add to total consumed/removed count
		bucket[maxCnt]--  //mark consumed this count
	}
	return res
}
