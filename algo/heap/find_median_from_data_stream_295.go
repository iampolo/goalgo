package main

import "container/heap"

/**
https://leetcode.com/problems/find-median-from-data-stream/
*/

type MedianFinder struct {
	smHeap *Heap
	laHeap *Heap
}

/** initialize your data structure here. */
func Constructor() MedianFinder {
	return MedianFinder{
		smHeap: NewHeap(func(i, j int) bool {
			return i > j
		}),
		laHeap: NewHeap(func(i, j int) bool {
			return i < j
		}),
	}
}

func (mf *MedianFinder) AddNum(num int) {
	if mf.smHeap.Len() == 0 || num <= mf.smHeap.Peek() {
		heap.Push(mf.smHeap, num) //
	} else {
		heap.Push(mf.laHeap, num)
	}

	if mf.smHeap.Len()-1 > mf.laHeap.Len() {
		heap.Push(mf.laHeap, heap.Pop(mf.smHeap))
	} else if mf.smHeap.Len() < mf.laHeap.Len() {
		heap.Push(mf.smHeap, heap.Pop(mf.laHeap))
	}
}

func (mf *MedianFinder) FindMedian() float64 {
	size := mf.laHeap.Len() + mf.smHeap.Len()

	if size == 0 {
		return 0
	} else if size%2 == 1 {
		return float64(mf.smHeap.Peek())
	} else {
		return float64(mf.smHeap.Peek() + mf.laHeap.Peek())/2
	}
}

type Heap struct {
	value    []int
	lessFunc func(i, j int) bool
}

func (h *Heap) Less(i, j int) bool {
	return h.lessFunc(h.value[i], h.value[j])
}

func (h *Heap) Swap(i, j int) {
	h.value[i], h.value[j] = h.value[j], h.value[i]
}

func (h *Heap) Len() int {
	return len(h.value)
}

func (h *Heap) Peek() int {
	return h.value[0]
}

func (h *Heap) Pop() (x interface{}) { //trick on =
	h.value, x = h.value[:h.Len()-1], h.value[h.Len()-1]
	return x
}

func (h *Heap) Push(x interface{}) {
	h.value = append(h.value, x.(int))
}

func NewHeap(less func(int, int) bool) *Heap {
	return &Heap{lessFunc: less}
}
