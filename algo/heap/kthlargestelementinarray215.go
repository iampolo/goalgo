package main

import (
	"container/heap"
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

/*
Find the kth largest element in an unsorted array. Note that it is the kth
largest element in the sorted order, not the kth distinct element.

Example 1:

Input: [3,2,1,5,6,4] and k = 2
Output: 5

Example 2:

Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4

Note:
You may assume k is always valid, 1 ≤ k ≤ array's length.
*/
//8ms : O(k + (n-k)logk)
func findKthLargest(nums []int, k int) int {
	h := &util.IntHeap{}
	heap.Init(h)

	for i := 0; i < len(nums); i++ {
		heap.Push(h, nums[i])
		if h.Len() > k {
			heap.Remove(h, 0)
		}
	}

	return (*h)[0]
}

/********************************************************************************/
func main_() {
	nums := []int{3, 2, 1, 5, 6, 4}
	nums = []int{8, 4, 10, 7, 2, 11, 6, 13, 3}

	k := 5
	res := findKthLargest(nums, k)
	fmt.Println(res)


	fmt.Println(findKthLargest_quickselect(nums, k))
}
/********************************************************************************/
func findKthLargest_quickselect(nums[]int, k int) int {
	left, right := 0, len(nums) - 1

	kth := len(nums) - k
	for left < right {
		pivot := partition(nums, left, right)
		if pivot < kth {
			left = pivot + 1
		} else if pivot > kth {
			right = pivot - 1
		} else {
			return nums[pivot]
		}
	}
	return nums[left]
}

func partition(nums []int, left int, right int) int {
	pivot := left + (right - left) / 2
	nums[pivot], nums[right] = nums[right], nums[pivot]
	rr := right - 1
	for left <= rr {
		if nums[left] <= nums[right] {
			left++
		} else if nums[rr] >= nums[right] {
			rr--
		} else {
			nums[left], nums[rr] = nums[rr], nums[left]
		}
	}
	nums[left], nums[right] = nums[right], nums[left]
	return left
}

/********************************************************************************/
func findKthLargest_slow(nums []int, k int) int {
	if len(nums) == 0 || k == 0 {
		return 0
	}
	for i := 0; i < k; i++ {
		max := i
		for j := i; j < len(nums); j++ {
			if nums[max] < nums[j] {
				max = j
			}
		}
		nums[i], nums[max] = nums[max], nums[i]
	}
	return nums[k-1]

}
