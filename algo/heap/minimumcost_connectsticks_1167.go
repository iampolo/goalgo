package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/minimum-cost-to-connect-sticks/

// The earlier combined sticks are added more times, so if we want to minimize cost,
// we will want to combine the smaller ones as early as possible.
func connectSticks(sticks []int) int {
	pq := util.PriorityQueue{}
	for _, v := range sticks {
		pq.Push(&util.Item{v, 0, 0})
	}

	for _, r := range pq {
		fmt.Println(r)
	}
	return 0
}

func main_1167() {
	s := []int{1, 8, 3, 5}

	connectSticks(s)
}
