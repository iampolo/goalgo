package main

import (
	"container/heap"
)

//https://leetcode.com/problems/process-tasks-using-servers/

//https://leetcode.com/problems/process-tasks-using-servers/discuss/1309604/Golang-700ms-heap-solution-with-comment
// ProcessTasksUsingServers1882

func assignTasks(servers []int, tasks []int) []int {

	availSrv := &serverHeap{}
	for i := 0; i < len(servers); i++ {
		heap.Push(availSrv, []int{i, servers[i], 0})
	}

	runningSrv := &serverHeap{}


	res := make([]int, len(tasks))
	idx, time := 0, 0
	for idx < len(tasks) {
		//peak without going through heap.* won't work
		for runningSrv.Len() > 0 && runningSrv.Peek().([]int)[2] <= time {
			cur := heap.Pop(runningSrv).([]int) //this task is done
			cur[2] = 0
			heap.Push(availSrv, cur)
		}

		for availSrv.Len() > 0 && idx <= time && idx < len(tasks) {
			cur := heap.Pop(availSrv).([]int)
			//assigning a task to the server
			cur[2] = time + tasks[idx]
			heap.Push(runningSrv, cur)
			res[idx] = cur[0]
			idx++
		}

		if availSrv.Len() == 0 {
			time = runningSrv.Peek().([]int)[2]
		} else {
			time = idx
		}
	} //

	return res
}
