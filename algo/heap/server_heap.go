package main

// https://leetcode.com/problems/process-tasks-using-servers/discuss/1309604/Golang-700ms-heap-solution-with-comment
type serverHeap [][]int

func (sh *serverHeap) Swap(i, j int) {
	(*sh)[i], (*sh)[j] = (*sh)[j], (*sh)[i]
}

func (sh *serverHeap) Len() int {
	return len(*sh)
}

//can't create a Less that support differnt
// comparision logic
func (sh *serverHeap) Less(i, j int) bool {
	if (*sh)[i][2] != 0 {
		return (*sh)[i][2] < (*sh)[j][2]
	}
	//weight is the same
	if (*sh)[i][1] == (*sh)[j][1] {
		return (*sh)[i][0] < (*sh)[j][0]
	}
	//weight is diff.
	return (*sh)[i][1] < (*sh)[j][1]
}

func (sh *serverHeap) Push(x interface{}) {
	*sh = append(*sh, x.([]int))
}

func (sh *serverHeap) Pop() interface{} {
	v := (*sh)[len(*sh)-1] //pick the last one
	*sh = (*sh)[:len(*sh)-1]
	return v
}

//peak without going through heap.* won't work
// so, hardcoded to return the first element in the array
// https://stackoverflow.com/questions/63328886/peeking-into-the-top-of-the-priority-queue-in-go
func (sh *serverHeap) Peek() interface{} {
	return (*sh)[0]
}