package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)
//BuG

// https://leetcode.com/problems/swim-in-rising-water/
func swimInWater(grid [][]int) int {

	heap := new(MinHeap)  //need a callback func to allow custom sort

	heap.Push(grid[0][0])
	set := NewSet()
	set.Add(0)

	time := 0
	len := len(grid[0])

	for heap.Len() > 0 {
		cur := heap.Pop().(int) //return the min

		x := cur / len
		y := cur % len

		time = Max(time, grid[x][y]) //get the max

		if x == len-1 && y == len-1 {
			return time
		}

		for i := 0; i < 4; i++ {
			nx, ny := x+DIR[i], y+DIR[i+1]
			pos := nx*len + ny

			if nx >= 0 && ny >= 0 && nx < len && ny < len && !set.Contains(pos) {
				set.Add(pos)
				heap.Push(grid[nx][ny])
			}
		}
	}

	return -1
}

var DIR = [...]int{0, -1, 0, 1, 0} //fixed sized array instead of slice
