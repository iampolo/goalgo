package main

import (
	"fmt"
)

func swimInWater(grid [][]int) int {

	l := len(grid)
	//guess a time
	lo, hi := 0, l*l-1

	for lo < hi {
		vis := make([][]bool, l)
		for i := range vis {
			vis[i] = make([]bool, l)
		}
		mid := lo + (hi-lo)/2
		if dfs(grid, 0, 0, mid, vis) {
			hi = mid //find shorter time
		} else {
			lo = mid + 1 //prevent infinite loop
		}
	}

	return lo
}

func dfs(grid [][]int, r int, c int, time int, vis [][]bool) bool {
	if grid[r][c] > time {
		return false
	}

	if r == len(grid)-1 && c == len(grid)-1 {
		return true
	}

	vis[r][c] = true
	for i := 0; i < 4; i++ {
		nr, nc := r+DIR[i], c+DIR[i+1]
		if nr >= 0 && nc >= 0 && nr < len(grid) && nc < len(grid) &&
			!vis[nr][nc] && grid[nr][nc] <= time {
			if dfs(grid, nr, nc, time, vis) {
				return true
			}
		} //
	}

	return false
}

var DIR = [...]int{0, -1, 0, 1, 0} //fixed sized array instead of slice

func main() {

	grid := [][]int{{0, 1}, {1, 3}}

	fmt.Println(swimInWater(grid))

}
