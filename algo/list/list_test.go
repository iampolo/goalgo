package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/model"
	"testing"
)

func Test143(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5}

	ll := model.CreateList(arr)

	reorderList(ll)

	fmt.Println(ll.String())
}

func Test24(t *testing.T) {
	arr := []int{1, 2, 3, 4, 5, 7}

	ll := model.CreateList(arr)

	ll = swapPairs(ll)

	fmt.Println("res:", ll.String())
}