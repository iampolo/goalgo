package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/model"
)

func main() {
	test_725()
}

func test_725() {

	head := model.CreateList([]int{1, 2, 3, 4, 5})
	k := 3

	fmt.Println(splitListToParts_v2(head, k))

	head = model.CreateList([]int{1, 2, 3, 4, 5})
	fmt.Println(splitListToParts(head, k))
}

func test_reverse() {
	head := model.CreateList([]int{1, 2, 3, 4, 5})
	fmt.Println(reverseBetween_v2(head, 2, 4))
}
