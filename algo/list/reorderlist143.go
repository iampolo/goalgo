package main

import (
	"gitlab.com/iampolo/goalgo/algo/model"
)

func reorderList(head *model.ListNode)  {
	if head == nil {
		return
	}

	slow, fast := head, head
	for fast.Next != nil && fast.Next.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}

	second := slow.Next
	slow.Next = nil
	second = reverse(second)

	for head  != nil {
		t := head.Next
		head.Next = second

		head = second
		second = t
	}
}

func reverse(node *model.ListNode) *model.ListNode {
	if node == nil || node.Next == nil {
		return node
	}
	next := node.Next
	newHead := reverse(next)
	next.Next = node
	node.Next = nil
	return newHead
}