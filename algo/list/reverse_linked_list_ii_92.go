package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/**
https://leetcode.com/problems/reverse-linked-list-ii/submissions/
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
*/
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	if head == nil {
		return head
	}
	temp := new(ListNode)
	pre := new(ListNode)
	pre, temp.Next = temp, head

	//fixate the left and right boundaries
	m, n := head, head
	for left-1 > 0 {
		left--
		pre = m
		m = m.Next
	}
	for right-1 > 0 {
		right--
		n = n.Next
	}

	for m != n {
		pre.Next = m.Next
		m.Next = n.Next
		n.Next = m
		m = pre.Next
	}

	return temp.Next
}

func reverseBetween_v2(head *ListNode, left int, right int) *ListNode {
	if head == nil {
		return head
	}

	cur := head
	var prev *ListNode = nil //pointing at one before left
	for left > 1 {           //no move if it is exact 1
		left--
		right--
		prev = cur
		cur = cur.Next
	}

	tail := cur  //the node at left, will be the tail of the reversed portion.
	prevv := prev

	for right > 0 {
		next := cur.Next
		cur.Next = prev

		prev = cur
		cur = next
		right--
	}

	if prevv == nil { //the left is at 1!
		head = prev
	} else {
		prevv.Next = prev
	}

	tail.Next = cur
	return head
}
