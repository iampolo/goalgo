package main

import . "gitlab.com/iampolo/goalgo/algo/model"

// https://leetcode.com/problems/split-linked-list-in-parts/

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

func splitListToParts_v2(head *ListNode, k int) []*ListNode {
	getLen := func(node *ListNode) int {
		var length int
		for node != nil {
			length++
			node = node.Next
		}
		return length
	}

	length := getLen(head)

	//nodes per group
	nodeCnt := length / k
	//one extra for the first few group
	extra := length % k

	//pre create and each element is a nil
	res := make([]*ListNode, k)
	for i := 0; i < k && head != nil; i++ {
		res[i] = head

		nCnt := nodeCnt
		if i < extra {
			nCnt++
		}
		for j := 0; j < nCnt - 1 /*&& head != nil*/; j++ { // no need to check nil
			head = head.Next
		}

		//cut the link
		tmp := head.Next
		head.Next = nil
		head = tmp
	}

	return res
}

func splitListToParts(head *ListNode, k int) []*ListNode {
	getLen := func(node *ListNode) int {
		var length int
		for node != nil {
			length++
			node = node.Next
		}
		return length
	}

	length := getLen(head)

	//nodes per group
	nodeCnt := length / k
	//one extra for the first few group
	rem := length % k

	var res []*ListNode
	for i := 0; i < k; i++ {
		partHead := &ListNode{}
		cur := partHead

		//the first i < remainder parts has one more
		nCnt := nodeCnt
		if i < rem {
			nCnt++
		}
		for j := 0; j < nCnt && head != nil; j++ {
			cur.Next = head

			head = head.Next
			cur = cur.Next
		}
		cur.Next = nil //de-link the possible linkage to the head
		res = append(res, partHead.Next)
	}

	return res
}
