package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/*
Given a linked list, swap every two adjacent nodes and return its head.
You may not modify the values in the list's nodes, only nodes itself may be changed.

Example:
Given 1->2->3->4, you should return the list as 2->1->4->3.

 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
*/
func swapPairs(head *ListNode) *ListNode {
	if head == nil {
		return head;
	}

	dummy := &ListNode{Val: -1} //a node before the first node
	prev := dummy
	prev.Next = head
	cur := head                         //cur right after prev
	for cur != nil && cur.Next != nil { //do if there are two nodes
		prev.Next = cur.Next
		cur.Next = cur.Next.Next
		prev.Next.Next = cur
		//advance and maintain prev and cur pointers meaning
		prev = cur
		cur = cur.Next
	}
	return dummy.Next
}

func swapPairs_my(head *ListNode) *ListNode {
	if head == nil {
		return head;
	}

	dummy := &ListNode{Val: -1}
	dummy.Next = head
	cur := head
	for cur != nil && cur.Next != nil { //do if there are two nodes
		next := cur.Next.Next;
		cur.Next.Next = cur
		cur.Next = next

		dummy.Next = cur
		cur = next

	}

	return dummy
}
