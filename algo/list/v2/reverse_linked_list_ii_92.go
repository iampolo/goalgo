package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/**
https://leetcode.com/problems/reverse-linked-list-ii/submissions/
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }

@see ReverseLinkedListII92#Method 3
*/
func reverseBetween(head *ListNode, left int, right int) *ListNode {
	dummy := new(ListNode)
	head, dummy.Next = dummy, head

	for i := 0; i < left-1; i++ { //move to the front of left
		head = head.Next
	}

	//similar to original reverse a linkedlist: lc206
	var curr, prev *ListNode = head.Next, nil
	for i := 0; i < right-left+1; i++ {
		next := curr.Next
		curr.Next = prev
		prev = curr
		curr = next
	}
	head.Next.Next = curr //curr point the one after right once the loop finishes.
	head.Next = prev      //prev point at the right once the loop finishes
	return dummy.Next
}

//not changing head
func reverseBetween_V2(head *ListNode, left int, right int) *ListNode {
	dummy := new(ListNode)
	beforem := new(ListNode)
	beforem, dummy.Next = dummy, head

	for i := 0; i < left-1; i++ { //move to the front of left
		beforem = beforem.Next
	}

	//similar to original reverse a linkedlist: lc206
	var curr, prev *ListNode = beforem.Next, nil
	for i := 0; i < right-left+1; i++ {
		next := curr.Next
		curr.Next = prev
		prev = curr
		curr = next
	}
	beforem.Next.Next = curr //curr point the one after right once the loop finishes.
	beforem.Next = prev      //prev point at the right once the loop finishes
	return dummy.Next
}

func main() {

	head := CreateList([]int{1, 2, 3, 4, 5})
	//head = CreateList([]int{1})
	l, r := 2, 4
	fmt.Println(reverseBetween_V2(head, l, r))

}
