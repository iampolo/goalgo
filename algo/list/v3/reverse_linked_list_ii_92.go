package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/**
https://leetcode.com/problems/reverse-linked-list-ii/submissions/
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
*/

/**
https://leetcode.com/problems/reverse-linked-list/
*/
func reverseList_206(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	last := reverseList_206(head.Next)
	head.Next.Next = head
	head.Next = nil
	return last
}
/*************************/

func main() {

	head := CreateList([]int{1, 2, 3, 4, 5})
	head = CreateList([]int{1})
	l, r := 1, 1

	fmt.Println(reverseBetween(head, l, r))
}

func reverse(head *ListNode, cnt int) (*ListNode, *ListNode) {
	if cnt == 1 { //the last one
		return head, head.Next //there could be another node,we only do m..n
	}
	last, next := reverse(head.Next, cnt-1)
	//change the head pointer at this level
	head.Next.Next = head
	head.Next = next

	return last, next
}

func reverseBetween(head *ListNode, left int, right int) *ListNode {
	dummy := new(ListNode)
	beforem := new(ListNode)
	beforem, dummy.Next = dummy, head

	for i := 0; i < left-1; i++ { //move to the front of left
		beforem = beforem.Next
	}

	//start the reverse at node_left
	newHead, _ := reverse(beforem.Next, right-left+1)
	beforem.Next = newHead
	return dummy.Next
}
