package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/**
https://leetcode.com/problems/reverse-linked-list-ii/submissions/
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
*/

/*************************/
func main() {

	head := CreateList([]int{1, 2, 3, 4, 5})
	head = CreateList([]int{1})
	head = CreateList([]int{1, 2, 3, 4, 5})
	l, r := 1, 2

	fmt.Println(reverseBetween(head, l, r))
}

//recursion
func reverseBetween(head *ListNode, m int, n int) *ListNode {
	left = head
	stop = false
	reverse(head, m, n)
	return head
}

var left *ListNode
var stop bool
func reverse(right *ListNode, m, n int) {
	//can't go any deeper
	if n == 1 {
		return
	}
	//move right for this layer
	right = right.Next

	if m > 1 {
		left = left.Next
	}

	//go the next layer: left -> 	right ->
	reverse(right, m - 1, n - 1)

	//go the next layer: left ->     <- right
	if left == right || right.Next == left {
		stop = true
	}

	//reverse by swapping values
	if !stop {
		tmp := right.Val
		right.Val = left.Val
		left.Val = tmp

		left = left.Next
	}
	//back to previous recursion:go back to the previous Right
}
