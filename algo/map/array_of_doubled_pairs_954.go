package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

// https://leetcode.com/problems/array-of-doubled-pairs/

// Given an array of integers arr of even length, return true if and only if it is possible to
// reorder it such that arr[2 * i + 1] = 2 * arr[2 * i] for every 0 <= i < len(arr) / 2.
//
//
//
//Example 1:
//
//Input: arr = [3,1,3,6]
//Output: false
//
//Example 2:
//
//Input: arr = [2,1,2,6]
//Output: false
//
//Example 3:
//
//Input: arr = [4,-2,2,-4]
//Output: true
//Explanation: We can take two groups, [-2,-4] and [2,4] to form [-2,-4,2,4] or [2,4,-2,-4].
//
//Example 4:
//
//Input: arr = [1,2,4,16,8,4]
//Output: false
//
//
//Constraints:
//
//0 <= arr.length <= 3 * 10^4
//arr.length is even.
//-10^5 <= arr[i] <= 10^5
//
//find_original_array_from_doubled_arr_2007.go
//
func canReorderDoubled_v2(nums []int) bool {
	cntMap := map[int]int{}
	sort.Ints(nums)

	for _, n := range nums {
		//look forward to find the complement
		if cnt, exists := cntMap[n*2]; exists && cnt > 0 {
			cntMap[n*2]-- //mark as paired
			continue
		}

		if n%2 == 0 {
			//look backward to find the complement
			if cnt, exists := cntMap[n/2]; exists && cnt > 0 {
				cntMap[n/2]-- //the complement exists and mark
				continue
			}
		}
		//add the number
		cntMap[n]++
	}

	for _, v := range cntMap {
		if v > 0 {
			return false
		}
	}
	return true
}

// the question is asking to put the numbers in the two groups
// the value in group one is multiple of the value of elements in another group.
// IMPORTANT
// i < len(4)/2
// i = 0    => a[0] and a[1]
// i = 1    -> a[2] and a[3]
// so it is always a pair, and never overlay
//
func canReorderDoubled(arr []int) bool {
	cntMap := make(map[int]int)

	for _, v := range arr {
		cntMap[v] = cntMap[v] + 1
	}

	//sort the array make it easier
	//sort by abs value, so that -4 can be in front of -8, [-4,-8], and not [-8, -4]
	//if the nums are not sorted like this, it is equivalent to not sorted,
	//then special handling is needed.
	// [ -4, -2, 2, 4]
	//  -4/2=>-2, 2*2 => 4  : use divide or multiple
	sort.SliceStable(arr, func(i, j int) bool {
		return Abs(arr[i]) < Abs(arr[j])
	})

	//scan from smaller abs-value.  up to half is enough
	for _, v := range arr {
		var exist bool
		var cnt int
		//is the number used before?
		if cnt, exist = cntMap[v]; exist && cnt == 0 {
			continue
		}
		//if the compliment doesn't exist
		if cnt, compExist := cntMap[2*v]; !compExist || cnt == 0 {
			return false
		}

		// mark numbers as used
		cntMap[v]--
		cntMap[v*2]--
	}

	return true
}

// https://leetcode.com/problems/array-of-doubled-pairs/discuss/799634/Golang
