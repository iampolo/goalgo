package main

import (
	"fmt"
	"strings"
)

/**
https://leetcode.com/problems/custom-sort-string/
*/

func customSortString(order string, str string) string {
	bucket := make([]int, 26)

	for _, c := range str {
		bucket[c-'a']++
	}

	res := strings.Builder{}

	for _, c := range order {
		for bucket[c-'a'] > 0 {
			fmt.Fprintf(&res, "%c", c)
			bucket[c-'a']--
		}
	}

	for i := 'a'; i <= 'z'; i++ {
		for bucket[i-'a'] > 0 {
			fmt.Fprintf(&res, "%c", i)
			bucket[i-'a']--
		}
	}

	return res.String()
}
