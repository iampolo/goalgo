package main

import "sort"

// https://leetcode.com/problems/find-original-array-from-doubled-array/
// An integer array original is transformed into a doubled array changed by appending twice the value of every
// element in original, and then randomly shuffling the resulting array.
//
// Given an array changed, return original if changed is a doubled array. If changed is not a doubled array, return
// an empty array. The elements in original may be returned in any order.
//
//
//
// Example 1:
//
// Input: changed = [1,3,4,2,6,8]
// Output: [1,3,4]
// Explanation: One possible original array could be [1,3,4]:
// - Twice the value of 1 is 1 * 2 = 2.
// - Twice the value of 3 is 3 * 2 = 6.
// - Twice the value of 4 is 4 * 2 = 8.
// Other original arrays could be [4,3,1] or [3,1,4].
//
// Example 2:
//
// Input: changed = [6,3,0,1]
// Output: []
// Explanation: changed is not a doubled array.
//
// Example 3:
//
// Input: changed = [1]
// Output: []
// Explanation: changed is not a doubled array.
//
//
// Constraints:
//
// 1 <= changed.length <= 10^5
// 0 <= changed[i] <= 10^5
//
//BUG
func findOriginalArray_map(changed []int) []int {
	cntMap := make(map[int]int)

	var res []int
	for _, v := range changed {
		double := v * 2
		if _, exist := cntMap[double]; exist {
			if cntMap[double] == 1 {
				delete(cntMap, double)
			} else {
				cntMap[double]--
			}
			res = append(res, v)
		} else {
			cntMap[v]++
		}
	}
	if len(res) != len(changed)/2 {
		return []int{}
	}
	return res
}

//good
// [1,3,4,2,6,8]
func findOriginalArray_map_v2(changed []int) []int {
	if len(changed)%2 != 0 {
		return []int{}
	}
	//important
	sort.Ints(changed)
	cntMap := make(map[int]int)

	var res []int
	idx := len(changed) - 1
	for idx >= 0 {
		double := changed[idx] * 2

		if _, exist := cntMap[double]; exist {
			if cntMap[double] == 1 {
				delete(cntMap, double)
			} else {
				cntMap[double]--
			}
			res = append(res, changed[idx])
		} else {
			cntMap[changed[idx]]++
		}
		idx--
	}
	if len(res) != len(changed)/2 {
		return []int{}
	}
	return res
}

//
func findOriginalArray_fastest(changed []int) []int {
	sort.Ints(changed)
	count := make(map[int]int)
	var res []int
	for i := 0; i < len(changed); i++ {
		if count[changed[i]] == 0 {
			count[changed[i]*2] ++
			res = append(res, changed[i])
		} else {
			count[changed[i]] --
			if count[changed[i]] == 0 {
				delete(count, changed[i])
			}
		}
	}
	if len(count) == 0 {
		return res
	}
	return []int{}
}

func findOriginalArray(changed []int) []int {
	if len(changed)%2 != 0 {
		return []int{}
	}

	//Unsorted Map doesn't work!!
	//store like this also means they number are sorted
	//bucket sort
	cntMap := make([]int, 100001)
	for _, v := range changed {
		cntMap[v]++
	}

	var res []int
	for k, _ := range cntMap {

		//#1 - loop to continuously check
		//instead of /, use *
		for cntMap[k] > 0 && k*2 < 100001 && cntMap[k*2] > 0 {
			cntMap[k*2]--
			cntMap[k]--
			res = append(res, k)
		}
	}

	for _, v := range cntMap {
		//#2 - check
		if v > 0 {
			return []int{}
		}
	}
	return res
}
