package main

import (
	"fmt"
	"math/rand"
)

//https://www.youtube.com/watch?v=t5SwbbAfh-0
//https://leetcode.com/problems/insert-delete-getrandom-o1-duplicates-allowed/
//https://leetcode.com/problems/insert-delete-getrandom-o1-duplicates-allowed/discuss/543578/Golang-32ms-O(1)-code-with-detail-explanation

type RandomizedCollection struct {
	data []int
	pos  map[int][]int
}

//https://stackoverflow.com/questions/42716852/how-to-update-map-values-in-go
/** Initialize your data structure here. */
func Constructor() RandomizedCollection {
	return RandomizedCollection{
		data: make([]int, 0),
		pos:  make(map[int][]int),
	}
}

/** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
func (rc *RandomizedCollection) Insert(val int) bool {
	_, isExist := rc.pos[val]

	rc.data = append(rc.data, val) //add anyway
	idx := len(rc.data) - 1        //new index

	if !isExist {
		rc.pos[val] = make([]int, 0)
	}
	rc.pos[val] = append(rc.pos[val], idx) //new elements' index as dup. is possible

	return !isExist
}

/** Removes a value from the collection. Returns true if the collection contained the specified element. */
func (rc *RandomizedCollection) Remove(val int) bool {
	if _, ok := rc.pos[val]; !ok {
		return false
	}

	idxLst := rc.pos[val]           //a list of ids: may have dup.
	delIdx := idxLst[len(idxLst)-1] //get the last one, no requirement on preference

	idxLst = idxLst[:len(idxLst)-1] //one element less now
	rc.pos[val] = idxLst            //how to change the data in-place: or needs to reassign back
	if len(idxLst) == 0 {
		delete(rc.pos, val)
	}

	lastIdx := len(rc.data) - 1
	if lastIdx != delIdx {
		lastEle := rc.data[lastIdx]
		rc.data[delIdx] = lastEle //to be deleted set the last ele

		set := rc.pos[lastEle]

		//TODO  use a set
		var idx int
		for idx = len(set) - 1; idx >= 0; idx-- {
			if set[idx] == lastIdx { //must exist
				break
			}
		}
		copy(set[idx:], set[idx+1:])
		set = set[:len(set)-1]
		///

		set = append(set, delIdx)
		rc.pos[lastEle] = set //get a new set of indexes
	}
	rc.data = rc.data[:len(rc.data)-1] //truncate slice as the last element is moved

	return true
}

/** Get a random element from the collection. */
func (rc *RandomizedCollection) GetRandom() int {
	if len(rc.data) == 0 {
		return 0
	}
	r := rand.Intn(len(rc.data))
	return rc.data[r]
}

func Test() {
	data := []string{"aa", "xd", "kk"}

	for _, s := range data {
		fmt.Println("s=:", s)
	}

	fmt.Println("one", data[0])
}

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Insert(val);
 * param_2 := obj.Remove(val);
 * param_3 := obj.GetRandom();
 */
