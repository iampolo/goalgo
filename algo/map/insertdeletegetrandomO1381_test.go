package main

import (
	"fmt"
	"testing"
)

func Test3(t *testing.T) {
	r := Constructor()

	fmt.Println(r.Insert(4))
	fmt.Println(r.Insert(3))
	fmt.Println(r.Insert(4))
	fmt.Println(r.Insert(2))
	fmt.Println(r.Insert(4))
	fmt.Println(r.Remove(4))
	fmt.Println(r.Remove(3))
	fmt.Println(r.Remove(4))
	fmt.Println(r.Remove(4))

}

func TestCase2(t *testing.T) {
	r := Constructor()

	fmt.Println(r.Insert(0))
	fmt.Println(r.Insert(1))
	fmt.Println(r.Remove(0))
	fmt.Println(r.Insert(2))
	fmt.Println(r.Remove(1))
	fmt.Println(r.GetRandom())
}
