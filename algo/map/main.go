package main

import "fmt"

func main() {
	test_2007()
	test_954()
}

func test_954() {
	nums := []int{4, -2, -4, 2}
	//nums = []int{3, 1, 3, 6}

	fmt.Println(canReorderDoubled(nums))
	fmt.Println(canReorderDoubled_v2(nums))
}

func test_2007() {
	changed := []int{1, 3, 4, 2, 6, 8}
	//changed = []int{0, 0, 0, 0}
	//changed = []int{2, 5, 4, 5}
	//changed = []int{1, 2, 3, 2, 4, 6, 2, 4, 6, 4, 8, 12}
	//changed = []int{40, 7, 78, 12, 40, 28, 33, 27, 35, 90, 56, 44, 42, 38, 36, 3, 12, 68, 86, 14, 27, 80, 33, 40, 12, 74, 20, 50, 15, 54, 76, 13, 40, 3, 43, 88, 14, 54, 20, 0, 100, 10, 23, 30, 27, 50, 84, 24, 15, 45, 94, 66, 6, 22, 20, 34, 25, 100, 28, 6, 37, 10, 18, 82, 96, 0, 76, 40, 32, 33, 48, 70, 24, 80, 20, 40, 50, 4, 19, 25, 66, 38, 46, 44, 98, 47, 26, 54, 38, 39, 41, 20, 49, 8, 16, 6, 50, 30, 20, 66}
	fmt.Println(findOriginalArray(changed))
	fmt.Println(findOriginalArray_map(changed))
	fmt.Println(findOriginalArray_map_v2(changed))
}

func test_1180() {
	s := "loonbalxballpoon"
	s = "nlaebolko"
	s = "lloo"
	fmt.Println(maxNumberOfBalloons(s))
}
func test_791() {
	o, s := "cba", "abcd"

	fmt.Println(customSortString(o, s))
}

func test_1381() {
	r := Constructor()

	fmt.Println(r.Insert(-1))
	fmt.Println(r.Remove(-2))
	fmt.Println(r.Insert(-2))
	fmt.Println(r.GetRandom())
	fmt.Println(r.Remove(-1))
	fmt.Println(r.Insert(-2))
	fmt.Println(r.GetRandom())
}
