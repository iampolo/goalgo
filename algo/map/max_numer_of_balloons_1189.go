package main

import "math"

// https://leetcode.com/problems/maximum-number-of-balloons/
func maxNumberOfBalloons_v3(text string) int {
	b, a, l, o, n := 0, 0, 0, 0, 0
	for _, ch := range text {
		switch ch {
		case 'b':
			b++
			break
		case 'a':
			a++
			break
		case 'l':
			l++
			break
		case 'o':
			o++
			break

		case 'n':
			n++
			break
		}
	}
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}

	return min(b, min(a, min(l/2, min(o/2, n))))
}

func maxNumberOfBalloons_v2(text string) int {
	cnt := make(map[byte]int)
	for _, ch := range text {
		cnt[byte(ch)]++
	}

	funcMin := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}

	//0 will be returned if a char is missing
	min := cnt['b']
	min = funcMin(min, cnt['a'])
	min = funcMin(min, cnt['l']/2)
	min = funcMin(min, cnt['o']/2)
	min = funcMin(min, cnt['n'])
	return min
}

func maxNumberOfBalloons(text string) int {
	cnt := make(map[byte]int)
	for _, ch := range text {
		cnt[byte(ch)]++
	}

	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}

	match := make(map[byte]int)
	// ensure the count
	// ensure all char in balloon have matches.
	occurrance := math.MaxInt32
	for ch, v := range cnt {
		if ch == 'l' || ch == 'o' {
			occurrance = min(occurrance, v/2)
			match[ch] = 1
		} else if ch == 'b' || ch == 'a' || ch == 'n' {
			occurrance = min(occurrance, v)
			match[ch] = 1
		}
	}
	if occurrance == math.MaxInt32 || len(match) != 5 {
		return 0
	}
	return occurrance
}
