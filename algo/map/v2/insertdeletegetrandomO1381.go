package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math/rand"
)

type RandomizedCollection struct {
	data []int
	pos  map[int]*Set
}

//https://stackoverflow.com/questions/42716852/how-to-update-map-values-in-go
/** Initialize your data structure here. */
func Constructor() RandomizedCollection {
	return RandomizedCollection{
		data: make([]int, 0),
		pos:  make(map[int]*Set),
	}
}

/** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
func (rc *RandomizedCollection) Insert(val int) bool {
	_, isExist := rc.pos[val]

	idx := len(rc.data)            //0-based index, so current length is next items's index
	rc.data = append(rc.data, val) //add anyway

	if !isExist {
		rc.pos[val] = NewSet()
	}
	rc.pos[val].Add(idx)

	return !isExist
}

/** Removes a value from the collection. Returns true if the collection contained the specified element. */
func (rc *RandomizedCollection) Remove(val int) bool {
	if _, ok := rc.pos[val]; !ok {
		return false
	}

	idxSet := rc.pos[val]
	delIdx := idxSet.Pop().(int) //must not be nil, cast to int

	if idxSet.Size() == 0 {
		delete(rc.pos, val) //remove the empty set
	}

	lastIdx := len(rc.data) - 1
	if lastIdx != delIdx {
		lastEle := rc.data[lastIdx]

		rc.data[delIdx] = lastEle //to be deleted set the last ele

		set := rc.pos[lastEle]
		set.Remove(lastIdx)
		set.Add(delIdx)
	}
	rc.data = rc.data[:len(rc.data)-1] //truncate slice as the last element is moved

	return true
}

/** Get a random element from the collection. */
func (rc *RandomizedCollection) GetRandom() int {
	if len(rc.data) == 0 {
		return 0
	}
	r := rand.Intn(len(rc.data))
	return rc.data[r]
}

func main() {
	r := Constructor()

	fmt.Println(r.Insert(-1))
	fmt.Println(r.Remove(-2))
	fmt.Println(r.Insert(-2))
	fmt.Println(r.GetRandom())
	fmt.Println(r.Remove(-1))
	fmt.Println(r.Insert(-2))
	fmt.Println(r.GetRandom())
}

func Test() {
	set := Setv2{Set: NewSet()}

	_ = set

}

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Insert(val);
 * param_2 := obj.Remove(val);
 * param_3 := obj.GetRandom();
 */
