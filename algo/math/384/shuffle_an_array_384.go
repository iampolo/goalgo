package main

import (
	"fmt"
	"math/rand"
	"time"
)

/**
https://leetcode.com/problems/shuffle-an-array/submissions/
*/
type Solution struct {
	nums []int
}

func Constructor(nums []int) Solution {
	return Solution{
		nums: nums,
	}
}

/** Resets the array to its original configuration and return it. */
func (this *Solution) Reset() []int {
	return this.nums
}

/** Returns a random shuffling of the array. */
func (this *Solution) Shuffle() []int {
	temp := make([]int, len(this.nums))
	copy(temp, this.nums)

	length := len(this.nums)
	for i := 0; i < len(this.nums); i++ {
		r := rand.Intn(length-i) + i
		temp[i], temp[r] = temp[r], temp[i]
	}

	return temp
}

func (this *Solution) Shuffle_V2() []int {
	temp := make([]int, len(this.nums))
	copy(temp, this.nums)

	rand.Shuffle(len(this.nums), func(i, j int) {
		temp[i], temp[j] = temp[j], temp[i]
	})
	return temp
}

func main() {

}

func test() {
	fmt.Println(time.Now().UnixNano())

	rand.NewSource(43)
	for i := 0; i < 10; i++ {
		fmt.Print(int(rand.Float32()*5+1), "")
	}
	fmt.Println()
	rand.NewSource(43)
	for i := 0; i < 10; i++ {
		fmt.Print(int(rand.Float32()*5+1), "")
	}
}
