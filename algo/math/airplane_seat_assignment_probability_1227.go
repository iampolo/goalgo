package main

//https://leetcode.com/problems/airplane-seat-assignment-probability/
//n passengers board an airplane with exactly n seats. The first passenger has lost the ticket and picks
//a seat randomly. But after that, the rest of passengers will:
//
//Take their own seat if it is still available,
//Pick other seats randomly when they find their seat occupied
//
//What is the probability that the n-th person can get his own seat?  <<<<<<<<<<<<<<<<
//
//
//
//Example 1:
//
//Input: n = 1
//Output: 1.00000
//Explanation: The first person can only get the first seat.
//
//Example 2:
//
//Input: n = 2
//Output: 0.50000
//Explanation: The second person has a probability of 0.5 to get the second seat (when first person gets
//the first seat).
//
//
//Constraints:
//
//1 <= n <= 10^5
func nthPersonGetsNthSeat(n int) float64 {
	//What is the probability that the n-th person can get his own seat?
	if n == 1 {
		return 1
	}
	return 0.5
}

//https://leetcode.com/problems/airplane-seat-assignment-probability/discuss/407781/Proof-by-mathematical-induction-that-answer-is-12-when-n-greater-2.
func nthPersonGetsNthSeat_v2(n int) float64 {
	if n == 1 {
		return 1
	}

	goodseat := float64(1)/float64(n)
	badseat := (float64(n)-float64(2))/float64(n)
	return goodseat + badseat * nthPersonGetsNthSeat(n-1)
}