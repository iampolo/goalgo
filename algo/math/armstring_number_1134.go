package main

import (
	"strconv"
)

/**
https://leetcode.com/problems/armstrong-number/submissions/
Given an integer n, return true if and only if it is an Armstrong number.

The k-digit number n is an Armstrong number if and only if the kth power of each digit sums to n.



Example 1:

Input: n = 153
Output: true
Explanation: 153 is a 3-digit number, and 153 = 13 + 53 + 33.
Example 2:

Input: n = 123
Output: false
Explanation: 123 is a 3-digit number, and 123 != 13 + 23 + 33 = 36.


Constraints:

1 <= n <= 10^8
*/
func isArmstrong(n int) bool {

	length := len(strconv.Itoa(n))
	//OR
	//length = numLength(n)

	sum := 0
	num := n
	for num > 0 {
		sum += Pow(num%10, length)
		num /= 10
	}

	return sum == n
}

func Pow(x, n int) int {
	if x == 0 {
		return 0
	}
	if n == 0 {
		return 1
	}

	tmp := Pow(x, n/2)
	if n%2 == 0 {
		return tmp * tmp
	} else {
		//there is an extra x
		return x * tmp * tmp
	}
}

func numLength(n int) int {
	cnt := 0

	for n > 0 {
		cnt++
		n /= 10
	}
	return cnt
}

