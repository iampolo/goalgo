package main

import (
	"strconv"
)

//  https://leetcode.com/problems/base-7/
//Given an integer num, return a string of its base 7 representation.
//
//
//
//Example 1:
//
//Input: num = 100
//Output: "202"
//Example 2:
//
//Input: num = -7
//Output: "-10"
//
//
//Constraints:
//
//-10^7 <= num <= 10^7
func convertToBase7(num int) string {
	//sign := 1
	if num < 0 {
		return "-" + convertToBase7(-num)
	}
	if num < 7 {
		return strconv.Itoa(num)
	}
	return convertToBase7(num/7) + convertToBase7(num%7)
}

//keep a base:1, 10, 100,1000
//good
func convertToBase7_v3(num int) string {
	base := 1
	var res int
	for num != 0 {
		res += base * (num % 7)
		num /= 7
		base *= 10
	}
	return strconv.Itoa(res)
}

//-7%7=0
//-8%7=-1
func convertToBase7_v2(num int) string {
	if num == 0 {
		return "0"
	}
	n := num
	if num < 0 {
		n = -num
	}

	var res string
	for n != 0 {
		res = strconv.Itoa(n%7) + res
		n /= 7
	}
	if num < 0 {
		return "-" + res
	}
	return res
}
