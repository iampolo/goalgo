package main

// https://leetcode.com/problems/beautiful-array/
// https://leetcode.com/problems/beautiful-array/discuss/186679/Odd-%2B-Even-Pattern-O(N)
// https://www.youtube.com/watch?v=O-1ucu8ErEo&ab_channel=HappyCoding
func beautifulArray(n int) []int {
	lst := []int{1}

	for len(lst) < n {
		var cur []int
		//for odds
		for _, x := range lst {
			if x*2-1 <= n {
				cur = append(cur, x*2-1)
			}
		}
		//for evens
		for _, x := range lst {
			if x*2 <= n {
				cur = append(cur, x*2)
			}
		}
		lst = cur
	}
	return lst
}
