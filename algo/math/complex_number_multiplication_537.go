package main

import (
	"fmt"
	"strconv"
	"strings"
)

// https://leetcode.com/problems/complex-number-multiplication/
// A complex number can be represented as a string on the form "real+imaginaryi" where:
//
//- real is the real part and is an integer in the range [-100, 100].
//- imaginary is the imaginary part and is an integer in the range [-100, 100].
//- i2 == -1.
//
//Given two complex numbers num1 and num2 as strings, return a string of the complex number that represents their multiplications.
//
//
//Example 1:
//
//Input: num1 = "1+1i", num2 = "1+1i"
//Output: "0+2i"
//Explanation: (1 + i) * (1 + i) = 1 + i2 + 2 * i = 2i, and you need convert it to the form of 0+2i.
//
//Example 2:
//
//Input: num1 = "1+-1i", num2 = "1+-1i"
//Output: "0+-2i"
//Explanation: (1 - i) * (1 - i) = 1 + i2 - 2 * i = -2i, and you need convert it to the form of 0+-2i.
//
//
//Constraints:
// - num1 and num2 are valid complex numbers.
//
// Hints:
// Multiplication of two complex numbers can be done as:
//  (a+ib)×(x+iy)=ax+i^2by+i(bx+ay)=ax−by+i(bx+ay)
//
func complexNumberMultiply(num1 string, num2 string) string {
	real1, img1 := parseNumStr(num1)
	real2, img2 := parseNumStr(num2)

	return fmt.Sprintf("%d+%di",real1*real2-img1*img2, real1*img2+img1*real2)
}

func parseNumStr(num string) (int, int) {
	idx := strings.IndexByte(num, '+')
	//ignore error
	real, _ := strconv.Atoi(num[:idx])
	img, _ := strconv.Atoi(num[idx+1 : len(num)-1])
	return real, img
}
