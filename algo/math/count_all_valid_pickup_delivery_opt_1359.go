package main

/*
https://leetcode.com/problems/count-all-valid-pickup-and-delivery-options/
Given n orders, each order consist in pickup and delivery services.

Count all valid pickup/delivery possible sequences such that delivery(i) is always after
of pickup(i).

Since the answer may be too large, return it modulo 10^9 + 7.



Example 1:

Input: n = 1
Output: 1
Explanation: Unique order (P1, D1), Delivery 1 always is after of Pickup 1.
Example 2:

Input: n = 2
Output: 6
Explanation: All possible orders:
(P1,P2,D1,D2), (P1,P2,D2,D1), (P1,D1,P2,D2), (P2,P1,D1,D2), (P2,P1,D2,D1) and (P2,D2,P1,D1).
This is an invalid order (P1,D2,P2,D1) because Pickup 2 is after of Delivery 2.
Example 3:

Input: n = 3
Output: 90


Constraints:

* 1 <= n <= 500


*/
/*
https://leetcode.jp/leetcode-1359-count-all-valid-pickup-and-delivery-options-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/
 */
func countOrders(n int) int {
	//dp[i][j]: # of combination for i picked and j available order, and vice versa
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
	}
	var dfs func(p, n int) int
	dfs = func(p, n int) int {
		if p == 0 && n == 0 {
			//all picked and delivered
			return 1
		}
		if p < 0 || n < 0 {
			return 0
		}
		// dp[n][p] also works
		if dp[p][n] > 0 {
			return dp[p][n]
		}

		var ways int
		// Count all choices of picking an order.
		//pick one order from n, and multiple the next level to count
		//the total combination of these order:n
		ways = (n * dfs(p+1, n-1)) % MOD

		// Count all choices of delivering an picked order.
		//deliver a picked order from p, and count all the combination
		//of delivery by using dfs
		//as each p is the same, we just need to count the combination!
		ways += (p * dfs(p-1, n)) % MOD

		dp[p][n] = ways
		return ways
	}
	return dfs(0, n)
}
