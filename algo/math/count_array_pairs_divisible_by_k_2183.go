package main

/*
https://leetcode.com/problems/count-array-pairs-divisible-by-k/
Given a 0-indexed integer array nums of length n and an integer k, return the number of pairs (i, j) such that:

* 0 <= i < j <= n - 1 and
* nums[i] * nums[j] is divisible by k.


Example 1:

Input: nums = [1,2,3,4,5], k = 2
Output: 7
Explanation:
The 7 pairs of indices whose corresponding products are divisible by 2 are
(0, 1), (0, 3), (1, 2), (1, 3), (1, 4), (2, 3), and (3, 4).
Their products are 2, 4, 6, 8, 10, 12, and 20 respectively.
Other pairs such as (0, 2) and (2, 4) have products 3 and 15 respectively, which are not divisible by 2.

Example 2:

Input: nums = [1,2,3,4], k = 5
Output: 0
Explanation: There does not exist any pair of indices whose corresponding product is divisible by 5.


Constraints:

 - 1 <= nums.length <= 10^5
 - 1 <= nums[i], k <= 10^5

*/

/*
https://www.calculatorsoup.com/calculators/math/gcf.php
https://www.mathsisfun.com/greatest-common-factor.html

 (n1 * n2) % k == 0
 then gcd(n1, k) * gcd(n2, k) % k == 0

T: O(n*sqrt(n))
S: O(sqrt(n))

*/
func countPairs(nums []int, k int) int64 {
	allGcd := make(map[int]int64)

	var res int64
	for _, n := range nums {
		curGcd := gcd(n, k)

		for gm, v := range allGcd {
			if (gm*curGcd)%k == 0 {
				res += v
			}
		}

		allGcd[curGcd]++
	}
	return res
}

func gcd(a, b int) int {
	if b == 0 {
		return a
	}
	return gcd(b, a%b)
}
