package main

//https://leetcode.com/problems/count-primes/

func countPrime(n int) int {
	isPrime := make([]bool, n)

	for i := 2; i < len(isPrime); i++ {
		isPrime[i] = true
	}

	cnt := 0
	for i := 2; i < n; i++ {
		if !isPrime[i] {
			continue
		}
		for j := i * i; j < n; j += i {
			isPrime[j] = false
		}
	}

	return cnt
}
