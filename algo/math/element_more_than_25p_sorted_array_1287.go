package main

//https://leetcode.com/problems/element-appearing-more-than-25-in-sorted-array/
//Given an integer array sorted in non-decreasing order, there is exactly one integer in the array that occurs more
//than 25% of the time, return that integer.
//
//
//
//Example 1:
//
//Input: arr = [1,2,2,6,6,6,6,7,10]
//Output: 6
//
//Example 2:
//
//Input: arr = [1,1]
//Output: 1
//
//
//Constraints:
//
//1 <= arr.length <= 10^4
//0 <= arr[i] <= 10^5
//

func findSpecialInteger_bs(arr []int) int {

	length := len(arr)
	//position 25%, 50%, 75% -> indexes
	one, two, three := length/4, length/2, length*3/4
	for _, v := range []int{one, two, three} {
		first := search_first(arr, arr[v])
		last := search_last(arr, arr[v])
		if float64(last-first+1)/float64(length) > .25 {
			return arr[v]
		}
	}
	return -1
}

func search_last(arr []int, t int) int {
	lo, hi := 0, len(arr)
	for lo < hi {
		mi := lo + (hi-lo)/2
		if arr[mi] <= t {
			lo = mi + 1
		} else {
			hi = mi
		}
	}
	return lo - 1
}

func search_first(arr []int, t int) int {
	lo, hi := 0, len(arr)
	for lo < hi {
		mi := lo + (hi-lo)/2
		if arr[mi] >= t {
			hi = mi
		} else {
			lo = mi + 1
		}
	}
	return lo
}

func findSpecialInteger_counter(arr []int) int {
	cntMap := make(map[int]int)
	for i := range arr {
		cntMap[arr[i]]++
	}

	for k, v := range cntMap {
		if float64(v)/float64(len(arr)) > .25 {
			return k
		}
	}
	return -1
}

func findSpecialInteger(arr []int) int {

	for i := 0; i < len(arr); {
		cnt := 1
		j := i
		for ; j < len(arr)-1; j++ {
			if arr[j] != arr[j+1] {
				break
			}
			cnt++
		}
		i = j + 1

		if float64(cnt)/float64(len(arr)) > .25 {
			return arr[j]
		}
	}
	return -1
}
