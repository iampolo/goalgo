package main

// Convex hull
// https://www.youtube.com/watch?v=ccxFHzL2aok&t=1721s&ab_channel=%E5%82%85%E7%A0%81%E7%88%B7
// https://en.wikipedia.org/wiki/Convex_hull_algorithms
//https://www.cnblogs.com/grandyang/p/7745697.html
//https://www.youtube.com/watch?v=ccxFHzL2aok&t=616s&ab_channel=%E5%82%85%E7%A0%81%E7%88%B7
//https://leetcode.com/problems/erect-the-fence/discuss/103306/C%2B%2B-and-Python-easy-wiki-solution
//func outerTrees_jarvismarch(trees [][]int) [][]int {
//	if len(trees) < 4 {
//		return trees //de-dup?
//	}
//
//	//find the lest most point: the one has least x
//	lm := 0
//	for i := 0; i < len(trees); i++ {
//		if trees[i][0] < trees[lm][0] {
//			lm = i
//		}
//	}
//	res := [][]int{}
//	keys := make(map[string]bool)
//
//	p := lm
//	for {
//		q := (p + 1) % len(trees)
//
//		//check if i is more counterclockwise than q
//		for i := 0; i < len(trees); i++ {
//			if orientation(trees[p], trees[i], trees[q]) < 0 {
//				q = i
//			}
//		} //
//
//		for i := 0; i < len(trees); i++ {
//			if i != p && i != q && orientation(trees[p], trees[i], trees[q]) == 0 && inBetween(trees[p], trees[i], trees[q]) {
//				if _, ok := keys[string(trees[i][0])+"-"+string(trees[i][1])]; !ok {
//					keys[string(trees[i][0])+"-"+string(trees[i][1])] = true
//					res = append(res, trees[i])
//				}
//			}
//		}
//		if _, ok := keys[string(trees[q][0])+"-"+string(trees[q][1])]; !ok {
//			keys[string(trees[q][0])+"-"+string(trees[q][1])] = true
//			res = append(res, trees[q])
//		}
//		p = q
//
//		if p == lm {
//			break
//		}
//	} //
//
//	return res
//}

// cross product of three points
// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// -1 --> Counterclockwise
func orientation(p, q, r []int) int {
	return (q[1]-p[1])*(r[0]-q[0]) - (q[0]-p[0])*(r[1]-q[1])
}

func inBetween(p, i, q []int) bool {
	x := p[0] <= i[0] && i[0] <= q[0] || q[0] <= i[0] && i[0] <= p[0]
	y := p[1] <= i[1] && i[1] <= q[1] || q[1] <= i[1] && i[1] <= p[1]
	return x && y
}
