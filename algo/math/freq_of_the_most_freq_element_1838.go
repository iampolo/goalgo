package main

import "sort"

//https://leetcode.com/problems/frequency-of-the-most-frequent-element/
//The frequency of an element is the number of times it occurs in an array.
//
//You are given an integer array nums and an integer k. In one operation, you can choose an index of nums and
//increment the element at that index by 1.
//
//Return the maximum possible frequency of an element after performing at most k operations.
//
//
//
//Example 1:
//
//Input: nums = [1,2,4], k = 5
//Output: 3
//Explanation: Increment the first element three times and the second element two times to make nums = [4,4,4].
//4 has a frequency of 3.
//
//Example 2:
//
//Input: nums = [1,4,8,13], k = 5
//Output: 2
//Explanation: There are multiple optimal solutions:
//- Increment the first element three times to make nums = [4,4,8,13]. 4 has a frequency of 2.
//- Increment the second element four times to make nums = [1,8,8,13]. 8 has a frequency of 2.
//- Increment the third element five times to make nums = [1,4,13,13]. 13 has a frequency of 2.
//
//Example 3:
//
//Input: nums = [3,9,6], k = 2
//Output: 1
//
//
//Constraints:
//
//- 1 <= nums.length <= 10^5
//- 1 <= nums[i] <= 10^5
//- 1 <= k <= 10^5
//
func maxFrequency_TLE(nums []int, k int) int {

	if len(nums) == 0 {
		return 0
	}
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	sort.Ints(nums)
	maxCnt := 0
	for i := len(nums) - 1; i >= 0; i-- {
		cnt, diff := 1, 0
		for j := i - 1; j >= 0; j-- {
			if i == j {
				continue
			}
			diff += nums[i] - nums[j]
			if k-diff < 0 {
				break
			}
			cnt++
		}
		maxCnt = max(maxCnt, cnt)
	}

	return maxCnt
}

func maxFrequency(nums []int, k int) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	sort.Ints(nums)
	maxLen := 1
	left, sum := 0, 0
	for i := range nums {
		sum += nums[i] //current sum in the window
		//check the diff of required sum and current sum and see if it satisfy k
		if (i-left+1)*nums[i]-sum > k {
			sum -= nums[left] //shrink the window
			left++
		}
		maxLen = max(maxLen, i-left+1)
	}

	return maxLen
}
