package main

import "strconv"

/*
https://leetcode.com/problems/largest-multiple-of-three/
A number is a multiple of three if and only if its sum of digits is a multiple of three.

*/

/*
Given an array of digits digits, return the largest multiple of three that can be formed by concatenating
some of the given digits in any order. If there is no answer return an empty string.

Since the answer may not fit in an integer data type, return the answer as a string. Note that the returning
answer must not contain unnecessary leading zeros.



Example 1:

Input: digits = [8,1,9]
Output: "981"
Example 2:

Input: digits = [8,6,7,1,0]
Output: "8760"
Example 3:

Input: digits = [1]
Output: ""


Constraints:

1 <= digits.length <= 10^4
0 <= digits[i] <= 9
*/

/*
https://leetcode.com/problems/largest-multiple-of-three/discuss/517814/Java-Easy-to-understand-solution-using-bucket-(With-explanation)-1ms
https://leetcode.jp/leetcode-1363-largest-multiple-of-three-%E8%A7%A3%E9%A2%98%E6%80%9D%E8%B7%AF%E5%88%86%E6%9E%90/

remainder is either 1 or 2 when dividing by 3
1, 4, or 7, if exists, or two of 2, 5, or 8.
	e.g.:
	(2+5) % 3 = 1
	(5+8) % 3 = 1
	(2+8) % 3 = 1


解题思路分析：

首先我们应该明确一个数学上的概念，如果一个数字能被三整除，那么该数字上每一位之和也能被三整除，比如183能被3整除（183/3=61），
那么1+8+3=12，12也能被三整除。

我们先循环遍历一遍数组中的数字，该循环中我们需要做4件事情：

统计出0-9每种数字的个数
计算出数组中所有数字和
统计出与三相除余数为1的最小数min1_1，及次小数 min1_2。
统计出与三相除余数为2的最小数min2_1，及次小数 min2_2。
循环结束后，如果所有数字和能被3整除，说明我们使用数组中所有的数字可以组成一个能被3整除的数字。如果和与3的余数是1，说明我们需要从数
组中剔除出一个最小的与3余数是1的数字 min1_1 。如果这个数字不存在，那么数组中一定存在 min2_1 和 min2_2，将这两个数字在数组中的个
数减一。反之，如果和与3的余数是2，说明我们需要从数组中剔除出一个最小的与3余数是2的数字min2_1 。如果这个数字不存在，那么数组中一定
存在 min1_1 和 min1_2，将这两个数字在数组中的个数减一。

经过上述操作，我们保证数组中剩下的数字和一定能被三整除，我们利用这些数字组成一个最大数即可（最大数即是将大的数字尽量放在高位）。
*/
func largestMultipleOfThree(digits []int) string {
	//1st and 2nd numbers their remainder is 1
	//1st and 2nd numbers their remainder is 2
	min11, min12 := 10, 10 //as 10 bigger than [0..9]
	min21, min22 := 10, 10

	cnt := make([]int, 10)
	var sum int

	for _, d := range digits {
		if d%3 == 1 {
			//keep the min two numbers with reminder 1
			if d <= min11 {
				min11, min12 = d, min11
			} else if d <= min22 {
				min12 = d
			}
		} else if d%3 == 2 {
			if d <= min21 {
				min21, min22 = d, min21
			} else if d <= min22 {
				min22 = d
			}
		}

		sum += d
		cnt[d]++
	}
	if sum == 0 {
		return "0"
	}
	if sum%3 == 1 {
		//to get the largest result, remove the smallest num with remainder 1
		if min11 < 10 {
			cnt[min11]--
		} else { //can only remove two numbers with remainder 2
			cnt[min21]--
			cnt[min22]--
		}
	} else if sum%3 == 2 {
		if min21 < 10 {
			cnt[min21]--
		} else {
			cnt[min11]--
			cnt[min12]--
		}
	}
	//for the largest number withe remaining digits, from larger digts to smaller ones
	var res string
	for d := 9; d >= 0; d-- {
		for ; cnt[d] > 0; cnt[d]-- {
			res += strconv.Itoa(d)
		}
	}
	//no leading 0 is allowed
	// [0, 0, 1]; [0]; [1]
	if len(res) > 0 && res[0] == '0' {
		return "0"
	}
	return res
}
