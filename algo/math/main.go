package main

import (
	"fmt"
)

func main() {
	test_1363()
}

func test_1363() {
	di := []int{8, 5}
	di = []int{4,0,0,0}
	fmt.Println(largestMultipleOfThree(di))
}

func test_2183() {
	nu := []int{1, 2, 3, 4, 5}
	k := 2
	fmt.Println(countPairs(nu, k))
}

func test_2128() {
	grid := [][]int{{0, 1, 0}, {1, 0, 1}, {0, 1, 0}}
	fmt.Println(removeOnes_v2(grid))
	fmt.Println(removeOnes(grid))
}

func test_1287() {
	arr := []int{1, 2, 2, 6, 6, 6, 6, 7, 10}
	fmt.Println(findSpecialInteger(arr))
	fmt.Println(findSpecialInteger_bs(arr))
}

func test_1524() {
	arr := []int{1, 2, 3, 4, 5, 6, 7}
	arr = []int{1, 3, 5}
	arr = []int{3, 5, 8}
	arr = []int{100, 100, 99, 99}
	arr = []int{10, 10, 9, 9}
	fmt.Println(numOfSubarrays_v2(arr))
	fmt.Println(numOfSubarrays_v3(arr))
	fmt.Println(numOfSubarrays(arr))
}

func test_504() {
	n := 100
	n = -7
	n = -8
	fmt.Println(convertToBase7(n))
	fmt.Println(convertToBase7_v2(n))
	fmt.Println(convertToBase7_v3(n))
}

func test_1703() {
	nums := []int{0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1}
	k := 5
	//k = 2

	//nums = []int{1, 0, 0, 0, 0, 0, 1, 1}
	//k = 3

	nums = []int{1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1}
	k = 4
	fmt.Println(minMoves(nums, k))

}

func test_1131() {
	nums1 := []int{1, 2, 3, 4}
	nums2 := []int{-1, 4, 5, 6}

	fmt.Println(maxAbsValExpr(nums1, nums2))
}

func test_1838() {
	nums := []int{1, 2, 4}
	k := 5

	nums = []int{3, 9, 6}
	k = 2
	fmt.Println(maxFrequency_TLE(nums, k))
	fmt.Println(maxFrequency(nums, k))
}

func test_325() {
	nums := []int{1, -1, 5, -2, 3}
	k := 3
	fmt.Println(maxSubArrayLen(nums, k))
}

func test_537() {
	num1, num2 := "1+1i", "1+1i"
	fmt.Println(complexNumberMultiply(num1, num2))

}
func test_932() {
	n := 5

	fmt.Println(beautifulArray(n))

}

func test() {
	fmt.Println(isArmstrong(1634))
}
