package main

import "testing"

func Test_newInteger_660(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "lc_660",
			args: args{
				n: 10,
			},
			want: 11,
		},
		{
			name: "lc_660",
			args: args{
				n: 9,
			},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := newInteger_660_v2(tt.args.n); got != tt.want {
				t.Errorf("newInteger_660() = %v, want %v", got, tt.want)
			}
		})
	}
}
