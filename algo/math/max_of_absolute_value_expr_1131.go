package main

import "math"

// https://leetcode.com/problems/maximum-of-absolute-value-expression/
// Given two arrays of integers with equal lengths, return the maximum value of:
//    |arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| + |i - j|
//
// where the maximum is taken over all 0 <= i, j < arr1.length.
//
//  F1 = |arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| + |i - j|
//  split F1 into non-absolute forms
//  1) a1[i]-a1[j]+a2[i]-a2[j]+i-j => (a1[i]+a2[i]+i)-(a1[j]+a2[j]+j)
//  2) ...
//  8) -a1[i]+a1[j]-a2[i]+a2[j]-i+j => -(a1[i]+a2[i]+i)+(a1[j]+a2[j]+j)

// we try to split i, j separately -> O(n)
// absolute |expression1| + |expression2| + |expression3| -> 8 cases can happen
// +++, ++-, +-+, -++, +--, -+-, --+, ---
// 8 cases -> 4 case below unique
// 1. (arr1[i] + arr2[i] + i) - (arr1[j] + arr2[j] + j)
// 2. (arr1[i] + arr2[i] - i) - (arr1[j] + arr2[j] - j)
// 3. (arr1[i] - arr2[i] + i) - (arr1[j] - arr2[j] + j)
// 4. (arr1[i] - arr2[i] - i) - (arr1[j] - arr2[j] - j)
//
func maxAbsValExpr(a1 []int, a2 []int) int {
	min := func(a, b int) int {
		if a < b {
			return a
		}
		return b
	}
	max := func(a, b int) int {
		if a > b {
			return a
		}
		return b
	}

	max1, max2, max3, max4 := math.MinInt32, math.MinInt32, math.MinInt32, math.MinInt32
	min1, min2, min3, min4 := math.MaxInt32, math.MaxInt32, math.MaxInt32, math.MaxInt32

	for i := range a1 {
		max1 = max(max1, a1[i]+a2[i]+i)
		min1 = min(min1, a1[i]+a2[i]+i)

		max2 = max(max2, a1[i]+a2[i]-i)
		min2 = min(min2, a1[i]+a2[i]-i)

		max3 = max(max3, a1[i]-a2[i]-i)
		min3 = min(min3, a1[i]-a2[i]-i)

		max4 = max(max4, a1[i]-a2[i]+i)
		min4 = min(min4, a1[i]-a2[i]+i)
	}
	diff1 := max1 - min1
	diff2 := max2 - min2
	diff3 := max3 - min3
	diff4 := max4 - min4

	return max(max(diff1, diff2), max(diff3, diff4))
}

//TODO
func maxAbsValExpr_manhattan(a1 []int, a2 []int) int {
	return 0
}
