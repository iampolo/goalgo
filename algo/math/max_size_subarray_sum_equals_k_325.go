package main

import "math"

// https://leetcode.com/problems/maximum-size-subarray-sum-equals-k/
// Given an integer array nums and an integer k, return the maximum length of a subarray that sums to k.
//If there isn't one, return 0 instead.
//
//
//Example 1:
//
//Input: nums = [1,-1,5,-2,3], k = 3
//Output: 4
//Explanation: The subarray [1, -1, 5, -2] sums to 3 and is the longest.
//
//Example 2:
//
//Input: nums = [-2,-1,2,1], k = 1
//Output: 2
//Explanation: The subarray [-1, 2] sums to 1 and is the longest.
//
//
//Constraints:
//
//1 <= nums.length <= 2 * 10^5
//-10^4 <= nums[i] <= 10^4
//-10^9 <= k <= 10^9

// https://leetcode.com/problems/maximum-size-subarray-sum-equals-k/discuss/77797/Golang-O(n)-solution
// refer : https://leetcode.com/problems/path-sum-iii/discuss/91910/golang-on-solution-using-map-with-some-explanation
// ContinuousSubarraySum523.java
// pathSum_437() in go
//
func maxSubArrayLen(nums []int, k int) int {
	//pos := make(map[int]int)
	//pos[0] = -1 //if preSum is the same as k

	pos := map[int]int{0: -1}
	longest, preSum := 0, 0
	for i := 0; i < len(nums); i++ {
		preSum += nums[i]
		if v, exist := pos[preSum-k]; exist {
			longest = int(math.Max(float64(longest), float64(i-v)))
		}
		if _, exist := pos[preSum]; !exist {
			pos[preSum] = i
		}
	}
	return longest
}
