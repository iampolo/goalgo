package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/minimum-adjacent-swaps-for-k-consecutive-ones/

// https://zxi.mytechroad.com/blog/sliding-window/leetcode-1703-minimum-adjacent-swaps-for-k-consecutive-ones/
// We only care positions of 1s, we can move one element from position x to y (assuming x + 1 ~ y are all zeros)
// in y – x steps. e.g. [0 0 1 0 0 0 1] => [0 0 0 0 0 1 1], move first 1 at position 2 to position 5, cost is 5 – 2 = 3.
//
// Given a size k window of indices of ones, the optimal solution it to use the median number as center. We can compute
// the cost to form consecutive numbers:
//
// BRUTE-FORCE:
// e.g. [1 4 7 9 10] => [5 6 7 8 9] cost = (5 – 1) + (6 – 4) + (9 – 8) + (10 – 9) = 8
//
// However, naive solution takes O(n*k) => TLE.
//
// USE MATH FORMULA INSTEAD:
// We can use prefix sum to compute the cost of a window in O(1) to reduce time complexity to O(n)
//
// First, in order to use sliding window, we change the target of every number in the window to the median number.
// FACT 1/2
// e.g. [1 4 7 9 10] => [7 7 7 7 7]
//			instead of calc. one by one, use `right - left`
//       cost = (7 – 1) + (7 – 4) + (7 – 7) + (9 – 7) + (10 – 7) = (9 + 10) – (1 + 4) = right – left.
// FACT 2/2
// [5 6 7 8 9] => [7 7 7 7 7] takes extra 2 + 1 + 1 + 2 = 6 steps = (k / 2) * ((k + 1) / 2),
// these extra steps should be deducted from the final answer.
//
// Note:
// Aggregate all 1s to the median 1 will lead to the minimum distance
// If k is odd, I aggregate (k-1)/2 left 1s and (k-1)/2 right 1s to the median 1
// If k is even, I aggregate k/2 left 1s and k/2-1 right 1s to the median 1
// ->This can be summarized to aggregate k/2 left 1s and (k-1)/2 right 1s to the median 1 for all k
//
//https://leetcode.com/problems/minimum-adjacent-swaps-for-k-consecutive-ones/discuss/987593/C%2B%2B-O(n)-solution-with-prefix-sum-and-window-with-detailed-explanation-and-example
//https://leetcode.com/problems/minimum-adjacent-swaps-for-k-consecutive-ones/discuss/987347/JavaC%2B%2BPython-Solution
//
func minMoves(nums []int, k int) int {
	var pos []int

	for i, v := range nums {
		if v == 1 {
			pos = append(pos, i)
		}
	}

	//build the prefix-sum array
	sum := make([]int, len(pos)+1)
	sum[0] = 0 //extra element at the beginning to make prefix-sum calc. easier
	for i := 1; i < len(sum); i++ {
		sum[i] = pos[i-1] + sum[i-1]
	}

	var res = math.MaxInt32
	//scan the array with k as the window size
	for i := 0; i < len(pos)-k+1; i++ {
		//use the formula of right - left: the middle one is the median, i is the window offset
		// left: k/2, right: (k+1)/2
		res = Min(res, sum[i+k]-sum[k/2+i]-sum[(k+1)/2+i]+sum[i])
	}
	res -= (k / 2) * ((k + 1) / 2) //TODO
	return res
}
