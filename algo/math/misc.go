package main

import (
	"fmt"
	"strconv"
)

// https://leetcode.com/problems/remove-9/
// Start from integer 1, remove any integer that contains 9 such as 9, 19, 29...
//
//Now, you will have a new integer sequence [1, 2, 3, 4, 5, 6, 7, 8, 10, 11, ...].
//
//Given an integer n, return the nth (1-indexed) integer in the new sequence.
//
//
//
//Example 1:
//
//Input: n = 9
//Output: 10
//Example 2:
//
//Input: n = 10
//Output: 11
//
//
//Constraints:
//
//1 <= n <= 8 * 10^8

// a number with digit 9 is a base-9 number
func newInteger_660(n int) int {
	base := 1
	var res int
	for n != 0 {
		res += base * (n % 9)
		n /= 9
		base *= 10
	}
	return res
}

func newInteger_660_v2(n int) int {
	var res string
	for n != 0 {
		res = fmt.Sprint(n % 9) + res
		n /= 9
	}
	v, _ := strconv.Atoi(res)
	return v
}

//https://leetcode.com/problems/remove-9/discuss/106558/what-if-remove-number-7
// I think remove 7 is the same as remove 9 with mapping 7->8, 8->9. For example, you get answer
// as 18317 and the final answer would be 19318.

func reverse(num int) int{
	n := 7
	fmt.Printf("%08b\n", n)
	base := 2
	rev := 0
	for n > 0 {
		rev = rev*base + n%base
		n /= base
	}
	fmt.Printf("%08b\n", rev)
	return rev
}