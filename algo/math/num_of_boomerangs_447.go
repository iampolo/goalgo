package main

// https://leetcode.com/problems/number-of-boomerangs/
//You are given n points in the plane that are all distinct, where points[i] = [xi, yi]. A boomerang is a tuple of
//points (i, j, k) such that the distance between i and j equals the distance between i and k (the order of the tuple matters).
//
//Return the number of boomerangs.
//
//
//
//Example 1:
//
//Input: points = [[0,0],[1,0],[2,0]]
//Output: 2
//Explanation: The two boomerangs are [[1,0],[0,0],[2,0]] and [[1,0],[2,0],[0,0]].
//
//Example 2:
//
//Input: points = [[1,1],[2,2],[3,3]]
//Output: 2
//
//Example 3:
//
//Input: points = [[1,1]]
//Output: 0
//
//
//Constraints:
//
//- n == points.length
//- 1 <= n <= 500
//- points[i].length == 2
//- -10^4 <= xi, yi <= 10^4
//- All the points are unique.
//
func numberOfBoomerangs(points [][]int) int {
	// sqrt((x2-x1)^2 +(x2-x1)^2)
	// if there are n points have the same distance to a,
	// then the total boomerangs is n*(n-1)
	// say 3 points, we need 2 points, 3P2 => 3*2 => 6
	//
	//  for all the groups of points,
	//  number of ways to select 2 from n =
	//  nP2 = n!/(n - 2)! = n * (n - 1)

	var max int
	for i := range points {
		cntMap := make(map[int]int)
		for j := 0; j < len(points); j++ {
			if i == j {
				continue
			}
			p1 := points[j][0] - points[i][0]
			p2 := points[j][1] - points[i][1]
			dist := p1*p1 + p2*p2
			cntMap[dist]++
		}

		//if it is just one point, the result is 0
		for _, v := range cntMap {
			max += v*(v-1)
		}
	}

	return max
}
