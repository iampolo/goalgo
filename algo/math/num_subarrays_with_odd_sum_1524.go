package main

//https://leetcode.com/problems/number-of-sub-arrays-with-odd-sum/
//Given an array of integers arr, return the number of subarrays with an odd sum.
//
//Since the answer can be very large, return it modulo 10^9 + 7.
//
//
//
//Example 1:
//
//Input: arr = [1,3,5]
//Output: 4
//Explanation: All subarrays are [[1],[1,3],[1,3,5],[3],[3,5],[5]]
//All sub-arrays sum are [1,4,9,3,8,5].
//Odd sums are [1,9,3,5] so the answer is 4.
//
//Example 2:
//
//Input: arr = [2,4,6]
//Output: 0
//Explanation: All subarrays are [[2],[2,4],[2,4,6],[4],[4,6],[6]]
//All sub-arrays sum are [2,6,12,4,10,6].
//All sub-arrays have even sum and the answer is 0.
//
//Example 3:
//
//Input: arr = [1,2,3,4,5,6,7]
//Output: 16
//
//
//Constraints:
//
//1 <= arr.length <= 10^5
//1 <= arr[i] <= 100
//
func numOfSubarrays(arr []int) int {
	res, odd, even := 0, 0, 0
	for _, n := range arr {
		even += 1
		if n%2 == 1 { //if this number is odd
			odd, even = even, odd
		}
		res = (res + odd) % (1e9 + 7)
	}
	return res
}

func numOfSubarrays_v2(arr []int) int {
	//odd+even=>odd
	//even+even=>even
	//odd+odd=>even
	//even-odd=>odd
	//odd-even=>odd

	//even is 1 initially?
	//To reflect the initial sum==0, it is an even.
	odd, even, sum := 0, 1, 0
	for _, n := range arr {
		sum += n
		if sum%2 == 0 {
			even++
		} else {
			odd++
		}
	}
	return (even * odd) % (1e9 + 7)
}

const MOD = 1e9 + 7

func numOfSubarrays_v3(arr []int) int {
	//number of of subarrays with odd and even sums respectively
	//counters
	odd, even := 0, 1

	//odd-even=>odd
	//even-odd=>odd
	//apply this rule to presum
	var curSum int
	var res int
	for i := range arr {
		curSum += arr[i]
		//check if this curSum(presum) is odd or even
		//if curSum is not even, check how many are odd, as even-odd will yield odd (subarray), vice versa.
		if curSum%2 == 0 { //if curSum is even, we add odd, as even+odd=odd
			res = (res + odd) % MOD
			even++
		} else {
			res = (res + even) % MOD
			odd++
		}
	}
	return res
}
