package main

import "math/rand"

type Solution struct {
	sums []int
}

func Constructor(w []int) Solution {
	total := 0
	sums := make([]int, len(w))
	for i := 0; i < len(w); i++ {
		total += w[i]
		sums[i] = total
	}

	return Solution{sums : sums}
}

func (this *Solution) PickIndex() int {
	size := len(this.sums)
	random := rand.Intn(this.sums[size - 1])
	l, r := 0, size - 1
	for l < r {
		m := l + (r - l) / 2
		if this.sums[m] <= random {
			l = m + 1
		} else {
			r = m
		}
	}
	return l
}
