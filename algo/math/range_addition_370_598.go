package main

import (
	"fmt"
	"sort"
)

/**
https://leetcode.com/problems/range-addition/

https://massivealgorithms.blogspot.com/2016/06/leetcode-370-range-addition.html

http://www.cnblogs.com/grandyang/p/5628786.html
Assume you have an array of length n initialized with all 0's and are given k update operations.
Each operation is represented as a triplet: [startIndex, endIndex, inc] which increments each element of
subarray A[startIndex ... endIndex] (startIndex and endIndex inclusive) with inc.
Return the modified array after all k operations were executed.

Example:
Given:

    length = 5,
    updates = [
        [1,  3,  2],
        [2,  4,  3],
        [0,  2, -2]
    ]

Output:

    [-2, 0, 3, 5, 3]
Explanation:
Initial state:
[ 0, 0, 0, 0, 0 ]

After applying operation [1, 3, 2]:
[ 0, 2, 2, 2, 0 ]

After applying operation [2, 4, 3]:
[ 0, 2, 5, 5, 3 ]

After applying operation [0, 2, -2]:
[-2, 0, 3, 5, 3 ]
Hint:
Thinking of using advanced data structures? You are thinking it too complicated.
For each update operation, do you really need to update all elements between i and j?
Update only the first and end element is sufficient.
The optimal time complexity is O(k + n) and uses O(1) extra space.

https://discuss.leetcode.com/topic/49691/java-o-k-n-time-complexity-solution
http://shirleyisnotageek.blogspot.com/2016/10/range-addition.html
The idea is to utilize the fact that the array initializes with zero. The hint suggests us that we only
needs to modify the first and last element of the range. In fact, we need to increment the first element
in the range and decreases the last element + 1 (if it's within the length) by inc. Then we sum up all
previous results. Why does this work? When we sum up the array, the increment is passed along to the
subsequent elements until the last element. When we decrement the end + 1 index, we offset the increment
so no increment is passed along to the next element.

*/

func getModifiedArray_bf(length int, updates [][]int) []int {
	res := make([]int, length)

	for _, up := range updates {
		for i := up[0]; i <= up[1]; i++ {
			res[i] += up[2]
		}
	}

	return res
}

/**
	0	0	0	0	0
	    2	 	 	-2
			3
	-2	  	  	(2)
                    ^ -(2)
			    ^ -(-2)
then add up from left right
    -2  0   3   5   3

	- a number (e.g.:-2) can be carried from left to right add to right
	- when an operation is ended, we need to decrement from the series, ie: -2 at pos:end-1

*/
func getModifiedArray_v2(length int, updates [][]int) []int {

	res := make([]int, length)
	for _, up := range updates {
		res[up[0]] += up[2]
		if up[1]+1 < length {
			res[up[1]+1] -= up[2] //mark as end of this operation
		}
	}

	for i := 1; i < len(res); i++ {
		res[i] += res[i-1]
	}

	return res
}

func getModifiedArray(length int, updates [][]int) []int {

	sort.SliceStable(updates, func(i, j int) bool {
		return updates[i][0] < updates[j][0]
	})

	fmt.Println(updates)

	return nil
}

// https://leetcode.com/problems/range-addition-ii/
// You are given an m x n matrix M initialized with all 0's and an array of operations ops, where
// ops[i] = [ai, bi] means M[x][y] should be incremented by one for all 0 <= x < ai and 0 <= y < bi.
//
// Count and return the number of maximum integers in the matrix after performing all the operations.
//
//Example 1:
//
//
//Input: m = 3, n = 3, ops = [[2,2],[3,3]]
//Output: 4
//Explanation: The maximum integer in M is 2, and there are four of it in M. So return 4.
//
//Example 2:
//
//Input: m = 3, n = 3, ops = [[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3],[2,2],[3,3],[3,3],[3,3]]
//Output: 4
//
//Example 3:
//
//Input: m = 3, n = 3, ops = []
//Output: 9
//
//
//Constraints:
//
//1 <= m, n <= 4 * 10^4
//0 <= ops.length <= 10^4
//ops[i].length == 2
//1 <= a_i <= m
//1 <= b_i <= n
//
func maxCount_598_fb(m int, n int, ops [][]int) int {

	arr := make([][]int, m)
	for i := 0; i < m; i++ {
		arr[i] = make([]int, n)
	}

	for _, o := range ops {
		for i := 0; i < o[0]; i++ {
			for j := 0; j < o[1]; j++ {
				arr[i][j] += 1
			}
		}
	}

	cnt := 0
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if arr[i][j] == arr[0][0] {  //as [0][0] is the one will always be updated
				cnt++
			}
		} //for
	}
	return cnt
}

func maxCount_598(m int, n int, ops [][]int) int {
	//as the ops will always update [0,0]
	//we just need to find the min row and col

	r, c := m, n
	for _, o := range ops {
		// min: the farthest can reach for all the operations
		r = min(o[0], r)
		c = min(o[1], c)
	}
	return r * c
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
