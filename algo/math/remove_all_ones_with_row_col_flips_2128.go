package main

/*
https://leetcode.com/problems/remove-all-ones-with-row-and-column-flips/
You are given an m x n binary matrix grid.

In one operation, you can choose any row or column and flip each value in that row or column
(i.e., changing all 0's to 1's, and all 1's to 0's).

Return true if it is possible to remove all 1's from grid using any number of operations or false otherwise.



Example 1:


Input: grid = [[0,1,0],[1,0,1],[0,1,0]]
Output: true
Explanation: One possible way to remove all 1's from grid is to:
- Flip the middle row
- Flip the middle column

Example 2:


Input: grid = [[1,1,0],[0,0,0],[0,0,0]]
Output: false
Explanation: It is impossible to remove all 1's from grid.

Example 3:


Input: grid = [[0]]
Output: true
Explanation: There are no 1's in grid.


Constraints:

m == grid.length
n == grid[i].length
1 <= m, n <= 300
grid[i][j] is either 0 or 1.


hint: find the pattern
 - patterns on rows must be the same, either 11100, or 00011 (reverse)
	- stores the pattern and reversed pattern of the first row in a Set
    - check the other rows to ensure that they all have the same pattern as stored in the Set
 - change the 1st row, and toggle the entire column if it is 1
	- 2nd row onward must be either all 0s or all 1s
 -

861. Score After Flipping Matrix
*/

// similar to removeOnes()
func removeOnes_v2(grid [][]int) bool {
	//flip those rows if their 1st cell is 1
	for _, v := range grid { //row by row
		if v[0] == 1 { //if the 1st col of each row is 1
			for i := range v { //flip the binary in this row
				v[i] = 1 - v[i]
			}
		}
	}
	//flip those column if its 1st cell is 1
	for i, v := range grid[0] {
		if v == 1 {
			for j := range grid { //row: flip the entire column
				grid[j][i] = 1 - grid[j][i]
			}
		} //for
	} //for

	//the above operations are all what is needed to flip those possible matrix; otherwise, return false
	//there should be no more 1s if it is possible
	for i := range grid {
		for _, v := range grid[i] {
			if v == 1 {
				return false
			}
		}
	}
	return true
}

func removeOnes(grid [][]int) bool {
	//flip the column first; whenever the 1st cell is 1
	for i, v := range grid[0] {
		if v == 1 {
			for c := 0; c < len(grid); c++ {
				grid[c][i] = 1 - grid[c][i]
			}
		}
	}
	//start from the 2nd row
	for i := 1; i < len(grid); i++ {
		var sum int
		for j := 0; j < len(grid[0]); j++ {
			sum += grid[i][j]
		}
		//either all 0 or all 1 to be valid or flippable
		if sum != 0 && sum != len(grid[0]) {
			return false
		}
	}
	return true
}
