package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

// https://leetcode.com/problems/guess-number-higher-or-lower/

// We are playing the Guess Game. The game is as follows:
//
// I pick a number from 1 to n. You have to guess which number I picked.
//
// Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.
//
// You call a pre-defined API int guess(int num), which returns 3 possible results:
//
// - -1: The number I picked is lower than your guess (i.e. pick < num).
// - 1: The number I picked is higher than your guess (i.e. pick > num).
// - 0: The number I picked is equal to your guess (i.e. pick == num).
// - Return the number that I picked.
//
//
//
// Example 1:
//
// Input: n = 10, pick = 6
// Output: 6
//
// Example 2:
//
// Input: n = 1, pick = 1
// Output: 1
//
// Example 3:
//
// Input: n = 2, pick = 1
// Output: 1
//
// Example 4:
//
// Input: n = 2, pick = 2
// Output: 2
//
//
// Constraints:
//
// 1 <= n <= 2^31 - 1
// 1 <= pick <= n

/**
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is lower than the guess number
 *			      1 if num is higher than the guess number
 *               otherwise return 0
 * func guess(num int) int;
 */
func guess(n int) int {
	return 0
}

func guessNumber_ternary_search(n int) int {
	left, right := 1, n

	for left <= right {
		m1 := left + (right-left)/3
		m2 := right - (right-left)/3
		g1 := guess(m1)
		g2 := guess(m2)

		if g1 == 0 {
			return m1
		} else if g2 == 0 {
			return m2
		} else if g1 < 0 {
			right = m1 - 1
		} else if g2 > 0 {
			left = m2 + 1
		} else { //in between m1 and m2
			left = m1 + 1
			right = m2 - 1
		}
	}
	return -1
}

func guessNumber(n int) int {
	left, right := 1, n

	for left <= right {
		m := left + (right-left)/2
		g := guess(m)
		if g == 0 {
			return m
		} else if g > 0 {
			left = m + 1
		} else {
			right = m - 1
		}
	}
	return -1
}

// https://leetcode.com/problems/guess-number-higher-or-lower-ii/
// Every guessing pattern would have a corresponding cost in its worst case.
// We want to minimize this cost.
func getMoneyAmount(n int) int {
	dp := make([][]int, n+1)
	for i := range dp {
		dp[i] = make([]int, n+1)
		for j := range dp[i] {
			dp[i][j] = -1
		}
	}

	var miniMax func(i, j int) int
	miniMax = func(i, j int) int {
		if i >= j {
			//there is only one num, no cost at all
			return 0
		}
		// check historical data
		if dp[i][j] != -1 {
			return dp[i][j]
		}

		dp[i][j] = math.MaxInt32
		//guess/try a number k from the range of i and j, k is also the k to take
		//consider the max cost since we dont know the target, but we want to take the minimum among the choices?!
		// minimax strategy?!
		// TODO
		//you pay 2$ in the worst case if you choose 2 or pay 3$ in the worst case if you pick 3$. So we
		//will pick the min of the worst cases which is 2$ and hence 2 is the answer for (2, 3) sub-problem.
		//(Notice the minimax?) So, the total cost paid in this is 1$ + 2$ = 3$.
		for k := i; k <= j; k++ {
			left := miniMax(k+1, j)
			right := miniMax(i, k-1)
			dp[i][j] = Min(dp[i][j], Max(left, right)+k) //k is the cost to take
		}
		//the result in this level
		return dp[i][j]
	}
	res := miniMax(1, n)
	for i := range dp {
		for j := range dp[i] {
			fmt.Printf("%d ", dp[i][j])
		}
		fmt.Println()
	}

	return res
}

// the for loop can be mapped to the recursive version
func getMoneyAmount_v2(n int) int {
	//row 0 doesn't count, and exact row at the end is the n, we need to select.
	dp := make([][]int, n+2)
	for i := range dp {
		dp[i] = make([]int, n+2)
	}

	//guess range: minimum to number, and up to n
	//begin with two numbers at least
	//matrix traversal!!
	for s := 2; s <= n; s++ { //data size
		for i := 1; i+s-1 <= n; i++ {
			j := i + s - 1
			min := math.MaxInt32
			for k := i; k <= j; k++ { //guess each num in the range
				min = Min(min, Max(dp[i][k-1], dp[k+1][j])+k)
			}
			dp[i][j] = min
		}
	} //for

	return dp[1][n]
}
