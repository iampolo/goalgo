package main

import . "gitlab.com/iampolo/goalgo/algo/util"

//https://leetcode.com/problems/stone-game/
func stoneGame(piles []int) bool {
	return play(piles, 0, len(piles)-1) > 0
}

// choose DP as we need to try all strategies.
// so greedily can't be used here
// recursion w/o memo
func play(piles []int, l, r int) int {
	if l > r {
		return 0
	}

	if l == r {
		return l
	}
	// calc with the net score instead of total score.
	// score(Alex) > score(Lee)
	// score(Alex) - score(Lee) = score; score > 0
	return Max(piles[l]-play(piles, l+1, r),
		piles[r]-play(piles, l, r-1))
}
