package main

// https://leetcode.com/problems/stone-game-ix/submissions
//  Alice and Bob continue their games with stones. There is a row of n stones, and each stone has an associated value.
// You are given an integer array stones, where stones[i] is the value of the ith stone.
//
// Alice and Bob take turns, with Alice starting first. On each turn, the player may remove any stone from stones.
// The player who removes a stone loses if the sum of the values of all removed stones is divisible by 3.
// Bob will win automatically if there are no remaining stones (even if it is Alice's turn).
//
// Assuming both players play optimally, return true if Alice wins and false if Bob wins.
//
// Example 1:
//
// Input: stones = [2,1]
// Output: true
// Explanation: The game will be played as follows:
// - Turn 1: Alice can remove either stone.
// - Turn 2: Bob removes the remaining stone.
// The sum of the removed stones is 1 + 2 = 3 and is divisible by 3. Therefore, Bob loses and Alice wins the game.
//
// Example 2:
//
// Input: stones = [2]
// Output: false
// Explanation: Alice will remove the only stone, and the sum of the values on the removed stones is 2.
// Since all the stones are removed and the sum of values is not divisible by 3, Bob wins the game.
//
// Example 3:
//
// Input: stones = [5,1,2,4,3]
// Output: false
// Explanation: Bob will always win. One possible way for Bob to win is shown below:
// - Turn 1: Alice can remove the second stone with value 1. Sum of removed stones = 1.
// - Turn 2: Bob removes the fifth stone with value 3. Sum of removed stones = 1 + 3 = 4.
// - Turn 3: Alice removes the fourth stone with value 4. Sum of removed stones = 1 + 3 + 4 = 8.
// - Turn 4: Bob removes the third stone with value 2. Sum of removed stones = 1 + 3 + 4 + 2 = 10.
// - Turn 5: Alice removes the first stone with value 5. Sum of removed stones = 1 + 3 + 4 + 2 + 5 = 15.
// Alice loses the game because the sum of the removed stones (15) is divisible by 3. Bob wins the game.
//
//
// Constraints:
//
// 1 <= stones.length <= 10^5
// 1 <= stones[i] <= 10^4
func stoneGameIX(stones []int) bool {
	//they are playing optimally
	//they aware the rules, so the will make a calculated choice. They will try hard not to pick multiple of three.
	//for easier programming, we can reduce stones to val%3 => modulo of 3

	//reduce the stones to mod3 for easy handling
	//player won't select stone[i]==0, as it is always multiple of 3
	//so they will pick stone[i]==1, or 2, and check the selection won't the multiple of 3.

	values := make([]int, 3)
	for _, v := range stones {
		values[v%3]++
	}
	//alice will pick either 1 or 2, and avoid picking 3

	return canAliceWin(values, 1, len(stones)) || canAliceWin(values, 2, len(stones))
}

func canAliceWin(values []int, sel int, n int) bool {
	if values[sel] == 0 {
		return false //Alice can't win as there is no number
	}
	tmp := make([]int, 3)
	copy(tmp, values)

	sum := sel //idx is also the value due to modulo
	tmp[sel]--
	//start with 1, as alice made her choice before calling this func
	for i := 1; i < n; i++ {
		if tmp[1] > 0 && (sum+1)%3 != 0 { //select 1 as make sure it is not multiple of 3
			tmp[1]--
			sum += 1
		} else if tmp[2] > 0 && (sum+2)%3 != 0 { //select 2 as make sure it is not multiple of 3
			tmp[2]--
			sum += 2
		} else if tmp[0] > 0 && sum%3 != 0 {
			tmp[0]--
			//no selectable values
		} else {
			//this player has no available stone to select for this stone, he lost,
			// as any other choices will make the sum to multiple of 3
			return i%2 == 1 //Bob:1,3,5,...
		}
	} //

	//no more stone, Alice lose
	return false
}
