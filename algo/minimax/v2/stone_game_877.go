package v2

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/stone-game/
func StoneGame(piles []int) bool {
	memo := make([][]int, len(piles))
	for i := range memo {
		memo[i] = make([]int, len(memo))
		for j := range memo {
			memo[i][j] = math.MinInt32
		}
	}
	return play(piles, 0, len(piles)-1, memo) > 0
}
// https://www.youtube.com/watch?v=xJ1Rc30Pyes&ab_channel=HuaHua
// recursion w/ memo
// O(n^2)
func play(piles []int, l, r int, memo [][]int) int {
	if l > r {
		return 0
	}
	if l == r {
		return piles[l]
	}
	if memo[l][r] != math.MinInt32 {
		return memo[l][r]
	}

	memo[l][r] = Max(piles[l]-play(piles, l+1, r, memo),
		piles[r]-play(piles, l, r-1, memo))

	return memo[l][r]
}
