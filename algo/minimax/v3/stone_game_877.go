package v3

import . "gitlab.com/iampolo/goalgo/algo/util"

//https://leetcode.com/problems/stone-game/
func StoneGame(piles []int) bool {
	memo := make([][]int, len(piles))
	for i := 0; i < len(memo); i++ {
		memo[i] = make([]int, len(piles))
		for j := range memo {
			memo[i][j] = -1
		}
	}

	return play(piles, 0, len(piles)-1, memo, 0) > 0
}

// https://www.youtube.com/watch?v=xJ1Rc30Pyes&ab_channel=HuaHua
// Mini-max
// O(n^2)
func play(piles []int, l, r int, memo [][]int, player int) int {
	if l > r {
		return 0
	}

	if memo[l][r] != -1 {
		return memo[l][r]
	}

	// Analyze level by level
	if player == 0 {
		left := piles[l] + play(piles, l+1, r, memo, 1-player)
		right := piles[r] + play(piles, l, r-1, memo, 1-player)
		memo[l][r] = Max(left, right)
	} else {
		left := -piles[l] + play(piles, l+1, r, memo, 1-player)
		right := -piles[r] + play(piles, l, r-1, memo, 1-player)
		memo[l][r] = Min(left, right)
	}

	// Return the result collected for this level
	return memo[l][r]
}
