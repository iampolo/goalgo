package model

import (
	"bytes"
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func CreateList(array []int) *ListNode {
	head := new(*ListNode)
	curr := head
	for e := range array {
		if *curr == nil {
			*curr = new(ListNode)
		}
		(**curr).Val = array[e]
		curr = &((*curr).Next)
	}
	return *head
}

func NewList(val int) *ListNode {
	return &ListNode{Val: val}
}

func (lst *ListNode) String() string {
	var b bytes.Buffer
	for lst != nil {
		fmt.Fprintf(&b, "%d ", lst.Val)
		lst = lst.Next
	}
	return b.String()
}
