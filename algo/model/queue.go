package model

type Queue []interface{}

func (q *Queue) Poll() interface{} {
	r := (*q)[0]
	*q = (*q)[1:]
	return r
}

func (q *Queue) Offer(i interface{}) {
	(*q) = append(*q, i)
}

func (q *Queue) Peek() interface{} {
	return (*q)[0]
}

func (q *Queue) IsEmpty() bool {
	return len(*q) == 0;
}

func (q *Queue) Len() int {
	return len(*q)
}
