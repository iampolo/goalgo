package model

import (
	"fmt"
	"testing"
)

func TestQueue(t *testing.T) {
	q := Queue{}

	for i := 0; i < 5; i++ {
		q.Offer(1 << uint(i))
	}


	fmt.Println(q.Poll())

	for !q.IsEmpty() {
		fmt.Println(q.Len(), q.Poll())
	}

}
