package model

//https://yourbasic.org/golang/implement-stack/
//Amortized time complexity :
/*
https://yourbasic.org/algorithms/amortized-time-complexity-analysis/


other example:
http://www.code2succeed.com/stack-implementation-in-golang/
https://briandouglas.me/posts/2017/01/30/stacks-in-go

thread-safe:
https://flaviocopes.com/golang-data-structure-stack/
*/


type Stack []interface{}

func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

func (s *Stack) Push(i interface{}) {
	*s = append(*s, i)
}

func (s *Stack) Pop() interface{} {
	end := len(*s) - 1
	r := (*s)[end]
	*s = (*s)[:end]
	return r
}

func (s *Stack) Peek() interface{} {
	return (*s)[len(*s)-1]
}

func (s *Stack) Len() int {
	return len(*s)
}
