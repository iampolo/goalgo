package model

import (
	"fmt"
	"testing"
)

func TestStack(t *testing.T) {
	stack := new(Stack)

	stack.Push(10)
	stack.Push(11)
	stack.Push("BOS")

	fmt.Println(stack.Len())

	for !stack.IsEmpty() {
		fmt.Println(stack.Pop())
	}

	st = append(st, "b")
	st = append(st, "a")
	st = append(st, "c")

	fmt.Println(st[:len(st)])
}

var st []string