package model

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func NewNode(val int) *TreeNode {
	return &TreeNode{
		Val: val,
	}
}

func Array2BinaryTree(array []interface{}) *TreeNode {
	root := new(*TreeNode)
	insertArrayToTreeNode(root, array, 0)
	return *root
}

func insertArrayToTreeNode(n **TreeNode, array []interface{}, index int) {
	if index >= len(array) || array[index] == nil {
		return
	}
	*n = new(TreeNode)
	(*n).Val = array[index].(int)
	insertArrayToTreeNode(&((*n).Left), array, 2*index+1)
	insertArrayToTreeNode(&((*n).Right), array, 2*index+2)
}

func CreateBst(array []int) *TreeNode {
	var root *TreeNode
	for _, v := range array {
		root = createBst(root, v)
	}

	return root
}
func createBst(root *TreeNode, val int) *TreeNode {
	if root == nil {
		root = &TreeNode{Val: val}
		return root
	}
	if val < root.Val {
		root.Left = createBst(root.Left, val)
	} else {
		root.Right = createBst(root.Right, val)
	}
	return root
}

func PrintPostOrder(root *TreeNode) {
	fmt.Println(CreatePostOrder(root))
}

func CreatePostOrder(root *TreeNode) []int {
	res := []int{}
	res = postOrder(root, res)
	return res
}

func postOrder(root *TreeNode, lst []int) []int {
	if root == nil {
		return lst
	}
	lst = postOrder(root.Left, lst)
	lst = postOrder(root.Right, lst)
	lst = append(lst, root.Val)
	return lst
}

func PrintInOrder(root *TreeNode) {
	fmt.Println(CreateInOrder(root))
}

func CreateInOrder(root *TreeNode) []int {
	res := []int{}
	res = inOrder(root, res)
	return res
}

func inOrder(root *TreeNode, lst []int) []int {
	if root == nil {
		return lst
	}
	lst = inOrder(root.Left, lst)
	lst = append(lst, root.Val)
	lst = inOrder(root.Right, lst)
	return lst
}
