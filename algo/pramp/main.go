package main

import "fmt"

func main() {
	test_shortest_cell_path()
}

func test_shortest_cell_path() {

	sr, sc := 0, 0
	tr, tc := 2, 0

	grid := [][]int{{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}}
	fmt.Println(shortestCelPath(grid, sr, sc, tr, tc))

	grid = [][]int{{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 0, 1, 1}}
	fmt.Println(shortestCelPath(grid, sr, sc, tr, tc))
}
