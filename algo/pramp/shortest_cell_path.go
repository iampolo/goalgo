package main

/*
https://www.pramp.com/challenge/Y56aZmaj9Ptmd9wV9xvL

Shortest Cell Path
In a given grid of 0s and 1s, we have some starting row and column sr, sc and a target row and column tr, tc.
Return the length of the shortest path from sr, sc to tr, tc that walks along 1 values only.

Each location in the path, including the start and the end, must be a 1. Each subsequent location in the path must be
4-directionally adjacent to the previous location.

It is guaranteed that grid[sr][sc] = grid[tr][tc] = 1, and the starting and target positions are different.

If the task is impossible, return -1.

Examples:

input:
grid = [[1, 1, 1, 1], [0, 0, 0, 1], [1, 1, 1, 1]]
sr = 0, sc = 0, tr = 2, tc = 0
output: 8
(The lines below represent this grid:)
1111
0001
1111

grid = [[1, 1, 1, 1], [0, 0, 0, 1], [1, 0, 1, 1]]
sr = 0, sc = 0, tr = 2, tc = 0
output: -1
(The lines below represent this grid:)
1111
0001
1011
Constraints:

[time limit] 5000ms
[input] array.array.integer grid
1 ≤ arr.length = arr[i].length ≤ 10
[input] integer sr
[input] integer sc
[input] integer tr
[input] integer tc
All sr, sc, tr, tc are valid locations in the grid, grid[sr][sc] = grid[tr][tc] = 1, and (sr, sc) != (tr, tc).
[output] integer

*/

var DIRS = []int{0, -1, 0, 1, 0}

type Empty struct{}

var NIL Empty

func shortestCelPath(grid [][]int, sr, sc int, tr, tc int) int {
	var queue [][]int
	vis := make(map[int]Empty)

	cols := len(grid[0])
	queue = append(queue, []int{sr, sc})
	vis[sr*cols+sc] = NIL

	var steps int
	for len(queue) > 0 {
		for i := len(queue); i > 0; i-- {
			cur := queue[0]
			queue = queue[1:]

			if cur[0] == tr && cur[1] == tc {
				return steps
			}

			for j := 0; j < 4; j++ {
				nx, ny := cur[0]+DIRS[j], cur[1]+DIRS[j+1]

				if nx >= 0 && nx < len(grid) && ny >= 0 && ny < len(grid[0]) && grid[nx][ny] == 1 {
					if _, ok := vis[nx*cols+ny]; ok {
						continue
					}

					queue = append(queue, []int{nx, ny})
					vis[nx*cols+ny] = NIL
				} //
			}
		}
		steps++
	}

	return -1
}
