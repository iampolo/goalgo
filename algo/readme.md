
### Algo - Go examples 
https://github.com/golang/go/wiki

### GOlang skills
https://github.com/golang/go/wiki/SliceTricks
https://ueokande.github.io/go-slice-tricks/

## How to Code in GO https://www.digitalocean.com/community/tutorial_series/how-to-code-in-go
* https://assets.digitalocean.com/books/how-to-code-in-go.pdf
* https://www.digitalocean.com/community/tutorials/building-go-applications-for-different-operating-systems-and-architectures

### Variable idioms
https://www.digitalocean.com/community/tutorials/how-to-use-variables-and-constants-in-go
* Only use long form, var i int, when you’re not initializing the variable.
* Use short form, i := 1, when declaring and initializing.
* If you did not desire Go to infer your data type, but you still want to use short variable  
  declaration, you can wrap your value in your desired type, with the following syntax:
  
### Three ways to sort in golang
https://yourbasic.org/golang/how-to-sort-in-go/
- sort/sort.go in go library
- Slice(x interface{}, less func(i, j int) bool)
  
### Impl. queue with slice
https://stackoverflow.com/questions/2818852/is-there-a-queue-implementation

### Queue and Stack
https://gist.github.com/moraes/2141121
https://yourbasic.org/golang/implement-fifo-queue/
https://yourbasic.org/golang/implement-stack/

### PriorityQueue in Go
- https://medium.com/swlh/min-heaps-in-go-golang-fb3d9666c03f

###2 patterns for a do-while loop in Go
https://yourbasic.org/golang/do-while-loop/
```shell 
for ok := true; ok; ok = condition {
  work()
}
```

### Go LRU/Condition variables/concurrency/Hydration/Pools/DI/Logging
### CouchDB And MongoDB Performance
* https://www.openmymind.net/
* https://www.openmymind.net/Condition-Variables/
* https://www.openmymind.net/Buying-Concurrency-With-Memory/
* https://www.openmymind.net/Practical-SOA-Hydration-Part-1/
* https://www.openmymind.net/Concurrent-Friendly-Logging-With-Go/
* Learning NoSQL
  * https://www.openmymind.net/2011/8/15/How-You-Should-Go-About-Learning-NoSQL/

#### zero values

```
# run main.go
go run ./algo/dp/v3

https://github.com/phea/leetcode-go
https://github.com/ganeshskudva/Leetcode-Golang
```


Go Data structure lib

```asciidoc
https://github.com/emirpasic/gods#treemap

https://github.com/karalabe/cookiejar

https://github.com/emirpasic/gods

```