package main

//https://forum.letstalkalgorithms.com/t/valid-tic-tac-toe-state/697/2
func ValidTicTacToe(board []string) bool {
	xCount, oCount := 0, 0
	xs, cs := 0, 0
	for i := 0; i < len(board); i++ {
		if board[i] == "XXX" {
			xs++
		} else if board[i] == "OOO" {
			cs++
		}
		yax := string([]byte{board[0][i], board[1][i], board[2][i]})
		if yax == "XXX" {
			xs++
		} else if yax == "OOO" {
			cs++
		}
		for _, ch := range board[i] {
			if ch == 'X' {
				xCount++
			} else if ch == 'O' {
				oCount++
			}
		}
	}
	if oCount > xCount || xCount-oCount > 1 {
		return false
	}

	dg1 := string([]byte{board[0][0], board[1][1], board[2][2]})
	dg2 := string([]byte{board[0][2], board[1][1], board[2][0]})
	if dg1 == "XXX" || dg2 == "XXX" {
		xs++
	} else if dg1 == "OOO" || dg2 == "OOO" {
		cs++
	}

	if (cs > 0 && xs > 0) || (xs > 0 && xCount != oCount+1) || (cs > 0 && xCount != oCount) {
		return false
	}

	return true
}
