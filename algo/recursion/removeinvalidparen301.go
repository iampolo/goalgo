package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)
//https://stackoverflow.com/questions/20195296/golang-append-an-item-to-a-slice
func removeInvalidParentheses(s string) []string {
	res := make([]string, 0)
	remove(s, &res, 0, 0, []byte{'(', ')'})
	return res;
}

func remove(s string, res *[]string, slow int, fast int, pair []byte) {

	for cnt, i := 0, fast; i < len(s); i++ {
		//count the paren to identify any invalid case
		if s[i] == pair[0] {
			cnt++
		}
		if s[i] == pair[1] {
			cnt--;
		}
		if cnt >= 0 {
			continue
		}
		//invalid found
		for j := slow; j <= i; j++ {
			if s[j] == pair[1] && (j == slow|| s[j-1] != pair[1]) { //find the first invalid
				remove(s[:j]+s[j+1:], res, j, i, pair)
			}
		}
		return
	}

	s = util.ReverseString(s)
	if pair[0] == '(' {
		remove(s, res, 0, 0, []byte{')','('})
	} else {
		*res = append(*res, s)
	}
}
func test_301() {
	s := "()())()"
	fmt.Println(removeInvalidParentheses(s))
}
