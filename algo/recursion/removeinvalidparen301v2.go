package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

func removeInvalidParenthesesGetOne(s string) string {
	return removeGetOne(s, 0, 0, []byte{'(', ')'})
}

func removeGetOne(s string, slow int, fast int, pair []byte) string {

	for cnt, i := 0, fast; i < len(s); i++ {
		//count the paren to identify any invalid case
		if s[i] == pair[0] {
			cnt++
		}
		if s[i] == pair[1] {
			cnt--;
		}
		if cnt >= 0 {
			continue
		}
		//invalid found
		for j := slow; j <= i; j++ {
			if s[j] == pair[1] && (j == slow || s[j-1] != pair[1]) { //find the first invalid
				t := removeGetOne(s[:j]+s[j+1:], j, i, pair)
				fmt.Println("t:", t)
				if t != "" {return t}
			}
		}
		return "" //there is no answer
	}
	s = util.ReverseString(s)
	if pair[0] == '(' {
		fmt.Println("+++")
		return removeGetOne(s, 0, 0, []byte{')', '('})
	}
	return s //return possible answer
}
func test_301v2() {
	s := "()())()"
	s = ")(("
	s = "(x-x"
	fmt.Println(removeInvalidParenthesesGetOne(s))
}
