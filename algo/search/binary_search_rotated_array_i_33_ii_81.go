package main

// https://leetcode.com/problems/search-in-rotated-sorted-array-ii/solution/
func search_81(nums []int, target int) bool {
	l, r := 0, len(nums)-1

	for l < r { //the last one is not checked
		m := l + (r-l)/2
		if nums[m] == target {
			return true
		}
		if nums[m] < nums[r] {
			if nums[m] < target && target <= nums[r] {
				l = m + 1
			} else {
				r = m - 1
			}
		} else if nums[l] < nums[m] {
			if nums[l] <= target && target < nums[m] {
				r = m - 1
			} else {
				l = m + 1
			}
		} else {
			if nums[m] == nums[r] {
				r--
			}
			if nums[l] == nums[m] {
				l++
			}
		}
	}

	// 1, 0, 1, 1, 1
	//    l  m  r
	//   l r
	if nums[l] == target {
		return true
	}
	return false
}

//https://leetcode.com/problems/search-in-rotated-sorted-array
func search_33(nums []int, target int) int {
	l, r := 0, len(nums)-1

	for l <= r { //the last one is not checked
		m := l + (r-l)/2
		if nums[m] == target {
			return m
		}

		if nums[m] < nums[r] {
			if nums[m] < target && target <= nums[r] {
				l = m + 1
			} else {
				r = m - 1
			}
		} else {
			if nums[l] <= target && target < nums[m] {
				r = m - 1
			} else {
				l = m + 1
			}
		}
	}

	// 1, 0, 1, 1, 1
	//    l  m  r
	//   l r
	return -1
}
