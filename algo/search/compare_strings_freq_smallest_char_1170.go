package main

import (
	"fmt"
	"sort"
)

//https://leetcode.com/problems/compare-strings-by-frequency-of-the-smallest-character/

func numSmallerByFrequency(queries []string, words []string) []int {

	freqArr := make([]int, len(words))
	for i, w := range words {
		freqArr[i] = minCharCnt(w)
	}

	sort.Ints(freqArr)
	res := make([]int, len(queries))
	for i, q := range queries {
		cnt := minCharCnt(q)
		res[i] = search(freqArr, cnt)
	}

	return res
}

func search(arr []int, t int) int {
	l, r := 0, len(arr)-1 //

	for l <= r {
		m := l + (r-l)/2
		if arr[m] <= t {
			l = m + 1
		} else {
			r = m - 1
		}
	}
	return len(arr) - l //not found will return 0
}

func testSearch() {
	arr := []int{1, 2, 3, 4}
	t := 1
	fmt.Println(search(arr, t))
}

func main_() {
	str := []string{"a", "aa", "aaa", "aaaa"}
	q := []string{"bbb", "cc"}
	fmt.Println(numSmallerByFrequency(q, str))

	testSearch()
}

//https://leetcode.com/problems/compare-strings-by-frequency-of-the-smallest-character/discuss/366529/Go-uses-binary-search-library
// chrCnt := make([]int, 26)
func minCharCnt(str string) int {
	cnt := 0
	min := int('z' - 'a')
	for i := 0; i < len(str); i++ {
		cur := int(str[i] - 'a')

		if cur < min {
			min = cur
			cnt = 1
		} else if cur == min {
			cnt++
		}
	}
	return cnt
}
