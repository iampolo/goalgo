package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

/**
https://leetcode.com/problems/find-k-closest-elements/
Given a sorted integer array arr, two integers k and x, return the k closest integers to x in the array.
The result should also be sorted in ascending order.

An integer a is closer to x than an integer b if:

|a - x| < |b - x|, or
|a - x| == |b - x| and a < b


Example 1:

Input: arr = [1,2,3,4,5], k = 4, x = 3
Output: [1,2,3,4]
Example 2:

Input: arr = [1,2,3,4,5], k = 4, x = -1
Output: [1,2,3,4]


Constraints:

- 1 <= k <= arr.length
- 1 <= arr.length <= 10^4
- arr is sorted in ascending order.
- -10^4 <= arr[i], x <= 10^4

*/
//https://leetcode.com/problems/find-k-closest-elements/discuss/202785/Very-simple-Java-O(n)-solution-using-two-pointers

//O(n) - no binary search
//1.1
func findClosestElements(arr []int, k int, x int) []int {
	if len(arr) == 0 {
		return nil
	}

	left, right := 0, len(arr)-1
	for right-left >= k {
		if util.Abs(arr[left]-x) > util.Abs(arr[right]-x) {
			left++
		} else {
			right--
		}
	}
	return arr[left : right+1]
}

//O(n) - no binary search
//1.2
func findClosestElements_v2(arr []int, k int, x int) []int {
	if len(arr) == 0 {
		return nil
	}

	left, right := 0, len(arr)-1
	for right-left >= k {
		if x-arr[left] <= arr[right]-x { //<= give more change for right to shrink left
			right--
		} else {
			left++
		}
	}
	return arr[left : right+1]
}

/**
2.0 - advanced
find the left bound of the k-closest array
O(logn)
*/
func findClosestElements_v3(arr []int, k int, x int) []int {
	if len(arr) == 0 {
		return nil
	}

	left, right := 0, len(arr)-k
	for left < right {
		mid := left + (right-left)/2
		if x-arr[mid] > arr[mid+k]-x {
			left = mid + 1
		} else {
			right = mid
		}
	}
	return arr[left : left+k]
}

//??
func findClosestElements_v4(arr []int, k int, x int) []int {
	i := sort.Search(len(arr)-k, func(i int) bool {
		return x-arr[i] <= arr[i+k]-x
	})
	return arr[i : i+k]
}

/*****************************************************************************************************/
func findClosestElements_bs(arr []int, k int, x int) []int {

	if len(arr) == 0 {
		return []int{}
	}

	//define the boundary
	left := largestSmEq(arr, x)
	right := left + 1

	lb, rb := left, right
	for ; x > 0; x-- {
		if right >= len(arr) ||
			left >= 0 && util.Abs(arr[left]-x) <= util.Abs(arr[right]-x) {
			lb = left
			left--
		} else {
			rb = right
			right++
		}
	}

	fmt.Println(lb, rb)
	if rb == 1 || rb == right {
		lb = 99999999
	}

	return nil
}

func largestSmEq(arr []int, t int) int {
	l, r := 0, len(arr)-1

	for l < r-1 {
		m := l + (r-l)/2

		if arr[m] <= t {
			l = m
		} else {
			r = m - 1
		}
	}

	if arr[r] <= t {
		return r
	} else if arr[l] <= t {
		return l
	}
	return -1
}

func largestSmallerThan(nums []int, target int) int {
	left, right := 0, len(nums)-1

	for left < right-1 {
		m := left + (right-left)/2
		if nums[m] >= target {
			right = m - 1
		} else {
			left = m
		}
	}
	if nums[right] < target {
		return right
	} else if nums[left] < target {
		return left
	}
	return -1
}

/*****************************************************************************************************/

// https://yourbasic.org/golang/copy-explained/
func TestCopyOnly() {
	nums := []int{1, 2, 3, 4, 5, 6}

	var t = make([]int, 10)
	copy(t, nums)

	ta := []int{6, 5, 4, 3, 2, 1}
	copy(ta[2:], nums)

	fmt.Println("test: ", t)
	fmt.Println("ta: ", ta)
}
