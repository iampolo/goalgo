package main

import (
	"gitlab.com/iampolo/goalgo/algo/util"
)

/**
https://leetcode.com/problems/find-peak-element/submissions/

*/
func findPeakElement_bs(nums []int) int {
	//go to left
	l, r := 0, len(nums)-1
	for l < r {
		m := l + (r-l+1)/2 //+1 has meaning, point to 2nd mid, if there are two middles
		if nums[m-1] > nums[m] {
			r = m - 1
		} else {
			l = m
		}
	}
	return l
}

func findPeakElement_v3(nums []int) int {
	//go to right
	l, r := 0, len(nums)-1
	for l < r {
		m := l + (r-l)/2
		if nums[m] < nums[m+1] {
			l = m + 1
		} else {
			r = m
		}
	}
	return l
}

func findPeakElement(nums []int) int {
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] > nums[i+1] {
			return i
		}
	}
	return len(nums) - 1
}

//
func findPeakValleyElement_facebook(nums []int) []int {
	//var ans []int
	ans := []int{}
	ans = search_pv(0, len(nums)-1, nums, ans)
	return ans
}

//https://stackoverflow.com/questions/20195296/golang-append-an-item-to-a-slice/20197469#
func search_pv(l, r int, nums []int, ans []int) []int {
	if l >= r {
		return ans
	}
	m := l + (r-l)/2
	if m-l != util.Abs(nums[m]-nums[l]) {
		ans = search_pv(l, m, nums, ans) //go left portion
	}

	if nums[m-1] == nums[m+1] {
		ans = append(ans, nums[m]) //it is either a peak or a valley
	}

	if r-m != util.Abs(nums[r]-nums[m]) {
		ans = search_pv(m, r, nums, ans)
	}
	return ans
}

//
