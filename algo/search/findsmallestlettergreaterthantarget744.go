package main

//binary search

func nextGreatestLetter(letters []byte, target byte) byte {
	left, right := 0, len(letters)-1
	for left <= right {
		mid := left + (right-left)/2

		if letters[mid] <= target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}

	return letters[left%len(letters)]
}

/*****************************************************************************/
func nextGreatestLetter_v2(letters []byte, target byte) byte {
	idx := binarySearch(letters, target+1)
	if (idx >= len(letters)) {
		return letters[0]
	}
	return letters[idx]
}

func binarySearch(letters []byte, target byte) int {
	lo, hi := 0, len(letters)

	for lo < hi {
		mid := lo + (hi-lo)/2
		if letters[mid] < target {
			lo = mid + 1
		} else {
			hi = mid
		}
	}
	return lo
}

//bug
func smallestGreaterThan(nums []int, target int) int {
	left, right := 0, len(nums)-1
	for left < right {
		mid := left + (right-left)/2

		if nums[mid] < target {
			left = mid + 1
		} else {
			right = mid
		}
	}
	if left < len(nums) {
		return nums[left]
	} else {
		return -1
	}
}
func smallestGreaterThan_better(nums []int, target int) int {
	left, right := 0, len(nums)-1
	for left < right {
		mid := left + (right-left)/2

		if nums[mid] <= target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	if left < len(nums) {
		return nums[left]
	} else {
		return -1
	}
}
