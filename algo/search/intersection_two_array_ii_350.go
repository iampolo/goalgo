package main

import "sort"

// https://leetcode.com/problems/intersection-of-two-arrays-ii/submissions/
// Given two integer arrays nums1 and nums2, return an array of their intersection. Each element in the result must
// appear as many times as it shows in both arrays and you may return the result in any order.
//
//
//
//Example 1:
//
//Input: nums1 = [1,2,2,1], nums2 = [2,2]
//Output: [2,2]
//
//Example 2:
//
//Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
//Output: [4,9]
//Explanation: [9,4] is also accepted.
//
//
//Constraints:
//
//1 <= nums1.length, nums2.length <= 1000
//0 <= nums1[i], nums2[i] <= 1000
//
//
//Follow up:
//
//What if the given array is already sorted? How would you optimize your algorithm?
//What if nums1's size is small compared to nums2's size? Which algorithm is better?
//What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
//

func intersect_v1(nums1 []int, nums2 []int) []int {
	var res []int
	cnt2 := make(map[int]int)
	for _, n := range nums2 {
		cnt2[n]++
	}
	for _, n := range nums1 {
		if cnt, ok := cnt2[n]; ok && cnt >0  {
			res = append(res, n)
			cnt2[n]--
		}
	}
	return res
}

func intersect(nums1 []int, nums2 []int) []int {
	var res []int
	sort.Ints(nums1)
	sort.Ints(nums2)
	for i, j := 0, 0; i < len(nums1) && j < len(nums2); {
		if nums1[i] < nums2[j] {
			i++
		}  else if nums1[i] > nums2[j] {
			j++
		} else {
			res = append(res, nums1[i])
			i++
			j++
		}
	}
	return res
}
