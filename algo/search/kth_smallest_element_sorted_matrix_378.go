package main

/**
https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/
*/
func kthSmallest(matrix [][]int, k int) int {

	lo, hi := matrix[0][0], matrix[len(matrix)-1][len(matrix)-1]

	for lo < hi {
		mid := lo + (hi-lo)/2
		cnt := cntElment(matrix, mid)
		if cnt < k {
			lo = mid + 1
		} else {
			hi = mid
		}
	}

	return lo
}

func cntElment(matrix [][]int, target int) int {
	r, c, cnt := len(matrix)-1, 0, 0 //start from left-bottom

	for r >= 0 && c < len(matrix) {
		if matrix[r][c] <= target {
			c++
			cnt += r + 1
		} else {
			r--
		}
	}
	return cnt
}
