package main

import "math"

//
/**
 * // This is the BinaryMatrix's API interface.
 * // You should not implement it, or speculate about its implementation
 * type BinaryMatrix struct {
 *     Get func(int, int) int
 *     Dimensions func() []int
 * }
 */

type BinaryMatrix struct {
	Get        func(int, int) int
	Dimensions func() []int
}

func leftMostColumnWithOne(bm BinaryMatrix) int {
	//[r,c]
	dim := bm.Dimensions()

	search := func(row int, cols int) int {
		l, r := 0, cols-1

		for l <= r {
			m := l + (r-l)/2
			if bm.Get(row, m) < 1 {
				l = m + 1
			} else {
				r = m - 1
			}
		}

		if l < cols && bm.Get(row, l) == 1 {
			return l
		}
		return -1
	}

	leftMost := math.MaxInt32
	for r := 0; r < dim[0]; r++ {
		idx := search(r, dim[1])
		if idx >= 0 && idx < leftMost {
			leftMost = idx
		}
	}
	if leftMost == math.MaxInt32 {
		return -1
	}
	return leftMost
}
