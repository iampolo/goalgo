package main

// return the biggest index that is smaller than the target
func search_300(dp []int, l, r int, target int) int {
	for l <= r {
		m := l + (r-l)/2
		if dp[m] >= target {
			r = m - 1
		} else {
			l = m + 1
		}
	}
	return r
}

func lengthOfLIS(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	dp := make([]int, len(nums)+1)

	var curLen int
	dp[1], curLen = nums[0], 1

	for i := 1; i < len(nums); i++ {
		//where to insert nums[i]
		idx := search_300(dp, 1, curLen, nums[i])

		//if idx is the same as current len, nums[i] is larger than the current last element in the subsequecne
		if idx == curLen {
			//longer subsequence
			curLen++
			dp[curLen] = nums[i]
		} else {
			// idx is the biggest and smaller than dp[curLen-1]
			// it can replace dp[curLen]
			dp[idx+1] = nums[i]
		}
	}
	return curLen
}
