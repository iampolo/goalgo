package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/*
https: //leetcode.com/problems/longest-mountain-in-array/discuss/568092/Go-Two-Pointers
*/

/*
	dont' support
	[7, 4, 3, 2, 1]

	[2,1,4,7,3,2,5]
*/
func longestMountain(arr []int) int {

	n := len(arr)
	var max int
	idx := 0
	for idx < len(arr) {
		// set the the starting point of a mountain
		r := idx

		if r+1 < n && arr[r] < arr[r+1] {
			//go up
			for r+1 < n && arr[r] < arr[r+1] {
				r++
			}
			//if there is a down hill
			if r+1 < n && arr[r] > arr[r+1] {
				for r+1 < n && arr[r] > arr[r+1] {
					r++
				}
				max = Max(max, r-idx+1)
			} //if
		} //

		//r might not've moved at all
		idx = Max(idx+1, r)
	}

	return max
}

/*
https://leetcode.com/problems/longest-mountain-in-array/solution/
*/
func longestMountain_v2(arr []int) int {

	n := len(arr)
	var max int
	idx := 0
	for idx < len(arr) {
		// set the the starting point of a mountain
		base := idx

		//go up
		for idx+1 < n && arr[idx] < arr[idx+1] {
			idx++
		}

		if base == idx { //no uphill
			idx++
			continue
		}
		peak := idx

		//if there is a downhill
		for idx+1 < n && arr[idx] > arr[idx+1] {
			idx++
		}
		if peak == idx {
			idx++ //no downhill
			continue
		}

		max = Max(max, idx-base+1)
	}

	return max
}

/*
  https://leetcode.com/problems/find-in-mountain-array/
  // This is the MountainArray's API interface.
  // You should not implement it, or speculate about its implementation
  type MountainArray struct {
  }

  	func (this *MountainArray) get(index int) int {}
 	func (this *MountainArray) length() int {}
*/

func findInMountainArray_1095(target int, mountainArr *MountainArray) int {
	left, right, peak := 0, mountainArr.length()-1, -1

	for left < right {
		m := left + (right-left)/2
		if mountainArr.get(m) < mountainArr.get(m+1) {
			left = m + 1
			peak = left
		} else {
			right = m
		}
	}
	//search the left half of the peak
	left = 0
	right = peak
	for left <= right {
		m := left + (right-left)/2
		if mountainArr.get(m) < target {
			left = m + 1
		} else if mountainArr.get(m) > target {
			right = m - 1
		} else {
			return m
		}
	}
	//search the right half of the peak
	left = peak
	right = mountainArr.length() - 1
	for left <= right {
		m := left + (right-left)/2
		if mountainArr.get(m) > target {
			left = m + 1
		} else if mountainArr.get(m) < target {
			right = m - 1
		} else {
			return m
		}
	}
	return -1
}

type MountainArray struct {
}

func (this *MountainArray) get(index int) int { return 0 }
func (this *MountainArray) length() int       { return 0 }
