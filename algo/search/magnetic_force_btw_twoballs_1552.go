package main

import "sort"

// https://leetcode.com/problems/magnetic-force-between-two-balls/
//In universe Earth C-137, Rick discovered a special form of magnetic force between two balls if they are put in his
//new invented basket. Rick has n empty baskets, the i th basket is at position[i] , Morty has m balls and
//needs to distribute the balls into the baskets such that the minimum magnetic force between any two balls
//is maximum.
//Rick stated that magnetic force between two different balls at positions x and y is |x - y| .
//Given the integer array position and the integer m . Return the required force.

// WoodCut183
// 410. Split Array Largest Sum
//774. Minimize Max Distance to Gas Station
//875. Koko Eating Bananas
//1011. Capacity To Ship Packages Within D Days
//FirstBadVersion278
//1231. Divide Chocolate
//1482. Minimum Number of Days to Make m Bouquets
//1300. Sum of Mutated Array Closest to Target (Medium)
//1044. Longest Duplicate Substring (Hard)
//668. Kth Smallest Number in Multiplication Table (Hard)
//719. Find K-th Smallest Pair Distance (Hard)
func maxDistance(position []int, m int) int {

	//maximize the minimum distance between balls
	//try the minimum distance needs to place all the balls in those baskets
	//  -> if the trial is too big, then try a little smaller distance so that all the balls can be placed
	//	-> if we do find a minimum distance, we then try a little larger distance do maximize the distance!
	// -> binary search

	//find how many balls can be places in in the basket with the input force(distance)
	cnt := func(dist int) int {
		last := position[0]
		balls := 1 //1st ball is always at pos[0]
		for i := 1; i < len(position); i++ {
			//ensure the distance(force) between balls
			if dist <= position[i]-last {
				balls++
				last = position[i] //set for next position where the next round will count from
			}
		}
		return balls
	}

	sort.Ints(position)

	l, r := 0, position[len(position)-1]-position[0]
	//As a rule of thumb, use m = l + (r-l)//2 with l = m + 1 and r = m,
	//and use m = r - (r-l)//2 with l = m and r = m - 1. This can prevent m from
	//stucking at r (or l) after the updating step.
	for l < r {
		dist := r - (r-l)/2
		if cnt(dist) >= m {
			l = dist
		} else {
			r = dist - 1
		}
	}
	return l
}
