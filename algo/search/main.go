package main

import (
	"fmt"
	v2 "gitlab.com/iampolo/goalgo/algo/search/v2"
)

func main() {
	test_302()
}

func test_1891() {
	r := []int{5,7,9}
	k := 4
	fmt.Println(maxLength_v2_bug(r, k))
}

func test_1870() {
	dist := []int{1, 3, 2}
	hour := float64(2.7)
	fmt.Println(minSpeedOnTime(dist, hour))
}

func test_302() {
	image := [][]byte{
		{'0', '0', '1', '0'},
		{'0', '1', '1', '0'},
		{'0', '1', '0', '0'}}
	x, y := 0, 2
	fmt.Println(minArea_search(image, x, y))
	fmt.Println(minArea_bf(image, x, y))
	fmt.Println(minArea_dfs(image, x, y))
}

func test_1552() {
	pos := []int{1, 2, 3, 4, 7}
	m := 3

	//pos = []int{5, 4, 3, 2, 1, 1000000000}
	//m = 2

	fmt.Println(maxDistance(pos, m))
	fmt.Println(v2.MaxDistance(pos, m))
}

func test_33() {
	nums := []int{1, 3}
	t := 3
	fmt.Println(search_33(nums, t))
}

func test_633() {
	n := 4
	n = 1
	fmt.Println(judgeSquareSum_v2(n))
}

func testPeakValley() {
	nums := []int{1, 2, 3, 4, 3, 2, 3, 2, 1, 2}

	fmt.Println(findPeakValleyElement_facebook(nums))

}

func test() {
	nums := []int{1, 2, 3, 4, 5}
	k, x := 3, 2

	fmt.Println(findClosestElements(nums, k, x))

	fmt.Println(largestSmallerThan(nums, 5))

	nums = []int{0, 1, 2, 3, 14, 15, 40}

	k, x = 4, 3
	fmt.Println(findClosestElements(nums, k, x))
}

func test300() {
	nums := []int{4, 10, 4, 3, 8, 9}
	fmt.Println(lengthOfLIS(nums))
}

func test378() {
	matrix := [][]int{{1, 3, 5},
		{6, 7, 12},
		{11, 14, 14}}
	k := 6
	fmt.Println(kthSmallest(matrix, k))

}
