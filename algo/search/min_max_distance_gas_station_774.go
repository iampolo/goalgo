package main

import "math"

// https://leetcode.com/problems/minimize-max-distance-to-gas-station/
//
// You are given an integer array stations that represents the positions of the gas stations on the x-axis. You are
// also given an integer k.
//
// You should add k new gas stations. You can add the stations anywhere on the x-axis, and not necessarily on an
// integer position.
//
// Let penalty() be the maximum distance between adjacent gas stations after adding the k new stations.
//
// Return the smallest possible value of penalty(). Answers within 10^-6 of the actual answer will be accepted.
//
// Example 1:
//
// Input: stations = [1,2,3,4,5,6,7,8,9,10], k = 9
// Output: 0.50000
//
// Example 2:
//
// Input: stations = [23,24,36,39,46,56,57,65,84,98], k = 1
// Output: 14.00000
//
//
// Constraints:
//
// 10 <= stations.length <= 2000
// 0 <= stations[i] <= 10^8
// stations is sorted in a strictly increasing order.
// 1 <= k <= 10^6
//
// min_max_pair_sum_in_array_1877.go
//
func minmaxGasDist(s []int, k int) float64 {

	//the distance within 10^-6 is acceptable, loop till this condition is met
	lo, hi := float64(0), float64(s[len(s)-1]-s[0])

	for (hi - lo) >= 1e-6 {
		//if select mi as the distance, how many additional stations can be placed in between these existing stations
		mi := lo + (hi-lo)/2
		var cnt int
		for i := 0; i < len(s)-1; i++ {
			//calc the stations in between, so -1 : math.  like 1-4 ; (4-1)-1 => 2 only
			cnt += int(math.Ceil((float64(s[i+1])-float64(s[i]))/mi)) - 1
		}
		if cnt > k { //can do more station than we need
			lo = mi //more space
		} else {
			hi = mi //less space, so that more stations can be placed.
		}
	}
	return lo
}
