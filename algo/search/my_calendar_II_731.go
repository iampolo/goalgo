package main

import (
	"fmt"
	"github.com/emirpasic/gods/maps/treemap"
)

type MyCalendarTwo struct {
}

func IIConstructor() MyCalendarTwo {
	return MyCalendarTwo{}
}

func (mc *MyCalendarTwo) Book(start int, end int) bool {
	m := treemap.NewWithIntComparator() // empty (keys are of type int)
	m.Put(1, "x")                       // 1->x
	m.Put(2, "b")                       // 1->x, 2->b (in order)
	m.Put(1, "a")                       // 1->a, 2->b (in order)
	_, _ = m.Get(2)                     // b, true
	_, _ = m.Get(3)                     // nil, false
	fmt.Println(m.Values())                   // []interface {}{"a", "b"} (in order)


	_ = m.Keys()                        // []interface {}{1, 2} (in order)
	m.Remove(1)                         // 2->b
	m.Clear()                           // empty
	m.Empty()                           // true
	m.Size()                            // 0


	return false
}

func main_x() {

	ca := IIConstructor()

	ca.Book(10, 60)

}
