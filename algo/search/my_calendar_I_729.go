package main

type MyCalendar struct {
	root *treeNode
}

type treeNode struct {
	start, end  int
	left, right *treeNode
}

func newConstructor() MyCalendar {
	return MyCalendar{
		root: nil,
	}
}

func (mc *MyCalendar) Book(start int, end int) bool {
	if start > end {
		return false
	}

	return mc.insert(&mc.root, start, end)
}

func (mc *MyCalendar) insert(node **treeNode, start, end int) bool {
	if *node == nil {
		// inserted to a leaf node
		*node = &treeNode{
			start: start,
			end:   end,
		}
		return true
	}

	if (*node).start >= end { // end if exclusive -> use >= here
		//outside the current range, it is good, so go to deeper level to see if insert is possible
		return mc.insert(&(*node).left, start, end)
	} else if (*node).end <= start {
		return mc.insert(&(*node).right, start, end)
	}
	return false
}

/**
 * Your MyCalendar object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Book(start,end);
 */
