package main

import (
	"math/rand"
)

type Solution struct {
	psum []int
}

/*
https://leetcode.com/problems/random-pick-with-weight/discuss/154432/Very-easy-solution-based-on-uniform-sampling-with-explanation
say we have the numbers 1, 5, 2 easiest solution is to construct the following array
arr[] = {0,1,1,1,1,1,2,2}
then generate a random number between 0 and 7 and return the arr[rnd]. This is solution is
not really good though due to the space requirements it has (imagine a number beeing 2billion).

The solution here is similar but instead we construct the following array (accumulated sum)
{1, 6, 8} generate a number between 1-8 and say all numbers generated up to 1 is index 0.
all numbers generated greater than 1 and up to 6 are index 1 and all numbers greater than 6 and
up to 8 are index 2. After we generate a random number to find which index to return we use binary search.
*/

func Constructor(w []int) Solution {
	psum := make([]int, len(w))
	psum[0] = w[0]
	for i := 1; i < len(w); i++ {
		psum[i] = psum[i-1] + w[i]
	}
	return Solution{psum: psum}
}

/*
linear search
 */
func (this *Solution) PickIndex() int {
	/*
		rand.Seed(time.Now().UnixNano())
		fmt.Println(size , this.psum[size-1], rand.Intn(21))
	*/

	/*
		1	2 	3	4	3 		#nums
		1	3	6	10	13   	#prefix sum
		idx = 6.61146
			idx < psum[3] => return 3
	 */
	size := len(this.psum)
	target := rand.Intn(this.psum[size-1])

	var idx int
	for ; idx < len(this.psum); idx++ {
		if target < this.psum[idx] {
			return idx
		}
	}
	//should not happen
	return idx - 1
}

func (this *Solution) PickIndex_binarysearch() int {
	/*
		rand.Seed(time.Now().UnixNano())
		fmt.Println(size , this.psum[size-1], rand.Intn(21))
	*/

	size := len(this.psum)
	// use psum(prefix) is to ensure the probability of selecting a nums
	target := rand.Intn(this.psum[size-1])

	lo, hi := 0, size-1
	for lo < hi {
		mi := lo + (hi-lo)/2
		// we look for range that target is in
		// we should find psum[mi] > target
		if this.psum[mi] <= target {
			lo = mi + 1
		} else {
			hi = mi
		}
	}
	// return hi, as we set hi = mi
	return hi
}

func test528() {

	Domain()
}

/**
 * Your Solution object will be instantiated and called as such:
 * obj := Constructor(w);
 * param_1 := obj.PickIndex();
 */
