package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test528(t *testing.T) {
	w := []int{1, 3, 4, 4}
	wc := Constructor(w)
	fmt.Println("res:", wc.PickIndex())

}

func Test744(t *testing.T) {
	input := []byte{'a','z'}

	fmt.Println(nextGreatestLetter(input, 'a'))
	fmt.Println(nextGreatestLetter_v2(input, 'a'))
}

func Test744_2(t *testing.T) {

	w := []int{4, 5, 7, 8, 10}
	target := 10

	fmt.Println(smallestGreaterThan_better(w, target))

}

func Test658(t *testing.T) {
	nums := []int{0, 0, 1, 2, 3, 3, 4, 7, 7, 8}
	k, x := 3, 5

	ans := findClosestElements_v2(nums, k, x)
	assert.Equal(t, []int{3, 3, 4}, ans)

	nums = []int{1, 2, 3, 4, 5}
	k, x = 4, -1

	ans = findClosestElements_v3(nums, k, x)
	assert.Equal(t, []int{1,2,3,4}, ans)
}
