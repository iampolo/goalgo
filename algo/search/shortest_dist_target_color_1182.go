package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/shortest-distance-to-target-color/
/**
You are given an array colors, in which there are three colors: 1, 2 and 3.

You are also given some queries. Each query consists of two integers i and c, return the shortest distance
between the given index i and the target color c. If there is no solution return -1.



Example 1:

Input: colors = [1,1,2,1,3,2,2,3,3], queries = [[1,3],[2,2],[6,1]]
Output: [3,0,3]
Explanation:
The nearest 3 from index 1 is at index 4 (3 steps away).
The nearest 2 from index 2 is at index 2 itself (0 steps away).
The nearest 1 from index 6 is at index 3 (3 steps away).

Example 2:

Input: colors = [1,2], queries = [[0,3]]
Output: [-1]
Explanation: There is no 3 in the array.


Constraints:

1 <= colors.length <= 5*10^4
1 <= colors[i] <= 3
1 <= queries.length <= 5*10^4
queries[i].length == 2
0 <= queries[i][0] < colors.length
1 <= queries[i][1] <= 3

@see ShortestDistTargetColor1182
*/
func shortestDistanceColor(colors []int, queries [][]int) []int {

	mapping := make(map[int][]int)

	for i, v := range colors {
		mapping[v] = append(mapping[v], i)
	}

	res := make([]int, 0)
	for _, q := range queries {
		idx := search_idx(mapping[q[1]], q[0])
		res = append(res, idx)
	}

	return res
}

func search_idx(data []int, idx int) int {
	if len(data) == 0 {
		return -1
	}

	l := 0
	r := len(data) - 1

	//leave two elements at the end to compare
	for l+1 < r {
		mid := l + (r-l)/2
		if data[mid] == idx {
			return 0 //the same color and at the same idx
		}

		if data[mid] < idx {
			l = mid     //moving to right
		} else {
			r = mid		//moving to left
		}
	}
	if Abs(data[l] - idx) <= Abs(data[r] - idx) {
		return Abs(data[l] - idx)
	} else {
		return Abs(data[r] - idx)
	}
}

func main_1182() {
	col := []int{1, 1, 2, 1, 3, 2, 2, 3, 3}
	qa := [][]int{{1, 3}}
	fmt.Println(shortestDistanceColor(col, qa))
}
