package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/smallest-rectangle-enclosing-black-pixels/

func minArea_dfs(image [][]byte, x int, y int) int {
	l, r, t, b := y, y, x, x

	rows, cols := len(image), len(image[0])
	var dfs func(x, y int)
	dfs = func(x, y int) {
		if x < 0 || x >= rows || y < 0 || y >= cols || image[x][y] == '0' {
			return
		}

		image[x][y] = '0' //mark visited
		l = Min(l, y)
		r = Max(r, y)
		t = Min(t, x)
		b = Max(b, x)

		for i := 0; i < 4; i++ {
			dfs(x+DIRS[i], y+DIRS[i+1])
		}
	}
	dfs(x, y)
	return (r - l + 1) * (b - t + 1)
}

var DIRS = [...]int{0, 1, 0, -1, 0}

func minArea_bf(image [][]byte, x int, y int) int {

	//x is the vertical - row
	//y is horizontal - col
	l, r, t, b := y, y, x, x

	for i := 0; i < len(image); i++ {
		for j := 0; j < len(image[0]); j++ {
			if image[i][j] == '1' {
				l = Min(j, l)
				r = Max(j+1, r)
				t = Min(t, i)
				b = Max(b, i+1)
			}
		}
	}

	return (r - l) * (b - t)
}

// ***************************************************************************************
// FirstLastPosElementSortedArray34
func minArea_search(image [][]byte, x int, y int) int {
	rows, cols := len(image), len(image[0])
	// search left and right, and boundaries by using y-axis
	left := searchCol(0, y, 0, rows, image, true)
	right := searchCol(y+1, cols, 0, rows, image, false) //y+1:find 1 cell after the 1st '1'
	// search upper and lower boundaries by using x-axis
	top := searchRow(0, x, left, right, image, true)
	bottom := searchRow(x+1, rows, left, right, image, false)

	return (right - left) * (bottom - top)
}

//rows => O(log rows )
//cols : do a scan
// total => o(cols log rows)
func searchRow(u, b int, l, r int, img [][]byte, isTop bool) int {
	for u < b {
		m := u + (b-u)/2
		left := l
		for left < r && img[m][left] == '0' {
			left++
		}

		if left < r == isTop {
			b = m //go up
		} else {
			u = m + 1 //go down
		}
	}
	return u
}

// search from left to right, also scan down to bottom to find the vertical border
func searchCol(l, r int, u, b int, img [][]byte, isLeft bool) int {
	for l < r {
		m := l + (r-l)/2
		top := u
		for top < b && img[top][m] == '0' {
			top++ //search down and stop at 1st '1'
		}
		//if isLeft==false, and we found 1st '1', we should go right (l=m+1)
		if top < b == isLeft { //shrink left or shrink right depends on the direction we are searching
			r = m //e.g. if finding left border, it is most left
		} else {
			l = m + 1 //1st '1' is on the right
		}
	}
	return l //the 1st '1'
}
