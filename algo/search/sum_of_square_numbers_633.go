package main

import "math"

// https://leetcode.com/problems/sum-of-square-numbers/
//
// Given a non-negative integer c, decide whether there're two integers a and b such that a^2 + b^2 = c.
//
//
//
//Example 1:
//
//Input: c = 5
//Output: true
//Explanation: 1 * 1 + 2 * 2 = 5
//
//Example 2:
//
//Input: c = 3
//Output: false
//
//Example 3:
//
//Input: c = 4
//Output: true
//
//Example 4:
//
//Input: c = 2
//Output: true
//
//Example 5:
//
//Input: c = 1
//Output: true
//
//
//Constraints:
//
//0 <= c <= 2^31 - 1

func judgeSquareSum_v2(c int) bool {
	for a := 0; a*a <= c; a++ {
		b := math.Sqrt(float64(c-a*a))
		if b - math.RoundToEven(b) == 0.0 {
			return true
		}
	}
	return false
}

// corner cases 0 and 1
func judgeSquareSum(c int) bool {
	l, r := 0, int(math.Sqrt(float64(c)))
	for l <= r {
		sum := l*l + r*r
		if sum == c {
			return true
		} else if sum < c {
			l++
		} else {
			r--
		}
	}
	return false
}