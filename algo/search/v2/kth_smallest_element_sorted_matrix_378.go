package v2

import "fmt"

/**
https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/
https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/discuss/85209/Golang-solution-using-priority-queue
*/
func kthSmallest(matrix [][]int, k int) int {
	r, c := len(matrix), len(matrix[0])

	fmt.Println(r, c)
	return 0
}

type Elem struct {
	m   int
	n   int
	val int
}

// https://golang.org/pkg/container/heap/
type Elems []Elem

func (pq Elems) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq Elems) Len() int {
	return len(pq)
}

func (pq Elems) Less(i, j int) bool {
	return pq[i].val < pq[j].val
}

func (pq *Elems) Pop() interface{} {
	vpq := *pq
	x := vpq[len(vpq)-1]
	*pq = vpq[:len(vpq)-1]
	return x
}

func (pq *Elems) Push(num interface{}) {
	numElm, _ := num.(Elem)
	*pq = append(*pq, numElm)
}
