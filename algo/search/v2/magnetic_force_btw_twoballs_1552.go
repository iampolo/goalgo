package v2

import (
	"sort"
)

// MaxDistance https://leetcode.com/problems/magnetic-force-between-two-balls/
//In universe Earth C-137, Rick discovered a special form of magnetic force between two balls if they are put in his
//new invented basket. Rick has n empty baskets, the i th basket is at position[i] , Morty has m balls and
//needs to distribute the balls into the baskets such that the minimum magnetic force between any two balls is maximum.
//Rick stated that magnetic force between two different balls at positions x and y is |x - y| .
//Given the integer array position and the integer m . Return the required force.
//
func MaxDistance(position []int, m int) int {

	isOK := func(dist int) bool {
		last := position[0]
		balls := 1 //1st ball is always at pos[0]
		for i := 1; i < len(position); i++ {
			//ensure the distance(force) between balls
			if dist <= position[i]-last {
				balls++
				last = position[i] //set for next position where the next round will count from
			}
		}
		return balls >= m
	}

	sort.Ints(position)

	optimal := 0
	l, r := 0, position[len(position)-1]-position[0]
	//classic binary search with the help of optimal to store what we need
	for l <= r {
		dist := l + (r-l)/2
		if isOK(dist) {
			optimal = dist //save it cos dist will be skipped in the next round
			l = dist + 1 //if ok, set this as the minimum window for next round: a larger force can be generated
		} else {
			r = dist - 1
		}
	}
	return optimal
}
