package v2

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/shortest-distance-to-target-color/
/**
You are given an array colors, in which there are three colors: 1, 2 and 3.

You are also given some queries. Each query consists of two integers i and c, return the shortest distance
between the given index i and the target color c. If there is no solution return -1.



Example 1:

Input: colors = [1,1,2,1,3,2,2,3,3], queries = [[1,3],[2,2],[6,1]]
Output: [3,0,3]
Explanation:
The nearest 3 from index 1 is at index 4 (3 steps away).
The nearest 2 from index 2 is at index 2 itself (0 steps away).
The nearest 1 from index 6 is at index 3 (3 steps away).

Example 2:

Input: colors = [1,2], queries = [[0,3]]
Output: [-1]
Explanation: There is no 3 in the array.


Constraints:

1 <= colors.length <= 5*10^4
1 <= colors[i] <= 3
1 <= queries.length <= 5*10^4
queries[i].length == 2
0 <= queries[i][0] < colors.length
1 <= queries[i][1] <= 3

@see ShortestDistTargetColor1182
*/
func shortestDistanceColor(colors []int, queries [][]int) []int {
	dp := make([][]int, len(colors))
	for i := 0; i < len(colors); i++ {
		dp[i] = make([]int, 4)
		for j := 0; j < len(dp[i]); j++ {
			dp[i][j] = math.MaxInt32
		}
	} //for

	/* idx \ color
	         	0 	1	2	3
	      	0       0   x  	x
		  	1		0   x	x
		  	2		1	0	x
			3		...
	*/
	dp[0][colors[0]] = 0
	for i := 1; i < len(colors); i++ {
		dp[i][colors[i]] = 0

		for c := 1; c < 4; c++ {
			dp[i][c] = Min(dp[i][c], dp[i-1][c]+1) //check with left
		}
	}
	for _, x := range dp {
		fmt.Println(x)
	}

	for i := len(colors) - 2; i >= 0; i-- {
		for c := 1; c < 4; c++ {
			dp[i][c] = Min(dp[i][c], dp[i+1][c]+1) //check with right
		}
	}

	res := make([]int, 0)
	for _, q := range queries {
		if dp[q[0]][q[1]] != math.MaxInt32 {
			res = append(res, dp[q[0]][q[1]])
		} else {
			//never can reach to this position
			res = append(res, -1)
		}
	}

	return res
}

func main() {
	col := []int{1, 1, 2, 1, 3, 2, 2, 3, 3}
	qa := [][]int{{1, 3}}
	fmt.Println(shortestDistanceColor(col, qa))
}
