package main

// https://leetcode.com/problems/binary-subarrays-with-sum/solution/
// Given a binary array nums and an integer goal, return the number of non-empty subarrays with a sum goal.
//
//A subarray is a contiguous part of the array.
//
//
//Example 1:
//
//Input: nums = [1,0,1,0,1], goal = 2
//Output: 4
//Explanation: The 4 subarrays are bolded and underlined below:
//[1,0,1,0,1]
//[1,0,1,0,1]
//[1,0,1,0,1]
//[1,0,1,0,1]
//
//Example 2:
//
//Input: nums = [0,0,0,0,0], goal = 0
//Output: 15
//
//
//Constraints:
//
//1 <= nums.length <= 3 * 10^4
//nums[i] is either 0 or 1.
//0 <= goal <= nums.length
//
//1358. Number of Substrings Containing All Three Characters
//1248. Count Number of Nice Subarrays
//1234. Replace the Substring for Balanced String
//1004. Max Consecutive Ones III
//930.  Binary Subarrays With Sum
// 992. Subarrays with K Different Integers
//904.  Fruit Into Baskets
//862.  Shortest Subarray with Sum at Least K
//209.  Minimum Size Subarray Sum

// SubarraySumEqualsK560
func numSubarraysWithSum_presum(nums []int, goal int) int {
	res, presum := 0, 0

	cntMap := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		presum += nums[i]
		diff := presum - goal //diff:the sum of opposite subarray
		if _, exist := cntMap[diff]; exist {
			res += cntMap[diff]
		}
		//save this presum : the sum of a subarray
		cntMap[presum] = cntMap[presum] + 1
		if presum == goal {
			res++
		}
	}

	return res
}

func numSubarraysWithSum(nums []int, goal int) int {

	var sum int
	for _, n := range nums {
		sum += n
	}

	//record the index # of each 1 in the array
	//add -1 and length of the array at the front and end of the idx array
	idx := make([]int, sum+2)
	idx[0] = -1
	ii := 1
	for i := range nums {
		if nums[i] == 1 {
			idx[ii] = i
			ii++
		}
	}
	idx[ii] = len(nums)

	var res int
	if goal == 0 {
		//how many portion has consecutive of 0s
		//apply n*(n+1)/2 -> # of subarray
		for i := 0; i < len(idx)-1; i++ {
			w := idx[i+1] - idx[i] - 1 //0s between two 1s
			res += w * (w + 1) / 2
		}
		return res
	}

	//TODO?
	// (x_i - x_i-1) * (x_i+goal - x_i+goal-1)
	for i := 1; i < len(idx)-goal; i++ {
		j := i + goal - 1
		left := idx[i] - idx[i-1]
		right := idx[j+1] - idx[j]

		res += left * right
	}

	return res
}
