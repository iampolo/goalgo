package main

import "strings"

// https://leetcode.com/problems/find-the-longest-substring-containing-vowels-in-even-counts/
func findTheLongestSubstring(s string) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	pos := make(map[int]int)
	pos[0] = -1
	state, maxLen := 0, 0

	for i, ch := range s {

		idx := strings.Index("aeiou", string(ch))
		if idx >= 0 {
			state ^= 1 << idx
		}

		if _, exist := pos[state]; exist {
			maxLen = max(maxLen, i-pos[state])
		} else {
			pos[state] = i
		}
	}
	return maxLen
}
