package main

// https://leetcode.com/problems/find-longest-awesome-substring/
//  Given a string s. An awesome substring is a non-empty substring of s such that we can make any number of swaps
//  in order to make it palindrome.
//
// Return the length of the maximum length awesome substring of s.
//
//
//
// Example 1:
//
// Input: s = "3242415"
// Output: 5
// Explanation: "24241" is the longest awesome substring, we can form the palindrome "24142" with some swaps.
//
// Example 2:
//
// Input: s = "12345678"
// Output: 1
//
// Example 3:
//
// Input: s = "213123"
// Output: 6
// Explanation: "213123" is the longest awesome substring, we can form the palindrome "231132" with some swaps.
//
// Example 4:
//
// Input: s = "00"
// Output: 2
//
//
// Constraints:
//
// 1 <= s.length <= 10^5
// s consists only of digits.

// containing_vowel_in_even_cnt_1371.go
// odd - even = odd
// even - odd = odd
func longestAwesome(s string) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	min := func(a, b int) int {
		if a > b {
			return b
		}
		return a
	}
	// 0 - 9, 10 digits, there are 2^10 bitmasks can be formed.
	states := [1024]int{}
	for i := range states {
		states[i] = len(s)
	}

	states[0] = -1
	res, mask := 0, 0

	for i := 0; i < len(s); i++ {
		mask ^= 1 << (s[i] - '0')
		res = max(res, i-states[mask]) //this can find the even chars

		for j := 0; j <= 9; j++ {
			//this helps to find the opposite states of current mask
			res = max(res, i-states[mask^(1<<j)])
		}
		states[mask] = min(states[mask], i)
	}
	return res

}
