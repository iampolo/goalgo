package main

// https://leetcode.com/problems/longest-turbulent-subarray/
// Given an integer array arr, return the length of a maximum size turbulent subarray of arr.
//
//A subarray is turbulent if the comparison sign flips between each adjacent pair of elements in the subarray.
//
//More formally, a subarray [arr[i], arr[i + 1], ..., arr[j]] of arr is said to be turbulent if and only if:
//
//For i <= k < j:
//arr[k] > arr[k + 1] when k is odd, and
//arr[k] < arr[k + 1] when k is even.
//Or, for i <= k < j:
//arr[k] > arr[k + 1] when k is even, and
//arr[k] < arr[k + 1] when k is odd.
//
//
//Example 1:
//
//Input: arr = [9,4,2,10,7,8,8,1,9]
//Output: 5
//Explanation: arr[1] > arr[2] < arr[3] > arr[4] < arr[5]
//
//Example 2:
//
//Input: arr = [4,8,12,16]
//Output: 2
//
//Example 3:
//
//Input: arr = [100]
//Output: 1
//
//
//Constraints:
//
//1 <= arr.length <= 4 * 10^4
//0 <= arr[i] <= 10^9
func maxTurbulenceSize_v3(arr []int) int {
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}
	inc, dec, res := 1, 1, 1
	for i := 1; i < len(arr); i++ {
		if arr[i-1] < arr[i] {
			inc = dec+1
			dec =1
		} else if arr[i-1] > arr[i] {
			dec = inc + 1
			inc = 1
		} else {
			dec = 1
			inc = 1
		}
		res = max(res, max(inc, dec))
	}
	return res
}

func maxTurbulenceSize_v2(arr []int) int {
	compare := func(a, b int) int {
		if a == b {
			return 0
		} else if a < b {
			return -1
		}
		return 1
	}
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	left, res := 0, 1
	for i := 1; i < len(arr); i++ {
		sign := compare(arr[i-1], arr[i])
		if sign == 0 {
			left = i //res either 1 or whatever calculated before
		} else if i == len(arr)-1 || sign*compare(arr[i], arr[i+1]) != -1 {
			//direction trick, the product of signs of two adj. elements must be -1 if they are turbulent
			//end of the array or direction changed
			res = max(res, i-left+1)
			left = i
		}
	}
	return res
}

func maxTurbulenceSize(arr []int) int {
	compare := func(a, b int) int {
		if a == b {
			return 0
		} else if a < b {
			return -1
		}
		return 1
	}
	max := func(a, b int) int {
		if a < b {
			return b
		}
		return a
	}

	pre, res, sign := 0, 0, 0
	for i, j := 0, 0; j < len(arr); j++ {
		if j == len(arr)-1 {
			//j, j-1 are already considered.
			return max(res, j-i+1)
		}
		sign = compare(arr[j], arr[j+1])
		if sign == 0 {
			res = max(res, j-i+1)
			i = j + 1 //jump the next one as there are dup.
		} else if sign != -pre { //math trick
			res = max(res, j-i+1)
			i = j
		}
		pre = sign
	}
	return res
}
