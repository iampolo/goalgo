package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/*
https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/
Given a string s and an integer k, return the length of the longest substring of
s that contains at most k distinct characters.

Example 1:

Input: s = "eceba", k = 2
Output: 3
Explanation: The substring is "ece" with length 3.
Example 2:

Input: s = "aa", k = 1
Output: 2
Explanation: The substring is "aa" with length 2.


Constraints:

1 <= s.length <= 5 * 10^4
 <= k <= 50
*/

func lengthOfLongestSubstringKDistinct(s string, k int) int {
	cnt := make(map[byte]int)

	left, max := 0, 0
	for i := range s {
		cnt[s[i]]++

		if len(cnt) > k {
			cnt[s[left]]--
			if cnt[s[left]] == 0 {
				delete(cnt, s[left])
			}
			left++
		}
		max = Max(max, i-left+1)
	}
	return max
}
