package main

import "fmt"

func main() {
	test_1248()
}

func test_1248() {
	n := []int{2, 1, 2, 1, 2, 2, 1, 2}
	k := 2
	fmt.Println(numberOfSubarrays_1248_calc(n, k))
}

func test_1542() {
	s := "3242414"
	fmt.Println(longestAwesome(s))
}

func test_730() {
	nums := []int{1, 0, 1, 0, 1}
	goal := 2

	//nums = []int{1, 0, 1, 0, 0, 1}
	//goal = 2
	//
	//nums = []int{1, 0, 1, 0, 1, 0, 0, 1}
	//goal = 2

	fmt.Println(numSubarraysWithSum_presum(nums, goal))
	fmt.Println(numSubarraysWithSum(nums, goal))
}

func test_978() {
	nums := []int{9, 4, 2, 10, 7, 8, 8, 1, 9}
	fmt.Println(maxTurbulenceSize_v3(nums))
	fmt.Println(maxTurbulenceSize_v2(nums))
	fmt.Println(maxTurbulenceSize(nums))
}

func test_482() {
	s := "5F3Z-2e-9-w"
	k := 3
	fmt.Println(licenseKeyFormatting_482(s, k))
	fmt.Println(licenseKeyFormatting_482_v2(s, k))
}

func test_917() {
	s := "ab-cd"
	fmt.Println(reverseOnlyLetters(s))
}

func test_42() {

	nums := []int{4, 2, 0, 3, 2, 5}

	fmt.Println(trap_v3_stack(nums))
}

func test_19() {
	nums := []int{1, 2, 4, 8, 16, 32, 64, 128}
	t := 82
	fmt.Println(threeSumClosest_16_bs(nums, t))
	fmt.Println(threeSumClosest_16(nums, t))
}

func test_18() {
	nums := []int{1, 0, -1, 0, -2, 2}
	target := 0
	nums = []int{-2, -1, -1, 1, 1, 2, 2}
	fmt.Println(fourSum_18(nums, target))
	fmt.Println(fourSum_18_v2(nums, target))
}
