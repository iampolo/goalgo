package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
)

/**
https://leetcode.com/problems/max-consecutive-ones-ii/solution/
*/
func findMaxConsecutiveOnes_487(nums []int, k int) int {
	l, r := 0, 0
	res, chgCnt := 0, 0

	for r < len(nums) {
		if nums[r] == 0 {
			chgCnt++
		}

		for chgCnt == 2 { //to many change
			if nums[l] == 0 {
				chgCnt--
			}
			l++
		}

		if r-l+1 > res && chgCnt <= 1 {
			res = util.Max(res, r-l+1)
		}
		r++
	}
	return res
}

/**

 */
func longestOnes(nums []int, k int) int {

	l, r := 0, 0
	res := 0

	for r < len(nums) {
		if nums[r] == 1 || (nums[r] == 0 && k > 0) {
			if nums[r] == 0 {
				k--
			}
			r++
		} else {
			res = util.Max(res, r-l) //r is pointing at 0
			for k == 0 {
				if nums[l] == 0 { //adjust left boundary
					k++
				} //
				l++
			}
		} //
	}
	res = util.Max(res, r-l) //r is pointing at 0
	return res
}

func main_1004() {
	nums := []int{1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0}
	k := 2

	fmt.Println(findMaxConsecutiveOnes_487(nums, k))
	fmt.Println(longestOnes(nums, k))
}
