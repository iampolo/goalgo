package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/maximum-score-of-a-good-subarray/
//You are given an array of integers nums (0-indexed) and an integer k.
//
//The score of a subarray (i, j) is defined as min(nums[i], nums[i+1], ..., nums[j]) * (j - i + 1). A good subarray
//is a subarray where i <= k <= j.
//
//Return the maximum possible score of a good subarray.
//
//
//
//Example 1:
//
//Input: nums = [1,4,3,7,4,5], k = 3
//Output: 15
//Explanation: The optimal subarray is (1, 5) with a score of min(4,3,7,4,5) * (5-1+1) = 3 * 5 = 15.
//
//Example 2:
//
//Input: nums = [5,5,4,5,4,1,1,1], k = 0
//Output: 20
//Explanation: The optimal subarray is (0, 4) with a score of min(5,5,4,5,4) * (4-0+1) = 4 * 5 = 20.
//
//
//Constraints:
//
// - 1 <= nums.length <= 10^5
// - 1 <= nums[i] <= 2 * 10^4
// - 0 <= k < nums.length
//
func maximumScore_v3(nums []int, k int) int {
	//lc84 - Largest Rectangle in Histogram

	var st []int //stack
	st = append(st, -1)

	res := nums[k]
	for i := 0; i <= len(nums); i++ {
		cur := 0
		if i == len(nums) {
			cur = 0 //mainly to finish the last processing
		} else {
			cur = nums[i]
		}
		for st[len(st)-1] != -1 && nums[st[len(st)-1]] >= cur {
			//pop()
			height := nums[st[len(st)-1]]
			st = st[:len(st)-1]

			width := i-st[len(st)-1]-1

			//meet the criteria
			if st[len(st)-1] < k && i > k { //check index
				res = Max(res, height * width)
			}
		}
		//new element that has the mini value
		st = append(st, i)
	}

	return res
}

func maximumScore_v2(nums []int, k int) int {

	//find and store the Running Min in an array
	runningMin := make([]int, len(nums))
	runningMin[k] = nums[k] //position is given and is the center
	for i := k + 1; i < len(nums); i++ {
		runningMin[i] = Min(runningMin[i-1], nums[i])
	}
	for i := k - 1; i >= 0; i-- {
		runningMin[i] = Min(runningMin[i+1], nums[i])
	}

	//we will have the mini. element all the way and also the window size
	lo, hi := 0, len(nums)-1
	res := nums[k]
	for lo < hi {
		res = Max(res, (hi-lo+1)*(Min(runningMin[lo], runningMin[hi])))
		if runningMin[lo] < runningMin[hi] {
			lo++
		} else {
			hi--
		}
	}
	return res
}

func maximumScore(nums []int, k int) int {
	// i <= k <= j, the subarray could be at k or expand to left or right indefinitely
	// but since we want to keep the mini. smaller, so when we expand to left or right, we choose the large side
	// 1) keep the mini. smaller, 2) expand to have longer length

	//assume k is valid, we start with element at position k
	mini := nums[k]
	res := nums[k]
	i, j := k, k
	for i > 0 || j < len(nums)-1 { //conditions: so that there is room to move
		//determine if we should go i-1 or j+1
		if i == 0 {
			j++ //go right
		} else if j == len(nums)-1 {
			i-- //can only go left
		} else if nums[i-1] < nums[j+1] {
			j++ //go to larger side
		} else {
			i--
		}
		mini = Min(mini, Min(nums[i], nums[j]))
		res = Max(res, mini*(j-i+1))
	}

	return res
}
