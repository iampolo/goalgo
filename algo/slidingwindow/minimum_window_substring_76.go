package main

import "math"

// https://leetcode.com/problems/minimum-window-substring/
// Given two strings s and t of lengths m and n respectively, return the minimum window substring of s
// such that every character in t (including duplicates) is included in the window. If there is no such substring,
// return the empty string "".
//
// The testcases will be generated such that the answer is unique.
//
// A substring is a contiguous sequence of characters within the string.
//
//
//
// Example 1:
//
//Input: s = "ADOBECODEBANC", t = "ABC"
//Output: "BANC"
//Explanation: The minimum window substring "BANC" includes 'A', 'B', and 'C' from string t.
//
//Example 2:
//
//Input: s = "a", t = "a"
//Output: "a"
//Explanation: The entire string s is the minimum window.
//
//Example 3:
//
//Input: s = "a", t = "aa"
//Output: ""
//Explanation: Both 'a's from t must be included in the window.
//Since the largest window of s only has one 'a', return empty string.
//
//
//Constraints:
//
//m == s.length
//n == t.length
//1 <= m, n <= 105
//s and t consist of uppercase and lowercase English letters.

func minWindow(s string, t string) string {
	cntMap := make(map[byte]int)

	for i := range t {
		cntMap[t[i]]++
	}

	idx := -1 //assume not found
	left := 0
	matchCnt := 0
	maxLen := math.MaxInt32

	for r := 0; r < len(s); r++ {
		ch := s[r]

		if cnt, exist := cntMap[ch]; !exist {
			continue
		} else {
			cntMap[ch] = cnt - 1
			if cnt == 1 {
				matchCnt++
			}
		}

		for len(cntMap) == matchCnt {
			if r-left+1 < maxLen { //update the better length
				maxLen = r - left + 1
				idx = left
			}
			//remove the beginning
			ch := s[left]
			left++
			if cnt, exist := cntMap[ch]; !exist {
				continue
			} else {
				cntMap[ch] = cnt + 1
				if cnt == 0 {
					matchCnt--
				}
			} //
		}
	}
	if idx == -1 {
		return ""
	}
	return s[idx : idx+maxLen]
}
