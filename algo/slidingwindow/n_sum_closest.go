package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
	"sort"
)

// O(n^2 logn)
func threeSumClosest_16_bs(nums []int, target int) int {

	sort.Ints(nums)
	diff := math.MaxInt32
	for i := 0; i < len(nums)-2 && diff != 0; i++ {
		for j := i + 1; j < len(nums)-1; j++ {
			comp := target - nums[i] - nums[j]
			idx := BinarySearch(nums, j+1, comp)

			var hi, lo int
			if idx >= 0 {
				hi = idx
			} else {
				hi = -(idx + 1)
			}
			lo = hi - 1

			if hi < len(nums) && Abs(comp-nums[hi]) < Abs(diff) {
				diff = comp - nums[hi]
			}
			if lo > j && Abs(comp-nums[lo]) < Abs(diff) {
				diff = comp - nums[lo]
			}
		} //for
	}

	return target - diff
}

//https://leetcode.com/problems/3sum-closest/
// O(n^2)
func threeSumClosest_16(nums []int, target int) int {
	if len(nums) < 3 {
		return -1 //
	}
	//find the smallest diff
	sort.Ints(nums)
	diff := math.MaxInt32
	for i := 0; i < len(nums)-2 && diff != 0; i++ {
		l, r := i+1, len(nums)-1
		for l < r {
			cur := nums[i] + nums[l] + nums[r]

			if Abs(target-cur) < Abs(diff) {
				diff = target - cur
			}
			if cur > target {
				r--
			} else {
				l++
			}
		}
	}
	return target - diff
}
