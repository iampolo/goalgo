package main

import "sort"

/**
https://leetcode.com/problems/4sum/
*/
func fourSum_18(nums []int, target int) [][]int {

	var res [][]int //not fixing size, so use append

	sort.Ints(nums)

	for i := 0; i < len(nums)-3; i++ {
		if i > 0 && nums[i-1] == nums[i] {
			continue
		}

		for j := i + 1; j < len(nums)-2; j++ {
			if j > i+1 && nums[j-1] == nums[j] {
				continue
			}

			l, r := j+1, len(nums)-1
			for l < r {
				sum := nums[i] + nums[j] + nums[l] + nums[r]
				if sum > target {
					r--
				} else if sum < target {
					l++
				} else {
					//result
					var cur []int
					cur = append(cur, nums[i])
					cur = append(cur, nums[j])
					cur = append(cur, nums[l])
					cur = append(cur, nums[r])
					res = append(res, cur)
					l++
					r--
					for l < r && nums[l] == nums[l-1] {
						l++
					}
					for l < r && nums[r] == nums[r+1] {
						r--
					} //
				} //
			} //
		}
	} //
	return res
}

/**********************************************************/
func fourSum_18_v2(nums []int, target int) [][]int {

	var res [][]int
	sort.Ints(nums)
	for i := 0; i < len(nums)-3; i++ {
		if i > 0 && nums[i-1] == nums[i] {
			continue
		}

		for j := i + 1; j < len(nums)-2; j++ {
			if j > i+1 && nums[j-1] == nums[j] {
				continue
			}

			cur := twoSum(nums, target-nums[i]-nums[j], j)
			for _, r := range cur {
				var t []int
				t = append(t, nums[i])
				t = append(t, nums[j])
				t = append(t, r[0])
				t = append(t, r[1])

				res = append(res, t)
			}
		}
	}
	return res
}

func twoSum(nums []int, target, j int) [][]int {
	l, r := j+1, len(nums)-1

	var res [][]int
	for l < r {
		sum := nums[l] + nums[r]
		if sum < target {
			l++
		} else if sum > target {
			r--
		} else {
			var cur []int
			cur = append(cur, nums[l])
			cur = append(cur, nums[r])

			res = append(res, cur)
			l++
			r--
			//de-dup
			for l < r && nums[l-1] == nums[l] {
				l++
			}
			for l< r && nums[r] == nums[r+1] {
				r--
			}
		}
	}

	return res
}
/**********************************************************/
// https://leetcode.com/problems/two-sum/
func twoSum_v2_1(nums []int, target int) []int {
	saved := make(map[int]int)

	for i := 0; i < len(nums); i++ {
		comp := target - nums[i]
		if j, ok := saved[comp]; ok {
			return []int{j, i}
		}
		saved[nums[i]] = i
	}

	return []int{}
}

