package main

import (
	"strings"
)

//https://leetcode.com/problems/reverse-only-letters/

// Given a string s, reverse the string according to the following rules:
//
//All the characters that are not English letters remain in the same position.
//All the English letters (lowercase or uppercase) should be reversed.
//Return s after reversing it.
//
//
//Example 1:
//
//Input: s = "ab-cd"
//Output: "dc-ba"

//Example 2:
//
//Input: s = "a-bC-dEf-ghIj"
//Output: "j-Ih-gfE-dCba"

//Example 3:
//
//Input: s = "Test1ng-Leet=code-Q!"
//Output: "Qedo1ct-eeLg=ntse-T!"
//
//
//Constraints:
//
//1 <= s.length <= 100
//s consists of characters with ASCII values in the range [33, 122].
//s does not contain '\"' or '\\'.
func reverseOnlyLetters(s string) string {
	isLetter := func(ch byte) bool {
		return ch >= 'a' && ch <= 'z' ||
			ch >= 'A' && ch <= 'Z'
	}

	sbyte := []byte(s)
	left, right := 0, len(s)-1
	for left < right {
		if !isLetter(sbyte[left]) {
			left++
		} else if !isLetter(sbyte[right]) {
			right--
		} else {
			sbyte[left], sbyte[right] = sbyte[right], sbyte[left]
			left++
			right--
		}
	}

	return string(sbyte)
}

// https://leetcode.com/problems/license-key-formatting/
// You are given a license key represented as a string s that consists of only alphanumeric characters and dashes.
// The string is separated into n + 1 groups by n dashes. You are also given an integer k.
//
//We want to reformat the string s such that each group contains exactly k characters, except for the first group,
//which could be shorter than k but still must contain at least one character. Furthermore, there must be a dash
//inserted between two groups, and you should convert all lowercase letters to uppercase.
//
//Return the reformatted license key.
//
//Example 1:
//
//Input: s = "5F3Z-2e-9-w", k = 4
//Output: "5F3Z-2E9W"
//Explanation: The string s has been split into two parts, each part has 4 characters.
//Note that the two extra dashes are not needed and can be removed.
//
//Example 2:
//
//Input: s = "2-5g-3-J", k = 2
//Output: "2-5G-3J"
//Explanation: The string s has been split into three parts, each part has 2 characters except the first part as it
//could be shorter as mentioned above.
//
//
//Constraints:
//
//1 <= s.length <= 10^5
//s consists of English letters, digits, and dashes '-'.
//1 <= k <= 10^4
func licenseKeyFormatting_482(s string, k int) string {

	var sb strings.Builder
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '-' {
			continue
		}
		if sb.Len()%(k+1) == k { //lenght equals to exactly k
			sb.WriteString("-")
		}
		sb.WriteString(strings.ToUpper(string(s[i])))
	}
	tmp := []byte(sb.String())
	for i := 0; i < len(tmp)/2; i++ {
		tmp[i], tmp[len(tmp)-1-i] = tmp[len(tmp)-1-i], tmp[i]
	}

	return string(tmp)
}

func licenseKeyFormatting_482_v2(s string, k int) string {
	s = strings.Replace(s, "-","", -1)
	s = strings.ToUpper(s)
	mod := len(s) % k //get the first part
	if mod == 0 {
		mod += k     //1st part is the same as other
	}
	for mod < len(s) {
		s = s[:mod] + "-" + s[mod:]
		mod += k+1
	}
	return s
}
