package main

/*
https://leetcode.com/problems/count-number-of-nice-subarrays/submissions/
https://leetcode.jp/leetcode-1248-count-number-of-nice-subarrays%e8%a7%a3%e9%a2%98%e6%80%9d%e8%b7%af%e5%88%86%e6%9e%90/

lc828
*/
func numberOfSubarrays_1248_calc(nums []int, k int) int {
	// there is one valid subarray between any two odd positions
	var odd []int
	for i, n := range nums {
		if n%2 == 1 {
			//find and store the number of odd
			odd = append(odd, i)
		}
	}
	var res int
	for i := 0; i+k-1 < len(odd); i++ {
		// if the nums[0] is odd, the length is 1, left == -1 for easier calc.
		// pos[0, ...]
		left, right := -1, len(nums)
		if i-1 >= 0 {
			left = odd[i-1]
		}
		//i + k is a possible next odd number index: the number of odd numbers
		if i+k < len(odd) {
			right = odd[i+k]
		}
		res += (odd[i] - left) * (right - odd[i+k-1])
	}
	return res
}

func numberOfSubarrays_1248(nums []int, k int) int {
	left, leftCnt, oddCnt, res := 0, 0, 0, 0

	for _, n := range nums {
		if n%2 == 1 {
			oddCnt++
		}

		if oddCnt == k {
			// a new valid subarray found
			leftCnt = 0
			// moving the window
			// each move is a subarray
			for oddCnt == k {
				res++
				leftCnt++ //a new subarray is built
				if nums[left]%2 == 1 {
					oddCnt--
				}
				left++
			}
		} else {
			// leftCnt is the valid subarray found so far
			// 0 at the beginning
			res += leftCnt
		}
	}
	return res
}

/*
https://leetcode.com/problems/subarrays-with-k-different-integers/
Given an integer array nums and an integer k, return the number of good subarrays of nums.

A good array is an array where the number of different integers in that array is exactly k.

For example, [1,2,3,1,2] has 3 different integers: 1, 2, and 3.
A subarray is a contiguous part of an array.



Example 1:

Input: nums = [1,2,1,2,3], k = 2
Output: 7
Explanation: Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2]
Example 2:

Input: nums = [1,2,1,3,4], k = 3
Output: 3
Explanation: Subarrays formed with exactly 3 different integers: [1,2,1,3], [2,1,3], [1,3,4].


Constraints:

1 <= nums.length <= 2 * 10^4
1 <= nums[i], k <= nums.length
*/
func subarraysWithKDistinct_992(nums []int, k int) int {
	return 1
}
