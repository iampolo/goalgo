package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test239(t *testing.T) {
	nums := []int{-7, -8, 7, 5, 7, 1, 6, 0}
	k := 4

	assert.Equal(t, []int{7, 7, 7, 7, 7}, maxSlidingWindow(nums, k))
}
