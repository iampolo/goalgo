package main

import "gopkg.in/karalabe/cookiejar.v1/collections/deque"

//https://leetcode.com/problems/sliding-window-maximum/discuss/65939/Golang-solution-using-slice-as-deque
func maxSlidingWindow(nums []int, k int) []int {
	if len(nums) == 0 {
		return []int{}
	}

	res := []int{}
	deque := deque.New()
	for i := 0; i < len(nums); i++ {
		for !deque.Empty() && nums[deque.Right().(int)] <= nums[i] {
			deque.PopRight()
		}
		if !deque.Empty() && deque.Left().(int) <= i-k {
			deque.PopLeft()
		}

		deque.PushRight(i)
		if i >= k-1 {
			res = append(res, nums[deque.Left().(int)])
		}
	}
	return res
}
