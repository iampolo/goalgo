package main

import (
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/trapping-rain-water/
// https://leetcode.com/problems/trapping-rain-water/discuss/1159188/Stack-implementation

func trap_v3_stack(height []int) int {
	//stack
	st := make([]int, 0)

	var area int
	for i := 0; i < len(height); i++ {
		for len(st) > 0 && height[st[len(st)-1]] < height[i] {
			pop := st[len(st)-1]
			st = st[:len(st)-1]
			if len(st) == 0 {
				break
			}
			top := st[len(st)-1]
			dist := i - top - 1
			netHeight := Min(height[i], height[top]) - height[pop]

			area += netHeight * dist
		}
		st = append(st, i)
	}
	return area
}

func trap_v2(height []int) int {
	if len(height) == 0 {
		return 0
	}
	left := make([]int, len(height))
	l := len(height)

	left[0] = height[0]
	for i := 1; i < l; i++ {
		left[i] = Max(left[i-1], height[i])
	}

	right := make([]int, l)
	right[l-1] = height[l-1]
	for i := len(height) - 2; i >= 0; i-- {
		right[i] = Max(right[i+1], height[i])
	}

	var res int
	for i := 1; i < len(height)-1; i++ {
		res += Min(left[i], right[i]) - height[i]
	}

	return res
}

func trap(height []int) int {
	left := make([]int, len(height))

	max := -1
	for i := 0; i < len(height); i++ {
		max = Max(height[i], max)
		left[i] = max
	}

	right := make([]int, len(height))
	max = -1
	for i := len(height) - 1; i >= 0; i-- {
		max = Max(height[i], max)
		right[i] = max
	}

	var res int
	for i := 1; i < len(height)-1; i++ {
		res += Min(left[i], right[i]) - height[i]
	}

	return res
}
