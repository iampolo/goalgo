package main

import "sort"
/**
https://leetcode.com/problems/valid-triangle-number/discuss/104169/Java-Solution-3-pointers

@see ThreeSumClosest16
 */
func triangleNumber(nums []int) int {
	var cnt int

	sort.Ints(nums)
	for i := 2; i < len(nums); i++ {
		l,r := 0, i - 1

		for l < r {
			if nums[l] + nums[r] > nums[i] {
				cnt+=r-l
				r--
			} else {
				l++
			}
		}
	}

	return cnt
}
