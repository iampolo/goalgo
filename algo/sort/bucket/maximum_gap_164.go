package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"
)

//https://leetcode.com/problems/maximum-gap/
//Given an integer array nums, return the maximum difference between two successive elements
//in its sorted form. If the array contains less than two elements, return 0.
//
//You must write an algorithm that runs in linear time and uses linear extra space.
//
//
//
//Example 1:
//
//Input: nums = [3,6,9,1]
//Output: 3
//Explanation: The sorted form of the array is [1,3,6,9], either (3,6) or (6,9) has the maximum difference 3.

//Example 2:
//
//Input: nums = [10]
//Output: 0
//Explanation: The array contains less than 2 elements, therefore return 0.
//
//
//Constraints:
//
//1 <= nums.length <= 10^4
//0 <= nums[i] <= 10^9
//
func maximumGap(nums []int) int {
	if len(nums) < 2 {
		return 0
	}
	//what is exact 2 elements

	min, max := nums[0], nums[1]

	for _, n := range nums {
		min = Min(min, n)
		max = Max(max, n)
	}

	len := len(nums)
	minB := make([]int, len-1)
	maxB := make([]int, len-1)
	for i := 0; i < len-1; i++ {
		minB[i] = math.MaxInt64
		maxB[i] = math.MinInt64
	}

	gap := int(math.Ceil(float64(max-min) / float64(len-1)))
	for _, n := range nums {
		if n == min || n == max {
			continue
		}
		id := (n - min) / gap
		minB[id] = Min(n, minB[id])
		maxB[id] = Max(n, maxB[id])
	}

	maxGap := math.MinInt64
	prev := min
	for i := 0; i < len-1; i++ {
		if minB[i] == math.MaxInt64 {
			continue
		}

		maxGap = Max(maxGap, minB[i]-prev) //cur num - prev round's max
		prev = maxB[i]                     //used in next round
	}

	maxGap = Max(maxGap, max-prev)
	return maxGap
}

func main() {
	n := []int{3, 6, 9, 1}
	n = []int{100, 3, 2, 1}

	res := maximumGap(n)
	fmt.Println(res)
}
