package sort

import (
	"fmt"
	"sort"
)

type matrix [][]int

func (m matrix) Len() int {
	return len(m)
}

func (m matrix) Less(i, j int) bool {
	for x := range m[i] { //for each row
		if m[i][x] == m[j][x] {
			continue
		}
		return m[i][x] < m[j][x]
	}
	return false
}

func (m matrix) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

//func main() {
//	withslicestable()
//}

func custom() {
	mat := [][]int{
		{2, 3, 1},
		{6, 3, 5},
		{1, 4, 9},
	}
	m := matrix(mat)

	sort.Sort(m)
	fmt.Println(m)

	withlib()
}

func withlib() {
	mat := [][]int{
		{2, 3, 1},
		{6, 3, 5},
		{1, 4, 9},
	}

	sort.Slice(mat, func(i, j int) bool {
		for x := range mat[i] {
			if mat[i][x] == mat[j][x] {
				continue
			}
			return mat[i][x] < mat[j][x]
		}
		return false
	})
	fmt.Println(mat)
}


func withslicestable() {
	mat := [][]int{
		{2, 3, 1},
		{6, 3, 5},
		{1, 4, 9},
	}
	k := 1
	//sort.SliceStable(mat, func(i, j int) bool {
	//	return mat[i][k] < mat[j][k]
	//})
	fmt.Println(mat)
	k = 2
	sort.SliceStable(mat, func(i, j int) bool {
		return mat[k][i] < mat[k][j]
	})
	fmt.Println(mat)
}