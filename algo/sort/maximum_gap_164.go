package sort

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"

)

//https://leetcode.com/problems/maximum-gap/
//Given an integer array nums, return the maximum difference between two successive elements
//in its sorted form. If the array contains less than two elements, return 0.
//
//You must write an algorithm that runs in linear time and uses linear extra space.
//
//
//
//Example 1:
//
//Input: nums = [3,6,9,1]
//Output: 3
//Explanation: The sorted form of the array is [1,3,6,9], either (3,6) or (6,9) has the maximum difference 3.

//Example 2:
//
//Input: nums = [10]
//Output: 0
//Explanation: The array contains less than 2 elements, therefore return 0.
//
//
//Constraints:
//
//1 <= nums.length <= 10^4
//0 <= nums[i] <= 10^9
//
func maximumGap(nums []int) int {
	if len(nums) < 2 {
		return 0
	}

	sort.Ints(nums)

	var max int
	fmt.Println("max: ", max)

	for i := 0; i < len(nums) - 1; i++ {
		max = Max(nums[i + 1] - nums[i], max)
	}

	return max
}

func main_() {
	n := []int{3, 6, 9, 1}

	res := maximumGap(n)
	fmt.Println(res)
}