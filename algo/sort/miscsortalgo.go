package sort

import (
	"math/rand"
)

//https://yourbasic.org/golang/quicksort-optimizations/
func Pivot(v []int) int {
	n := len(v)
	return Median(v[rand.Intn(n)],
		v[rand.Intn(n)],
		v[rand.Intn(n)])
}

func Median(a, b, c int) int {
	if a < b {
		switch {
		case b < c:
			return b
		case a < c:
			return c
		default:
			return a
		}
	}
	switch {
	case a < c:
		return a
	case b < c:
		return c
	default:
		return b
	}
}

//find and swap median to the middle
func Median2(data []int) int {
	left, right := 0, len(data)-1
	mid := left + (right-left)/2

	if data[left] > data[mid] {
		data[left], data[mid] = data[mid], data[left]
	}
	if data[left] > data[right] {
		data[left], data[right] = data[right], data[left]
	}

	if data[mid] > data[right] {
		data[mid], data[right] = data[right], data[mid]
	}

	return data[mid]
}

// https://leetcode.com/problems/sort-an-array/submissions/
func bubbleSort_912(nums []int) []int {
	for i := 0; i < len(nums); i++ {
		//before the last two elements and the elements that have been sorted
		for j := 0; j < len(nums)-1-i; j++ {
			if nums[j] > nums[j+1] {
				nums[j], nums[j+1] = nums[j+1], nums[j]
			}
		}
	}
	return nums
}

//https://leetcode.com/problems/sort-an-array
func quick_sort_912(nums []int) []int {
	quick_sort(nums, 0, len(nums)-1)
	return nums
}

func quick_sort(nums []int, lo, hi int) {
	if lo >= hi {
		return
	}
	pIdx := partition(nums, lo, hi)
	quick_sort(nums, lo, pIdx-1)
	quick_sort(nums, pIdx+1, hi)
}

func partition(nums []int, lo, hi int) int {
	pivot := lo + (hi-lo)/2
	nums[hi], nums[pivot] = nums[pivot], nums[hi]
	right := hi - 1

	for lo <= right {
		if nums[lo] <= nums[hi] {
			lo++
		} else if nums[right] >= nums[hi] {
			right--
		} else {
			nums[lo], nums[right] = nums[right], nums[lo]
			lo++
			right--
		}
	}
	nums[lo], nums[hi] = nums[hi], nums[lo]
	return lo //pivot index
}
