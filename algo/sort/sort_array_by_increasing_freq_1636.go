package sort

import (
	"container/heap"
	. "gitlab.com/iampolo/goalgo/algo/util/v2"
	"sort"
)

//https://leetcode.com/problems/sort-array-by-increasing-frequency/discuss/1092260/Four-golang-solutions-with-explanation-and-images

//Given an array of integers nums, sort the array in increasing order based on the frequency of the values.
//If multiple values have the same frequency, sort them in decreasing order.
//
//Return the sorted array.
//
//
//
//Example 1:
//
//Input: nums = [1,1,2,2,2,3]
//Output: [3,1,1,2,2,2]
//Explanation: '3' has a frequency of 1, '1' has a frequency of 2, and '2' has a frequency of 3.
//
//Example 2:
//
//Input: nums = [2,3,1,3,2]
//Output: [1,3,3,2,2]
//Explanation: '2' and '3' both have a frequency of 2, so they are sorted in decreasing order.
//
//Example 3:
//
//Input: nums = [-1,1,-6,4,5,-6,1,4,1]
//Output: [5,-1,4,4,-6,-6,1,1,1]
//
//
//Constraints:
//
//1 <= nums.length <= 100
//-100 <= nums[i] <= 100

func frequencySort(nums []int) []int {
	freq := make(map[int][]int)
	// [1,1,1..]
	// [-1,-1,-1...]
	h := &IntHeap{}
	for _, n := range nums {
		freq[n] = append(freq[n], n)
	}

	for _, v := range freq {
		heap.Push(h, v)
	}

	var res []int
	for h.Len() > 0 {
		res = append(res, heap.Pop(h).([]int)...)

	}
	return res
}

func frequencySortv2(nums []int) []int {
	i := make(map[int]int)
	var freq [][]int
	// [0]...6,6..
	// [1]...-1,-1...
	for _, n := range nums {
		if _, ok := i[n]; !ok {
			freq = append(freq, []int{})
			i[n] = len(freq) - 1 //save the index
		}
		freq[i[n]] = append(freq[i[n]], n)
	}

	sort.Slice(freq, func(i, j int) bool {
		if len(freq[i]) == len(freq[j]) {
			return freq[i][0] > freq[j][0] //sort by value: desc
		}
		return len(freq[i]) < len(freq[j]) //sort by count: asc
	})

	var res []int
	for _, f := range freq {
		res = append(res, f...)
	}
	return res
}

func frequencySortV1(nums []int) []int {
	cnt := make(map[int]int)
	for _, n := range nums {
		cnt[n]++
	}
	var freq [][]int
	for k, v := range cnt {
		freq = append(freq, []int{v, k})
	}

	for i := 1; i < len(freq); i++ {
		//prev has bigger freq, or prev and curr has equal freq, but the prev one has smaller value: swap
		if i > 0 && (freq[i-1][0] > freq[i][0] || freq[i-1][0] == freq[i][0] && freq[i-1][1] < freq[i][1]) {
			freq[i-1], freq[i] = freq[i], freq[i-1]
			i -= 2
		}
	}

	var res []int
	for i := range freq {
		for c := 0; c < freq[i][0]; c++ {
			res = append(res, freq[i][1])
		}
	}

	return res
}
