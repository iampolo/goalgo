package main

import (
	"fmt"
	"unicode"
)

// https://leetcode.com/problems/basic-calculator-ii/
//Constraints:
//
//1 <= s.length <= 3 * 105
//s consists of integers and operators ('+', '-', '*', '/') separated by some number of spaces.
//s represents a valid expression.
//All the integers in the expression are non-negative integers in the range [0, 2^31 - 1].
//The answer is guaranteed to fit in a 32-bit integer.
func calculate_227(s string) int {
	// there is always an init +0
	//num:curnum,
	//sign stores the previous sign
	// since there could be * and /, the num needs to be stored first so that it can be
	// used for * and / before any * or -
	var st []int
	num, sign := 0, byte('+')

	for i := range s {
		if unicode.IsDigit(rune(s[i])) {
			num = num*10 + int(s[i]-'0')
		}

		if i+1 == len(s) || s[i] == '+' || s[i] == '-' || s[i] == '*' || s[i] == '/' {
			if sign == '+' {
				st = append(st, num)
			} else if sign == '-' {
				st = append(st, -num)
			} else if sign == '*' {
				cur, st := st[len(st)-1], st[:len(st)-1]
				st = append(st, cur*num)
			} else if sign == '/' {
				//pop and apply /
				cur, st := st[len(st)-1], st[:len(st)-1]
				st = append(st, cur/num)
			}
			sign = s[i]
			num = 0
		}
	}
	// there may be more than one values if * / exist
	// nums are stored in the stack
	var res int
	for _, v := range st {
		res += v
	}
	return res
}

func RunCode() {
	res := calculate_227("3+2*2")
	fmt.Println("calculator result: ", res)

	s := "(7-8+9)"
	//s = "2147483647"
	fmt.Println(calculate_224_v2(s))
	fmt.Println(calculate_224(s))
}

// ***************************************************************************
// https://leetcode.com/problems/basic-calculator/

// stack
func calculate_224_v2(s string) int {
	if len(s) == 0 {
		return 0
	}

	// there is always an init +0
	res, sign, num := 0, 1, 0
	var st []int
	st = append(st, sign)

	for i := range s {
		if s[i] >= '0' && s[i] <= '9' {
			num = num*10 + int(s[i]-'0')
		} else if s[i] == '+' || s[i] == '-' {
			res += sign * num
			sign = st[len(st)-1]
			if s[i] == '-' {
				sign *= -1
			}
			num = 0
		} else if s[i] == '(' {
			st = append(st, sign)
		} else if s[i] == ')' {
			st = st[:len(st)-1]
		}
	}
	res += sign * num
	return res
}

//
func calculate_224(s string) int {
	if len(s) == 0 {
		return 0
	}
	return calc(s, 0)[0]
}

// recursion
func calc(s string, idx int) []int {
	// there is always an init +0
	num, sign := 0, 1

	for idx < len(s) {
		ch := s[idx]
		if unicode.IsDigit(rune(ch)) {
			v := int(ch - '0')
			for idx+1 < len(s) && unicode.IsDigit(rune(s[idx+1])) {
				v = v*10 + int(s[idx+1]-'0')
				idx++
			}
			num += sign * v
		} else if ch == '(' {
			ret := calc(s, idx+1)
			num += sign * ret[0]
			idx = ret[1]
		} else if ch == ')' {
			break
		} else if ch == '+' {
			sign = 1
		} else if ch == '-' {
			sign = -1
		}
		idx++
	}

	return []int{num, idx}
}
