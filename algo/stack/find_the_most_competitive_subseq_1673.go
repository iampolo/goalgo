package main

// https://leetcode.com/problems/find-the-most-competitive-subsequence/

func mostCompetitive(nums []int, k int) []int {
	//only keep k elements in the stack
	var st []int

	size := len(nums)
	for i := range nums {
		//size - i : remaining numbers
		//nums[i] is good? if yes, needs to be added anyway,
		//but see if removal of old number is needed
		for len(st) > 0 && st[len(st)-1] > nums[i] && len(st) + (size - i) > k {
			//get rid of the larger one
			st = st[:len(st)-1] //remove the large one
		}
		st = append(st, nums[i])
	}

	return st[:k]
}

func mostCompetitive_v2(nums []int, k int) []int {
	var deque []int

	cnt := len(nums) - k //cnt : the # can be removal if necessary
	for i := range nums {
		for len(deque) > 0 && deque[len(deque)-1] > nums[i] && cnt > 0 {
			deque = deque[:len(deque)-1]
			cnt--
		}
		deque = append(deque, nums[i])
	}
	return deque[:k]
}
// heap
// https://leetcode.com/problems/find-the-most-competitive-subsequence/discuss/952996/Golang-stack-and-min-healp-solutions-with-explanation
