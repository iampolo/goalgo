package main

import (
	"fmt"
	"reflect"
)

func main() {
	test_1249()
}

func test_907() {
	arr := []int{3, 1, 2, 4}
	arr = []int{11, 81, 94, 43, 3}
	arr = []int{71, 55, 82, 55}
	fmt.Println(sumSubarrayMins(arr))
	fmt.Println(sumSubarrayMins_v3(arr))
	fmt.Println(sumSubarrayMins_v2(arr))
}
func test_1249() {
	s := "())()"
	s = "lee(t(c)o)de)"
	fmt.Println(minRemoveToMakeValid(s))
}

func test_316() {
	s := "cbacdcbc"
	fmt.Println(removeDuplicateLetters(s))
}

func test_402() {
	k := 3
	num := "1432219"

	num = "10200"
	k = 1

	//num = "112"
	num = "9"
	k = 1
	num = "10"
	k = 2

	fmt.Println(removeKdigits(num, k))
}

func test_1673() {
	nums := []int{2, 4, 3, 3, 5, 4, 9, 6}
	k := 4
	//nums = []int{71, 18, 52, 29, 55, 73, 24, 42, 66, 8, 80, 2}
	//k = 3
	fmt.Println(mostCompetitive(nums, k))
	fmt.Println(mostCompetitive_v2(nums, k))
}

func test_224() {
	RunCode()
}

func testGOCode() {

	s := "3290str"

	for i, x := range s {
		fmt.Println(i, string(s[i]), x, reflect.TypeOf(x).Kind())
	}

	fmt.Println(reflect.TypeOf(3))

}
