package main

/*
https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
*/
// slow!
func minRemoveToMakeValid(s string) string {
	remove := make([]bool, len(s))

	var st []int
	//use stack to match up the ')' with a '('
	//mark remove for any extra ')'
	//and what are remained in the stack are the extra '('
	for i, c := range s {
		if c == '(' {
			st = append(st, i) //push
		} else if c == ')' {
			if len(st) > 0 {
				st = st[:len(st)-1] //pop
			} else {

				remove[i] = true
			}
		}
	}
	var i int
	for len(st) > 0 {
		i, st = st[len(st)-1], st[:len(st)-1] //pop
		remove[i] = true
	}

	var res string
	for i := 0; i < len(s); i++ {
		if remove[i] {
			continue
		}
		res += string(s[i])
	}

	return res
}

