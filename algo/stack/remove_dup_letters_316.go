package main

// https://leetcode.com/problems/remove-duplicate-letters/
//Note: This question is the same as 1081: https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/

//1673: Find the Most Competitive Subsequence
//1425. Constrained Subsequence Sum
//1130. Minimum Cost Tree From Leaf Values
//1019. Next Greater Node In Linked List
//907. Sum of Subarray Minimums
//901. Online Stock Span
//856. Score of Parentheses
//503. Next Greater Element II
//496. Next Greater Element I
// 321. Create Maximum Number
// 85. Maximal Rectangle
//84. Largest Rectangle in Histogram
//42. Trapping Rain Water
func removeDuplicateLetters(s string) string {

	chCnt := [26]int{}
	for _, ch := range s {
		chCnt[ch-'a']++
	}

	vis := make([]bool, 26)
	var st []byte

	for i := range s {
		idx := s[i] - 'a'

		chCnt[idx]--
		if vis[idx] {
			continue
		}
		for len(st) > 0{
			peek := st[len(st)-1]
			if peek > s[i] && chCnt[peek-'a'] > 0 {
				st = st[:len(st)-1]   //bigger char out
				vis[peek-'a'] = false //no longer at correct position
			} else {
				break
			}
		}

		st = append(st, s[i]) //good position so far
		vis[idx] = true //mark as correct position
	}

	return string(st)
}
