package main

// https://leetcode.com/problems/remove-k-digits/
// Given string num representing a non-negative integer num, and an integer k, return the smallest
//possible integer after removing k digits from num.
//
//
//
//Example 1:
//
//Input: num = "1432219", k = 3
//Output: "1219"
//Explanation: Remove the three digits 4, 3, and 2 to form the new number 1219 which is the smallest.
//
//Example 2:
//
//Input: num = "10200", k = 1
//Output: "200"
//Explanation: Remove the leading 1 and the number is 200. Note that the output must not contain leading
//zeroes.
//
//Example 3:
//
//Input: num = "10", k = 2
//Output: "0"
//Explanation: Remove all the digits from the number and it is left with nothing which is 0.
//
//
//Constraints:
//
//- 1 <= k <= num.length <= 10^5
//- num consists of only digits.
//- num does not have any leading zeros except for the zero itself.

func removeKdigits(num string, k int) string {
	var deque []byte

	cnt := k //cnt : the # can be removal if necessary
	for i := range num {
		for len(deque) > 0 && deque[len(deque)-1] > num[i] && cnt > 0 {
			deque = deque[:len(deque)-1]
			cnt--
		}
		deque = append(deque, num[i])
	}
	//for dup digts
	for cnt > 0 {
		deque = deque[:len(deque)-1]
		cnt--
	}

	//leading zero
	zero := true
	var res []byte
	for i := range deque {
		if deque[i] == '0' && zero {
			continue
		}
		res = append(res, deque[i])
		zero = false
	}
	if len(res) == 0 {
		return "0"
	}
	return string(res)
}
