package main

import "gitlab.com/iampolo/goalgo/algo/model"

type MyStack struct {
	q1, q2 *model.Queue
}

/** Initialize your data structure here. */
func Constructor() MyStack {
	return MyStack{q1: &model.Queue{}, q2: &model.Queue{}}
}

/** Push element x onto stack. */
func (this *MyStack) Push(x int) {
	this.q1.Offer(x)
}

/** Removes the element on top of the stack and returns that element. */
func (this *MyStack) Pop() int {
	res := this.q1.Poll()
	if !this.q1.IsEmpty() {
		for !this.q1.IsEmpty() {
			this.q2.Offer(res)
			res = this.q1.Poll()
		}
		this.q1, this.q2 = this.q2, this.q1
	}
	return res.(int)
}

/** Get the top element. */
func (this *MyStack) Top() int {
	res := this.Pop()
	this.Push(res)
	return res
}

/** Returns whether the stack is empty. */
func (this *MyStack) Empty() bool {
	return this.q1.IsEmpty()
}

/**
 * Your MyStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(x);
 * param_2 := obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.Empty();
 */