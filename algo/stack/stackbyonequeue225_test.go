package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test225(t *testing.T) {
	s := Constructor()

	s.Push(1)
	s.Push(2)

	fmt.Println(s.Pop())
	fmt.Println(s.Pop())
	assert.Panics(t, func() { s.Pop() }, "deem to be out of range")

	fmt.Println(s.Empty())
}
