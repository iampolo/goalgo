package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

//https://leetcode.com/problems/sum-of-subarray-minimums/
//Given an array of integers arr, find the sum of min(b), where b ranges over every (contiguous) subarray of arr.
//Since the answer may be large, return the answer modulo 10^9 + 7.
//
//
//Example 1:
//
//Input: arr = [3,1,2,4]
//Output: 17
//Explanation:
//Subarrays are [3], [1], [2], [4], [3,1], [1,2], [2,4], [3,1,2], [1,2,4], [3,1,2,4].
//Minimums are 3, 1, 2, 4, 1, 1, 2, 1, 1, 1.
//Sum is 17.
//
//Example 2:
//
//Input: arr = [11,81,94,43,3]
//Output: 444
//
//
//Constraints:
//
//1 <= arr.length <= 3 * 10^4
//1 <= arr[i] <= 3 * 10^4
//
//The number of subarrays with 3 in it will be anything that starts in [L..3] and ends in [3..R].
//So we have [L..3] starting positions and [3..R] ending positions. In other words, we have d1 starting
//positions and d2 ending positions. So the total number of subarrays is just d1*d2.
//
//For example:
//in [9,7,8,3,4,6]
//we have 4 choices to start with (9,7,8,3)
//we have 3 choices to end with (3,4,6)
//
//So answer is just 4*3.
//
func sumSubarrayMins_v3(arr []int) int {

	st := Stack{}
	var res int
	for i := 0; i <= len(arr); i++ {
		cur := 0 //or a very small number
		if i < len(arr) {
			cur = arr[i]
		}

		for st.Len() > 0 && arr[st.Peek().(int)] > cur {
			j := st.Pop().(int)
			k := -1 //empty stack
			if st.Len() > 0 {
				k = st.Peek().(int)
			}
			res += arr[j] * (i - j) * (j - k)
			res = res % MOD
		}
		st.Push(i)
	}
	return res
}

//***************************************************************************
func sumSubarrayMins(arr []int) int {
	//for each element in the arr
	//find the subarray on the left as well as right, and keep this element as the minimum.

	// the length of the subarray if left[i] or right[i] is the smallest in that subarray
	left, right := make([]int, len(arr)), make([]int, len(arr))

	st := Stack{}
	for i := 0; i < len(arr); i++ {
		for st.Len() > 0 && arr[st.Peek().(int)] >= arr[i] {
			st.Pop()
		}
		left[i] = i + 1
		if st.Len() > 0 {
			left[i] = i - st.Peek().(int)
		}
		st.Push(i)
	}

	st = Stack{}
	for i := len(arr) - 1; i >= 0; i-- {
		for st.Len() > 0 && arr[st.Peek().(int)] > arr[i] {
			st.Pop()
		}
		right[i] = len(arr) - i
		if st.Len() > 0 {
			right[i] = st.Peek().(int) - i
		}
		st.Push(i)
	}
	fmt.Println(left, right)

	var res int
	for i := 0; i < len(arr); i++ {
		//arr[i] is the smallest in the subarray, find the elements on the left and on the right
		res += arr[i] * left[i] * right[i]
		res = res % MOD
	}
	return res
}

const MOD int = 1e9 + 7

//***************************************************************************
type mins struct {
	val, count int
}

func sumSubarrayMins_v2(A []int) int {
	left := make([]int, len(A))
	right := make([]int, len(A))
	var lstack []mins
	var rstack []mins

	for i := 0; i < len(A); i++ {
		count := 1
		for len(lstack) != 0 && lstack[len(lstack)-1].val > A[i] {
			count += lstack[len(lstack)-1].count
			lstack = lstack[:len(lstack)-1]
		}
		lstack = append(lstack, mins{A[i], count})
		left[i] = count
	}

	for i := len(A) - 1; i >= 0; i-- {
		count := 1
		for len(rstack) != 0 && rstack[len(rstack)-1].val >= A[i] {
			count += rstack[len(rstack)-1].count
			rstack = rstack[:len(rstack)-1]
		}
		rstack = append(rstack, mins{A[i], count})
		right[i] = count
	}

	sum := 0

	for i := 0; i < len(A); i++ {
		sum += A[i] * left[i] * right[i]
		sum %= MOD
	}

	return sum
}

/*
https://leetcode.com/problems/sum-of-subarray-ranges/submissions/
*/
func subArrayRanges_2104(nums []int) int64 {
	var res int64
	for i := 0; i < len(nums); i++ {
		min, max := nums[i], nums[i]
		for j := i; j < len(nums); j++ {
			min = Min(min, nums[j])
			max = Max(max, nums[j])
			res += int64(max - min)
		}
	}
	return res
}
