'''https://leetcode.com/discuss/interview-question/395045/Facebook-or-Phone-Screen-or-Caesar-Cipher'''

from collections import defaultdict

alphabet = 'abcdefghijklmnopqrstuvwxyz'

def group_ceaser(los):
    storage = defaultdict(list)

    for i, string in enumerate(los):
        pos = [alphabet.index(c) for c in string]
        minPos = min(pos)
        pos = [num - minPos for num in pos]
        storage[tuple(pos)].append(string)

    return storage.values()


if __name__ == '__main__':
    print(group_ceaser(["abc", "bcd", "acd", "dfg", "ace", "bdf", "random"]))
    print(group_ceaser(["abc", "ace", "xyz", "fhj"]))


