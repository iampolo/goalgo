package main

import (
    "fmt"
    "strconv"
    "strings"
)

//https://leetcode.com/problems/count-and-say/discuss/15995/Examples-of-nth-sequence
func countAndSay38(n int) string {
    var result strings.Builder
    s1 := "1"
    if n <= 1 {
        return s1
    }
    tmp := s1
    fmt.Println(tmp)

    var j int
    for i := 2; i <= n; i++ {
        for j = 0; j < len(tmp); j++ {
            count := 1
            ts := tmp[j] //cnt for this char
            for j < len(tmp) - 1 && tmp[j] == tmp[j + 1] {
                count++
                j++
            }
            result.WriteString(strconv.Itoa(count) + string(ts))
        }
        tmp = result.String()
        fmt.Println(tmp)
        result.Reset()
    }

    return tmp
}

func TestCount() {
	countAndSay38(6)
}
