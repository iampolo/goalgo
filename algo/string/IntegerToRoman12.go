package main

import (
	"fmt"
)

func intToRoman(num int) string {
	if num < 1 {
		return ""
	}

	symbol := []string{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
	values := []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}
	res := ""
	for i := 0; i < len(values); i++ {
		for num - values[i] >= 0 {
			res += symbol[i]
			num -= values[i]
		}
	}
	//for loop way 2
	for i, _ := range values {
		fmt.Println("i:", i)
	}

	return res
}
/************************************************************************************/
func TestIntToRoman() {
	res := intToRoman(4)
	fmt.Println("result: " , res)
	fmt.Println("result: " , intToRoman_v2(40))
}
/************************************************************************************/
type roman struct {
	S string
	N int
}

var romans = []roman{
	{"I", 1},
	{"IV", 4},
	{"V", 5},
	{"IX", 9},
	{"X", 10},
	{"XL", 40},
	{"L", 50},
	{"XC", 90},
	{"C", 100},
	{"CD", 400},
	{"D", 500},
	{"CM", 900},
	{"M", 1000},
}

func intToRoman_v2(num int) string {
	var res string
	pos := len(romans) - 1

	for num > 0 {
		for num >= romans[pos].N {
			res += romans[pos].S
			num -= romans[pos].N
		}
		pos--
	}
	return res
}