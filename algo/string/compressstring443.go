package main

import (
	"bytes"
	"regexp"
	"strconv"
)
//RLE - run length encoding
//https://exercism.io/tracks/go/exercises/run-length-encoding/solutions/8eb5d34d7acc445586c76f6de4f42197

/// - Tag: str_char_test
func compress_(s string) string {
	var b bytes.Buffer
	for n := 0; n < len(s); {
		c := s[n : n+1] //c becomes a string

		repeat, err := regexp.Compile(c + "+")
		if err != nil {
			return "err" + err.Error()
		}

		long := repeat.FindString(s[n:])
		var short string
		if len(long) == 1 {
			short = long
		} else {
			short = strconv.Itoa(len(long)) + long[:1]
		}
		//b.Write([]byte(short))
		b.WriteString(short)
		n += len(long)
	}

	return b.String()
}

func compress_v1(s string) string {
	var b bytes.Buffer

	slow := 0;
	for i := 0; i < len(s); {

		cnt := 1
		for i + 1 < len(s) && s[i] == s[i+1] {
			cnt++
			i++
		}

		b.WriteByte(s[i])
		slow++
		if cnt > 1 {
			b.WriteString(strconv.Itoa(cnt))
		}
		i++

	}

	return b.String()
}

func compress_lc443(chars []byte) int {
	slow := 0
	for i := 0; i < len(chars); {
		cnt := 1
		for i+1 < len(chars) && chars[i] == chars[i+1] {
			cnt++
			i++
		}

		chars[slow] = chars[i]
		slow++
		if cnt > 1 {
			for _, c := range strconv.Itoa(cnt) {
				chars[slow] = byte(c)
				slow++
			}
		}
		i++
	}
	return slow
}