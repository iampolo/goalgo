package main

import (
	"sort"
)

// https://leetcode.com/problems/group-anagrams/

// Given an array of strings strs, group the anagrams together. You can return the answer in any order.
//
//An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
//typically using all the original letters exactly once.
//
//
//
//Example 1:
//
//Input: strs = ["eat","tea","tan","ate","nat","bat"]
//Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
//
//Example 2:
//
//Input: strs = [""]
//Output: [[""]]
//
//Example 3:
//
//Input: strs = ["a"]
//Output: [["a"]]
//
//
//Constraints:
//
//1 <= strs.length <= 104
//0 <= strs[i].length <= 100
//strs[i] consists of lower-case English letters.

func groupAnagrams_v2(strs []string) [][]string {
	grp := map[[26]int][]string{}

	for _, s := range strs {
		arr := [26]int{}
		for i := 0; i < len(s); i++ {
			arr[s[i]-'a'] += 1
		}
		grp[arr] = append(grp[arr], s)
	}

	res := [][]string{}
	for _, v := range grp {
		res = append(res, v)
	}
	return res
}

func groupAnagrams(strs []string) [][]string {

	grp := make(map[string][]string)

	for _, s := range strs {
		r := []rune(s)
		sort.Slice(r, func(i, j int) bool {
			return r[i] < r[j]
		})

		grp[string(r)] = append(grp[string(r)], s)

	}
	res := [][]string{}
	for _, v := range grp {
		res = append(res, v)
	}
	return res
}
