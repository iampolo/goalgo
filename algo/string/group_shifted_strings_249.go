package main

import (
	"fmt"
	"strings"
)

// https://leetcode.com/problems/group-shifted-strings/

// We can shift a string by shifting each of its letters to its successive letter.
//
//For example, "abc" can be shifted to be "bcd".
//We can keep shifting the string to form a sequence.
//
//For example, we can keep shifting "abc" to form the sequence: "abc" -> "bcd" -> ... -> "xyz".
//Given an array of strings strings, group all strings[i] that belong to the same shifting sequence. You may
//return the answer in any order.
//
//
//
//Example 1:
//
//Input: strings = ["abc","bcd","acef","xyz","az","ba","a","z"]
//Output: [["acef"],["a","z"],["abc","bcd","xyz"],["az","ba"]]
//
//Example 2:
//
//Input: strings = ["a"]
//Output: [["a"]]
//
//
//Constraints:
//
//1 <= strings.length <= 200
//1 <= strings[i].length <= 50
//strings[i] consists of lowercase English letters.

func groupStrings(strings []string) [][]string {
	//
	dict := map[string][]string{}

	for _, s := range strings {
		key := getKey(s)

		dict[key] = append(dict[key], s)
	}

	res := [][]string{}
	for _, v := range dict {
		res = append(res, v)
	}

	return res
}

func getKey(s string) string {
	//find difference between chars in a string
	sb := strings.Builder{}
	for i := 1; i < len(s); i++ {
		diff := (s[i] - s[i-1] + 26) % 26
		//use space to separate the numbers
		//prevent case like 11 vs 1 1
		sb.WriteString(fmt.Sprintf("%2d", diff))
	}
	return sb.String()
}
