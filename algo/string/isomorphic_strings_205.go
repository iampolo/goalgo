package main

import "gitlab.com/iampolo/goalgo/algo/util"

/**
https://leetcode.com/problems/isomorphic-strings/discuss/318692/Golang-easy-way-to-solve
*/

func isIsomorphic(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}

	sPos, tPos := map[uint8]int{}, map[uint8]int{}
	for i := range s {
		if sPos[s[i]] != tPos[t[i]] {
			return false
		}
		sPos[s[i]] = i + 1
		tPos[t[i]] = i + 1
	}
	return true
}

func isIsomorphic_v2(s string, t string) bool {
	used := util.NewSet()
	mapping := map[uint8]uint8{}

	for i := 0; i < len(s); i++ {
		sc, tc := s[i], t[i]

		if val, ok := mapping[sc]; ok && val != tc {
			return false
		}
		if _, ok := mapping[sc]; !ok && used.Contains(tc) {
			return false
		}

		mapping[sc] = tc
		used.Add(tc)

	}
	return true
}
