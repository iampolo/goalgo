package main

import . "gitlab.com/iampolo/goalgo/algo/util"

/*
https://leetcode.com/problems/longest-happy-prefix/

LongestHappyPrefix1392
*/

func longestPrefix(s string) string {

	hashs, hashe, pow := 0, 0, 1
	l, r := 0, len(s)-1

	var maxlen int
	for l < len(s)-1 {
		hashs = hashs*31 + int(s[l])
		hashe = hashe + int(s[r])*pow
		if hashs == hashe {
			maxlen = Max(maxlen, l + 1)
		}
		l++
		r--

		pow *= 31
	}
	if maxlen == 0 {
		return ""
	}
	return s[:maxlen]
}
