package main

// https://leetcode.com/problems/longest-uncommon-subsequence-i/

func findLUSlength_521(a string, b string) int {

	if !isSubsequence(a, b) || !isSubsequence(b, a) {
		if len(a) > len(b) {
			return len(a)
		}
		return len(b)
	}
	return -1
}

func findLUSlength_521_v2(a string, b string) int {
	if a == b {
		return -1
	}
	if len(a) > len(b) {
		return len(a)
	}
	return len(b)
}
