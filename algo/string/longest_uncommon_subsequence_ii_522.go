package main

import (
	"sort"
)

// https://leetcode.com/problems/longest-uncommon-subsequence-ii/
// Given an array of strings strs, return the length of the longest uncommon subsequence between them. If the
// longest uncommon subsequence does not exist, return -1.
//
//An uncommon subsequence between an array of strings is a string that is a subsequence of one string but not
//the others.
//
//A subsequence of a string s is a string that can be obtained after deleting any number of characters from s.
//
//For example, "abc" is a subsequence of "aebdc" because you can delete the underlined characters in "aebdc"
//to get "abc". Other subsequences of "aebdc" include "aebdc", "aeb", and "" (empty string).
//
//
//Example 1:
//
//Input: strs = ["aba","cdc","eae"]
//Output: 3
//
//Example 2:
//
//Input: strs = ["aaa","aaa","aa"]
//Output: -1
//
//
//Constraints:
//
// 	1 <= strs.length <= 50
//	1 <= strs[i].length <= 10
//strs[i] consists of lowercase English letters.
//
// distinct_subsequences_115.go
//
type ByLength []string

func (s ByLength) Len() int {
	return len(s)
}
func (s ByLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByLength) Less(i, j int) bool {
	return len(s[i]) > len(s[j])

}
func findLUSlength_v3(strs []string) int {
	sort.Sort(ByLength(strs))
	for i := 0; i < len(strs); i++ {
		var flag = true
		for j := 0; j < len(strs); j++ {
			if i == j {
				continue
			}
			if isSubsequence(strs[i], strs[j]) {
				flag = false
				break
			}
		} //for
		if flag {
			return len(strs[i])
		}
	}
	return -1
}

// ******************************************
// O(n * x^2) : x avg. length of the strings
// O(1)
func findLUSlength_v2(strs []string) int {
	//the longest string is the real candidate, we can sort the string array first.
	res := -1
	for i := 0; i < len(strs); i++ {
		j := 0
		for ; j < len(strs); j++ {
			if i == j {
				continue
			}
			if isSubsequence(strs[i], strs[j]) {
				break
			}
		} //for
		if j == len(strs) && len(strs[i]) > res {
			res = len(strs[i])
		}
	}
	return res
}

func isSubsequence(s, t string) bool {
	if len(s) == 0 {
		return true
	}
	if len(s) > len(t) {
		return false
	}
	j := 0
	for i := 0; i < len(t); i++ {
		if s[j] == t[i] {
			j++
			if j == len(s) {
				return true
			}
		}
	}
	return false
}

// ******************************************
// O(n * 2^x) x the avg. length of the strings
//
func findLUSlength(strs []string) int {
	cntMap := make(map[string]int)
	for _, s := range strs {
		//generate the possible subsequences cases
		//the one occurred only once and the longest is the answer.
		for i := 0; i < (1 << len(s)); i++ {
			var tmp string //empty string is also a subsequence
			//the bit-1 is used to get the char from the string
			for j := 0; j < len(s); j++ {
				if ((i >> j) & 1) == 1 {
					tmp += string(s[j])
				}
			}
			cntMap[tmp] = cntMap[tmp] + 1
		}
	}

	res := -1
	for k, v := range cntMap {
		if v == 1 && len(k) > res {
			res = len(k)
		}
	}
	return res
}
