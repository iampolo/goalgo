package main

import (
	"fmt"
	"reflect"
)

func main() {
	//fmt.Println(numSplits("aacaba"))
	test_1554()
}

func test_1554() {
	w := []string{"ab", "cd", "yz"}
	w = []string{"abcd", "acbd", "aacd"}
	w= []string{"mfbdoohablgehdiohdhllhhoodohdnklec","mfbdoohablgehdiombjjbdlfaeejblknnf","mfbdoohablgehdiohiokmamdloeajmnhcd","mfbdoohablgehdiohjlhmdaclacfbjfafa","mfbdoohablgehdiohnibmonbanccdecjio","mfbdoohablgehdloheclhdgiekhhgebfka","mfbdoohablgehdiohacbnlkifleonkchbk","mfbdoohablgehdiohiiemgjicloiikljag","mfbdoohablgehdiohhekjcclhnhfehhakg","mfbdoohablgehdiohkkehgfajgdodbobkk","mfbdoohablgehdiohhbmklnblkbefmnija","mfbdoohablgehdiohcfgeeeigbjhkemhfb","mcbdoohabfgehdioholeoonogjgffnkbcb","mlbdoohablgehdiohbglfgkhlknakhjhnc","mfbdoohablgehdiohcbfalkjigjecmohlh","mfbdoohcblgehdiohlhgncmcnghhkojbfj","mfbdobhablgehdiohhgidmdgihgbioibhf","mebdoohablgehdiohlockkkgijomcmbmfj","mfbdoohablgehdiohmjhdgahlbelglhidh","mfbdhohablgehdiohlgcnjgbhnaijjhklc","mfbdoohablgehdiohdknmoolfdaokklmbm","mfbdoohablgehdiohkcnfmjncllfnhakae","mfbdoohablgehdiohhdocbblhgakmioijm","mfbdoohablgehdiobhcmbdmhedcbccgnce","mfbdoohablgehdiohnekbenflgknalgidn","mfbdoohablgehdiohmkecdlnoblhoednob","mfbdoohablgehdiohkbogbjlljeffolgad","mfbdoohablgehdiohdfllhhoodohdnklec","mfbdoohablgehdiohadmoljmkgaahoakeo","mfbdoohablgehdiohnlmgjonokaknoboai","mfbdoohablgehdiohihcciaifdhkbngkhd","mfbdoohablgehdiohonbkkhbfblaggdfoa"}
	fmt.Println(differByOne(w))
}

func test_443() {
	chars := []byte{'a', 'a', 'b', 'b', 'c', 'c', 'c'}
	res := compress_443(chars)
	fmt.Println(res, string(chars[0:res]))
}

func test_1592() {
	text := " practice   makes   perfect"
	text = "  this   is  a sentence "
	text = " practice   makes   perfect"
	//text = "text"
	fmt.Println(reorderSpaces_1592(text))
}

func test_1055() {
	s, t := "abc", "abcbc"
	fmt.Println(shortestWay_v1(s, t))
}

func test_792() {
	words := []string{"a", "bb", "acd", "ace"}
	s := "abcde"

	fmt.Println(numMatchingSubseq(s, words))
}

func test_929() {
	emails := []string{
		"a+nadgkj.ansjfnakjn.a.df.a@g.com",
		"test.email+alex@leetcode.com",
		"test.e.mail+bob.cathy@leetcode.com",
		"testemail+david@lee.tcode.com",
	}

	fmt.Println(numUniqueEmails_929_v2(emails))
	fmt.Println(numUniqueEmails_929(emails))
}

func test_522() {
	strs := []string{"aba", "cdc", "eae"}
	fmt.Println(findLUSlength_v3(strs))
	fmt.Println(findLUSlength_v2(strs))
}

func test_249() {
	s := []string{"abc", "bcd", "acef", "xyz", "az", "ba", "a", "z", "al"}
	fmt.Println(numSplits("aacaba"))
	test_249()
	fmt.Println(groupStrings(s))
}

func test_49() {
	s := []string{"eat", "tea", "tan", "ate", "nat", "bat"}

	fmt.Println(groupAnagrams(s))
}

func test205() {
	s, t := "foo", "bar"
	fmt.Println(isIsomorphic(s, t))

	fmt.Println(reflect.TypeOf(s[0]))
}

func test1209() {
	s := "abbbaca"
	fmt.Println(removeDuplicates_v2(s))

	s = "deeedbbcccbdaa"
	k := 3

	s = "abcd"
	k = 2
	fmt.Println(removeDuplicates_1209_v2(s, k))
	fmt.Println(removeDuplicates_1209(s, k))
}
