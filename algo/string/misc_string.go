package main

import (
	"fmt"
	"strconv"
	"strings"
)

//
//https://leetcode.com/problems/rearrange-spaces-between-words/
//You are given a string text of words that are placed among some number of spaces. Each word consists of one or
//more lowercase English letters and are separated by at least one space. It's guaranteed that text contains at least one word.
//
//Rearrange the spaces so that there is an equal number of spaces between every pair of adjacent words and that number
//is maximized. If you cannot redistribute all the spaces equally, place the extra spaces at the end, meaning the
//returned string should be the same length as text.
//
//Return the string after rearranging the spaces.
//
//
//
//Example 1:
//
//Input: text = "  this   is  a sentence "
//Output: "this   is   a   sentence"
//Explanation: There are a total of 9 spaces and 4 words. We can evenly divide the 9 spaces between the words:
//9 / (4-1) = 3 spaces.
//
//Example 2:
//
//Input: text = " practice   makes   perfect"
//Output: "practice   makes   perfect "
//Explanation: There are a total of 7 spaces and 3 words. 7 / (3-1) = 3 spaces plus 1 extra space. We place this
//extra space at the end of the string.
//
//Example 3:
//
//Input: text = "hello   world"
//Output: "hello   world"
//
//Example 4:
//
//Input: text = "  walks  udp package   into  bar a"
//Output: "walks  udp  package  into  bar  a "
//Example 5:
//
//Input: text = "a"
//Output: "a"
//
//
//Constraints:
//
//1 <= text.length <= 100
//text consists of lowercase English letters and ' '.
//text contains at least one word.
//
func reorderSpaces_1592(text string) string {
	//other :
	//https://leetcode.com/problems/rearrange-spaces-between-words/discuss/863920/Golang-0ms-2MB-fields-solution
	spaceCnt := 0
	var words []string
	for i := 0; i < len(text); i++ {
		//skip space
		if text[i] == ' ' {
			spaceCnt++
			continue
		}

		str, j := "", i
		for ; j < len(text); j++ {
			if text[j] != ' ' {
				str += string(text[j])
			} else {
				break ///get one word at a time
			}
		}
		words = append(words, str)
		i = j - 1
	}

	var res strings.Builder
	if len(words) == 1 {
		res.WriteString(words[0])
		for i := 0; i < spaceCnt; i++ {
			res.WriteByte(' ')
		}
	} else { //must be more than 1 word
		span := spaceCnt / (len(words) - 1)
		for i := 0; i < len(words)-1; i++ {
			res.WriteString(words[i])
			for j := 0; j < span; j++ {
				res.WriteByte(' ')
			}
		}
		res.WriteString(words[len(words)-1])
		ending := spaceCnt % (len(words) - 1)
		for i := 0; i < ending; i++ {
			res.WriteByte(' ')
		}
	}
	return res.String()
}

// https://leetcode.com/problems/add-strings/discuss/1393636/Go-solution
func addstring_415(num1 string, num2 string) string {
	l1, l2 := len(num1)-1, len(num2)-1

	var sb strings.Builder
	carry := 0
	for l1 >= 0 || l2 >= 0 || carry > 0 {
		if l1 >= 0 {
			carry += int(num1[l1] - '0')
		}
		if l2 >= 0 {
			carry += int(num2[l2] - '0')
		}
		l1--
		l2--
		sb.WriteByte(byte(carry%10 + '0'))
		carry /= 10
	}
	return Reverse(sb.String())
}

func Reverse(s string) string {
	chars := []rune(s)
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		chars[i], chars[j] = chars[j], chars[i]
	}
	return string(chars)
}

//*************************************************************************************************
//https://leetcode.com/problems/unique-email-addresses/
//Every valid email consists of a local name and a domain name, separated by the '@' sign. Besides lowercase letters,
//the email may contain one or more '.' or '+'.
//
//For example, in "alice@leetcode.com", "alice" is the local name, and "leetcode.com" is the domain name.
//If you add periods '.' between some characters in the local name part of an email address, mail sent there will be
//forwarded to the same address without dots in the local name. Note that this rule does not apply to domain names.
//
//For example, "alice.z@leetcode.com" and "alicez@leetcode.com" forward to the same email address.
//If you add a plus '+' in the local name, everything after the first plus sign will be ignored. This allows certain
//emails to be filtered. Note that this rule does not apply to domain names.
//
//For example, "m.y+name@email.com" will be forwarded to "my@email.com".
//It is possible to use both of these rules at the same time.
//
//Given an array of strings emails where we send one email to each email[i], return the number of different addresses
//that actually receive mails.
//
//
//Example 1:
//
//Input: emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
//Output: 2
//Explanation: "testemail@leetcode.com" and "testemail@lee.tcode.com" actually receive mails.
//
//Example 2:
//
//Input: emails = ["a@leetcode.com","b@leetcode.com","c@leetcode.com"]
//Output: 3
//
//
//Constraints:
//
// - 1 <= emails.length <= 100
// - 1 <= emails[i].length <= 100
// - email[i] consist of lowercase English letters, '+', '.' and '@'.
// - Each emails[i] contains exactly one '@' character.
// - All local and domain names are non-empty.
// - Local names do not start with a '+' character.
func numUniqueEmails_929_v2(emails []string) int {
	m := make(map[string]int)
	for _, e := range emails {
		lIdx := strings.LastIndexByte(e, '@')
		local, domain := e[:lIdx], e[lIdx+1:]

		if pIdx := strings.Index(local, "+"); pIdx != -1 {
			local = local[:pIdx]
		}
		local = strings.ReplaceAll(local, ".", "")
		email := fmt.Sprintf("%s@%s", local, domain)
		m[email] = 0
	}
	return len(m)
}

func numUniqueEmails_929(emails []string) int {
	m := make(map[string]int)
	for _, e := range emails {
		parts := strings.Split(e, "@")
		locals := strings.Split(parts[0], "+")
		emailLocal := strings.ReplaceAll(locals[0], ".", "")

		//fmt.Println(emailLocal + "@" + parts[1])
		m[emailLocal+"@"+parts[1]] = 0
	}
	return len(m)
}

//https://leetcode.com/problems/string-compression/
func compress_443(chars []byte) int {
	s, f := 0, 0
	for f < len(chars) {

		cnt := 1
		for f < len(chars)-1 && chars[f] == chars[f+1] {
			f++
			cnt++
		}

		chars[s] = chars[f]
		s++
		if cnt > 1 {
			for _, ch := range strconv.Itoa(cnt) {
				chars[s] = byte(ch)
				s++
			}
		}

		f++
	}
	return s
}

//https://leetcode.com/problems/sorting-the-sentence/
func sortSentence_1859(s string) string {
	words := strings.Split(s, " ")
	idxMap := make(map[int]string)
	for i := range words {
		last := len(words[i]) - 1
		pos := int(words[i][last] - '0')
		idxMap[pos] = words[i][0:last]
	}
	//assuming the data are correct
	var res string
	for i := 1; i <= len(idxMap); i++ {
		res += idxMap[i] + " "
	}
	if len(res) > 0 {
		return res[0 : len(res)-1] //take out the last space
	}
	return res
}

func sortSentence_1859_v2(s string) string {
	words := strings.Split(s, " ")
	res := make([]string, len(words))

	for _, w := range words {
		last := len(w) - 1
		pos := int(w[last] - '1') //tricky
		res[pos] = w[0:last]
	}
	return strings.Join(res, " ")
}

func sortSentence_1859_v3(s string) string {
	words := strings.Split(s, " ")
	res := make([]string, len(words)+1)

	for _, w := range words {
		last := len(w) - 1
		pos := int(w[last] - '0')
		res[pos] = w[:last]
	}
	return strings.Join(res[1:], " ")
}

/*
https://leetcode.com/problems/repeated-string-match/submissions/
*/
func repeatedStringMatch_686(a string, b string) int {
	alen, blen := len(a), len(b)

	for i := 0; i < alen; i++ {
		j := 0
		for ; j < blen; j++ {
			if a[(i+j)%alen] != b[j] {
				break
			}
		}
		if j == blen {
			return (i+j-1)/alen + 1
		}
	}
	return -1
}

/*
https://leetcode.com/problems/repeated-substring-pattern/
	s := abcabc
take half j := len(s)/2      //j must be a divisor of len(s)
     abcabc
	 i
		j        //i+j
check all i with j as the length
if i ==j at the end of the loop, return true; otherwise, false

*/
func repeatedSubstringPattern_459(s string) bool {
	//check for each possible length
	size := len(s)
	for j := size / 2; j > 0; j-- {
		if size%j != 0 {
			continue
		}
		i := 0 //scan from the 1st char for each length
		for i+j < size && s[i] == s[i+j] {
			i++
		}
		//was able to match till the end of s with length j
		if i+j == size {
			return true
		}
	}
	return false
}

/*
https://leetcode.com/problems/robot-return-to-origin/
*/
func judgeCircle_657(moves string) bool {
	x, y := 0, 0
	for _, ch := range moves {
		switch ch {
		case 'U':
			y++
		case 'D':
			y--
		case 'L':
			x--
		case 'R':
			x++
		}
	}
	return x == 0 && y == 0
}

/*
https://leetcode.com/problems/robot-bounded-in-circle/
*/

var DIRS = []int{0, -1, 0, 1, 0}

func isRobotBounded_1041(instructions string) bool {
	x, y := 0, 0
	face := 0

	// L and R is about turning the face of the robot
	for _, ch := range instructions {
		if ch == 'L' {
			face = (face + 3) % 4
		} else if ch == 'R' {
			face = (face + 1) % 4
		} else { //'G"
			x += DIRS[face]
			y += DIRS[face+1]
		}
	}
	//going back to origin or not facing North at other location - then cycle
	return x == 0 && y == 0 || face != 0

}
