package main

/**
https://leetcode.com/problems/number-of-good-ways-to-split-a-string/discuss/754719/Some-hints-to-help-you-solve-this-problem-on-your-own
 */
func numSplits(s string) int {
	left := make(map[string]int)

	for _, c := range s {
		left[string(c)]++
	}

	cnt := 0
	right := make(map[string]int)
	for _, c := range s {
		ch := string(c)

		left[ch]--
		if left[ch] == 0 {
			delete(left, ch)
		}
		right[ch]++
		if len(left) == len(right) {
			cnt++
		}
	}

	return cnt
}
