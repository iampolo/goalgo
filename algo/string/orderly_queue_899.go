package main

import (
	"sort"
	"strings"
)

//https://leetcode.com/problems/orderly-queue/
// dbac
// for k>1,
//
//bdac, select d
//bacd, select a
//bcda, select b
//cdab, select c
//dabc, select d
//abcd
// I think this works similar to how bubble sort works. In case of K == 1, we cannot switch two elements, so sorting
//is impossible. In case of K >= 2, theoretically, we can swap two unsorted elements by switching a[0] and a[1] and
//send them to the end of the list. Then, we rotate the array so that the array is arranged as a[1], a[0], a[2], a[3] ... ,
//which is equivalent to completion of the first step of a bubble sort. Now, using this protocol, we can basically
//sort the alphabets if K >= 2.
//like a bubble sort?!
func orderlyQueue(s string, k int) string {
	if k == 1 {
		//one char is allowed to move, every char is possible.
		res := s
		for i := 0; i < len(s); i++ {
			tmp := s[i:] + s[0:i]
			if res > tmp {
				res = tmp
			}
		}
		return res
	} else {
		tmp := strings.Split(s, "")
		sort.Strings(tmp)
		return strings.Join(tmp, "")
	}
}
