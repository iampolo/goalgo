package main

func removeDuplicates_v2(s string) string {

	arr := []byte{}
	for i := 0; i < len(s); i++ {
		if len(arr) > 0 && arr[len(arr)-1] == s[i] {
			arr = arr[:len(arr)-1]
		} else {
			arr = append(arr, s[i])
		}
	}

	return string(arr)
}

func removeDuplicates(s string) string {

	array := []byte(s)

	sl, fa := -1, 0
	for fa < len(array) {

		if sl == -1 || array[sl] != array[fa] {
			sl++
			array[sl] = array[fa]
			fa++
		} else {
			sl--
		}
	}
	return string(array[:sl+1])
}
