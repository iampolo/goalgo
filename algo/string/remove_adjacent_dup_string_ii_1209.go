package main

/**
https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string-ii/discuss/1040600/Go-solution
*/
func removeDuplicates_1209(s string, k int) string {
	arr := []byte{}

	for _, c := range s {
		//save k chars each time, and then check for dup
		arr = append(arr, byte(c))
		if len(arr) >= k {
			hasDup := true
			for j := 0; j < k; j++ {
				if arr[len(arr)-j-1] != arr[len(arr)-1] {
					hasDup = false
					break
				}
			}
			if hasDup {
				arr = arr[:len(arr)-k]
			}
		}
	}

	return string(arr)
}

/**
Good
 */
func removeDuplicates_1209_v2(s string, k int) string {
	cnt := make([]int, len(s))
	arr := make([]byte, len(s))

	idx := 0
	for i := 0; i < len(s); i++ {
		arr[idx] = s[i]

		if idx > 0 && arr[idx] == arr[idx-1] {
			cnt[idx] = cnt[idx-1] + 1
		} else {
			cnt[idx] = 1
		}

		if cnt[idx] == k {
			idx -= k
		}
		idx++
	}
	return string(arr[:idx])
}
