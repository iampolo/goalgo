package main

//https://leetcode.com/problems/rotate-array/discuss/54435/A-golang-solution
func rotate(nums []int, k int) {
	n := len(nums)
	k %= n
	if k == 0 {
		return
	}

	reverse(nums, 0, n-1)
	reverse(nums, 0, k-1)
	reverse(nums, k, n-1)
}

func reverse(nums []int, l, r int) {
	for l < r {
		tmp := nums[r]
		nums[r] = nums[l]
		nums[l] = tmp
		l++
		r--
	}
}

//********************************************************************
//https://leetcode.com/problems/rotate-array/discuss/54435/A-golang-solution
func rotate_189(nums []int, k int) {
	if len(nums) == 0 {
		return
	}
	copy(nums, append(nums[len(nums)-k%len(nums):], nums[0:len(nums)-k%len(nums)]...))
}
