package main

import "fmt"

// https://leetcode.com/problems/shortest-way-to-form-string/
// A subsequence of a string is a new string that is formed from the original string by deleting some (can be none)
// of the characters without disturbing the relative positions of the remaining characters. (i.e., "ace" is a
// subsequence of "abcde" while "aec" is not).
//
// Given two strings source and target, return the minimum number of subsequences of source such that their
// concatenation equals target. If the task is impossible, return -1.
//
//
//
// Example 1:
//
// Input: source = "abc", target = "abcbc"
// Output: 2
// Explanation: The target "abcbc" can be formed by "abc" and "bc", which are subsequences of source "abc".
//
// Example 2:
//
// Input: source = "abc", target = "acdbc"
// Output: -1
// Explanation: The target string cannot be constructed from the subsequences of source string due to the character "d" in target string.
//
// Example 3:
//
// Input: source = "xyz", target = "xzyxz"
// Output: 3
// Explanation: The target string can be constructed as follows "xz" + "y" + "xz".
//
//
// Constraints:
//
// - 1 <= source.length, target.length <= 1000
// - source and target consist of lowercase English letters.
//
func shortestWay_v1(s string, t string) int {
	idx := [26][]int{}
	for i := range idx {
		idx[i] = make([]int, len(s))
	}

	for i := 0; i < len(s); i++ {
		idx[s[i]-'a'][i] = i + 1
	}

	for i := 0; i < 26; i++ {
		pre := 0
		for j := len(s) - 1; j >= 0; j-- {
			if idx[i][j] == 0 {
				idx[i][j] = pre
			} else {
				pre = idx[i][j]
			}
		}
	}
	for i:= range idx {
		fmt.Println(idx[i])
	}

	res, j := 1, 0
	for i := 0; i < len(t); i++ {
		if j == len(s) {
			j = 0
			res++
		}
		if idx[t[i]-'a'][0] == 0 {
			return -1
		}
		j = idx[t[i]-'a'][j]
		if j == 0 {
			res++
			i--
		}
	}
	return res
}

func shortestWay(source string, target string) int {
	var cnt int
	for i := 0; i < len(target); {
		pos := i
		//find target char in the source string
		for j := 0; j < len(source); j++ {
			if i < len(target) && source[j] == target[i] {
				i++ //match one more char for target
			}
		}
		if pos == i {
			return -1 //never found match
		}
		cnt++
	}
	return cnt
}
