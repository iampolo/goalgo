package main

/*
https://leetcode.com/problems/strings-differ-by-one-character/submissions/
*/

func differByOne(dict []string) bool {
	// Compute a hash of each element in the dict, then remove one element
	// at a time from the hashes and compare to check if there is a match.
	mod := 1_000_000_007
	base := 31
	// Create hash of each word
	hashes := make([]int, len(dict))
	for i, w := range dict {
		for _, ch := range w {
			hashes[i] = (hashes[i]*base + int(ch-'a')) % mod
		}
	}
	/*
		int is either 4 bytes or 8 bytes depends on 32-bit or 64-bit machine
	 */
	//for k, v := range hashes {
	//	fmt.Printf(" %d %d\n", reflect.TypeOf(k).Size(), reflect.TypeOf(v).Size())
	//}

	n := len(dict[0])
	pow := 1
	for j := n - 1; j >= 0; j-- {
		seen := make(map[int][]int)
		for i, w := range dict {
			// Remove current character from hash and check if it has been seen before
			h := (mod + hashes[i] - pow*int(w[j]-'a')%mod) % mod
			for _, otherIdx := range seen[h] {
				if w[:j] == dict[otherIdx][:j] &&
					w[j+1:] == dict[otherIdx][j+1:] {
					return true
				}
			}
			seen[h] = append(seen[h], i)
		}
		pow = pow * base % mod
	}
	return false
}
