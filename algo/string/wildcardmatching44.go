package main

import "fmt"

func isMatch(s string, p string) bool {
	slen, plen := len(s), len(p)
	spos, ppos := 0, 0
	starIdx, match := -1,0
	for spos < slen {
		if ppos < plen && (p[ppos] == '?' || s[spos] == p[ppos]) {
			spos++
			ppos++
		} else if ppos < plen && p[ppos] == '*' {
			starIdx = ppos
			match = spos
			ppos++
		} else if starIdx != -1 { //ie: >= 0
			ppos = starIdx + 1
			match++
			spos = match
		} else { return false }
	}

	for ; ppos < plen && p[ppos] == '*'; ppos++ {
	}
	return ppos == plen
}


func TestWildcard() {
	s := "aaaaaaaaaaaaaaapaaa"
	p := "*aa"

	s = "aca"
	p = "a*c"
	res := isMatch(s,p)
	fmt.Println(res)
}