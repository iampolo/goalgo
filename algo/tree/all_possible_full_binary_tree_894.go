package main

import . "gitlab.com/iampolo/goalgo/algo/model"

//https://leetcode.com/problems/all-possible-full-binary-trees/
//https://leetcode.com/problems/unique-binary-search-trees-ii/
// generateTrees_95
//
//https://www.youtube.com/watch?v=Ci-82MggDYI&ab_channel=happygirlzt
// the_score_of_students_solving_Math_exp_2019.go

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

var cache = make(map[int][]*TreeNode)

func allPossibleFBT_v2(n int) []*TreeNode {
	var res []*TreeNode
	if n%2 == 0 {
		return res
	}
	//already procesed, return cached version
	if _, exist := cache[n]; exist {
		return cache[n]
	}

	if n == 1 {
		res = append(res, &TreeNode{Val: 0})
		return res
	}

	//i starts with 1, so left side has one node initially
	//then, 3, 5, ...
	for i := 1; i < n; i += 2 {
		j := n - 1 - i //node on the right side
		left := allPossibleFBT_v2(i)
		right := allPossibleFBT_v2(j)

		for _, l := range left {
			for _, r := range right {
				node := &TreeNode{Val: 0}
				node.Left = l
				node.Right = r

				res = append(res, node)
			}
		} //
	}
	cache[n] = res
	return res
}

//*************=**********************************************************
/**
	The # of node must be odd
 	n = 0	nil
	n = 1	one tree with 1 node
	n = 3  	one tree with 3 nodes
	n = 5	 1 2 3 4 5
		if 2 is root : left 1, right 3,4,5
		if 4 is root : left 1,2,3  right: 5
	n = 7 	left could be 1, 3 or 5 nodes
			right could be 5 ,3 1 respectively
	n = 9 ...
 */
func allPossibleFBT(n int) []*TreeNode {
	var res []*TreeNode
	if n%2 == 0 {
		return res
	}
	if n == 1 {
		res = append(res, &TreeNode{Val: 0})
		return res
	}

	//i starts with 1, so left side has one node initially
	//then, 3, 5, ...
	for i := 1; i < n; i += 2 {
		j := n - 1 - i //node on the right side
		left := allPossibleFBT(i)
		right := allPossibleFBT(j)

		for _, l := range left {
			for _, r := range right {
				node := &TreeNode{Val: 0}
				node.Left = l
				node.Right = r

				res = append(res, node)
			}
		} //
	}

	return res
}



