package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
	"strconv"
)

// https://leetcode.com/problems/binary-tree-paths/
func binaryTreePaths(root *TreeNode) []string {
	ans := make([]string, 0)
	dfs(root, "", &ans)
	return ans
}

func dfs(node *TreeNode, cur string, ans *[]string) {
	if node == nil {
		return
	}

	if len(cur) == 0 {
		cur += strconv.Itoa(node.Val)
	} else {
		cur += "->" + strconv.Itoa(node.Val)
	}

	if node.Left == nil && node.Right == nil {
		*ans = append(*ans, cur)
		return
	}

	dfs(node.Left, cur, ans)
	dfs(node.Right, cur, ans)
}

