package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
)

/**
https://leetcode.com/problems/binary-tree-pruning/
 */

func pruneTree(root *TreeNode) *TreeNode {
	if root == nil {
		return root
	}

	root.Left = pruneTree(root.Left)
	root.Right = pruneTree(root.Right)

	if root.Val == 1 || root.Left != nil || root.Right != nil {
		return root
	}  else {
		return nil
	}
}

