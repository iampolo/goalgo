package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"sort"
)

/*
https://leetcode.com/problems/binary-tree-vertical-order-traversal/
https://leetcode.com/problems/binary-tree-vertical-order-traversal/discuss/1601785/golang-DFS-faster-than-100
*/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type pair struct {
	lvl int
	val int
}

type pairs []pair

func (p pairs) Len() int           { return len(p) }
func (p pairs) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p pairs) Less(i, j int) bool { return p[i].lvl < p[j].lvl }

func verticalOrder(root *TreeNode) [][]int {
	// col to a list of node (pair:{lvl and the node val})
	data := make(map[int][]pair)
	var res [][]int
	if root == nil {
		return res
	}

	lo, hi := 0, -1
	//Preorder
	var dfs func(node *TreeNode, lvl, col int)
	dfs = func(node *TreeNode, lvl, col int) {
		if node == nil {
			return
		}
		lo = Min(lo, col)
		hi = Max(hi, col)
		data[col] = append(data[col], pair{lvl, node.Val})

		dfs(node.Left, lvl+1, col-1)
		dfs(node.Right, lvl+1, col+1)
	}
	dfs(root, 0, 0)
	for i := lo; i <= hi; i++ {
		toSort := pairs(data[i]) //pass to an sortable object!!
		//Stable maintain the original order for those data with the same sort key(lvl)
		sort.Stable(toSort)

		var aRow []int
		for _, v := range toSort {
			aRow = append(aRow, v.val)
		}
		res = append(res, aRow)
	}
	return res
}
