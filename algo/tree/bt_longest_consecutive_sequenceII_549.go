package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
	. "gitlab.com/iampolo/goalgo/algo/util"
)

// https://leetcode.com/problems/binary-tree-longest-consecutive-sequence-ii/
func longestConsecutive(root *TreeNode) int {
	max := []int{0}
	longest(root, max)

	return max[0]
}

func longest(root *TreeNode, max []int) []int {
	if root == nil {
		return []int{0,0}
	}

	left := longest(root.Left, max)
	right := longest(root.Right, max)

	inc, dec := 1, 1 //inc and dec in this level
	if root.Left != nil {
		if root.Val + 1 == root.Left.Val {
			inc = left[0] + 1
		} else if root.Val - 1 == root.Left.Val {
			dec = left[1] + 1
		}
	}
	if root.Right != nil {
		if root.Val + 1 == root.Right.Val {
			inc = Max(right[0] + 1, inc)
		} else if root.Val - 1 == root.Right.Val {
			dec = Max(right[1] + 1, dec)
		}
	}


	max[0] = Max(max[0], inc + dec - 1)
	return []int{inc, dec}
}
