package main

/**

Given two integer arrays preorder and inorder where preorder is the preorder traversal of a binary tree and
inorder is the inorder traversal of the same tree, construct and return the binary tree.

Example 1:


Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
Output: [3,9,20,null,null,15,7]

Example 2:

Input: preorder = [-1], inorder = [-1]
Output: [-1]


Constraints:

1 <= preorder.length <= 3000
inorder.length == preorder.length
-3000 <= preorder[i], inorder[i] <= 3000
preorder and inorder consist of unique values.
Each value of inorder also appears in preorder.
preorder is guaranteed to be the preorder traversal of the tree.
inorder is guaranteed to be the inorder traversal of the tree.
*/
/** https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
 *
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
)

func buildTree(preorder []int, inorder []int) *TreeNode {
	inMap := make(map[int]int)
	for i, v := range inorder {
		inMap[v] = i
	}
	res := buildTheTree(preorder, 0, len(preorder)-1, inorder, 0, len(inorder)-1, inMap)
	return res
}

func buildTheTree(preorder []int, pleft, pright int, inorder []int, ileft, iright int, inMap map[int]int) *TreeNode {
	// check inorder map as inorder is the data set to be searched
	if ileft > iright {
		return nil
	}
	//the first preorder element is always the current node
	cur := &TreeNode{
		Val:   preorder[pleft],
		Left:  nil,
		Right: nil,
	}
	// use the 1st preorder element to split the left and the right nodes
	idx := inMap[preorder[pleft]]
	//idx-ileft=length of elements on left
	cur.Left = buildTheTree(preorder, pleft+1, pleft+(idx-ileft), inorder, ileft, idx-1, inMap)
	//idx-ileft+1=length of elements on left plus idx itself
	cur.Right = buildTheTree(preorder, pleft+idx-ileft+1, pright, inorder, idx+1, iright, inMap)
	return cur
}

func main_105() {
	pre := []int{3, 9, 20, 15, 7}
	ino := []int{9, 3, 15, 20, 7}

	pre = []int{1, 2, 3}
	ino = []int{3, 2, 1}

	root := buildTree(pre, ino)
	fmt.Println(root)
}
