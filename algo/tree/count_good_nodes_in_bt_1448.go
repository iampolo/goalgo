package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
	"math"
)

// https://leetcode.com/problems/count-good-nodes-in-binary-tree/
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func goodNodes(root *TreeNode) int {
	cnt := []int{0}
	preOrder(root, math.MinInt32, &cnt)
	return cnt[0]
}

func preOrder(node *TreeNode, max int, cnt *[]int) {
	if node == nil {
		return
	}
	if node.Val >= max {
		(*cnt)[0]++
		max = node.Val
	}

	preOrder(node.Left, max, cnt)
	preOrder(node.Right, max, cnt)
}

//
func goodNodes_v2(root *TreeNode) int {
	var cnt int
	preOrder_v2(root, math.MinInt32, &cnt)
	return cnt
}

func preOrder_v2(node *TreeNode, max int, cnt *int) {
	if node == nil {
		return
	}
	if node.Val >= max {
		*cnt++
		max = node.Val
	}

	preOrder_v2(node.Left, max, cnt)
	preOrder_v2(node.Right, max, cnt)
}

//
func goodNodes_v3(root *TreeNode) int {
	return preOrder_v3(root, root.Val)
}

func preOrder_v3(node *TreeNode, max int) int {
	if node == nil {
		return 0
	}

	//this level count
	cnt := 0
	if node.Val >= max {
		cnt++
		max = node.Val
	}

	//add up the cnt from the children
	cnt += preOrder_v3(node.Left, max)
	cnt += preOrder_v3(node.Right, max)
	return cnt
}

// preorder iterative

// level order
type NodeInfo struct {
	node *TreeNode
	max  int
}

func goodNodes_v4(root *TreeNode) int {
	if root == nil {
		return 0
	}

	queue := []NodeInfo{}
	queue = append(queue, NodeInfo{root, root.Val})
	res, max := 0, math.MinInt32
	for len(queue) > 0 {
		nextQueue := []NodeInfo{}
		//level by level
		for _, cur := range queue {
			max = cur.max    //save first
			if max <= cur.node.Val {
				res++
				max = cur.node.Val
			}

			if cur.node.Left != nil {
				nextQueue = append(nextQueue, NodeInfo{cur.node.Left, max})
			}
			if cur.node.Right != nil {
				nextQueue = append(nextQueue, NodeInfo{cur.node.Right, max})
			}
		} //
		queue = nextQueue
	}
	return res
}

//
