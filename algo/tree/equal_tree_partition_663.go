package main

import . "gitlab.com/iampolo/goalgo/algo/model"

// https://leetcode.com/problems/equal-tree-partition/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

// @see max_product_of_split_bt_1339.go
func checkEqualTree(root *TreeNode) bool {
	var sums []int64
	total := treeSum_663(root, &sums)
	// [0,-1,1] handling
	sums = sums[:len(sums)-1]
	if total%2 == 0 {
		half := total / 2
		for _, s := range sums {
			if s == half {
				return true
			}
		}
	}
	return false
}

func treeSum_663(node *TreeNode, sums *[]int64) int64 {
	if node == nil {
		return 0
	}

	left := treeSum_663(node.Left, sums)
	right := treeSum_663(node.Right, sums)

	lvl := int64(node.Val) + left + right
	*sums = append(*sums, lvl)

	return lvl
}
