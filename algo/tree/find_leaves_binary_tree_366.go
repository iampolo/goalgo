package main

import . "gitlab.com/iampolo/goalgo/algo/model"

/**
https://leetcode.com/problems/find-leaves-of-binary-tree/discuss/83773/1-ms-Easy-understand-Java-Solution
empty and nil slice:
https://stackoverflow.com/questions/29164375/correct-way-to-initialize-empty-slice
https://leetcode.com/problems/find-leaves-of-binary-tree/discuss/916517/Go-100-time-and-100-space
*/
func findLeaves(root *TreeNode) [][]int {
	arr := [][]int{}    //empty slice

	var t []int			//nil slice!!
	for root != nil {
		t, root = dfs_366(root)
		arr = append(arr, t)
	}
	return arr
}

func dfs_366(node *TreeNode) ([]int, *TreeNode) {
	if node == nil {
		return []int{}, nil
	}
	if node.Left == nil && node.Right == nil {
		return []int{node.Val}, nil
	}

	var res []int

	var tmp []int
	tmp, node.Left = dfs_366(node.Left)
	res = append(res, tmp...)

	tmp, node.Right = dfs_366(node.Right)
	res = append(res, tmp...)

	return res, node

}
