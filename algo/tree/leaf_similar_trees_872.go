package main

import . "gitlab.com/iampolo/goalgo/algo/model"

//https://leetcode.com/problems/leaf-similar-trees/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
//https://leetcode.com/problems/boundary-of-binary-tree/
//BoundaryofBinaryTree545
func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {
	var lst1, lst2 []int
	preorder(root1, &lst1)
	preorder(root2, &lst2)

	equal := func(a, b []int) bool {
		if len(a) != len(b) {
			return false
		}
		for i := range a {
			if a[i] != b[i] {
				return false
			}
		}
		return true
	}

	return equal(lst1, lst2)
}

func preorder(node *TreeNode, vals *[]int) {
	if node == nil {
		return
	}
	if node.Left == nil && node.Right == nil {
		*vals = append(*vals, node.Val)
		return
	}
	preorder(node.Left, vals)
	preorder(node.Right, vals)
}
