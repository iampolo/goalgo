package main

import . "gitlab.com/iampolo/goalgo/algo/model"

/**
https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/submissions/
*/
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
	if root == nil || root == p || root == q {
		return root
	}

	l := lowestCommonAncestor(root.Left, p, q)
	r := lowestCommonAncestor(root.Right, p, q)

	if l != nil && r != nil {
		return root
	}

	if l != nil {
		return l
	}
	return r
}

/**
https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/submissions/

BST
*/
func lowestCommonAncestor_235(root, p, q *TreeNode) *TreeNode {

	//compare the curr. root with the p and q

	if root == nil || root.Val > p.Val && q.Val > root.Val {
		return root
	} else if p.Val < root.Val && q.Val < root.Val {
		return lowestCommonAncestor(root.Left, p, q)
	} else if p.Val > root.Val && q.Val > root.Val {
		return lowestCommonAncestor(root.Right, p, q)
	}

	//either p, or q equals to curr. root
	return root
}

func lowestCommonAncestor_235_calc(root, p, q *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	for ; (root.Val-p.Val)*(root.Val-q.Val) > 0; {
		if root.Val > p.Val {
			root = root.Left
		} else {
			root = root.Right
		}
	}
	return root
}
