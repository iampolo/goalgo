package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
)

// https://leetcode.com/problems/maximum-product-of-splitted-binary-tree/discuss/?currentPage=1&orderBy=most_votes&query=

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

// sum * (totalSum - sum)
// O(n)
// equal_tree_partition_663.go
//
func maxProduct(root *TreeNode) int {
	var allSums []int64

	//O(n)
	totalSum := treeSum_postorder(root, &allSums)

	var product int64
	for _, sum := range allSums {
		tmp := sum * (totalSum-sum)
		product = max(tmp, product)

	}
	return int(product % ((1e9) + 7))
}

func treeSum_postorder(node *TreeNode, allSums *[]int64) int64 {
	if node == nil {
		return 0
	}

	left := treeSum_postorder(node.Left, allSums)
	right := treeSum_postorder(node.Right, allSums)

	lvlSum := int64(node.Val) + left + right
	*allSums = append(*allSums, lvlSum)
	return lvlSum
}

func max(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}