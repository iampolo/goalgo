package main

type Node struct {
	Val      int
	Children []*Node
}

// https://leetcode.com/problems/n-ary-tree-level-order-traversal/discuss/?currentPage=1&orderBy=most_votes&query=&tag=golang
func levelOrder_429(root *Node) [][]int {
	res := [][]int{}
	dfs_429(root, 0, &res)
	return res
}

func dfs_429(node *Node, lvl int, res *[][]int) {
	if node == nil {
		return
	}

	if len(*res) == lvl {
		*res = append(*res, []int{})
	}
	(*res)[lvl] = append((*res)[lvl], node.Val)
	for _, v := range node.Children {
		dfs_429(v, lvl+1, res)
	}
}
