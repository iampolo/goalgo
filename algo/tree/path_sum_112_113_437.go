package main

import . "gitlab.com/iampolo/goalgo/algo/model"

// https://leetcode.com/problems/path-sum-ii/
func pathSum(root *TreeNode, targetSum int) [][]int {
	var res [][]int

	dfs_113(root, targetSum, []int{}, &res)
	return res
}

func dfs_113(node *TreeNode, target int, cur []int, res *[][]int) {
	if node == nil {
		return
	}

	if node.Left == nil && node.Right == nil && node.Val == target {
		cur = append(cur, node.Val)
		newCur := make([]int, len(cur))
		copy(newCur, cur)
		*res = append(*res, newCur)
		return
	}

	if node.Left != nil {
		cur = append(cur, node.Val)
		dfs_113(node.Left, target-node.Val, cur, res)
		cur = cur[:len(cur)-1]
	}

	if node.Right != nil {
		cur = append(cur, node.Val)
		dfs_113(node.Right, target-node.Val, cur, res)
		cur = cur[:len(cur)-1]
	}
}

//*************************************************************************************************
// https://leetcode.com/problems/path-sum-iii/
func pathSum_437(root *TreeNode, targetSum int) int {
	lookup := make(map[int]int)
	lookup[0]=1 //case with sum equal exactly to targetsum
	return doPathSum(root, 0, targetSum, lookup)
}

func doPathSum(node *TreeNode, sum int, target int, lookup map[int]int) int {
	if node == nil {
		return 0
	}
	//presum order
	sum += node.Val

	//find the complement of sum
	var cur int
	if cnt, ok := lookup[sum-target]; !ok {
		cnt = 0
	} else {
		cur = cnt
	}

	//save the current sum for later lookup
	if _, ok := lookup[sum]; !ok {
		lookup[sum] = 1
	} else {
		lookup[sum]++
	}

	left := doPathSum(node.Left, sum, target, lookup)
	right := doPathSum(node.Right, sum, target, lookup)

	//backtracking,
	//remove the sum in this level before return back to parent, as it is no longer to be used.
	lookup[sum]--

	//finding in this level
	return cur + left + right
}
