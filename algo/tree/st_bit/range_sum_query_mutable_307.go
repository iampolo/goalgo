package main

import (
	"fmt"
	"strconv"
)

/**
https://leetcode.com/problems/range-sum-query-mutable/

Given an integer array nums, handle multiple queries of the following types:

- Update the value of an element in nums.
- Calculate the sum of the elements of nums between indices left and right inclusive where left <= right.

Implement the NumArray class:

- NumArray(int[] nums) Initializes the object with the integer array nums.
- void update(int index, int val) Updates the value of nums[index] to be val.
- int sumRange(int left, int right) Returns the sum of the elements of nums between indices left and
  right inclusive (i.e. nums[left] + nums[left + 1] + ... + nums[right]).


Example 1:

Input
["NumArray", "sumRange", "update", "sumRange"]
[[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
Output
[null, 9, null, 8]

Explanation
NumArray numArray = new NumArray([1, 3, 5]);
numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
numArray.update(1, 2);   // nums = [1, 2, 5]
numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8


Constraints:

1 <= nums.length <= 3 * 10^4
-100 <= nums[i] <= 100
0 <= index < nums.length
-100 <= val <= 100
0 <= left <= right < nums.length
At most 3 * 10^4 calls will be made to update and sumRange.

*/


// Segment Tree Version:
//https://leetcode.com/problems/range-sum-query-mutable/discuss/1281402/Golang-segment-tree-98

type NumArray struct {
	nums []int
	tree []int
}

func Constructor(nums []int) NumArray {

	na := NumArray{
		nums: make([]int, len(nums)),    //emtpy, and build from nums
		tree: make([]int, len(nums)+1),
	}
	for i, n := range nums {
		na.Update(i, n) //publish to BIT
	}
	return na
}

func (n *NumArray) Update(index int, val int) {
	//publish to the BIT: i+=(i&-i)
	for i := index + 1; i < len(n.tree); i += (i & -i) {
		n.tree[i] += val - n.nums[index]
	}
	n.nums[index] = val
}

func (n *NumArray) SumRange(left int, right int) int {
	//from left, so element at left is included
	return sum(right, n) - sum(left-1, n)
}

func sum(i int, n *NumArray) int {
	sum := 0
	for j := i + 1; j > 0; j -= (j & -j) {
		sum += n.tree[j]
	}
	return sum
}

func do207_bit() {
	nums := []int{1, 2, 3, 4, 5, 6}
	na := Constructor(nums)
	fmt.Println(na.SumRange(1, 3))

	testPrint()
}

func testPrint() {
	fmt.Println(strconv.FormatInt(8&-8, 2))
	fmt.Printf("%b \n", 8&-8)
	fmt.Printf("%d == %8b\n", -3, -3)
}
