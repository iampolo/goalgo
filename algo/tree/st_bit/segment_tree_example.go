package main

var tree []int
var n int

//#1
func numArray(nums []int) {
	if len(nums) == 0 {
		return
	}

	n = len(nums)
	tree = make([]int, n*2)
	buildTree(nums)
}

//#1
func buildTree(nums []int) {
	l := len(nums)

	//put the original array at the end of the tree array
	j := 0
	for i := l; i < 2*l; i++ {
		tree[i] = nums[j]
		j++
	}

	//build the tree with element at the end portion
	for i := l - 1; i > 0; i-- {
		tree[i] = tree[i*2] + tree[i*2+1]
	}
}

//#2
func updateTree(pos, val int) {
	pos += n        //go to the original element
	tree[pos] = val //update

	//update the tree starting from pos
	for pos > 0 {
		left, right := pos, pos
		if pos%2 == 0 {
			//related to how the tree was built: the right node is at pos+1
			right = pos + 1
		} else {
			//related to how the tree was built: the left node is at pos-1
			left = pos - 1
		}

		//update parent where the aggregated value is stored
		tree[pos/2] = tree[left] + tree[right]
		pos /= 2 //up to the parent
	}
}

//#3
func rangeSum(l, r int) int {
	//go to the original elements, or go to the leave nodes in the ST
	l += n
	r += n

	// check the article TODO
	var sum int
	for l <= r {
		if l%2 == 1 { //is l right-node of curr node
			sum += tree[l]
			l++
		}
		if r%2 == 0 { //is r left-node of curr node
			sum += tree[r]
			r--
		}

		l /= 2
		r /= 2
	}
	return sum
}
