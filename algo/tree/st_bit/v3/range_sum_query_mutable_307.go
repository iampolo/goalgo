package main

import (
	"fmt"
	"strconv"
)

/**
https://leetcode.com/problems/range-sum-query-mutable/

Given an integer array nums, handle multiple queries of the following types:

- Update the value of an element in nums.
- Calculate the sum of the elements of nums between indices left and right inclusive where left <= right.

Implement the NumArray class:

- NumArray(int[] nums) Initializes the object with the integer array nums.
- void update(int index, int val) Updates the value of nums[index] to be val.
- int sumRange(int left, int right) Returns the sum of the elements of nums between indices left and
  right inclusive (i.e. nums[left] + nums[left + 1] + ... + nums[right]).


Example 1:

Input
["NumArray", "sumRange", "update", "sumRange"]
[[[1, 3, 5]], [0, 2], [1, 2], [0, 2]]
Output
[null, 9, null, 8]

Explanation
NumArray numArray = new NumArray([1, 3, 5]);
numArray.sumRange(0, 2); // return 1 + 3 + 5 = 9
numArray.update(1, 2);   // nums = [1, 2, 5]
numArray.sumRange(0, 2); // return 1 + 2 + 5 = 8


Constraints:

1 <= nums.length <= 3 * 10^4
-100 <= nums[i] <= 100
0 <= index < nums.length
-100 <= val <= 100
0 <= left <= right < nums.length
At most 3 * 10^4 calls will be made to update and sumRange.

*/

type NumArray struct {
	nums []int
	tree []int
}

func Constructor(nums []int) NumArray {

	na := NumArray{
		nums: nums,
		tree: make([]int, len(nums)+1),
	}
	for i, n := range nums {
		na.updateTree(i, n) //publish to BIT
	}
	return na
}

func (n *NumArray) updateTree(i, val int) {
	i = i + 1 //BIT element starts from 1
	for i <= len(n.nums) {
		n.tree[i] += val
		i += (i & -i)
	}
}

func (n *NumArray) Update(index int, val int) {
	diff := val - n.nums[index] //update first
	n.nums[index] = val
	n.updateTree(index, diff)
}

func (n *NumArray) SumRange(left int, right int) int {
	//from left, so element at left is included
	return sum(right, n) - sum(left-1, n)
}

func sum(i int, n *NumArray) int {
	sum := 0
	i = i + 1
	for i > 0 {
		sum += n.tree[i]
		i -= (i & -i)
	}
	return sum
}

func main() {
	nums := []int{1, 2, 3, 4, 5, 6}
	na := Constructor(nums)
	fmt.Println(na.SumRange(1, 3))

	testPrint()
}

func testPrint() {
	fmt.Println(strconv.FormatInt(8&-8, 2))
	fmt.Printf("%b \n", 8&-8)
	fmt.Printf("%d == %8b\n", -3, -3)
}
