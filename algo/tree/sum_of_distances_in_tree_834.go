package main

// https://leetcode.com/problems/sum-of-distances-in-tree/

//https://leetcode.com/problems/sum-of-distances-in-tree/discuss/1444531/Go-DFS-Solution
func sumOfDistancesInTree(n int, edges [][]int) []int {

	//build up adjLst for the tree
	adjLst := make([][]int, n)
	for _, ed := range edges {
		adjLst[ed[0]] = append(adjLst[ed[0]], ed[1])
		adjLst[ed[1]] = append(adjLst[ed[1]], ed[0])
	}

	dist, cnt := make([]int, n), make([]int, n)

	//post order traverse to get count for root(0)
	var postOrd func(node, parent int)
	postOrd = func(node, parent int) {
		for _, nei := range adjLst[node] {
			if nei == parent {
				continue //only wants to go forward
			}
			postOrd(nei, node)
			cnt[node] += cnt[nei]              //save the #ofnode from node
			dist[node] += dist[nei] + cnt[nei] //save the total length for the children of node
		}
		cnt[node]++ //node's node count of itself
	}

	var preOrd func(node, parent int)
	preOrd = func(node, parent int) {
		for _, nei := range adjLst[node] {
			if nei == parent {
				continue
			}

			//TODO formula
			dist[nei] = dist[node] + cnt[nei] - n - cnt[nei]
			preOrd(nei, node)
		}
	}
	postOrd(0, -1)
	preOrd(0, -1)

	return dist
}
