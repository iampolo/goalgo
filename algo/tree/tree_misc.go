package main

import (
	"container/list"
	. "gitlab.com/iampolo/goalgo/algo/util"
	"math"

	. "gitlab.com/iampolo/goalgo/algo/model"
)

//https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
func sortedArrayToBST_108(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}

	return buid_preorder(0, len(nums)-1, nums)
}

func buid_preorder(l, r int, nums []int) *TreeNode {
	if l > r {
		return nil
	}

	m := l + (r-l)/2

	node := &TreeNode{
		Val: nums[m],
	}

	node.Left = buid_preorder(l, m-1, nums)
	node.Right = buid_preorder(m+1, r, nums)
	return node
}

/***
 */
func postorderTraversal_lc145_v2(root *TreeNode) []int {
	var res []int
	if root == nil {
		return res
	}

	stack := list.New()
	stack.PushFront(root)

	var prev *TreeNode
	for stack.Len() > 0 {
		tmp := stack.Front()
		cur := tmp.Value.(*TreeNode)

		if prev == nil || prev.Left == cur || prev.Right == cur {
			//go down, and continue
			if cur.Left != nil {
				stack.PushFront(cur.Left)
			} else if cur.Right != nil {
				stack.PushFront(cur.Right)
			} else {
				stack.Remove(tmp)
				res = append(res, cur.Val)
			}
		} else if cur.Left == prev && cur.Right == nil || cur.Right == prev {
			//backing up no more child
			stack.Remove(tmp)
			res = append(res, cur.Val)
		} else {
			//still need to go right
			stack.PushFront(cur.Right)
		}
		prev = cur
	}
	return res
}

func postorderTraversal_lc145(root *TreeNode) []int {
	var res []int
	if root == nil {
		return res
	}

	stack := list.New()

	for stack.Len() > 0 || root != nil {

		if root != nil {
			res = append([]int{root.Val}, res...)
			stack.PushFront(root)
			root = root.Right
		} else {
			e := stack.Front()
			stack.Remove(e)
			node := e.Value.(*TreeNode)
			root = node.Left
		}
	}

	return res
}

// https://leetcode.com/problems/diameter-of-binary-tree/
// The length of a path between two nodes is represented by the number of edges between them.
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func diameterofbinarytree543(root *TreeNode) int {
	res := []int{0}
	post(root, res)
	return res[0]
}

func post(node *TreeNode, res []int) int {
	if node == nil {
		return 0
	}

	left := post(node.Left, res)
	right := post(node.Right, res)

	//the max #ofedge at current node, it doesn't count itself
	res[0] = Max(res[0], left+right)
	//return the the longest back to parent, +1 is used by parent:
	//one additional edge from parent's point of view
	return Max(left, right) + 1
}

// https://app.laicode.io/app/problem/142
// The diameter is defined as the longest distance from one leaf node to another leaf node. The
// distance is the number of nodes on the path.
//
//      5
//   /    \
//  2      11
//       /    \
//      6     14
//
//The diameter of this tree is 4 (2 → 5 → 11 → 14)
func diameter_142_laicode(root *TreeNode) int {
	res := []int{0}
	post142(root, res)
	return res[0]
}

func post142(node *TreeNode, res []int) int {
	if node == nil {
		return 0 //no node at all
	}
	left := post142(node.Left, res)
	right := post142(node.Right, res)

	// one leaf node to another leaf node: count only when there is are leaf nodes.
	//         1               1
	//        /
	//       2
	//        -> ans:0         -> 0
	//if the current node is a leaf, defer the max count to the parent
	if left != 0 && right != 0 {
		//the current node has children, good to do the max count, +1 for itself
		res[0] = Max(res[0], left+right+1)
		//return 1 + Max(left, right) //could also return from here
	}
	//
	//+1 represents the current node itself, return back t parent to be include in max counting
	return 1 + Max(left, right)
}

/**
 * https://leetcode.com/problems/construct-binary-search-tree-from-preorder-traversal/
 *
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func bstFromPreorder_1008(preorder []int) *TreeNode {
	var idx int
	var create func(min, max int) *TreeNode
	create = func(min, max int) *TreeNode {
		if idx == len(preorder) {
			return nil
		}

		if preorder[idx] < min || max < preorder[idx] {
			return nil
		}

		cur := &TreeNode{Val: preorder[idx]}
		idx++
		cur.Left = create(min, cur.Val)
		cur.Right = create(cur.Val, max)

		return cur
	}
	return create(math.MinInt32, math.MaxInt32)
}

func bstFromPreorder_1008_stack(pre []int) *TreeNode {
	st := Stack{}
	root := &TreeNode{Val: pre[0]}
	st.Push(root)

	for i := 1; i < len(pre); i++ {
		node := st.Peek().(*TreeNode)
		cur := &TreeNode{Val: pre[i]}

		for st.Len() > 0 && st.Peek().(*TreeNode).Val < cur.Val {
			node = st.Pop().(*TreeNode)
		}

		if node.Val < cur.Val {
			node.Right = cur
		} else {
			node.Left = cur
		}
		st.Push(cur) //child on the stack, since it is BST, it is the next to be validated.
	}

	return root
}

/**
 * https://leetcode.com/problems/find-mode-in-binary-search-tree/
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
//could also use morris algo.
//https://leetcode.com/problems/find-mode-in-binary-search-tree/discuss/98101/Proper-O(1)-space

//https://leetcode.com/problems/find-mode-in-binary-search-tree/discuss/98100/Java-4ms-Beats-100-Extra-O(1)-solution-No-Map
func findMode_501(root *TreeNode) []int {
	//inorder?
	var modes []int
	cnt, maxCnt := 0, 0
	prev := []*TreeNode{root}
	inorder(root, &cnt, &maxCnt, &modes, prev)

	var res []int
	for _, v := range modes {
		res = append(res, v)
	}
	return res
}

func inorder(node *TreeNode, cnt, maxCnt *int, modes *[]int, prev []*TreeNode) {
	if node == nil {
		return
	}
	inorder(node.Left, cnt, maxCnt, modes, prev)
	if prev[0].Val == node.Val {
		*cnt++
	} else {
		*cnt = 1
	}

	if *cnt == *maxCnt {
		*modes = append(*modes, node.Val)
	} else if *cnt > *maxCnt {
		//refresh for the new modes
		*modes = []int{}
		*modes = append(*modes, node.Val)
		*maxCnt = *cnt
	}
	prev[0] = node
	inorder(node.Right, cnt, maxCnt, modes, prev)
}

//https://leetcode.com/problems/univalued-binary-tree/
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
//A binary tree is uni-valued if every node in the tree has the same value.
//
//Given the root of a binary tree, return true if the given tree is uni-valued, or false otherwise.
//
//
//Example 1:
//
//
//Input: root = [1,1,1,1,1,null,1]
//Output: true
//
//Example 2:
//
//
//Input: root = [2,2,2,5,2]
//Output: false
//
//
//Constraints:
//
//The number of nodes in the tree is in the range [1, 100].
//0 <= Node.val < 100
//
func isUnivalTree_965_v2(root *TreeNode) bool {
	if root == nil {
		return true
	}
	if root.Left != nil && root.Left.Val != root.Val {
		return false
	}
	if root.Right != nil && root.Right.Val != root.Val {
		return false
	}
	return isUnivalTree_965_v2(root.Left) && isUnivalTree_965_v2(root.Right)
}
//******************************************************************************************
func isUnivalTree_965(root *TreeNode) bool {
	if root == nil {
		return true
	}
	return walk(root, root.Val)
}

func walk(node *TreeNode, prev int) bool {
	if node == nil {
		return true
	}

	if node.Val != prev {
		return false
	}

	l := walk(node.Left, prev)
	r := walk(node.Right, prev)

	return  l && r
}
//******************************************************************************************
