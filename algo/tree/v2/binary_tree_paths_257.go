package main

import (
	"fmt"
	. "gitlab.com/iampolo/goalgo/algo/model"
	"strconv"
)

// https://leetcode.com/problems/binary-tree-paths/
func binaryTreePaths(root *TreeNode) []string {
	ch := make(chan string)

	// slower
	go func() {
		defer close(ch)
		dfs(root, "", ch)
	}()
	paths := []string{}
	for s := range ch {
		paths = append(paths, s)
	}
	return paths
}

func dfs(node *TreeNode, cur string, ch chan string) {
	if node == nil {
		return
	}

	if node.Left == nil && node.Right == nil {
		ch <- cur + strconv.Itoa(node.Val)
	}

	s2 := fmt.Sprintf("%d->", node.Val)
	dfs(node.Left, cur+s2, ch)
	dfs(node.Right, cur+s2, ch)
}

func main() {
	arr := []interface{}{1, 2, 3, 4, 5}
	root := Array2BinaryTree(arr)

	fmt.Println(binaryTreePaths(root))
}
