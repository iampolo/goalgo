package main

//https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree-iii/
/**
 * Definition for Node.
 * type Node struct {
 *     Val int
 *     Left *Node
 *     Right *Node
 *     Parent *Node
 * }
 */

type Node struct {
	Val    int
	Left   *Node
	Right  *Node
	Parent *Node
}

func lowestCommonAncestor(p *Node, q *Node) *Node {
	set := make(map[*Node]bool)
	for p != nil {
		set[p] = true
		p = p.Parent
	}

	for q != nil {
		if _, exist := set[q]; exist {
			return q
		}
		q = q.Parent
	}
	return nil
}

//IntersectionOfTwoLinkedLists160

func lowestCommonAncestor_v3(p *Node, q *Node) *Node {


	return nil
}

//******************************************************************************
func lowestCommonAncestor_v2(p *Node, q *Node) *Node {
	//find how far to the root of the tree
	length := func(n *Node) int {
		var cnt int
		for n != nil {
			cnt++
			n = n.Parent
		}
		return cnt
	}
	pl, ql := length(p), length(q)
	if pl < ql {
		return find_lca(p, q, ql-pl)
	} else {
		return find_lca(q, p, pl-ql)
	}
}

func find_lca(shorter, longer *Node, diff int) *Node {
	for diff > 0 { //match the height of longer one to the that of the shorter one
		diff--
		longer = longer.Parent
	}

	for shorter != longer {
		shorter = shorter.Parent
		longer = longer.Parent
	}
	return shorter
}
