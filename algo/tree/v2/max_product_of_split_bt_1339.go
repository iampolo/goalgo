package main

import (
	. "gitlab.com/iampolo/goalgo/algo/model"
)

// https://leetcode.com/problems/maximum-product-of-splitted-binary-tree/discuss/?currentPage=1&orderBy=most_votes&query=

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

// sum * (totalSum - sum)
// O(n)
func maxProduct(root *TreeNode) int {
	//
	var treeSum func(node *TreeNode, totalSum int64, res *int64) int64
	treeSum = func(node *TreeNode, totalSum int64, res *int64) int64 {
		if node == nil {
			return 0
		}

		left := treeSum(node.Left, totalSum, res)
		right := treeSum(node.Right, totalSum, res)
		lvlSum := int64(node.Val) + left + right
		if lvlSum*(totalSum-lvlSum) > (*res) {
			(*res) = lvlSum * (totalSum - lvlSum)
		}
		return lvlSum
	}

	//
	var res int64
	//O(n)
	totalSum := treeSum(root, 0, &res)

	treeSum(root, totalSum, &res)
	return int(res % ((1e9) + 7))
}
