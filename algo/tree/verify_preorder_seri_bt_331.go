package main

import (
	"strings"
)

// https://leetcode.com/problems/verify-preorder-serialization-of-a-binary-tree/
// One way to serialize a binary tree is to use preorder traversal. When we encounter a non-null node, we record the
// node's value. If it is a null node, we record using a sentinel value such as '#'.
//
//
// For example, the above binary tree can be serialized to the string "9,3,4,#,#,1,#,#,2,#,6,#,#", where '#' represents a null node.
//
// Given a string of comma-separated values preorder, return true if it is a correct preorder traversal serialization of a binary tree.
//
// It is guaranteed that each comma-separated value in the string must be either an integer or a character '#' representing null pointer.
//
// You may assume that the input format is always valid.
//
// For example, it could never contain two consecutive commas, such as "1,,3".
// Note: You are not allowed to reconstruct the tree.
//
//
//
//Example 1:
//
//Input: preorder = "9,3,4,#,#,1,#,#,2,#,6,#,#"
//Output: true
//
//Example 2:
//
//Input: preorder = "1,#"
//Output: false
//
//Example 3:
//
//Input: preorder = "9,#,#,1"
//Output: false
//
//
//Constraints:
//
//1 <= preorder.length <= 104
//preoder consist of integers in the range [0, 100] and '#' separated by commas ','.

// https://leetcode.com/problems/verify-preorder-serialization-of-a-binary-tree/discuss/78566/Java-intuitive-22ms-solution-with-stack
func isValidSerialization_stack(preorder string) bool {
	st := []byte{}

	nodes := strings.Split(preorder, ",")
	for _, n := range nodes {
		for n == "#" && len(st) > 0 && string(st[len(st)-1]) == n {
			st = st[:len(st)-1]
			if len(st) == 0 {
				return false
			}
			st = st[:len(st)-1]
		}
		st = append(st, []byte(n)[0])
	}
	//the last '#' will stay in the stack for a valid tree
	return len(st) == 1 && st[0] == '#'
}

func isValidSerialization_v2(preorder string) bool {
	placeCnt := 1 //there must be an incoming
	for i, v := range preorder {
		if v != ',' {
			continue
		}

		placeCnt--
		if placeCnt < 0 {
			return false
		}
		if preorder[i-1] != '#' {
			placeCnt += 2 //need two more spaces for the children
		}
	}
	length := len(preorder)
	if preorder[length-1] == '#' {
		placeCnt--
	} else {
		placeCnt++ //add whatever should achieve the same
	}

	return placeCnt == 0 //has all the spaces used up?
}

func isValidSerialization(preorder string) bool {
	nodes := strings.Split(preorder, ",")
	placeCnt := 1 //there must be an incoming
	for _, v := range nodes {
		//consuming nodes, or placing the node to a virtual tree, the V.T. has a spot at the beginning
		placeCnt--
		if placeCnt < 0 {
			return false
		}
		if v != "#" {
			placeCnt += 2 //need two more spaces for the children
		}
	}

	return placeCnt == 0 //has all the spaces used up?
}
