package main

import "fmt"

func palindromePairs(words []string) [][]int {

	dict := make(map[string]int)
	for i, s := range words {
		dict[s] = i
	}

	res := make([][]int, 0)
	for i := 0; i < len(words); i++ {
		for j := 0; j <= len(words[i]); j++ {
			left := words[i][0:j]
			right := words[i][j:]
			if IsPalindrome(left) {
				rev := Reverse(right)
				if k, ok := dict[rev]; ok && k != i {
					res = append(res, []int{k, i})
				}
			}

			if IsPalindrome(right) {
				if len(right) == 0 {
					continue
				}

				rev := Reverse(left)
				if k, ok := dict[rev]; ok && k != i {
					res = append(res, []int{i, k})
				}
			}
		}
	}

	return res
}

func IsPalindrome(s string) bool {
	l, r := 0, len(s)-1

	for l < r {
		if s[l] != s[r] {
			return false
		}
		l++
		r--
	}
	return true
}

func Reverse(s string) (res string) {
	for _, v := range s {
		res = string(v) + res
	}
	return
}
func main() {
	words := []string{"abcd", "dcba", "lls", "s", "sssll"}

	//words = []string{"a", ""}

	fmt.Println(palindromePairs(words))
}
