package main

import (
	"fmt"
	"sort"
)

func suggestedProducts(products []string, searchWord string) [][]string {
	sort.Strings(products)

	root := &Trie{}

	for _, prod := range products {

		cur := root
		for i := 0; i < len(prod); i++ {
			idx := prod[i] - 'a'

			if cur.Next[idx] == nil {
				cur.Next[idx] = &Trie{}
			}
			cur = cur.Next[idx]
			if len(cur.Suggest) < 3 {
				cur.Suggest = append(cur.Suggest, prod)
			}
		}
	}

	res := make([][]string, 0)
	cur := root
	for i := 0; i < len(searchWord); i++ {
		c := searchWord[i]
		if cur != nil {
			cur = cur.Next[c-'a']
		}
		if cur != nil {
			res = append(res, cur.Suggest)
		} else {
			res = append(res, make([]string, 0))
		}
	}

	return res
}

func main_() {
	words := []string{"mobile", "mouse", "moneypot", "monitor", "mousepad"}

	res := suggestedProducts(words, "xxx")

	fmt.Println(res)
}

type Trie struct {
	Next    [26]*Trie
	Suggest []string
}
