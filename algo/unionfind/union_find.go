package unionfind

//https://leetcode.com/problems/max-area-of-island/discuss/711156/golang-unionfind-100-fast
type UnionFind struct {
	Ids  []int
	Rank []int
	Cnt  int
}

func NewUnionFind(len int) *UnionFind {
	return &UnionFind{
		Ids:  make([]int, len),
		Rank: make([]int, len),
		Cnt:  len,
	}
}

func (uf *UnionFind) Union(p, q int) {
	pi, qi := uf.Find(p), uf.Find(q)

	if pi == qi {
		return
	}

	if uf.Rank[pi] < uf.Rank[qi] {
		uf.Ids[pi] = qi
		uf.Rank[qi] += uf.Rank[pi]
	} else {
		uf.Ids[qi] = pi
		uf.Rank[pi] += uf.Rank[qi]
	}
	uf.Cnt--
}

func (uf *UnionFind) IsConnected(p, q int) bool {
	return uf.Find(p) == uf.Find(q)
}

func (uf *UnionFind) Find(p int) int {
	for p != uf.Ids[p] {
		uf.Ids[p] = uf.Ids[uf.Ids[p]]
		p = uf.Ids[p]
	}
	return p
}
