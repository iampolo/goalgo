package util
/*
https://husobee.github.io/heaps/golang/safe/2016/09/01/safe-heaps-golang.html
https://ieevee.com/tech/2018/01/29/go-heap.html



 */
//minheap
type IntHeap []int

func (h IntHeap) Len() int {
	return len(h)
}

func (h IntHeap) Less(i, j int) bool {
	return h[i] < h[j]
}

func (h IntHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n - 1]
	*h = old[0 : n-1]
	return x
}
