package util

import (
	"gitlab.com/iampolo/goalgo/algo/model"
	"sort"
)

func ReverseString(s string) string {
	str := []rune(s)
	for i, j := 0, len(str)-1; i < j; i, j = i+1, j-1 {
		str[i], str[j] = str[j], str[i];
	}
	return string(str)
}

//https://stackoverflow.com/questions/37695209/golang-sort-slice-ascending-or-descending/40932847
func SortDesc(nums []int) []int {
	sort.Slice(nums, func(i, j int) bool {
		return nums[i] > nums[j]
	})

	return nums
}

func SortAsc(nums []int) []int {
	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	return nums
}

func HasPrefix(s, prefix string) bool {
	return len(s) >= len(prefix) && s[:len(prefix)] == prefix
}

func HasSuffix(s, suffix string) bool {
	return len(s) >= len(suffix) && s[len(s)-len(suffix):] == suffix
}

func Contains(s, substr string) bool {
	for i := 0; i < len(s); i++ {
		if HasPrefix(s[i:], substr) {
			return true
		}
	}
	return false
}

func CreateList_(array []int) *model.ListNode {
	head := new(*model.ListNode)
	curr := head
	for e := range array {
		if *curr == nil {
			*curr = new(model.ListNode)
		}
		(**curr).Val = array[e]
		curr = &((*curr).Next)
	}
	return *head
}