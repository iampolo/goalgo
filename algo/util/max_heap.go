package util

type MaxHeap []int

func (h *MaxHeap) Less(i, j int) bool {
	return (*h)[i] > (*h)[j]
}

func (h *MaxHeap) Swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
}

func (h *MaxHeap) Len() int {
	return len(*h)
}

func (h *MaxHeap) Push(v interface{}) {
	*h = append(*h, v.(int))
}

func (h *MaxHeap) Pop() (v interface{}) {
	//the minimum one is at the end
	*h, v = (*h)[1:], (*h)[0]
	return v
}
