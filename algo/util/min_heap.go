package util

type MinHeap []int

func (h *MinHeap) Less(i, j int) bool {
	return (*h)[i] < (*h)[j]
}

func (h *MinHeap) Swap(i, j int) {
	(*h)[i], (*h)[j] = (*h)[j], (*h)[i]
}

func (h *MinHeap) Len() int {
	return len(*h)
}

func (h *MinHeap) Push(v interface{}) {
	*h = append(*h, v.(int))
}

func (h *MinHeap) Pop() (v interface{}) {
	//the minimum one is at the end
	*h, v = (*h)[:h.Len()-1], (*h)[h.Len()-1]
	return v
}
