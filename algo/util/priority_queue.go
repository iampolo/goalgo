package util

//https://husobee.github.io/heaps/golang/safe/2016/09/01/safe-heaps-golang.html
//https://hackernoon.com/today-i-learned-using-priority-queue-in-golang-6f71868902b7

type Item struct {
	Value    interface{}
	Priority int
	Index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Priority < pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].Index = i
	pq[j].Index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	item := x.(*Item)     //casting
	item.Index = len(*pq) //the latest Index is the length
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	item := old[len(old)-1]
	item.Index = -1 //TODO

	*pq = old[0 : len(old)-1]
	return item
}
