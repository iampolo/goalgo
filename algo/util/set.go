package util

//https://yourbasic.org/golang/implement-Set/
//https://stackoverflow.com/questions/34018908/golang-why-dont-we-have-a-set-datastructure

//Set
//https://www.davidkaya.com/Sets-in-golang/
//https://github.com/fatih/Set
type Void struct{} //struct{} takes 0 bytes

var Empty Void

///
type Setv2 struct { //test only
	*Set
}

type Set struct {
	m map[interface{}]struct{}
}

var KeyExist struct{}

func NewSet() *Set {
	s := &Set{}
	s.m = make(map[interface{}]struct{})
	return s
}

func (s *Set) Add(val interface{}) {
	s.m[val] = KeyExist
}

func (s *Set) Remove(val interface{}) {
	delete(s.m, val)
}

func (s Set) Contains(val interface{}) bool {
	_, c := s.m[val]
	return c
}

func (s *Set) Clean() {
	s.m = make(map[interface{}]struct{})
}

func (s Set) Size() int {
	return len(s.m)
}

func (s Set) Pop() interface{} {
	for item := range s.m {
		delete (s.m, item)
		return item
	}
	return nil

}
func (s Set) Peek() interface{} {
	for item := range s.m {
		return item
	}
	return nil
}
