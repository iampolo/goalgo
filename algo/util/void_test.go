package util

import (
	"fmt"
	"testing"
)

func TestSet(t *testing.T) {

	type void struct{}
	var member void

	set := make(map[string]void)

	set["Foo"] = member
	for k := range set {
		fmt.Println(k)
	}
}
