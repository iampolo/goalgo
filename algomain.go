package main

import (
	"fmt"
	"github.com/blang/semver"
)

func Hi() string {
	return "hi p:"  + Program
}


//go build -ldflags "-X main.Program=XXKKK"
var Program string

func main() {
	fmt.Println("hi", Hi())

	doSemVer()
}

func doSemVer() {
	ranges := []string{">=3.3.0 <3.4.0", ">=3.3 <3.4"}
	v := "3.3.1"
	_ = v
	for _, r := range ranges {
		inRange, err := semver.ParseRange(r)
		fmt.Println(r, ":", err)
		if err != nil {
			continue
		}
		version := semver.MustParse(v)

		// Check if the version is in the range
		fmt.Printf("%s within range? %t \n", v, inRange(version))
	}
}