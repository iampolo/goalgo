package main

import (
	"fmt"
	"github.com/goalgo/recursion"
)

func main() {
	board := []string {" X ", " O ", "   ",}
	res := recursion.ValidTicTacToe(board)
	fmt.Println(res)
}