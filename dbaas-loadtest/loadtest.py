#!/usr/bin python3.10

import htpasswd
import argparse
from os.path import exists
from pathlib import Path
import openshift as oc
import random
from multiprocessing import Process

parser = argparse.ArgumentParser(prog='loadtest', description="Program to loadtest dbaas operator")
parser.add_argument("--task",
                    choices=['createprojects', 'deleteprojects',
                             'giveadminrights','removeadminrights',
                             'createdbaastenants', 'removedbaastenants',
                             'createrandomobjects'],
                    nargs='?',
                    help='Select what to task perform',
                    )
parser.add_argument("--from", help="Where to start from", required=True, type=int)
parser.add_argument("--to", help="Where to end", required=True, type=int)
parser.add_argument("--file", help="Enter htpasswd file path")
parser.add_argument("--adminpass", help="Enter admin password")

args = vars(parser.parse_args())
print("Input values are {}".format(args))

to_target = args["to"]
from_target = args["from"]


def _get_role_binding_dict(user_name, project_name):
    return {
        "apiVersion": "rbac.authorization.k8s.io/v1",
        "kind": "RoleBinding",
        "metadata": {
            "name": "admin-0",
            "namespace": project_name
        },
        "roleRef": {
            "apiGroup": "rbac.authorization.k8s.io",
            "kind": "ClusterRole",
            "name": "admin"
        },
        "subjects": [
            {
                "apiGroup": "rbac.authorization.k8s.io",
                "kind": "User",
                "name": user_name
            }
        ]
    }


def _get_dbaas_tenant(user_name, project_name):
    return {
        "apiVersion": "dbaas.redhat.com/v1alpha1",
        "kind": "DBaaSTenant",
        "metadata": {
            "name": project_name
        },
        "spec": {
            "inventoryNamespace": project_name
        }
    }


def _get_rand_deployment(user_name, project_name):
    return {
        "kind": "Deployment",
        "apiVersion": "apps/v1",
        "metadata": {
            "name": "fake-nginx-" + user_name,
            "namespace": project_name,
            "labels": {
                "auto": "true",
                "app": "nginx-"+user_name
            }
        },
        "spec": {
            "replicas": 1,
            "selector": {
                "matchLabels": {
                    "app": "nginx-"+user_name
                }
            },
            "template": {
                "metadata": {
                    "labels": {
                        "app": "nginx-"+user_name,
                        "auto": "true"
                    }
                },
                "spec": {
                    "containers": [
                        {
                            "name": "deployed-pod",
                            "imagePullPolicy": "IfNotPresent",
                            "image": "docker.io/library/busybox:latest",
                            "command": [
                                "sleep",
                                "3600"
                            ],
                            "resources": {}
                        }
                    ]
                }
            },
            "strategy": {}
        },
        "status": {}
    }


def _get_rand_pod(user_name, project_name):
    return {
        "kind": "Pod",
        "apiVersion": "v1",
        "metadata": {
            "name": "busybox-" + user_name,
            "namespace": project_name,
            "labels": {
                "auto": "true",
                "run": "busybox-" + user_name
            }
        },
        "spec": {
            "containers": [
                {
                    "name": "busybox",
                    "imagePullPolicy": "IfNotPresent",
                    "image": "docker.io/library/busybox:latest",
                    "command": [
                        "sleep",
                        "3600"
                    ],
                    "resources": {}
                }
            ],
            "restartPolicy": "Always",
            "dnsPolicy": "ClusterFirst"
        },
        "status": {}
    }


def _get_rand_secret(user_name, project_name):
    return {
        "kind": "Secret",
        "apiVersion": "v1",
        "metadata": {
            "name": "secret-" + user_name,
            "namespace": project_name,
            "labels": {
                "auto": "true",
            }
        },
        "data": {
            "rubbish": "c2VjcmV0"
        }
    }


def _get_rand_config_map(user_name, project_name):
    return {
        "kind": "ConfigMap",
        "apiVersion": "v1",
        "metadata": {
            "name": "cm-"+user_name,
            "namespace": project_name,
            "labels": {
                "auto": "true",
            }
        },
        "data": {
            "key1": "config1"
        }
    }


def _process_batch(start_user_index, end_user_index):
    print("Launched batch {} to {} for task {}".format(start_user_index, end_user_index, args['task']))
    for user_index in range(start_user_index, end_user_index + 1, 1):
        user_name = "user-" + str(user_index)

        for project_index in range(1, 3, 1):
            project_name = "user-" + str(user_index) + "-project-" + str(project_index)
            match args['task']:
                case "createprojects":
                    oc.new_project("user-" + str(user_index) + "-project-" + str(project_index))
                case "deleteprojects":
                    oc.delete_project("user-" + str(user_index) + "-project-" + str(project_index))
                case "giveadminrights":
                    role_binding = _get_role_binding_dict(user_name, project_name)
                    oc.apply(role_binding)
                case "removeadminrights":
                    role_binding = _get_role_binding_dict(user_name, project_name)
                    oc.delete(role_binding, ignore_not_found=True)
                case "createdbaastenants":
                    dbaas_tenant = _get_dbaas_tenant(user_name, project_name)
                    oc.apply(dbaas_tenant)
                case "removedbaastenants":
                    dbaas_tenant = _get_dbaas_tenant(user_name, project_name)
                    oc.delete(dbaas_tenant, ignore_not_found=True)
                case "createrandomobjects":
                    deploy_template = _get_rand_deployment(user_name, project_name)
                    secret_template = _get_rand_secret(user_name, project_name)
                    configmap_template = _get_rand_config_map(user_name, project_name)
                    pod_template = _get_rand_pod(user_name, project_name)
                    options = ["deploy", "secret", "configmap", "pod"]
                    random_choose_object = random.choice(options)+"_template"
                    oc.apply(locals()[random_choose_object])
                    # oc.apply(deploy_template)

    print("Processed batch {} to {} for task {}".format(start_user_index, end_user_index, args['task']))


if __name__ == '__main__':
    if args["file"]:
        filename = args["file"]
        # if htpasswd file doesn't exist in local dir then create one.
        if not exists(filename):
            Path(filename).touch()
        with htpasswd.Basic(filename, mode="md5") as userdb:
            for index in range(from_target, to_target + 1, 1):
                try:
                    userdb.add("user-" + str(index), "user" + str(index))
                except htpasswd.basic.UserExists as error:
                    print(error)
    else:
        print("Projects from {} to {} will be processed for task {}".format(from_target, to_target, args['task']))
        processes = []
        batch_size = 50
        while to_target - from_target > batch_size:
            process = Process(target=_process_batch, args=(from_target, from_target+batch_size))
            process.start()
            processes.append(process)
            from_target += batch_size + 1
        else:
            process = Process(target=_process_batch, args=(from_target, to_target))
            process.start()
            processes.append(process)

        for p in processes:
            p.join()
