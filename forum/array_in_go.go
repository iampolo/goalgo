package main

import "fmt"


func multi_dimension (){
	grid := [2][2] int {{3,5}, {1,2}}
	fmt.Println(grid)

	var a = [5][2]int{ {0,0}, {1,2}, {2,4}, {3,6},{4,8}}
	for i := 0; i < len(a); i++ {

		for j := 0; j < len(a[i]); j++ {
			fmt.Print(a[i][j], " ")
		}
		fmt.Println(" ")
	}

}

func regular_array() {
	daysOfWeek := [7]string{"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}

	for idx, val := range daysOfWeek {
		fmt.Printf("day %d of week = %s \n", idx, val)
	}


	a := []int{1,2,3}
	fmt.Println(a, len(a))

	for _, val := range a {
		fmt.Println(val)
	}
}

func slice_() {
	names := [] string{}
	names = append(names, "John");

	names = append(names, "Pete ", " Jauks")
	fmt.Println(names)
}


func main() {
	slice_()
}
