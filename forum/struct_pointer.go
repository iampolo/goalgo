package main

import (
	"fmt"
)
//  https://www.callicoder.com/golang-structs/

type Person struct  {
	FirstName, LastName string
	Age int
}
var p = Person{LastName: "polo"}

func main() {
	fmt.Println(p)

	e := Person{}

	fmt.Println(e)
	changeit(&e)

	fmt.Println(e)

	ee := new(Person)
	ee.Age = 10

	fmt.Println(*ee)
	fmt.Println(ee)
}

func changeit(e *Person) {

	(*e).FirstName = "John"
}