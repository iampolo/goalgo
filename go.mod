module github.com/goalgo

require (
	github.com/gophercloud/gophercloud v0.1.0 // indirect
	github.com/headfirstgo/keyboard v0.0.0-20170926053303-9930bcf72703
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/openshift/custom-resource-status v0.0.0-20190822192428-e62f2f3b79f3
	github.com/openshift/ocs-operator v0.0.0-20190827122919-686f884f5440
	github.com/pelletier/go-toml v1.4.0
	github.com/rook/rook v1.0.5
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586 // indirect
	k8s.io/api v0.0.0-20190820101039-d651a1528133
	k8s.io/apimachinery v0.0.0-20190822052848-2ef880f74d2a
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/utils v0.0.0-20190809000727-6c36bc71fc4a // indirect
	sigs.k8s.io/controller-runtime v0.2.0
)
