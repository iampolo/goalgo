package main

import (
	"bufio"
	"fmt"
	hf "github.com/headfirstgo/keyboard"
	"log"
	"os"
	"strconv"
	"strings"
)

func getFloat() (float64, error) {
	reader := bufio.NewReader(os.Stdin)
	input, err := reader.ReadString('\n')
	if err != nil {
		return 0, err
	}
	input = strings.TrimSpace(input)
	number, err := strconv.ParseFloat(input, 64)
	if err != nil {
		return 0, err
	}
	return number, nil
}

func main() {
	
	fmt.Println("Enter a grade: ")
	grade, err := hf.GetFloat()
	if err != nil {
		log.Fatal(err)
	}

	var status string
	if grade >= 60 {
		status = "P"
	} else {
		status = "F"
	}
	fmt.Println(" grade: ", grade, " is " , status)

}
