#### Informer
* https://www.cncf.io/blog/2019/10/15/extend-kubernetes-via-a-shared-informer/
* https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/ch03.html#rest-client-config
* https://learning.oreilly.com/library/view/extending-kubernetes-elevate/9781484270950/html/503015_1_En_4_Chapter.xhtml
```shell
    https://github.com/onuryilmaz/multi-platform-go-build/blob/master/Dockerfile
    docker build -f Dockerfile.go.build -t quay.io/myeung/go-build-platform:v0.1 .

    docker run -v "$(pwd)":/go/src/in_cluster_client -it quay.io/myeung/go-build-platform:v0.1 bash 
    $ cd src/in_cluster_client
    $ export GOOS=linux
    $ go build -v
```

* go docker builder example
  * https://github.com/onuryilmaz/prometheus-pushgateway-cleaner/blob/main/Dockerfile
  
* informer
```shell
$ in_cluster_client
$ go run .
```
