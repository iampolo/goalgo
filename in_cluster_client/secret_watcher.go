package main

import (
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"time"
)

// https://learning.oreilly.com/library/view/extending-kubernetes-elevate/9781484270950/html/503015_1_En_4_Chapter.xhtml
func doSecretWatch() {
	clientset, err := NewK8sClient().GetClientSet()
	if err != nil {
		panic(err.Error())
	}
	for {
		secrets, err := clientset.CoreV1().Secrets("").List(context.TODO(), metav1.ListOptions{})
		if err != nil {
			panic(err.Error())
		}

		fmt.Printf("There are %d secrets in the cluster\n", len(secrets.Items))
		time.Sleep(10 * time.Second)
	}
}
