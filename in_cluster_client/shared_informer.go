package main

import (
	"fmt"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/tools/cache"
)

const (
	K8S_LABEL_AWS_REGION = "failure-domain.beta.kubernetes.io/region"
)

func onAdd(obj interface{}) {
	node := obj.(*corev1.Node)
	_, ok := node.GetLabels()[K8S_LABEL_AWS_REGION]
	if ok {
		fmt.Println("It has the label", node.Name)
	}
}

func LoadSharedInformer() {
	clientset, err := NewK8sClient().GetClientSet()
	if err != nil {
		panic(err.Error())
	}

	factory := informers.NewSharedInformerFactory(clientset, 0)
	informer := factory.Core().V1().Nodes().Informer()
	stopper := make(chan struct{})
	defer close(stopper)
	defer runtime.HandleCrash()
	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc: onAdd,
	})
	go informer.Run(stopper)
	if !cache.WaitForCacheSync(stopper, informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	<-stopper

}
