package main

import (
	"fmt"

	bsv "github.com/blang/semver"
	gosv "golang.org/x/mod/semver"

	"gitlab.com/iampolo/goalgo/algo/model"
	"sort"
	"strings"
	"testing"
)



func TestBAc(t *testing.T) {
	var node *model.ListNode
	_ = node

}

func startUp(item *model.ListNode) int {
	return 0
}
var Ocp4Versions = []string{"4.14", "4.13", "4.2", "4.1"}

func TestSort_v2(t *testing.T) {
	sort.Sort(sort.Reverse(sort.StringSlice(Ocp4Versions)))

	for _, v := range Ocp4Versions  {
		fmt.Println(v)
	}
	ocpVersion := "4.11.0"
	splitVersion := strings.Split(strings.TrimPrefix(ocpVersion, "v"), ".")
	fmt.Println(splitVersion)

	ocpVersion = "v4.11.0-0+rc.1"
	ver := bsv.MustParse(strings.TrimPrefix(ocpVersion,"v"))
	fmt.Println(ver)
	fmt.Println(gosv.Prerelease(ocpVersion))
}