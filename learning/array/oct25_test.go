package main

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestName(t *testing.T) {
	arr := [5]*int{0: new(int), 1: new(int)} //new() to get a address of the int
	fmt.Println(arr)

	fmt.Println(*arr[1])
}

func TestMultiDimension(t *testing.T) {
	var array = [4][2]int{{1, 2}, {3, 4}}
	fmt.Println(array)

	var a [16]int
	testCopy(&a)

	fmt.Println("a: ", a)
}

func testCopy(array *[16]int) {
	fmt.Println(array)
	array[2] = 10
	array = nil
}

func TestSlice(t *testing.T) {
	slice := [] int{1, 2, 3, 4, 5} //
	newslice := slice[1:3]         //length : 3 - 1, cap 5 - 1

	fmt.Println(newslice)
	newslice = append(newslice, 6)
	fmt.Println(newslice)
}

func TestSlice2(t *testing.T) {
	slice := [][]int{{10}, {102, 104}}

	fmt.Println(slice[1][0:2])
	assert.Panics(t, func() { doChange(slice)})
}

func doChange(m [][]int) {
	fmt.Println(m[0][0:2])
}

func TestMap(t *testing.T) {
	var dict map[int][]string

	fmt.Println("map: ",dict)
}
