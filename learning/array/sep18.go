package main

import (
	"fmt"
)
//https://www.geeksforgeeks.org/golang-pointer-to-an-array-as-function-argument/
/**
Note: In Golang it is not recommended to use Pointer to an Array as an Argument to Function as the code
become difficult to read. Also, it is not considered a good way to achieve this concept. To achieve this
you can use slice instead of passing pointers.
 */
func update(arr *[5] int) {

	(*arr)[2] = 999
}

func main() {
	arr := [5]int {1,2,3,4,5} //array
	update(&arr)

	fmt.Println(arr)


	sli := []int{7,8,9,10,11} //slice
	updateSlice(sli)
	fmt.Println(sli)
}

func updateSlice(sli []int) { //but this does support add new item
	sli[2] = 888
	sli = append(sli, 999)
	fmt.Println(sli)
}

