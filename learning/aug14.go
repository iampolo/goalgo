package learning

import (
	"fmt"
	"reflect"
)

func RunMe() {
	xs := []int{1,2,34}

	next := makEven()
	for _,x := range xs {
		fmt.Println(next(),  x)

	}
	var k bool = true
	fmt.Println(k,"--", reflect.TypeOf(k))

	pt := &xs

	fmt.Println("pt",*pt)
}


func avg(xs []float64) float64 {
	var ans float64
	for i, v := range xs {
		ans += float64(v)
		fmt.Println(i , v)
	}
	Gcd(10,20)
	return ans / float64(len(xs))
}

func add(nums ...int) (int, int) {
	for _, x := range nums {

		fmt.Print(x, " ")
	}
	return 0,0
}

func makEven() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}
