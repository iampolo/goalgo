package learning

import (
	"fmt"
	"math"
	"reflect"
)

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	return math.Sqrt(a*a + b*b)
}
func rectangleArea(x1, y1, x2, y2 float64) float64 {
	l := distance(x1, y1, x1, y2)
	w := distance(x1, y1, x2, y1)
	return l * w
}
func circleArea(c *Circle) float64 {
	return math.Pi * c.r * c.r
}
func Mainch7() {
	var rx1, ry1 float64 = 0, 0
	var rx2, ry2 float64 = 10, 10
	var cx, cy, cr float64 = 0, 0, 5
	k := &Circle{cx, cy, cr};
	fmt.Println(rectangleArea(rx1, ry1, rx2, ry2))
	fmt.Println(k.area())

	c := Circle{x: 500045}
	d := new(Circle)

	fmt.Println(c)
	fmt.Println("classic:", c.area(), reflect.TypeOf(*d))

	r := Rectangle{}
	fmt.Print(r.area())
}
type Circle struct  {
	x,y,r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	return l
}
func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

//###################################################################################
type Shape interface {
	area() float64
}

func TotalArea(circle []Circle, rec []Rectangle) float64{
	fmt.Println('A')
	for _, c := range circle {
		fmt.Print("in total:", c.r)
	}
	return 0;
}
func TotalAreaInter(shapes ...Shape) float64 {
	fmt.Println("calling with interface")
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area;
}

type MultiShape struct {
	shapes []Shape
}

func (m *MultiShape) area() float64 {
	return -10001.1245;
}


func TestMultiShapeInfer() {
	var m map[string]bool

	for k, _ := range m {
		fmt.Println(k)
	}

	multiShape := MultiShape{
		shapes: []Shape{
			&Circle{0, 0, 5},
		},
	}
	fmt.Println("-------------------")
	fmt.Println(multiShape)

}

