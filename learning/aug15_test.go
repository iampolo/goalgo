package learning

import "testing"

type testpair struct {
	values[]float64
	average float64
}

var tests = [] testpair{
	{[]float64{1, 2}, 1.5},
	{[]float64{1, 1, 1, 1, 1, 1}, 1},
	{[]float64{-1, 1}, 0},
}

func TestAvgMethod(t *testing.T) {

	for _, pair := range tests {
		v := avg(pair.values)
		if v != pair.average {
			t.Error("expected", pair.average)
		}
	}

}
