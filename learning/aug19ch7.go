package learning

import (
	"fmt"
)

type Person struct {
	Name string
}

func (p *Person) Talk() {
	fmt.Print("hi.......", p.Name)
}

type Android struct {
	Person
	Model string
}

func RunAug19() {
	a := new(Android);
	a.Talk()

}

