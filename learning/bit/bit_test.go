package bit_test

import (
	"fmt"
	"testing"
)

func TestBitWise(t *testing.T) {
	var u int8 = 10
	fmt.Printf("%08b\n", u)
	fmt.Printf("%08b\n", u|1<<2)

	var y uint8 = 1<<3 | 1<<2
	fmt.Printf("%08b", y)
}

