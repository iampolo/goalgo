package main

import "fmt"

type Age int

func (a Age) name() bool {
	return a >= 10 && a <= 35
}

type Celsius float64
type Fahrenheit float64

const (
	AbsoluteZeroC Celsius = -273.15
	FreezingC     Celsius = 0
	BoilingC      Celsius = 100
)

func CToF(c Celsius) Fahrenheit {
	return Fahrenheit(c*9/5 + 32)
}

func main() {
	var c Celsius

	c = 130
	fmt.Println(CToF(c))
	fmt.Println(BoilingC * AbsoluteZeroC)
}
