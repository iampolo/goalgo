package customer

//https://learning.oreilly.com/learning-paths/learning-path-complete/9781838825386/9781789134797-video4_1
import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCustomerMayNot(t *testing.T) {
	sut := Customer{"John", 20}
	mayBuy, err := sut.MayBuyBeer("USA")

	assert.Nilf(t, err, "unexpected error: %s", err)
	assert.Falsef(t, mayBuy, "should  not be %d", sut.Age)
}
func TestUnknownCountryCode(t *testing.T) {
	sut := Customer{"Pete", 23}

	mayBuyBeer, err := sut.MayBuyBeer("FRA")
	assert.NotNil(t, err)
	assert.False(t, mayBuyBeer)
}


