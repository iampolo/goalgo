package customer
//https://learning.oreilly.com/learning-paths/learning-path-complete/9781838825386/9781789134797-video4_3
type CreditScoreProvider interface {
	CreditScoreForCustomer(c *Customer) (int, error)
}

type PaymentMethodProvider struct {
	CSP CreditScoreProvider
}

func (p *PaymentMethodProvider) MethodsForCustomer(c *Customer) ([]string, error) {
	methods := []string{"prepay", "paypal"}
	score, err := p.CSP.CreditScoreForCustomer(c)
	if err != nil {
		return nil, err
	}

	if score >= 80 {
		methods = append(methods, "creditcard")
	}

	if score >= 90 {
		methods = append(methods, "invoice")
	}

	return methods, nil
}

type creditScoreProviderImpl struct {
}

func (p *creditScoreProviderImpl) CreditScoreForCustomer(c *Customer) (int, error) {
	return 0, nil
}
