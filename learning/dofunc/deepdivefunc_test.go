package dofunc

//Deep Dive into Go Functions
import (
	"errors"
	"fmt"
	"gitlab.com/iampolo/goalgo/learning/dofunc/simplemath"
	"io"
	"math"
	"testing"
)

func TestDoFunc(t *testing.T) {
	func() {
		fmt.Println("hello from func")
	}()
	f1 := func() {
		fmt.Println("hello from func - f1")
	}
	f1()

	myFunc := mathExpression()
	fmt.Println(myFunc(10, 22))

}

//returning func
func mathExpression() func(float64, float64) float64 {
	return func(f float64, f2 float64) float64 {
		return f + f2
	}
}

type MathExpr = string

const (
	AddExpr      = MathExpr("add")
	SubtractExpr = MathExpr("subtract")
	MultiplyExpr = MathExpr("multiply")
)

func TestConstFunc(t *testing.T) {
	myFunc := mathExpression()
	fmt.Println("func as param:", doubleIt(11, 23, myFunc))
}

func doubleIt(f1, f2 float64, hereFunc func(float64, float64) float64) float64 {
	return 2 * hereFunc(f1, f2)
}

func TestFindMathFunc(t *testing.T) {
	findMathFunc(SubtractExpr)(3, 2)
}

func findMathFunc(expr MathExpr) func(float64, float64) float64 {
	switch expr {
	case AddExpr:
		return simplemath.Add
	case SubtractExpr:
		return simplemath.Subtract
	default:
		return func(f, f1 float64) float64 {
			return 0.0
		}
	}
}

func TestPowWithValState(t *testing.T) {
	p := pow()

	val := p()
	fmt.Println("pow: ", val)
	val = p()
	fmt.Println("pow: ", val)
}

func pow() func() int64 {
	x := 1.0
	return func() int64 {
		x += 1
		return int64(math.Pow(x, 2))
	}
}

func TestSliceFunc(t *testing.T) {
	var funcs []func() int64
	for i := 0; i < 10; i++ {
		tmp := i
		funcs = append(funcs, func() int64 {
			return int64(math.Pow(float64(tmp), 2))
		})
	}
	for _, f := range funcs {
		fmt.Println(f())
	}
}

func TestErrorOccur(t *testing.T) {
	ReadSomething()
}

func ReadSomething() error {
	var r io.Reader = BadReader{err: errors.New("this is an error msg")}
	if _, err := r.Read([]byte("test reading msg")); err != nil {
		fmt.Println("error happened:", err)
		return err
	}
	return nil
}

type BadReader struct {
	err error
}

// overriding Read()

func (br BadReader) Read(p []byte) (n int, err error) {
	return -1, br.err
}

func TestSimpleReader(t *testing.T) {
	if err := ReadThisFile(); err != nil {
		fmt.Println(err)
	}
}

func ReadThisFile() error {
	var r io.Reader = &SimpleReader{}
	defer func() { // if multiply defers, calls in a Stack(LILO) way.
		fmt.Println("do sth crazy.")

		if p := recover(); p == io.EOF {
			fmt.Println("expected", p)
		} else if p != nil {
			fmt.Println("what a panic!", p)
		}
	}()
	for {
		val, err := r.Read([]byte("text that does nothing"))
		if err == io.EOF {
			fmt.Println("finished reading file, breaking out of loop")
			break
		} else if err != nil {
			return err
		}
		fmt.Println(val)
	}
	return nil
}

type SimpleReader struct {
	count int
}

func (br *SimpleReader) Read(p []byte) (n int, err error) {
	if br.count == 2 {
		panic("somethin bad hit:panic")
	}
	if br.count > 3 {
		return 0, io.EOF
	}
	br.count += 1
	return br.count, nil
}

func TestInnerStruct(t *testing.T) {

	type course struct {
		Author string
		Level  string
	}
	docker := course{"Nicdf John", "beginner"}
	fmt.Println(docker)
}
