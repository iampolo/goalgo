package dofunc

import (
	"fmt"
	"testing"
)

func TestFnnc(t *testing.T) {
	var f func(int) int
	f = func(a int) int {
		return a * a
	}
	fmt.Println(f(22))

}

func squares() func() int {
	//x := 1
	var x int
	return func() int {
		x++
		return x * x
	}
}

func TestInnerFunc(t *testing.T) {
	f := squares()
	fmt.Println(f());
	fmt.Println(f());
	fmt.Println(f());
	fmt.Println(f());

}
