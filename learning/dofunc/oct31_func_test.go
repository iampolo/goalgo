package dofunc

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"testing"
	"time"
)

func TestAnony(t *testing.T) {
	var pnt_i []func()

	for i := 0; i < 5; i++ {
		i := i //what if this line is commented out

		pnt_i = append(pnt_i, func() {
			fmt.Println("i : ", i)
		})
	}

	for _, pnt := range pnt_i {
		pnt()
	}
}
/**********************************************************************************************/
func TestVariadic(t *testing.T) {
	res, err := sum(2, 3, 4, 6, 4)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("sum",  res)

	res, err = sum(1)
	if err != nil {
		fmt.Println("err: ", err)
	}

}

func sum(vars ...int) (int, error) {
	if len(vars) == 0 {
		return -1, errors.New("param not defined.")
	}
	fmt.Println("size : ",  len(vars), vars[0])
	var tot int
	for _, i := range vars {
		tot += i
	}
	return tot,nil
}
/**********************************************************************************************/
func TestWeb(t *testing.T) {
	err := title("https://golang.org")

	fmt.Println(err)
}

func title(url string) error {
	defer trace("title call")()   //!! wow

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()    //!!

	ct := resp.Header.Get("Content-Type")
	if ct != "text/html" && !strings.HasPrefix(ct, "text/html") {
		resp.Body.Close()
		return fmt.Errorf("%s has type %s, not text/html", url, ct)
	}
	fmt.Println(ct)

	//panic(fmt.Sprintf("invalid............ "))

	return nil
}

func trace(s string) func() {
	start := time.Now()
	log.Printf("enter %s", s)
	return func() {log.Printf("exit %s (%s)", s, time.Since(start))}
}

