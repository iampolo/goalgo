package dofunc

import (
	"fmt"
	"golang.org/x/net/html"
	"log"
	"net/http"
	"sort"
	"testing"
	"time"
)

type Currency int

const (
	USD Currency  = iota
	EUR
	GBP
	RMB
)

func TestFunc(t *testing.T) {
	fmt.Println([14]byte{})

	str, err := findLinks("")
	if err == nil {
		fmt.Println(str)
	}
}
func WaitForServer(url string) error {
	const timeout = 1 * time.Minute
	deadline := time.Now().Add(timeout)
	for tries := 0; time.Now().Before(deadline); tries++ {
		_, err := http.Head(url)
		if err == nil {
			return nil
		}
		log.Printf("server not available (%s); retrying ...", err)
		time.Sleep(time.Second << uint(tries))
	}
	return fmt.Errorf("server %s failed to respond after %s", url, timeout)
}

func forEachNode(n *html.Node, pre, post func(n *html.Node)) {
	 if pre != nil {
	 	pre(n)
	 }

}


func Square(n int) int {return n * n}
func TestFuncVal(t *testing.T) {
	f := Square
	fmt.Println("val: ", f(3))
}

func findLinks(url string) ([]string, error) {

	return []string{}, nil
}
/******************************************************************************************/
func TestTopsort(t *testing.T) {
	fmt.Println(len(prereqs["compilers"]))
	for i, course := range topoSort(prereqs) {
		fmt.Printf("topsort res: %d: \t%s\n", i+1, course)
	}
}

func topoSort(m map[string][]string) []string {
	var order []string  //result
	seen := make(map[string]bool)
	var visitAll func(items []string) //variable for the recursive function

	visitAll = func(items []string) {
		for _, item := range items {
			if !seen[item] {
				seen[item] = true
				visitAll(m[item])   //Dfs
				order = append(order, item)
			}
		} //for
	}

	var keys []string
	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)
	visitAll(keys)
	return order
}

var prereqs = map[string][]string{
	"algorithms": {"data structures"},
	"calculus":   {"linear algebra"},
	"compilers": {
		"data structures",
		"formal languages",
		"computer organization",
	},
	"data structures":       {"discrete math"},
	"databases":             {"data structures"},
	"discrete math":         {"intro to programming"},
	"formal languages":      {"discrete math"},
	"networks":              {"operating systems"},
	"operating systems":     {"data structures", "computer organization"},
	"programming languages": {"data structures", "computer organization"},
}
