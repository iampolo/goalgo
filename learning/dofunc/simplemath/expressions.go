package simplemath

func Sum(values ...float64) float64 {
	t := 0.0
	for _, v := range values {
		t += v
	}
	return t
}


func Add(p, q float64) float64 {
	return p + q
}

func Subtract(p, q float64) float64 {
	return p - q
}
