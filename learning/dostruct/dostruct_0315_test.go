package dostruct

import (
	"fmt"
	"testing"
)

func TestDoStruct(t *testing.T) {
	s := struct {i int
				f float32
	} {i:3, f:1.0}
	fmt.Printf("%+v", s)
}
