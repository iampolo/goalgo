package dostruct

import (
	"fmt"
	"math"
	"time"
)

func DoStruct() {
	Peter.Name = "ppppp"

	const day = 24 * time.Hour
	fmt.Print(day.Seconds(), (Peter.Name == "ppppp"))

	fmt.Print("good")
}

type Employee struct {
	Id int
	Name string
	Address string
}

var Peter Employee


func Hypot(x, y float64) float64 {
	return math.Sqrt(x*x + y*y);
}
