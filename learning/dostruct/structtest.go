package dostruct

import (
	"fmt"
	"math"
)

func area_bak(c *Circle) float64 {
	fmt.Println("called 2")
	c.x = 101
	return math.Sqrt(c.x * c.y)
}

func (c *Circle) area() float64 {
	fmt.Println("called")
	c.x = 177
	c.r = 1.11
	return math.Sqrt(c.x * c.r)
}

func (s *Shape) area() float64 {
	return s.length * 1.13
}

func (s *Shape) draw() {
	fmt.Println("Drawing ")
}

func (r *Rectangle) area() float64 {
	l := r.x + r.y
	return l
}

type MultiShape interface {
	area() float64
}

type Shape struct {
	Type   string
	length float64
}

type Circle struct {
	Shape
	x, y, r float64
}

type Rectangle struct {
	x, y float64
}

type MultiShapeShow struct {
	shapes []MultiShape
}

func totalArea(shapes ...MultiShape) float64 {
	var area float64
	for _, s := range shapes {
		area += s.area()
	}
	return area
}

func UseStruct() {
	var k Circle
	c := new(Circle)

	//fmt.Println("call func: ", area(&c))
	c.Shape.draw()

	fmt.Println("call x?: ", c.area(), "k:", k.Shape)

	multiShapeShow := MultiShapeShow{
		shapes: []MultiShape{
			//Rectangle{10, 20},
		},
	}
	fmt.Println("shape show: ", multiShapeShow)

}
