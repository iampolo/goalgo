package dostruct

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/model"
	"testing"
)

type tree struct {
	val         int
	left, right *tree
}

func Sort(values []int) []int {
	var root *tree //struct pointer

	for _, v := range values {
		root = add(root, v)
	}

	appendValues(values[:0], root)

	return values
}

func TestTree(t *testing.T) {
	arr := []int{3, 2, 6, 8, 7, 4, 1, 5}
	arr = Sort(arr)
	fmt.Println(arr)


	res := make([][]int, 5)
	for i := range res {
		res[i] = make([]int, i + 1)
		fmt.Println(res[i], len(res[i]))
	}

	res[0] = append(res[0], 100)
	res[0][0] = 9
	for i := range res {
		fmt.Println(res[i], len(res[i]))
	}
}

func TestTree2(t *testing.T) {
	arr := []int{3, 2, 6, 8, 7, 4, 1, 5}

	root := model.CreateBst(arr)

	model.PrintInOrder(root)

	array := []interface{}{3, 2, 6, 8, 7, 4, 1, 5}
	root = model.Array2BinaryTree(array)

	model.PrintInOrder(root)
}

func appendValues(values []int, root *tree) []int {
	if root == nil {
		return values
	}

	values = appendValues(values, root.left)
	values = append(values, root.val)
	values = appendValues(values, root.right)

	return values
}

func add(t *tree, val int) *tree {
	if t == nil {
		t = &tree{val: val}
		return t
	}

	if val < t.val {
		t.left = add(t.left, val)
	} else {
		t.right = add(t.right, val)
	}
	return t
}
