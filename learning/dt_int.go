package learning

import "fmt"

func runIntCode() (val int16) {
	var u uint8 = 255;
	fmt.Print(u, u + 1, u*u)

	var apples int32 = 1
	var ora  int16 = 2
	var comb int16 = int16(apples) + int16(ora)
	fmt.Print(comb)
	return 1
}

func TestRunIntCode() {
	runIntCode()
}
