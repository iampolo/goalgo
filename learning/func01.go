package learning

import ("fmt")
func EvenGenerator() func() uint {
	i := uint(0)
	return func() (ret uint) {
		ret = i
		i += 2
		return
	}
}

func Fact(x int) int {
	if x == 0 {
		return 1
	}
	return x * Fact(x - 1)
}


func TestPointer() {
	x := 5
	y := 11

	fmt.Println(x, "-", y)
	zero(&x, y)
	fmt.Println(x, "-", y)
}

func zero(x *int , y int) {
	*x = 101
	y = 77
}

func average(xs []float64) float64 {
	total := 0.0
	return total / float64(len(xs))
}

func NestFunc() {
	a := 0
	incr := func() int {
		a++
		return a
	}
	fmt.Println(incr())
	fmt.Println(incr())
}

func ErrorHandling() {
	defer func() {
		str := recover()
		fmt.Println("recover .. ", str)
	}()
	panic("dont be panic")

}

