package list

import (
	"fmt"
	"testing"
)

type testiface interface {
	SayHello()
	Say(s string)
	IncI()
	GetI() int
}

type testifaceImpl struct{
	i int
}

type testEmbedded struct {
	*testifaceImpl
}

func (tst *testifaceImpl) IncI() {
	tst.i++
}

func (tst *testifaceImpl) GetI() int {
	return tst.i
}

func (tst *testifaceImpl) SayHello() {
	fmt.Println(tst.i)
}

func (tst *testifaceImpl) Say(s string) {
	fmt.Println("s is :  ", s)
}

func (tst *testifaceImpl) SayHi(s string) {
	fmt.Println("s is :  ", s)
}

func NewIt(x int) testiface {
	//return &testifaceImpl{i:x}
	return &testifaceImpl{i:x}
}

func testIface(v interface{}) {
	v, ok := v.(int)

	fmt.Println(v, "oK", ok)
}

func TestFace(t *testing.T) {

	te := testEmbedded{testifaceImpl:&testifaceImpl{i:21}}
	te.GetI()

	testIface("sldfj`")
	testIface(make(map[string]string))
	testIface(10)
}
