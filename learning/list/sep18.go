package list

import (
	"container/list"
	"fmt"
)
//https://www.dotnetperls.com/container-list-go
func main() {
	val := list.New()


	for i := 0; i< 5; i++ {
		val.PushBack(i)
	}

	fmt.Println(val.Len())

	for t := val.Front(); t != nil; t = t.Next() {
		fmt.Print(t.Value, " ")
	}
}
