package list

import (
	"fmt"
	"testing"
)

type Stack struct {
	items []int
}

func (s *Stack) Push(item int) {
	s.items = append(s.items, item)
}

func (s *Stack) Pop() int {
	length := len(s.items)
	if length == 0 {
		return -1
	}
	item, items := s.items[length-1], s.items[0:length-1]
	s.items = items
	return item
}

func (s *Stack) Peek() int {
	return s.items[len(s.items) - 1]
}

func TestStack(t *testing.T) {

	s := Stack{}

	s.Push(10)
	s.Push(9)
	s.Push(8)

	fmt.Println("peek: ", s.Peek())
	for len(s.items) > 0 {
		fmt.Println(s.Pop())
	}
}

