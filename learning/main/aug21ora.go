package main

import "fmt"

//https://learning.oreilly.com/learning-paths/learning-path-end/9781789803075/9781787125254-video1_8
func main() {
	c := testSayImpl{name: "oo"}
	c.Sayhello()
	fmt.Println(c )

	var d testiface
	d = &testSayImpl{name: "default"} //struct literal

	d.Sayhello()
	d.SayHi("what")
	d.ChangeName("coding")

	d.Sayhello()

	fmt.Println("_------------------------")
	e := NewTestSayImplWithParam("god")  //constructor
	e.Sayhello()

	f := NewTestSayImpl()
	f.SayHi("---")

	fmt.Println("_------------------------")

	te := testEmbedding{testSayImpl: &testSayImpl{name : "embedded."}}
	te.testSayImpl.Sayhello()
	te.Sayhello()

	iii := testEmbedding{}
	iii.SayHi("hi, embed")
	fmt.Println("empty embeed: ", iii)

	testEmptyIface("string")
	testEmptyIface(10)
}

//#1
type testiface interface {
	Sayhello()
	SayHi(s string)
	ChangeName(s string)
}

//#2 inteface method receiver
type testSayImpl struct {
	name string
}

func (tst *testSayImpl) ChangeName(s string) {
	tst.name = s + " ok !"
}

func (tst *testSayImpl) SayHi(s string) {
	fmt.Println("implement me" , s)
}

func (tst *testSayImpl) Sayhello() {
	fmt.Println("from inter: ", tst.name)
}

//constructor literal
func NewTestSayImpl() testiface {
	return new (testSayImpl) //just like &testSayImpl{}
}

func NewTestSayImplWithParam(n string) testiface {
	return  &testSayImpl{name: n}
}

//embedded
type testEmbedding struct {
	*testSayImpl
}

//empty interface
func testEmptyIface(v interface{}) {
	if _, ok := v.(int); ok {
		fmt.Println("v: ", v, ", ", ok)
	} else {
		fmt.Println("any thing else", ok)
	}

	switch v.(type) {
	case int:
		fmt.Println("Iam int ")
	default:
		fmt.Println("I am default")
	}
}
