package main

import (
	"fmt"
)



type FileMe struct {
}

func (a *FileMe) Close() error {
	return nil
}

func doMap() {
	x := make(map[string]int)

	x["a"] =  10

	y := map[string]int{
		"b": 10,
		"c": 2,
	}
	fmt.Println(y)

	if v, ok := x["b"]; ok {
		fmt.Println(v)
	}

	delete(y, "b")
	if v, ok := x["b"]; ok {
		fmt.Println(v)
	} else {
		fmt.Println("--:")
	}

}
//pointer, length nad capacity
func DoArraySlice() {

	var a [5] int = [5]int{1, 2, 3, 44, 5}

	sli := []int{1,2,3,5,4}

	fmt.Println(a)
	fmt.Println(sli[2:])


	sli = append(sli, 4, 2, 2, 3)
	fmt.Println(sli)
}

