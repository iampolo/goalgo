package main

import (
	"fmt"
	"testing"
)

//https://learning.oreilly.com/videos/modern-golang-programming/9781787125254/9781787125254-video1_8
/**
https://stackoverflow.com/questions/18125625/constructors-in-go
Effective Go
https://golang.org/doc/effective_go.html#allocation_new
https://github.com/golovers/effective-go
 */
func TestInter(t *testing.T) {
	c := testSayImpl{name: "oo"}
	c.Sayhello()
	fmt.Println(c )

	var d testiface
	d = &testSayImpl{name: "default"} //struct literal

	d.Sayhello()
	d.SayHi("what")
	d.ChangeName("coding")

	d.Sayhello()

	fmt.Println("_------------------------")
	e := NewTestSayImplWithParam("god")  //constructor
	e.Sayhello()

	f := NewTestSayImpl()
	f.SayHi("---")

	fmt.Println("_------------------------")

	//NewTestSayImplWithParam(x : v) won't work
	te := testEmbedding{testSayImpl: &testSayImpl{name : "embedded."}}
	te.testSayImpl.Sayhello()
	te.Sayhello()

	iii := testEmbedding{}
	iii.SayHi("hi, embed")
	fmt.Println("empty embeed: ", iii)

	testEmptyIface("string")
	testEmptyIface(10)
}

//#1
type testiface interface {
	Sayhello()
	SayHi(s string)
	ChangeName(s string)
}

//#2 inteface method receiver
type testSayImpl struct {
	name string
}

func (tst *testSayImpl) ChangeName(s string) {
	tst.name = s + " ok !"
}

func (tst *testSayImpl) SayHi(s string) {
	fmt.Println("implement me" , s)
}

func (tst *testSayImpl) Sayhello() {
	fmt.Println("from inter: ", tst.name)
}

//constructor literal
func NewTestSayImpl() testiface {
	return new (testSayImpl) //just like &testSayImpl{}
}

func NewTestSayImplWithParam(n string) testiface {
	return  &testSayImpl{name: n}
}

//embedded
type testEmbedding struct {
	*testSayImpl
}

//empty interface
func testEmptyIface(v interface{}) {
	if _, ok := v.(int); ok {
		fmt.Println("v: ", v, ", ", ok)
	} else {
		fmt.Println("any thing else", ok)
	}

	switch val := v.(type) {       // switch val := v.(type)
	case int:
		fmt.Println("Iam int " , val)
	default:
		fmt.Println("I am default", val)
	}
}
