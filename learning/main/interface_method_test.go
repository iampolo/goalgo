package main

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/model"
	"reflect"
	"testing"
)

func TestInter2(t *testing.T) {

	var ti testiface2
	ti = &testImpl{}   //#1
	ti = new(testImpl) //#2
	ti = NewImplVal(3)

	ti.Say("John")
	ti.Sayhello()

	ti.Incre()
	ti.Incre()

	fmt.Println("incre: ", ti.GetInt())
}

type testiface2 interface {
	Sayhello()
	Say(s string)
	Incre()
	GetInt() int

	Accept(v interface{})
}

type testImpl struct {
	i int
}

func NewImplVal(iVal int) testiface2 {
	return &testImpl{i: iVal}
}

func NewImpl() testiface2 {
	return new(testImpl)
}
func (tst *testImpl) Accept(v interface{}) {
	fmt.Printf("Type: %T ", v)
	fmt.Println(reflect.TypeOf(v).String())
}
func (tst *testImpl) GetInt() int {
	return tst.i
}
func (tst *testImpl) Incre() {
	tst.i += 1
}
func (tst *testImpl) Sayhello() {
	fmt.Println("ehllo")
}

func (tst *testImpl) Say(s string) {
	fmt.Println("Say hello:", s)
}

//embedded type'
type testEmbedding2 struct {
	*testImpl //
}

func TestEmbedded(t *testing.T) {
	te := testEmbedding2{testImpl: &testImpl{i: 3}}
	te.Incre()
	te.Incre()
	fmt.Println("EB test: ", te.GetInt())

	te.Accept("hel:w")
	te.Accept(new(model.TreeNode))
	te.Accept(model.TreeNode{})
}

func TestAnyThing(t *testing.T) {

	cases := [] struct {
		name string
		info string
	}{
		{
			name: "xxx",
			info: "xxx",
		},
	}

	fmt.Println(cases)

}