package main

import (
	"fmt"
	"github.com/goalgo/learning"
	"github.com/sirupsen/logrus"
)

func main() {

	fmt.Sprintf("")

	logrus.Println("hello....................")

	tesAug19ch7()
}

func tesAug19ch7() {
	c := learning.Circle{}
	r := learning.Rectangle{}

	learning.TotalArea([]learning.Circle{c}, []learning.Rectangle{r})
	fmt.Println(learning.TotalAreaInter(&c, &r))

	learning.TestMultiShapeInfer()
}

