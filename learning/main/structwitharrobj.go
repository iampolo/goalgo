package main

import "fmt"

type candidate struct {
	name        string "clone"
	interests   []string
	lang        string
	experienced bool
}
func NewCandidate() candidate {
	sth := candidate{}
	sth.name = "clone"
	return sth
}
func TestStruct() {
	cand := []candidate{
		{name: "polo",
			interests:   []string{"a", "b", "c"},
			lang:        "alien",
			experienced: true,
		},
		{lang: "Chi"},
	}

	sth := candidate{}
	fmt.Println(cand, sth)

	fmt.Println("------------------------")
	other := NewCandidate()
	fmt.Println(other)
}
