package learning_map

import (
	"fmt"
	"sort"
	"strconv"
)

/*
https://yourbasic.org/golang/how-to-sort-in-go/
https://stackoverflow.com/questions/37695209/golang-sort-slice-ascending-or-descending
*/
func DoMap() {
	var ages = make(map[string]int)
	ages["john"] = 13
	ages["alice"] = 31
	ages["charlie"] = 34

	var names []string

	fmt.Println("msg from master")
	for keys, vals := range ages {
		fmt.Println(keys, "-", vals)
		names = append(names, strconv.Itoa(vals))
	}

	sort.Strings(names)
	sort.Sort(sort.Reverse(sort.StringSlice(names)))
	fmt.Println("rev? ", names)

}

/****************************************************************************************************/

var prereqs = map[string][]string{
	"algo":  {"Data Struc."},
	"calcu": {"linear algebra."},
	"compiler": {
		"Data Struc.",
		"formal lang",
		"computer Organ",
	},
	"Data Struc.": {"discrete math"},
	"prog lang":   {"Data Struc.", "computer Organ"},
}

func topSort(m map[string][]string) []string {
	var order []string
	seen := make(map[string]bool)

	var visAll func(items []string)
	visAll = func(items []string) {
		for _, item := range items {
			if !seen[item] {
				seen[item] = true
				visAll(m[item])
				order = append(order, item)
			}
		}
	} //

	var keys []string
	for key := range m {
		keys = append(keys, key)
	}

	sort.Strings(keys)
	visAll(keys)
	return order
}
func DoTopSort() {
	for i, course := range topSort(prereqs) {
		fmt.Printf("%d: \t%s\n",  i + 1, course)
	}
}
