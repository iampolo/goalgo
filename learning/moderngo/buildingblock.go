package moderngo

import (
	"fmt"
	"image/color"
)

type Point struct{ x, y int }

type ColoredPoint struct {
	Point
	Color color.RGBA
	//color.RGBA
}

//https://learning.oreilly.com/library/view/go-design-patterns/9781788390552/ch06s06.html
func init() {
	fmt.Println("init moderngo")
}
