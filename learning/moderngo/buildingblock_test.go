package moderngo

import (
	"fmt"
	"image/color"
	"io"
	"sync"
	"testing"
)

func TestPackage(t *testing.T) {
	fmt.Println("-------")
}

func comp(a, b int) int {

	return 0
}

func TestStructObject(t *testing.T) {

	v := ColoredPoint{}
	_ = v

	red := color.RGBA{3, 0, 3, 3}
	var p = ColoredPoint{Point{1, 1}, red}
	_ = p
}

var (
	mu sync.Mutex
)

type WriteMe interface {
	io.Reader
	io.Writer
}

type ByteCount int

func (c *ByteCount) Write(x []byte) (int, error) {
	fmt.Println("has been written.")
	return 0, nil
}

func TestWriterType(t *testing.T) {
	var b ByteCount
	b.Write([]byte("hithere"))

	fmt.Println(b)

	var x WriteMe

	_ = x
}

func TestCreateStruct(t *testing.T) {
	x := struct {
		Name string
	}{
		Name: "",
	}
	fmt.Println(x)

}
