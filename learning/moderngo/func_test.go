package moderngo

import (
	"fmt"
	"sort"
	"strconv"
	"testing"
)

/**
https://learning.oreilly.com/library/view/go-design-patterns/9781788390552/ch05s03.html

https://yourbasic.org/golang/anonymous-function-literal-lambda-closure/
 */
func TestFunc(t *testing.T) {
	nums := []int{4, 32, 11, 77, 556, 3, 19, 88, 422}
	sort.Slice(nums, func(i, j int) bool {
		if nums[i] > nums[j] {
			return true
		}
		return false;
	})
	fmt.Println(nums)
}

func apply(nums []int, f func(int) int) func() {

	for idx, v := range nums {
		nums[idx] = f(v)
	}

	return func() {
		fmt.Println(nums)
	}
}


type Temp int
func (t *Temp) String() string {
	return strconv.Itoa(int(*t)) + " C"
}

func (p *Point) String() string {
	return fmt.Sprintf("%d, %d", p.x, p.y)
}

func TestInterfaceImpl(t *testing.T) {
	x := Temp(1)
	fmt.Println(x.String())

	y := &Point{2,3}
	fmt.Println(y.String())
}
