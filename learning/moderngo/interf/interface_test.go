package interf

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"testing"
)

//https://learning.oreilly.com/library/view/go-design-patterns/9781788390552/ch08s03.html

type Animal interface {
	GetGender() int
}

type Shape interface {
	area() float64
}

type Polygon interface { //interface embedding
	Shape
	perim() float64
}

var s Shape

/*******************************************************/
type Rect struct {
	name string
	len  float64
}

func (r Rect) area() float64 {
	return 0.0
}

func (r Rect) perim() float64 {
	return 0.0
}

func (a Rect) GetGender() int {
	return -100
}

func (s Rect) String() string {
	return s.name + ", " + string(s.GetGender())
}
/*******************************************************/
type Corner struct {    //struct embedding
	Rect
}
func (c Corner) GetGender() int  { //override
	return 100 + 1
}

func (c Corner) ServerResourcesForGroupVersion(gvk string) (src *metav1.APIResourceList, err error) {
	return nil, nil
}

func TestCorner(t *testing.T) {
	c := Corner{Rect: Rect{name: "Mar"}}
	doThings(c)
	doPthing(c)


	cc := Corner{}
	doAnimalThing(cc)

}
/*******************************************************/
func TestIntefaceCall(t *testing.T) {

	s := Rect{name: "xxx", len: 43.0}
	doThings(s)
	doPthing(s)

	doAnimalThing(s)
	a := Rect{}
	doAnimalThing(a)
	doThings(a)
}

func doPthing(p Polygon) {
	fmt.Println("p thing: ", p)
}

func doThings(s Shape) {
	fmt.Println("shape thing", s)
}

func doAnimalThing(a Animal) {
	fmt.Println("gender: ", a.GetGender())

}
