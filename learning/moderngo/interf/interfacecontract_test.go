package interf

//The Go Programming Language - ch7
import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"sort"
	"testing"
)

type ByteCounter int

func (c *ByteCounter) Write(p []byte) (int, error) {
	*c += ByteCounter(len(p))
	fmt.Println("**8")
	return 99 + len(p), nil
}

func TestWriter(t *testing.T) {
	var c ByteCounter
	c.Write([]byte("hi-there"))

	fmt.Fprintf(&c, "%s", "--")
	fmt.Println(c)
}

func TestFile(t *testing.T) {
	flag.Parse()
}

type MyValue struct {
}

func (v MyValue) String() string {
	return ""
}
func (v MyValue) Set(s string) error {
	return nil
}

func TestInterface(t *testing.T) {
	var w io.Writer
	w = os.Stdout
	_ = w
}

func TestSorting(t *testing.T) {
	var d = []string{"Hello", "foo"}
	sort.Sort(StringSlice(d))
	fmt.Println(d)
}

type StringSlice []string

func (s StringSlice) Len() int {
	return len(s);
}
func (s StringSlice) Less(i, j int) bool {
	return s[i] > s[j];
}
func (s StringSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func TestStringSort(t *testing.T) {
	var a = customSort{[]string{"", ""}, func(a, b *string) bool {
		return false
	}}
	_ = a

}

type customSort struct {
	t    []string
	less func(a, b *string) bool
}

type database map[string]float32

func (db database) ServeHTTP(w http.ResponseWriter, req *http.Request) {

}

func (db database) list(w http.ResponseWriter, r *http.Request) {

}

func (db database) price(w http.ResponseWriter) {

}
func TestHttpSvr(t *testing.T) {
	var db = database{"": 11}

	http.ListenAndServe("", db)

	mux := http.NewServeMux()
	mux.Handle("", http.HandlerFunc(db.list))

}
