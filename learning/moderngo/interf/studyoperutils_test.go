package interf

import (
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/discovery"
	clientv1 "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/scheme"

	"testing"
)
/***********************************************************************************************/
type K8SClient struct {
	client clientv1.Client
}

func NewMockClient() {
	SchemeGroupVersion := schema.GroupVersion{Group: "console.openshift.io", Version: "v1"}

	schemeBuilder := &scheme.Builder{GroupVersion: SchemeGroupVersion}
	scheme, _ := schemeBuilder.Build()

	client := fake.NewFakeClientWithScheme(scheme)
	fmt.Println(client)
}

func TestFakeClient(t *testing.T) {
	NewMockClient()
}

/***********************************************************************************************/
type Alien interface {
	//even though DiscoveryClient has many other method, here just one is also o
	ServerResourcesForGroupVersion(gvk string) (src *metav1.APIResourceList, err error)
}

func TestAny(t *testing.T) {
	var ali Alien

	cfg, _ := config.GetConfig()
	ali, err := discovery.NewDiscoveryClientForConfig(cfg)
	if err != nil {
		fmt.Println(err)
	}

	gv := schema.GroupVersion{Group: "console.openshift.io", Version: "v1"}
	res, err := ali.ServerResourcesForGroupVersion(gv.String())
	for _, r := range res.APIResources {
		fmt.Println(r.Name)
	}
}
