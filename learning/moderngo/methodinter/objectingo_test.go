package methodinter

//https://learning.oreilly.com/library/view/go-design-patterns/9781788390552/ch08s02.html

import (
	"fmt"
	"testing"
)

type fuel int

const (
	GASOLINE fuel = iota
)

type engine struct {
	fuel   fuel
	thrust int
}

func (e *engine) start() {
	fmt.Println("Engine started.")
}

type vehicle struct {
	make   string
	model  string
	thrust int
}
type truct struct {
	engine
	axels  int
	wheels int
	class  int
}

func TestAnything(t *testing.T) {
	f := fuel(2)
	fmt.Println(f)

	tx := truct{engine{fuel(4), 3}, 1, 2, 3}
	_ = tx
	fmt.Println(tx)
	tx.start()
	fmt.Println(tx.thrust)

}
