package methodinter

import (
	"fmt"
	"gitlab.com/iampolo/goalgo/learning/moderngo/oo"
)

type HowToPay interface {
	AvailableFunds() float32
	ProcessPayment(amount float32) bool
}

func UseItThen() {
	var use HowToPay
	use = &oo.Account{}
	fmt.Println(use.AvailableFunds())

	use = &oo.HybridAccount{}
	fmt.Println(use.AvailableFunds())
}