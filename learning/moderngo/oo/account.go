package oo

//OO Programming with Go
type Account struct {
}

func (a *Account) AvailableFunds() float32 {
	return 1.0
}

func (a *Account) ProcessPayment(amount float32) bool {
	return false
}

type CreditAccount struct {
	Account
}

func (c *CreditAccount) AvailableFunds() float32 {
	return 501
}

type CheckingAccount struct{}

func (ca *CheckingAccount) AvailableFunds() float32 {
	return 1.1
}

type HybridAccount struct {
	Account
	CheckingAccount
}

func (ha *HybridAccount) AvailableFunds() float32 {
	return ha.Account.AvailableFunds() * ha.CheckingAccount.AvailableFunds()
}

func doAccount() {
	ca := &HybridAccount{}
	ca.AvailableFunds()
}

