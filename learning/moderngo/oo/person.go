package oo

//Pluralsight course
import (
	"errors"
	"strings"
)

type Identifiable interface {
	ID() string
}

type Citizen interface {
	Country() string
	Identifiable
}

type Handler struct {
	handle string
	name   string
}

func (h Handler) Redirect() {
}

type TestHandler Handler

func (h TestHandler) randomFunc() {
}

//type definition : no = sign :copy the fields only
//type declaration : with = :  copy method and fields
// one copies the fields, and one copies the functions
type TwitterHanlder string

func (th TwitterHanlder) Rediret() string {
	return ""
}

type Name struct {
	firstName string
	LastName  string
}
type Person struct {
	Name
	TwitterHandler TwitterHanlder
	Citizen
}

func (p Person) ID() string {
	//return "99999"
	return p.Citizen.ID()
}

func NewPersion(ln, fn string, citizen Citizen) Person {
	return Person{
		Name: Name{
			firstName: fn,
			LastName:  ln,
		},
		Citizen: citizen,
	}
}

func (p Person) SetTwitterHandler(handler TwitterHanlder) error {
	if len(handler) == 0 {
		p.TwitterHandler = handler
	} else if !strings.HasPrefix(string(handler), "@") {
		return errors.New("twitter handler is wrong.")
	}
	return nil
}

func (p Person) GetLastName() string {
	return p.LastName
}

type SSN string

func (ssn SSN) ID() string {
	return string(ssn)
}

func (ssn SSN) Country() string {
	return "USA"
}

type hkID struct {
	id      string
	country string
}

func (id hkID) ID() string {
	return id.id
}
func (id hkID) Country() string {
	return id.country
}

func NewHKID(id, country string) Citizen {
	return hkID{
		id:      id,
		country: country,
	}
}
