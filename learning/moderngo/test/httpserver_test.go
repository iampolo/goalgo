package test

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

type Client struct {

}

func (c *Client) Get() (string, error) {

	return "", nil
}

func TestGet(t *testing.T) {
	want := "Success!"
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte(want))
	}))
	defer srv.Close()

}
