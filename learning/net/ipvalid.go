package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	n := len(os.Args)
	fmt.Println(n)
	if len(os.Args) != 1 {
		fmt.Println("no input")
		return
	}

	ip := net.ParseIP(os.Args[0])

	if ip !=   nil {
		fmt.Printf("%v Ok \n", ip)
	} else {
		fmt.Println("bad ", os.Args[0])
	}
}
