package sec

import (
	"bytes"
	"fmt"
	"strconv"
	"testing"
)

func TestBase64(t *testing.T) {
	arr := []byte( "Base 64 encoding")

	fmt.Println(arr)
	for i := 0; i < len(arr) - 3; i += 3 {
		tmp := arr[i : i+3]
		fmt.Println(string(tmp), tmp, strconv.FormatInt(122,2))

	}

	res := fmtBits(arr)
	fmt.Printf("%s", res)
}

func fmtBits(data[] byte) []byte {
	var buf bytes.Buffer
	for _, b := range data {
		fmt.Fprintf(&buf, "%08b", b)
	}
	buf.Truncate(buf.Len() - 1)
	return buf.Bytes()
}
