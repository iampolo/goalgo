package sec

import (
	"fmt"
	"hash/fnv"
	"testing"
)

func hash(s string) uint32 {
	h := fnv.New32()
	h.Write([]byte(s))
	return h.Sum32()
}

func TestHash(t *testing.T) {
	fmt.Println(hash("this shining"))
	fmt.Println(hash("doctor sleep"))
}
