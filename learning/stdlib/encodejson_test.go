package main

import (
	"encoding/json"
	"log"
	"testing"
)

func TestEncodeJson(t *testing.T) {
	c := make(map[string]interface{})

	c["name"] = "Json test"

	data, err := json.MarshalIndent(c, "", "   ")
	if err != nil {
		log.Println("error",err)
		return

	}

	log.Println(data, string(data))
}
