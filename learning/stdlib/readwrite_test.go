package main

import (
	"bytes"
	"fmt"
	"os"
	"testing"
)

func TestReadWrite(t *testing.T) {
	var b bytes.Buffer

	b.Write([]byte("Hi, there!"))

	fmt.Fprint(&b, " World!")

	b.WriteTo(os.Stdout)
}
