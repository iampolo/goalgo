package main

import (
	"fmt"
	"strings"
)

func main() {
	doChar()
}
func doRune() {
	str := strings.Split("HELLO", "")
	fmt.Println(str[:3+1])

	fmt.Println([]rune("World")[3] + 'a')
}


func doChar() {
	s := "redhat inc"
	fmt.Println(s[1:])
	fmt.Println(s[1])
}
