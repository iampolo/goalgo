package main

import (
	"bytes"
	"fmt"
	"gitlab.com/iampolo/goalgo/algo/util"
	"strings"
	"testing"
)

func TestChar(t *testing.T) {
	str := "apple"
	fmt.Println(string(str[0]))
	fmt.Println(str[0]+ ' ')
}

func TestStr(t *testing.T) {

	str := 'z' - 'a'

	fmt.Println(string(str + 'a'))

	s := "hello world"
	fmt.Println("Hi " + s[:6])

	s += " >>>"

	fmt.Println("Hi" + s[4:])

	fmt.Println(util.Contains("hello", "llo"))

	for i, r := range s {
		fmt.Println(i, r)
		if i == 5 {
			break
		}
	}

	fmt.Println(string(0x4eac))

	fmt.Println(basename("/a/b/filename.go"))

	fmt.Println(comma("1493"))

	fmt.Println("buffer: ", intToString([]int{10000}))

}

func comma(s string) string {
	n := len(s)
	if n <= 3 {
		return s
	}

	return comma(s[:n-3]) + "," + s[n-3:]
}
func intToString(values []int) string {
	var buf bytes.Buffer
	buf.WriteByte('[')

	for i, v := range values {
		if i > 0 {
			buf.WriteString(", ")
		}
		fmt.Fprintf(&buf, "%d", v)
	}
	buf.WriteByte(']')
	return buf.String()
}

func basename2(s string) string {
	slash := strings.LastIndex(s, "/")
	s = s[slash+1:]
	if dot := strings.LastIndex(s, "."); dot >= 0 {
		s = s[:dot]
	}
	return s
}

func basename(s string) string {
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '/' {
			s = s[i+1:]
			break;
		}
	}

	for i := len(s) - 1; i >= 0; i-- {
		if s[i] == '.' {
			s = s[:i];
			break
		}
	}
	return s
}
