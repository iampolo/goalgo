package learning

import (
	"fmt"
)
func testArray() {
	x := [3]string{"bob", "cat", "dog"}
	x[0] = "ok"
	fmt.Println("array: ", x[0])
}

const name string = "goland in go"

func testSlice() {
	var x []float64
	fmt.Println("slice here:" , x)

	y := make([]string, 3)
	fmt.Println("slice: '", y[0], "'")

	arr := [5] float32{1,2,3,4,5}

	sl1 := arr[0:4]
	fmt.Println(sl1)
	fmt.Println("length of slice:" ,len(sl1))
	sl1=	append(sl1, 11)
	fmt.Println("length of slice:" ,len(sl1))

}

func testMap() {
	//var x map[string]int
	x := make(map[string]int)
	x["bob"] = 10
	for i := 0; i < 5; i++ {
		x[string(i)] = i + 5
	}
	fmt.Println(x)
}

func testFunc(nums []int) int {

	return -1
}
func main() {

	x := []int {1,2,4,5}
	res := testFunc(x)
	fmt.Println("res: ", res)
}