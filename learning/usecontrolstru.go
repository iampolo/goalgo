package learning


import (
	"fmt"
	"log"
)
func useControlStructure() {
	var ready []string
	for range [5]int{} {
		ready = append(ready, "r")
	}
	fmt.Println(ready)
	log.Println("finished")
}

func TestRunControl() {

	useControlStructure()
}
