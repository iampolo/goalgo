package learning

import (
	"fmt"
	"strings"
)

func anyOne() {
	fmt.Println("----", strings.Repeat("-", 5))
	fmt.Println("Count:", strings.Count("test", "t"))
}

func TestRun() {
	anyOne()
}