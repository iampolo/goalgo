package openshift

import (
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"sigs.k8s.io/controller-runtime/pkg/scheme"
	"testing"

	clientv1 "sigs.k8s.io/controller-runtime/pkg/client"
)

func TestMyMock(t *testing.T) {
	fmt.Println("----------------")

	//srv := Build()
	//_ = srv
}

type MockXXXService struct {
	UpdateFunc          func(ctx context.Context, obj runtime.Object, opts ...clientv1.UpdateOption) error
	PatchFunc           func(ctx context.Context, obj runtime.Object, patch clientv1.Patch, opts ...clientv1.PatchOption) error
}

func (mock *MockXXXService) Update(ctx context.Context, obj runtime.Object, opts ...clientv1.UpdateOption) error {
	return nil
}

func (mock *MockXXXService) Patch(ctx context.Context, obj runtime.Object, patch clientv1.Patch, opts ...clientv1.PatchOption) error {
	return nil
}

func Build() *MockXXXService {
	var rb []runtime.Object
	var sb *scheme.Builder
	sb.Register(rb...)
	sc, _ := sb.Build()
	client := fake.NewFakeClientWithScheme(sc)

	return &MockXXXService{
		UpdateFunc: func(ctx context.Context, obj runtime.Object, opts ...clientv1.UpdateOption) error {
			return client.Update(ctx, obj, opts...)
		},
		PatchFunc:  func(ctx context.Context, obj runtime.Object, patch clientv1.Patch, opts ...clientv1.PatchOption) error {
			return client.Patch(ctx, obj, patch, opts...)
		},
	}
}

type MockXXX interface {
	Update(ctx context.Context, obj runtime.Object, opts ...clientv1.UpdateOption) error
	// Patch patches the given object's subresource. obj must be a struct
	// pointer so that obj can be updated with the content returned by the
	// Server.
	Patch(ctx context.Context, obj runtime.Object, patch clientv1.Patch, opts ...clientv1.PatchOption) error

	//MyFunc(ctx context.Context) error
}

