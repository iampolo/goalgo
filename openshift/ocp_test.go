package openshift

/**
how was go.mod updated:
	go get k8s.io/apimachinery@kubernetes-1.16.2
	go get github.com/openshift/api@release-4.3
*/
import (
	"fmt"
	consolev1 "github.com/openshift/api/console/v1"
	"gitlab.com/iampolo/goalgo/openshift/utils"
	"golang.org/x/mod/semver"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/version"
	"os"
	"strings"
	"testing"
)

const (
	RELATED_IMAGE_PREFIX         = "RELATED_IMAGE_COCKROACH_"
)

var knownTypes = map[schema.GroupVersion][]runtime.Object{
	consolev1.GroupVersion: {
		&consolev1.ConsoleLink{},
		&consolev1.ConsoleLinkList{},
		&consolev1.ConsoleYAMLSample{},
		&consolev1.ConsoleYAMLSampleList{},
	},
}

func TestOCP(t *testing.T) {
	for k, v := range knownTypes {
		fmt.Print(k, " ---  ")
		for tm := range v {
			fmt.Print(tm, " ")
		}
		fmt.Println()
	}

	var sample consolev1.ConsoleYAMLSample
	fmt.Println(sample)
}

func TestAPICall(t *testing.T) {
	kubeconfig, err := utils.GetConfig()
	if err != nil {
		fmt.Println("++", err)
	}
	consoleLinkName := "lkdsjf"
	//err = errors.New("test error")
	errf := fmt.Errorf("Couldn't retrieve Console Link %s %w:", consoleLinkName, err)
	fmt.Println( errf, "\n", kubeconfig)


	getSupportedCrdbVersions()
}

func getSupportedCrdbVersions() []string {
	supportedVersions := make([]string, 0)
	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		fmt.Println("e: ", e)
		if strings.HasPrefix(pair[0], RELATED_IMAGE_PREFIX) {
			version := strings.ReplaceAll(pair[0], RELATED_IMAGE_PREFIX, "")
			version = strings.ReplaceAll(version, "_", ".")
			supportedVersions = append(supportedVersions, version)
		}
	}
	return supportedVersions
}


func TestStrcut(t *testing.T) {
	val := func() int {
		return 10
	}
	fmt.Println("val from func", val())

	x := MyObj{}
	_ = x

	a := Point{3, 10}
	_ = a
	fmt.Println(a)
}

type Point struct{ x, y int }
type MyObj struct {
	Name LastName `json:name`
}

func (m MyObj) String() string {
	return ""
}

type LastName string

type Discv interface {
	ServerVersion() (*version.Info, error)
	ServerResourcesForGroupVersion(groupVersion string) (resources *metav1.APIResourceList, err error)
}

func TestSemen(t *testing.T) {
	cmp := semver.Compare("v4.13", "v4.4")
	fmt.Println(cmp)
}