package fake

import (
	"context"
	"k8s.io/apimachinery/pkg/runtime"
	clientv1 "sigs.k8s.io/controller-runtime/pkg/client"

)

type MockOperatorService struct {
	Client              clientv1.Client
	scheme              *runtime.Scheme
	CreateFunc          func(ctx context.Context, obj runtime.Object, opts ...clientv1.CreateOption) error
}
