package operator

import (
	"flag"
	"fmt"
	"github.com/goalgo/operator/apiv1"
	"github.com/stretchr/testify/assert"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"testing"
)

/////////////////////////////////////////////////////////////////////////////////////////////

func TestWhatEver_WillDELETE(t *testing.T) {
	//cr := &api.StorageCluster{}
	//
	//scheme, err := api.SchemeBuilder.Build()
	//assert.Nil(t, err, "Failed to get scheme")
	//fmt.Println(scheme)
	//fmt.Println("-----------------", cr)
	assert.Len(t, "", 0, "probably ok")

}
func TestClientGo_WillDELETE(t *testing.T) {
	path := "/home/marcoyeung/.kube/config"
	kubeconfig := flag.String("kubeconfig", path, "kubeconfig file")
	flag.Parse()
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	fmt.Println(err)
	clientset, err := kubernetes.NewForConfig(config)
	fmt.Println("what do we got:", clientset)

	pod, err := clientset.CoreV1().Pods("book").Get("example", metav1.GetOptions{});
	fmt.Println("pod si ", pod)
}

func TestRun(t *testing.T) {
	obj := apiv1.OCSInitTester{}
	fmt.Println(obj)

}
/*
func TestStorageClusterExampleDeviceSet(t *testing.T) {
	storageClassName := "gp2"
	volMode := corev1.PersistentVolumeBlock
	cr := &api.StorageCluster{
		TypeMeta: metav1.TypeMeta{
			Kind: "StorageCluster",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "storage-test",
			Namespace: "storage-test-ns",
		},
		Spec: api.StorageClusterSpec{
			ManageNodes:  false,
			InstanceType: "test-type",
			StorageDeviceSets: [] api.StorageDeviceSet{
				{
					Name:      "mock test only",
					Count:     2,
					Resources: corev1.ResourceRequirements{},
					Placement: rookalpha.Placement{},
					DataPVCTemplate: corev1.PersistentVolumeClaim{
						Spec: corev1.PersistentVolumeClaimSpec{
							StorageClassName: &storageClassName,
							AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
							VolumeMode: &volMode,
						},
					},
				},
			},
		},
	}
	assert.NotNil(t, cr, "Failed to create mock CR")

	request := reconcile.Request{
		NamespacedName: types.NamespacedName{
			Name:      "storage-test",
			Namespace: "storage-test-ns",
		},
	}
	reconciler := createFakeStorageClusterReconciler(t, cr)
	result, err := reconciler.Reconcile(request)
	assert.NoError(t, err)
	assert.Equal(t, reconcile.Result{}, result)
}*/
/*
func TestReconcileStorageClusterInterface(t *testing.T) {
	reconciler := ReconcileStorageCluster{}
	var i interface{} = reconciler
	_, ok := i.(ReconcileStorageCluster)
	assert.True(t, ok)
	//f, ok := i.(reconcile.Reconciler)
	//fmt.Println(f, ok)
	//assert.True(t, ok)
}*/