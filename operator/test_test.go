package operator

//import (
//	conditionsv1 "github.com/openshift/custom-resource-status/conditions/v1"
//	api "github.com/openshift/ocs-operator/pkg/apis/ocs/v1alpha1"
//	rookCephv1 "github.com/rook/rook/pkg/apis/ceph.rook.io/v1"
//	"github.com/stretchr/testify/assert"
//	corev1 "k8s.io/api/core/v1"
//	"k8s.io/apimachinery/pkg/api/errors"
//	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
//	"k8s.io/apimachinery/pkg/runtime"
//	"k8s.io/apimachinery/pkg/types"
//	"sigs.k8s.io/controller-runtime/pkg/client/fake"
//	"sigs.k8s.io/controller-runtime/pkg/reconcile"
//	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
//	"testing"
//)
//
//var logt = logf.Log.WithName("controller_storagecluster_test")
//
//func TestResourceNotFound(t *testing.T) {
//	reconciler := createFakeStorageClusterReconciler(t)
//	request := reconcile.Request{
//		NamespacedName: types.NamespacedName{
//			Name:      "storage-ns",
//			Namespace: "storage-test-ns",
//		},
//	}
//	_, err := reconciler.Reconcile(request)
//	assert.NoError(t, err)
//
//	obj := api.StorageCluster{}
//	err = reconciler.Client.Get(nil, request.NamespacedName, &obj)
//	assert.True(t, errors.IsNotFound(err))
//}
//
//func TestConditionsAdded(t *testing.T) {
//	sc := &api.StorageCluster{
//		ObjectMeta: metav1.ObjectMeta{
//			Name:      "storage-test",
//			Namespace: "storage-test-ns",
//		},
//	}
//	cc := &rookCephv1.CephCluster{
//		ObjectMeta: metav1.ObjectMeta{
//			Name:      "storage-test",
//			Namespace: "storage-test-ns",
//		},
//	}
//	request := reconcile.Request{
//		NamespacedName: types.NamespacedName{
//			Name:      "storage-test",
//			Namespace: "storage-test-ns",
//		},
//	}
//	reconciler := createFakeStorageClusterReconciler(t, sc, cc)
//	_, err := reconciler.Reconcile(request)
//	assert.NoError(t, err)
//
//	obj := &api.StorageCluster{}
//	err = reconciler.Client.Get(nil, request.NamespacedName, obj)
//	assert.NoError(t, err)
//	assert.NotEmpty(t, obj.Status.Conditions)
//	assert.Len(t, obj.Status.Conditions, 5)
//	expectedConditions := map[conditionsv1.ConditionType]corev1.ConditionStatus{
//		conditionsv1.ConditionAvailable:   corev1.ConditionTrue,
//		conditionsv1.ConditionProgressing: corev1.ConditionFalse,
//		conditionsv1.ConditionDegraded:    corev1.ConditionFalse,
//		conditionsv1.ConditionUpgradeable: corev1.ConditionTrue,
//		api.ReconcileCompleted:            corev1.ConditionTrue,
//	}
//	for cType, status := range expectedConditions {
//		found := assertCondition(*obj, cType, status)
//		if !found {
//			assert.Fail(t, "expected status condition not found")
//		}
//	}
//}
//
//func TestEnsureCephClusterCreate(t *testing.T) {
//	// write a test that will reach line reconcile.go 153 and return after that create, then use Client.Get
//	// to fetch the object and assert the name/namespace match what we expect
//}
//
//func TestEnsureCephClusterUpdate(t *testing.T) {
//	// write a test that will reach line 162 and return after the update, assert update does what we expected
//}
//
//func TestEnsureCephClusterNoConditions(t *testing.T) {
//	// write a test that will reach line 178, then after func returns, fetch the obj from Client.Get and assert
//	// status/state is what we expect it to be
//}
//
//func TestEnsureCephClusterNegativeConditions(t *testing.T) {
//	// write a test that will reach line 181, then after func returns, fetch the obj from Client.Get and assert
//	// status/state is what we expect it to be
//}
//
//func TestNewCephClusterCopiesStorageCluster(t *testing.T) {
//	storageDeviceSet := api.StorageDeviceSet{
//		Name: "test-sds",
//	}
//	pvc := &corev1.PersistentVolumeClaim{
//		ObjectMeta: metav1.ObjectMeta{
//			Name:      "storage-test",
//			Namespace: "storage-test-ns",
//		},
//	}
//	sc := &api.StorageCluster{
//		ObjectMeta: metav1.ObjectMeta{
//			Name:      "storage-test",
//			Namespace: "storage-test-ns",
//		},
//		Spec: api.StorageClusterSpec{
//			StorageDeviceSets: []api.StorageDeviceSet{
//				storageDeviceSet,
//			},
//			MonPVCTemplate: pvc,
//		},
//	}
//
//	cc := newCephCluster(sc)
//	assert.Equal(t, sc.Name, cc.Name)
//	assert.Equal(t, sc.Namespace, cc.Namespace)
//	assert.Equal(t, sc.Spec.StorageDeviceSets[0].Name, cc.Spec.Storage.StorageClassDeviceSets[0].Name)
//	assert.Equal(t, sc.Spec.MonPVCTemplate, cc.Spec.Mon.VolumeClaimTemplate)
//}
//
//func assertCondition(sc api.StorageCluster, conditionType conditionsv1.ConditionType, status corev1.ConditionStatus) bool {
//	for _, objCondition := range sc.Status.Conditions {
//		if objCondition.Type == conditionType {
//			if objCondition.Status == status {
//				return true
//			}
//		}
//	}
//	return false
//}
//
//func createFakeStorageClusterReconciler(t *testing.T, objs ...runtime.Object) ReconcileStorageCluster {
//	scheme := createFakeScheme(t, objs...)https://console-openshift-console.apps.mmikhail-mwocp42.devcluster.openshift.com/operatorhub/subscribe?pkg=ocs-operator&catalog=ocs-catalogsource&catalogNamespace=openshift-marketplace&targetNamespace=default
//	client := fake.NewFakeClientWithScheme(scheme, objs...)
//
//	return ReconcileStorageCluster{
//		Client:    client,
//		Scheme:    scheme,
//		ReqLogger: logt,
//	}
//}
//func createFakeScheme(t *testing.T, objs ...runtime.Object) *runtime.Scheme {
//	registerObjs := []runtime.Object{&api.StorageCluster{}}
//	registerObjs = append(registerObjs, objs...)
//	api.SchemeBuilder.Register(registerObjs...)
//	scheme, err := api.SchemeBuilder.Build()
//	if err != nil {
//		assert.Fail(t, "unable to build scheme")
//	}
//	return scheme
//}
