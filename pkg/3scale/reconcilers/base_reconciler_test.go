package reconcilers

import (
	"context"
	appsv1 "github.com/openshift/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	fakeclientset "k8s.io/client-go/kubernetes/fake"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"
	"testing"
)

func TestCreateObj(t *testing.T) {
	var (
		namespace = "operator-unittest"
	)
	objs := []runtime.Object{}
	cl := fake.NewFakeClient(objs...)
	clientAPIreader := fake.NewFakeClient(objs...)
	clientset := fakeclientset.NewSimpleClientset()
	recorder := record.NewFakeRecorder(100000)
	s := scheme.Scheme
	err := appsv1.AddToScheme(s)
	if err != nil {
		t.Fatal(err)
	}

	desiredConfigmap := &v1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "v1",
			Kind:       "ConfigMap",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "myConfigmap",
			Namespace: namespace,
		},
		Data: map[string]string{
			"somekey": "somevalue",
		},
	}

	baseRecon := NewBaseReconciler(cl, s, clientAPIreader,
		context.TODO(), log, clientset.Discovery(), recorder)

	err = baseRecon.ReconcileResource(&v1.ConfigMap{}, desiredConfigmap, CreateOnlyMutator)
	if err != nil {
		t.Error("sadff", err)
	}
}
