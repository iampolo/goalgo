import math

# https://drive.google.com/drive/u/0/folders/1-Rk4qpUbXUNj9T17HnHfL2xi51eOwJ4b

def maxSetSize(riceBags):
    riceBagsSet = set(riceBags)
    solutionTable = {}
    for riceBag in riceBags:
        sqrt = math.sqrt(riceBag)
        if sqrt.is_integer() and int(sqrt) in riceBagsSet:
            continue

        solutionTable[riceBag] = []

    max_set_size = 0
    for lowest_factor in solutionTable:
        curr = lowest_factor
        while curr in riceBagsSet:
            solutionTable[lowest_factor].append(curr)
            curr *= curr

        max_set_size = max(max_set_size, len(solutionTable[lowest_factor]))

    return max_set_size if max_set_size >= 2 else -1

if __name__ == '__main__':
    print(maxSetSize([3,9,4,2,16]))