# codeList
# shoppingCart

# check codelist fruit in order as ShoppingCart's order
# only codeList item count <= shoppingCart's item count == 1
#https://www.1point3acres.com/bbs/interview/amazon-software-engineer-290663.html
# 1. 有一个仓库，然后找距离0,0最近的K个点
# 2. 给一个整数数组，先建立bst，不需要rebalance，给两个点，求这两个点在树中的距离。

# Ex1:
# codelist:
# [
#     [apple, apple],
#     [orange, banana, orange]
# ]
# shoppingCart: [orange, apple, apple, orange, banana, orange]
# return 1, 因为codelist里的顺序和shoppingcart里除了第一个orange之后的水果顺序匹配
#
# Ex2:
# codelist:
# [
#     [orange, banana, orange]，
# [apple, apple]
# ]
# shoppingCart: [orange, apple, apple, orange, banana, orange]
# return 0, 因为codeList里的顺序和shoppingcart没法匹配。
#
# Ex3:
# codelist:
# [
#     [apple, apple],
#     [orange, banana, orange],
#     [pear, orange, grape]
# ]
# shoppingCart: [orange, apple, apple, orange, banana, orange, pear, grape]
# return 0, 因为codelist里最后一个[pear, orange, grape]中的orange没法和shoppingcart里的水果匹配。
#
# Ex4:
# codeList:
# [
#     [apple, apple],
#     [orange, anything, orange]
# ]
# shoppingCart:
# [orange, apple, apple, orange, mango, orange]
# return 1。

def isMatch(codeList, shoppingList):
    # sequence match need to match from the first item
    # edge case: shoppingList is empty

    num_of_items_in_codeList, num_of_items_in_shoppingList = len(codeList), len(shoppingList)
    if num_of_items_in_codeList < 1:
        return 1
    if num_of_items_in_shoppingList < 1:
        return 0
    idx_in_codeList, idx_in_shoppingList = 0, 0
    while idx_in_shoppingList < num_of_items_in_shoppingList:
        cur_List = codeList[idx_in_codeList]
        idx_in_cur_List = 0

        # first match this cur_list
        while (cur_List[idx_in_cur_List] == 'anything') or (cur_List[idx_in_cur_List] == shoppingList[idx_in_shoppingList]):
            idx_in_cur_List += 1
            if idx_in_cur_List == len(cur_List):
                idx_in_codeList += 1
                print("finish matching this list {}".format(cur_List))
                break
            idx_in_shoppingList += 1
            if idx_in_shoppingList == num_of_items_in_shoppingList:
                break
        idx_in_shoppingList += 1

        if idx_in_codeList == num_of_items_in_codeList:
            return 1
    return 0


if __name__ == "__main__":
    codeList = [["apple", "apple"], ["banana", "anything" "banana"]]
    shoppingList = ["orange", "apple", "apple", "banana", "orange", "banana"]
    print(codeList, shoppingList, isMatch(codeList, shoppingList))

    # codeList = [["apple", "apple"], ["orange", "banana", "orange"]]
    # shoppingList = ["orange", "apple", "apple", "orange", "banana", "orange"]
    # print(codeList, shoppingList, isMatch(codeList, shoppingList))
    #
    # codeList2 = [["orange", "banana", "orange"], ["apple", "apple"]]
    # shoppingList2 = ["orange", "apple", "apple", "orange", "banana", "orange"]
    # print(codeList2, shoppingList2, isMatch(codeList2, shoppingList2))
    #
    # codeList3 = [["apple", "apple"], ["orange", "banana", "orange"], ["pear", "orange", "grape"]]
    # shoppingList3 = ["orange", "apple", "apple", "orange", "banana", "orange", "pear", "grape"]
    # print(codeList3, shoppingList3, isMatch(codeList3, shoppingList3))
    #
    # codeList4 = [["apple", "apple"], ["orange", "anything", "orange"]]
    # shoppingList4 = ["orange", "apple", "apple", "orange", "mango", "orange"]
    # print(codeList4, shoppingList4, isMatch(codeList4, shoppingList4))
    #
    # codeList5 = [["apple", "banana"], ["orange", "apple"], ["anything", "orange"]]
    # # shoppingList5 = ["apple", "banana", "apple", "apple", "orange", "apple", "banana", "orange", "apple", "orange"]
    # shoppingList5 = ["apple", "banana", "apple", "apple", "orange", "apple", "banana", "orange", "apple", "orange"]
    # print(codeList5, shoppingList5, isMatch(codeList5, shoppingList5))
