
from heapq import *


def bestCombo(popularity, k):
    posTotal = 0
    ans = []
    out = []

    for p in popularity:
        if p > 0:
            posTotal += p


    for i in range(len(popularity)):
        popularity[i] = abs(popularity[i])

    popularity.sort()

    h = []
    n = len(popularity)

    heappush(h, (popularity[0], 0))

    while h and len(out) < k - 1:
        s = heappop(h)
        u = s[0]
        i = s[1]
        out.append(u)

        if (i + 1 < n):
            heappush(h, (u + popularity[i + 1], i + 1))
            heappush(h, (u - popularity[i] + popularity[i + 1], i + 1))


    ans.append(posTotal)

    for o in out:
        ans.append(posTotal - o)

    return ans

if __name__ == '__main__':
    print(bestCombo([-1, 2, 3, 4], 3))
