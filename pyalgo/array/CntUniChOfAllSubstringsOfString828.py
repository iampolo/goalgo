def uniqueLetterString(s: str) -> int:
    indices = {}
    for i, c in enumerate(s):
        if not c in indices:
            indices[c] = [-1]
        indices[c].append(i)

    for c in indices:
        indices[c].append(len(s))

    ans = 0
    for c, indexList in indices.items():
        for j in range(1, len(indexList) - 1):
            left = indexList[j] - indexList[j - 1]
            right = indexList[j + 1] - indexList[j]
            ans += (left * right)

    return ans


if __name__ == '__main__':
    str = "ABA"
    print(uniqueLetterString(str))
