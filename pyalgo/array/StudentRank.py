def solve(rank):
    n = len(rank)
    # exclusive boundary
    left_boundary = [-1] * n
    right_boundary = [n] * n
    mp = {}
    # calculate left boundary
    for i in range(n):
        if rank[i] + 1 in mp:
            left_boundary[i] = mp[rank[i] + 1]
        mp[rank[i]] = i

    # calculate right boundary
    mp = {}
    for i in range(n - 1, -1, -1):
        if rank[i] + 1 in mp:
            right_boundary[i] = mp[rank[i] + 1]
        mp[rank[i]] = i

    # calculate possibility of how the i-th number can contribute as a[i-1]
    res = 0
    for i in range(n):
        # possible start value of the sub-array
        count_of_start = i - left_boundary[i]
        count_of_end = right_boundary[i] - i
        j, k = i, i
        while j > left_boundary[i] and rank[j] <= rank[i]:
            j -= 1
        while k < right_boundary[i] and rank[k] <= rank[i]:
            k += 1
        # print(rank[i], nge_left[i], j)
        total_possible = count_of_start * count_of_end
        smaller_sub_count = (i - j) * (k - i)
        res += (total_possible - smaller_sub_count)
    return res


if __name__ == '__main__':
    print(solve([4, 1, 3, 2]))
    print(solve([1, 2, 3]))
    print(solve([8, 3, 6, 2, 4, 1, 7, 9, 5]))
    print(solve([8, 2, 5, 3]))
