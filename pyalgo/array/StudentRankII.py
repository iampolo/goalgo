# https://leetcode.com/playground/eWkHLSGD

def next_great_element_right(arr):
    n = len(arr)
    res = [n] * n
    # keep non-increasing monotonic array, like 4, 1, 3, 2
    stack = []
    for i in range(n):
        while stack and arr[i] > arr[stack[-1]]:
            res[stack.pop()] = i
        stack.append(i)
    return res


def next_great_element_left(arr):
    n = len(arr)
    res = [-1] * n
    stack = []
    for i in range(n - 1, -1, -1):
        while stack and arr[i] > arr[stack[-1]]:
            res[stack.pop()] = i
        stack.append(i)
    return res


def solve(rank):
    n = len(rank)
    # exclusive boundary
    left_boundary = [-1] * n
    mp = {}
    # calculate left boundary
    for i in range(n):
        if rank[i] + 1 in mp:
            left_boundary[i] = mp[rank[i] + 1]
        mp[rank[i]] = i

    # calculate right boundary
    mp = {}
    right_boundary = [n] * n
    for i in range(n - 1, -1, -1):
        if rank[i] + 1 in mp:
            right_boundary[i] = mp[rank[i] + 1]
        mp[rank[i]] = i

    nge_left = next_great_element_left(rank)
    nge_right = next_great_element_right(rank)

    # calculate possibility of how the i-th number can contribute as a[i-1]
    res = 0
    for i in range(n):
        # possible start value of the sub-array

        count_of_start = i - left_boundary[i]
        count_of_end = right_boundary[i] - i
        total_possible = count_of_start * count_of_end
        smaller_sub_count = (i - nge_left[i]) * (nge_right[i] - i)
        res += (total_possible - smaller_sub_count)
    return res

if __name__ == '__main__':
    print(solve([3, 5, 4, 2]))
    print(solve([1, 2, 3]))
    print(solve([8, 2, 5, 3]))
    print(solve([4, 1, 3, 2]))
    print(solve([1, 3, 5]))
    print(solve([8, 3, 6, 2, 4, 1, 7, 9, 5]))

