# https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=569447&page=1
# 给定一个array，每一个后面的元素是前面的+1或者-1，求波峰和波谷
# 比如[1,2,3,2,3,4,3,2,1]，输出[3,2,4]
'''

array = [1,2,3,4,3,2,3,2,1,2]
输出结果应该是[4,2,3,1]

把array分成两段[1,2,3,4,3]和[2,3,2,1,2]
先检查两段的端点是不是peak/valley，第二段起点2是，记录下来（并设一个Hashset来记录是不是访问过此元素），
看各个array是不是单调增或单调减，如果不是分成两个部分。这个例子中两个array均需要分成两个部分，
于是得到四个array [1,2,3], [4,3], [2,3,2], [1,2]

检查各段端点是不是peak/valley，发现第二段起点4是，记录下来，第三段起点2是但是已经访问过，不记录，第四段起点1是，
记录下来，之后发现第一个，第二个，第四个均是单调序列，不需要再细分

第三个分成两个部分[2,3], [2],检查端点是否为peak/valley即可得出最后答案

worst case其实是O(n)


'''

def AllExtreme(nums):
    result = []

    def search(left, right, nums):
        if left >= right:
            return
        mid = (left + right) // 2

        if mid - left != abs(nums[left] - nums[mid]): #not monotonic
            search(left, mid, nums)
        if nums[mid - 1] == nums[mid + 1]:
            result.append(nums[mid])

        if right - mid != abs(nums[right] - nums[mid]): #not monotonic
            search(mid, right, nums)
        return

    search(0, len(nums) - 1, nums)
    return result


if __name__ == '__main__':
    n = [1, 2, 3, 2, 3, 4, 3, 2, 1]
    n = [1, 2, 3, 4, 5, 4, 3, 2, 1]
    n = [1, 2, 1, 2, 3, 2, 3, 2, 3]
    print(n)
    print(AllExtreme(n))
