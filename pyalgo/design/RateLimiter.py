#https://leetcode.com/discuss/interview-question/124558/Uber-or-Rate-Limiter

import time
from time import sleep

class PreciseRateLimiter(object):
    def __init__(self, max_requests, time_interval_ms):
        self._max_requests = max_requests
        self._time_interval_ms = time_interval_ms
        self._clientIDs = {}


    def is_allowed(self, clientID):
        current_ms_time = int(round(time.time() * 1000))
