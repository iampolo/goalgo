
##https://www.1point3acres.com/bbs/thread-318820-1-1.html
def check_around(board, x, y):
    max_left = 0
    max_right = len(board)-1
    neighbors = [
        (x, y+1), (x, y-1),
        (x+1, y), (x-1, y)
    ]
    coords = []
    for ngb in neighbors:
        if ((ngb[0] >= max_left and ngb[0] <= max_right) and
                (ngb[1] >= max_left and ngb[1] <= max_right)):
            if board[ngb[0]][ngb[1]] == "*":
                if board[x][y] == "*":
                    coords.append((x,y))
                else:
                    coords.append((ngb))
    return coords

def main(board, n, step):
    coords = set()
    for i in range(n):
        for j in range(i%3, n, step):
            found = check_around(board, i, j)
            if found:
                coords.update(found)
    return coords

if __name__ == "__main__":
    n = 7
    step = 3
    board = [row[:] for row in [["-"]*n] * n]

    board[4][2] = "*"
    board[5][2] = "*"
    board[6][2] = "*"

    coords = main(board, n, step)
    assert set([(4,2), (5,2), (6,2)]) == coords
    print("checked!")