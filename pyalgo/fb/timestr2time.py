
#https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=449103&pid=4764808&page=1&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%255B3046%255D%255Bvalue%255D%3D2%26searchoption%255B3046%255D%255Btype%255D%3Dradio#pid4764808

def convertTime(timeStr):
    base = 1200
    if timeStr[-1] == 'a':
        base = 0
        timeStr = timeStr[:-1]

    if ':' in timeStr:
        hr, min = map(int, timeStr.split(':'))
    else:
        min = 0
        hr = int(timeStr)

    return hr * 100 + min

if __name__ == '__main__':
    print(convertTime("10:30p"))
