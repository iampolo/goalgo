#https://www.geeksforgeeks.org/rank-elements-array/

def rankify_improved(A):

    # create rank vector
    R = [0 for i in range(len(A))]

    # Create an auxiliary array of tuples
    # Each tuple stores the data as well
    # as its index in A
    T = [(A[i], i) for i in range(len(A))]

    # T[][0] is the data and T[][1] is
    # the index of data in A

    # Sort T according to first element
    T.sort(key=lambda x: x[0])
    print(T)

    (rank, n, i) = (1, 1, 0)

    while i < len(A):
        j = i

        # Get no of elements with equal rank
        while j < len(A) - 1 and T[j][0] == T[j + 1][0]:
            j += 1
        n = j - i + 1

        for j in range(n):

            # For each equal element use formula
            # obtain index of T[i+j][0] in A
            idx = T[i+j][1]
            R[idx] = rank + (n - 1) * 0.5

        # Increment rank and i
        rank += n
        i += n

    return R


if __name__ == "__main__":
    A = [1, 2, 5, 2, 1, 25, 2]
    A = [10, 12, 15, 12, 10, 25, 12]
    print(A)
    print(rankify_improved(A))