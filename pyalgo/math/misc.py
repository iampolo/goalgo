# https://leetcode.com/problems/remove-9/discuss/106583/Python-Straightforward-with-Explanation
# python2
class Solution:
    def newInteger_660(self, n):
        ans = ''
        # convert n directly to a base-9 number
        # 0 -> false
        while n:
            ans = str(n % 9) + ans
            n /= 9
        return int(ans)


if __name__ == '__main__':
    s = Solution()
    print(s.newInteger_660(10))



