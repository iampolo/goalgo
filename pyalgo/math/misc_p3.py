# https://leetcode.com/problems/remove-9/discuss/106583/Python-Straightforward-with-Explanation
# python3
class Solution:
    def newInteger_660(self, n: int) -> int:
        res = []
        while n:
            res.append(str(n % 9))
            n //= 9  # int division
        res.reverse()
        return int(''.join(res))


if __name__ == "__main__":
    s = Solution()
    print(s.newInteger_660(11))
