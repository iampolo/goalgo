
import collections

class Solution:
    def minDel(self, s):
        right = collections.Counter(s)
        left = collections.Counter()
        res = left['B'] + right['A']
        for c in s:
            left[c] += 1
            right[c] -= 1
            res = min(res, left['B'] + right['A'])
        return res


if __name__ == '__main__':
    a = Solution()
    assert a.minDel("BAAABAB") == 2
    print("ans: ",a.minDel("BAAABAB"))
    print("ans: ",a.minDel("BAAA"))
