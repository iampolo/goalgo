# AwardBudgeCuts

def budgetize(sals, budget):
    i = len(sals) - 1

    # sort salaries so you can cherrypick highest sals and start replacing them with k
    sorted_sals = sorted(sals)

    count = 1  # counts how many high salaries we are converting to k
    while i >= 0:
        # find the sum of all the salaries except the highest one(s)
        # as i decreases, this array will reduce in size i.e. we will exclude highest and second highest sal in
        # next iteration, then highest , second highest and third highest in next and so on..
        sum_of_sals_excluding_highest = sum(sorted_sals[:i])
        # find k by substracting this sum from the budget..
        k = (budget - sum_of_sals_excluding_highest) / count
        if max(sorted_sals[:i]) > k:  # this means there are still salries higher than k...
            i -= 1  # reduce i to now convert the next highest salary..
            count += 1  # increase by one since now another high salary will be converted to k..
        else:
            return k


if __name__ == "__main__":
    # print(budgetize([100, 200, 300, 400], 800))
    print(budgetize([2, 100, 50, 120, 1000], 190))
