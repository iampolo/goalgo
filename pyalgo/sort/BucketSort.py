#https://www.geeksforgeeks.org/bucket-sort-2/

def insertionSort(b):
    for i in range(1, len(b)):
        mi = i
        while mi > 0 and b[mi] < b[mi - 1]:
            b[mi], b[mi - 1] = b[mi - 1], b[mi]
            mi -= 1

    return b

def bucketSort(nums):
    arr = []
    total = 10

    for i in range(total):
        arr.append([])

    for n in nums:
        idx = int(n * total)
        arr[idx].append(n)

    for i in range(total):
        arr[i] = insertionSort(arr[i])


    k = 0
    for i in range(total):
        for j in range(len(arr[i])):
            x[k] = arr[i][j]
            k += 1
    return x

if __name__ == '__main__':
    x = [0.897, 0.565, 0.656,
         0.1234, 0.665, 0.3434]
    x = bucketSort(x)
    print(x)
