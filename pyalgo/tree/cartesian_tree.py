import unittest
from typing import List

'''
https://leetcode.com/discuss/interview-question/124671/Facebook-or-Phone-Screen-or-Cartesian-tree

Time complexity : finding min in a subarray is the bottleneck.
avg O(n lg n)
worst O(n^2) (when a tree is skewed)

If array is empty, return None.

Find min value / index.

Make it root node, build subtree with left/right parts, recursively

'''

class Node:
    def __init__(self, val: int):
        self.val = val
        self.left = None
        self.right = None


def create_tree(arr: List[int]) -> Node:
    if not arr:
        return None

    min_idx, min_val = min(enumerate(arr), key=lambda t: t[1])
    root = Node(min_val)
    root.left = create_tree(arr[:min_idx])
    root.right = create_tree(arr[min_idx + 1:])
    return root


class CreateTreeTest(unittest.TestCase):
    def test_given_example(self):
        inputs = []
        inputs.append([1, 2, 3, 5])
        inputs.append([5, 8, 1, 2])
        for i in inputs:
            head = create_tree(i)
            # head val is min
            self.assertEqual(head.val, min(i))

            # every child has bigger val that it's parent
            q = [head]
            while q:
                node = q.pop(0)
                if node.left:
                    self.assertTrue(node.left.val > node.val)
                    q.append(node.left)
                if node.right:
                    self.assertTrue(node.right.val > node.val)
                    q.append(node.right)

            # inorder traversal == input
            inorder = []
            stack = []
            curr = head
            while curr or stack:
                if curr:
                    stack.append(curr)
                    curr = curr.left
                else:
                    curr = stack.pop()
                    inorder.append(curr.val)
                    curr = curr.right
            self.assertEqual(inorder, i)


if __name__ == "__main__":
    unittest.main()
