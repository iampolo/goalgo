# https://piazza.com/class/j0eqhhdregb3i?cid=3825

def delete(self, word):
    """
    Delete the word if it exists. The requirement is to clear any unnecessary nodes in this trie during deletion.
    :type word: str
    :rtype: void
    """
    # Use an explicit stack to record the nodes along the path. Note the grand-root is not recorded
    stack = []
    curr = self._root
    for char in word:
        next = curr.children.get(char)
        if next is None:
            break
        curr = next
        stack.append(curr)

    # Word not found
    if len(stack) < len(word):
        return
    # Found word is not the leaf node
    elif len(stack[-1].children) > 0:
        stack[-1].is_word = False
    # Found word is the leaf node. Delete all unnecessary nodes
    else:
        stack[-1].is_word = False
        # Move backward along the path and find the first node which either has more than 2 splits or is a word
        while len(stack) > 0 and len(stack[-1].children) <= 1 and not stack[-1].is_word:
            node = stack.pop()
        if len(stack) > 0:
            stack[-1].children.pop(word[len(stack)])
        else:
            self._root.children.pop(word[0])
