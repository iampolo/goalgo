def breakAPalindrome(palindrome):
	res = ""
	idx = -1
	found = False
	for i, c in enumerate(palindrome[:len(palindrome):2]):
		if c != 'a':
			idx = i
			found = True
			break

	# In Python, strings are immutable, so you can't change their characters in-place.
	# So need to reconstruct the string ~!!!!!!
	if found:
		for i in range(len(palindrome)):
			if i != idx:
				res += palindrome
			else:
				res += "a"
		return res
	else:
		return "IMPOSSIBLE"

if __name__ == '__main__':
	print(breakAPalindrome("abba"))