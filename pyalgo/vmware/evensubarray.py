import heapq


def climbTheHill(slope):
    res1 = 0
    res2 = 0
    dif = 0

    pq = []  # min-heap - descending order: sl can't be greater than smallest in first k-1 elements
    for sl in slope:
        if len(pq) > 0 and pq[0] < sl:
            dif = sl - pq[0]
            res1 += dif
            heapq.heappop(pq)
            # heapq.heappush(pq, sl)
        heapq.heappush(pq, sl)

    pq = []  # max-heap - ascending order: sl can't be smaller than largest in first k-1 elements
    for sl in slope:
        if len(pq) > 0 and -pq[0] > sl:
            dif = -pq[0] - sl
            res2 += dif
            heapq.heappop(pq)
            # heapq.heappush(pq, -sl)
        heapq.heappush(pq, -sl)

    return min(res1, res2)


def evensubarray(nums, k):
    left = 0
    cnt = 0
    res = 0


if __name__ == '__main__':
    print(climbTheHill([0, 1, 2, 5, 5, 4, 4]))
    print(climbTheHill([7, 5, 6, 5, 2, 1, 0]))  # 1
    print(climbTheHill([9, 8, 7, 2, 3, 3]))  # 1

    for i, n in enumerate([1,2,3,4,5,6]):
        print("value", i, n)
