package stack

import (
	"fmt"
	"unicode"
)

func calculate(s string) int {
	stack, operand, curNum := []int{}, '+', 0
	for i := 0; i < len(s); i++ {
		if unicode.IsDigit(rune(s[i])) == true {
			curNum = curNum*10 + int(s[i]) - 48 // " sum up the multi-digit number e.g. "23" -> 2*10+3
		}
		if i+1 == len(s) || (s[i] == '+' || s[i] == '-' || s[i] == '*' || s[i] == '/') {
			if operand == '+' {
				stack = append(stack, curNum)
			} else if operand == '-' {
				stack = append(stack, -curNum)
			} else if operand == '*' {
				pop, stack := stack[len(stack)-1], stack[:len(stack)-1] // pop stack
				stack = append(stack, pop*curNum)                       // mutate the value and add it back
			} else if operand == '/' {
				pop, stack := stack[len(stack)-1], stack[:len(stack)-1] // pop stack
				stack = append(stack, pop/curNum)                       // mutate the value and add it back
			}
			operand = rune(s[i]) // reset operand
			curNum = 0           // reset current number
		}
	}
	result := 0 // sum up the numbers in the stack
	for _, num := range stack {
		result += num
	}
	return result
}

func RunCode() {
	res := calculate("3+2*2")
	fmt.Print("calculator result: ", res)
}